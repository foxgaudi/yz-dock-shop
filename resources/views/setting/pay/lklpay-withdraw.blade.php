@extends('layouts.base')
@section('title', "提现记录")
@section('content')
    <link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
    <style>
    </style>
    <div class="all">
        <div id="app" v-cloak>

            <div class="vue-head">
                <div class="vue-main-title" style="margin-bottom:20px">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">
                        提现记录
                        <el-link v-if="show_return == 'return'" :underline='false' type="primary"
                                 :href="'{{yzWebUrl('setting.payment-manage.index', [])}}'">
                            《 返回聚合支付
                        </el-link>
                    </div>
                    <div class="vue-main-title-button">
                    </div>
                </div>
                <div class="vue-search">
                    <div>
                        <el-form :inline="true" :model="search_form" class="demo-form-inline">

                            <el-form-item label="">
                                <el-input v-model="search_form.withdraw_sn" placeholder="提款流水号"></el-input>
                            </el-form-item>

                            <el-form-item label="">
                                <el-select placeholder="提现模式" style="width:120px;" v-model="search_form.draw_mode" clearable>
                                    <el-option label="D1" value="D1"></el-option>
                                    <el-option label="D0" value="D0"></el-option>
                                </el-select>
                            </el-form-item>

                            <el-form-item label="">
                                <el-select placeholder="结算模式" style="width:120px;" v-model="search_form.settle_type" clearable>
                                    <el-option label="主动提款" value="01"></el-option>
                                    <el-option label="自动结算" value="02"></el-option>
                                </el-select>
                            </el-form-item>

                            <el-form-item label="">
                                <el-select placeholder="提款状态" style="width:120px;" v-model="search_form.draw_status" clearable>
                                    <el-option label="提款已受理" value="DRAW.ACCEPTED"></el-option>
                                    <el-option label="提款冻结" value="DRAW.FREEZE"></el-option>
                                    <el-option label="提款处理中" value="DRAW.PROCESSING"></el-option>
                                    <el-option label="提款成功" value="DRAW.SUCCESS"></el-option>
                                    <el-option label="提款失败" value="DRAW.FAILED"></el-option>
                                </el-select>
                            </el-form-item>


                            <time-card :days_of_last_year="days_of_last_year" ref="time_card"></time-card>

                            <el-form-item label="">
                                <el-button type="primary" icon="el-icon-search" @click="search(1)">搜索</el-button>
                            </el-form-item>
                            {{--<el-form-item label=""><el-button  @click="exportData()">导出</el-button></el-form-item>--}}
                        </el-form>
                    </div>
                </div>
            </div>
            <div class="vue-main">
                <div class="vue-main-title" style="">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">
                        记录列表
                        <span style="text-align:left;font-size:14px;color:#999">
                            <span>商户号：[[search_form.merchant_no]]</span>&nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                </div>
                <div class="vue-main-title" style="margin-bottom:20px">
                    <el-table v-loading="loading" :data="list" style="width: 100%">

                        <el-table-column width="170px" align="center">
                            <template slot="header" slot-scope="scope">
                                <p>创建时间</p>
                                <p>完成时间</p>
                            </template>
                            <template slot-scope="scope">
                                <p class="p-text">[[scope.row.created_at]]</p>
                                <p class="p-text">[[scope.row.complete_at]]</p>
                            </template>
                        </el-table-column>

                        <el-table-column  label="提款流水号" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.withdraw_sn]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column align="center">
                            <template slot="header" slot-scope="scope">
                                <p>提现金额</p>
                                <p style="font-size: 12px">(含手续费)</p>
                            </template>
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.amount]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column  label="手续费" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.fee_rate]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column  label="提现模式" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.draw_mode]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column  label="结算模式" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.settle_type_name]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column align="center">
                            <template slot="header" slot-scope="scope">
                                <p>结算账户号</p>
                                <p>银行名称</p>
                            </template>
                            <template slot-scope="scope">
                                <p class="p-text">[[scope.row.details.acct_no]]</p>
                                <p class="p-text">[[scope.row.details.nbk_name]]</p>
                            </template>
                        </el-table-column>

                        <el-table-column  label="结算账户名" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.details.acct_name]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column  label="提款状态" align="center">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.status_name]]</span>
                            </template>
                        </el-table-column>

                        <el-table-column  label="结果信息">
                            <template slot-scope="scope">
                                <span style="margin-left: 10px">[[scope.row.details.memo]]</span>
                            </template>
                        </el-table-column>

                    </el-table>
                </div>
            </div>

            <!-- 分页 -->
            <div class="vue-page" v-if="total>0">
                <el-row>
                    <el-col align="right">
                        <el-pagination layout="prev, pager, next,jumper" @current-change="search" :total="total"
                                       :page-size="per_size" :current-page="current_page" background
                        ></el-pagination>
                    </el-col>
                </el-row>
            </div>

        </div>
    </div>

    @include('public.admin.timeCard')
    <script>
        var app = new Vue({
            el: "#app",
            delimiters: ['[[', ']]'],
            name: 'account',
            data() {

                return {

                    show_return:'return',
                    days_of_last_year:"365",
                    search_form:{
                        merchant_no:'',
                    },

                    list: [],
                    //页码数
                    current_page: 0,
                    //一页显示数据
                    per_size: 0,
                    //总数
                    total: 0,
                    //加载
                    loading: false,

                }
            },
            created() {
                let result = this.viewReturn();
                this.__initial(result);

            },
            mounted() {
                this.search(1);
            },
            methods: {
                //视图返回数据
                viewReturn() {
                    return {!! $data?:'{}' !!};
                },
                //初始化页面数据，请求链接
                __initial(data) {

                    if (data.days_of_last_year) {
                        this.days_of_last_year = data.days_of_last_year;
                    }

                    if (data.merchant_no) {
                        this.search_form.merchant_no = data.merchant_no;
                    }

                    if (data.show_return) {
                        this.show_return = data.show_return;
                    }

                },

                setList(response) {
                    this.list = response.data;
                    this.total = response.total;
                    this.current_page = response.current_page;
                    this.per_size = response.per_page;
                },

                search(page) {
                    let that = this;

                    if(this.$refs.time_card.times && this.$refs.time_card.times.length>0) {
                        this.search_form.start_time = parseInt(this.$refs.time_card.times[0] / 1000);
                        this.search_form.end_time = parseInt(this.$refs.time_card.times[1] / 1000);
                    } else {
                        this.search_form.start_time = "";
                        this.search_form.end_time ="";
                    }


                    that.loading = true;
                    that.$http.post("{!!yzWebFullUrl('setting.integration-pay.withdrawList')!!}", {
                        page: page,
                        search:this.search_form,
                    }).then(response => {
                        // console.log(response);
                        if (response.data.result == 1) {
                            that.setList(response.data.data);
                        } else {
                            that.$message.error(response.data.msg);
                        }
                        that.loading = false;
                    }), function (res) {
                        console.log(res);
                        that.loading = false;
                    };
                },

                exportData(){
                    let that = this;

                    let json = that.search_form;

                    if(this.$refs.time_card.times && this.$refs.time_card.times.length>0) {
                        json.start_time = parseInt(this.$refs.time_card.times[0] / 1000);
                        json.end_time = parseInt(this.$refs.time_card.times[1] / 1000);
                    } else {
                        json.start_time = "";
                        json.end_time ="";
                    }

                    let url = `{!! yzWebFullUrl('setting.integration-pay.withdrawExport') !!}`;


                    for(let i in json){
                        if (json[i]) {
                            url+="&search["+i+"]="+ json[i]
                        }
                    }

                    window.location.href = url;
                },

                getParam(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null) return unescape(r[2]);
                    return null;
                },

            },
        })
    </script>
@endsection