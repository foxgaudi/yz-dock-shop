@extends('layouts.base')
@section('content')

@section('title', trans('支付管理'))
<link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
<link href="{{static_url('yunshop/css/member.css')}}" media="all" rel="stylesheet" type="text/css"/>
<style>
  .add-people{
      width: 91px;
      height: 91px;
      border: dashed 1px #dde2ee;
      display:flex;
      flex-direction:column;
      justify-content:center;
      align-items:center;
    }
</style>
<div class="all">
    <div id="app" v-cloak>
      <div class="vue-head">
       <el-tabs v-model="activeComponent" @tab-click="handleClick">
       <el-tab-pane v-for="(tabItem,index1) in paySettings" :label="tabItem.title" :name="tabItem.component_name"  :key="index1">
          <div></div>
       </el-tab-pane>
      </el-tabs>
      </div>
      <ul v-if="componentLoaded" class="payment">
       <li v-for="(item,index) in paySettings">
          <component :is="item.component_name" :ref="item.component_name" :http_url="http_url" :component-data="item.data" v-show="activeComponent == item.component_name"
                    :key="index" v-if="$options.components[item.component_name]"></component>
       </li>
      </ul>

      <div class="vue-page" style="height: 70px;">
          <div class="vue-center">
                    <el-button type="primary" @click="submitData" >提交</el-button>
                </div>
            </div>         
      </div>
    </div>
</div>

@include('public.admin.tinymceee')
@include('public.admin.uploadMultimediaImg')

<script>
    const PaySetPageUrl = "{!! yzWebFullUrl('setting.payment-manage.index') !!}";
    const saveDataUrl = "{!! yzWebFullUrl('setting.payment-manage.submit-save') !!}"; //* 保存数据地址
    const getPaySetUrl = "{!! yzWebFullUrl('setting.payment-manage.pay-widget') !!}";
    const HttpUrl = "{!!  request()->getSchemeAndHttpHost().yzUrl('') !!}"; //* 保存数据地址
</script>
<script src="{{static_url('../resources/views/setting/pay/js/main.js?time='.time())}}"></script>
@endsection
