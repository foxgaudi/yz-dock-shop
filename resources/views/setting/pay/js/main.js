let vm = new Vue({
  el: "#app",
  delimiters: ['[[', ']]'],
  created() {

      let loadedComponentCount = 0;
      this.fetchData(this.getPaySettingUrl).then((paySettings) => {
          this.paySettings = paySettings;

          paySettings.forEach(item => {
              require.config({
                  paths: {
                      [item.component_name]: item.page_path + item.component_name
                  }
              });

              this.loadComponent(item.component_name).then((res) => {
                  loadedComponentCount++;
                  //* 当所有组件文件加载完成才会显示页面，因为vue的ref需要组件加载完后才能获取
                  if (loadedComponentCount == this.paySettings.length) {
                      this.componentLoaded = true;
                  }
              });
          });

      });

  },
  mounted(){
     //console.log(this.$refs[this.currentComponent]);
  },
  methods: {
      fetchData(URL, requestParams) {
          return new Promise((resolve, reject) => {
              this.$http.post(URL, requestParams).then(function (response) {
                      return response.json();
                  }).then(({result, data, msg}) => {
                      if (result == 0) {
                          this.$message({message: msg, type: "error",});
                          reject({ result, data, msg });
                      }
                      resolve(data);
                  }).catch((err) => {
                      reject(err);
                  });
          });
      },

      submitData() {

          if (!this.currentWidgetKey) {
              this.$message.warning('表单验证不通过');
              return false;
          }

          const component = this.$refs[this.activeComponent][0];
          let componentData = component.returnData();

          if (!componentData) {
              this.$message.warning('表单验证不通过');
              return false;
          }

          //json就是要提交的数据
          let json = {
              tab: this.currentWidgetKey,
              data:componentData,
          };
          this.saveData(json);

      },
      //点击提交保存数据
      saveData(submitData){
          this.saveStatus = false;
          this.$http.post(this.savePaySettingUrl, submitData).then(response => {
              console.log(response,'response');
              if (response.data.result) {
                  this.$message({type: 'success',message: '成功!'});
                  this.$forceUpdate();
                  //window.location.href = this.paySetPageUrl;
              } else{
                  console.log(response.data.msg);
                  this.$message({type: 'error',message: response.data.msg});
              }
              this.saveStatus = true
          }),function(res){
              console.log(res);
          };
      },
      //切换tab选项  
      handleClick(tab, event){
        this.currentComponent = tab.name;
        this.paySettings.forEach(item => {
            if(item.component_name == this.currentComponent){
                this.currentWidgetKey = item.widget_key;
            }
        });
      },
      //加载选项卡组件
      loadComponent(name){
        return new Promise((resolve, reject) => {
          const pageLoading = this.$loading({target: ".payment", text: "页面加载中",});

          require([name],(options) => {
            //console.log(options);
            // 注册组件
            this.$options.components[name] = options;

              if (options && options.style) {
                  const styleEl = document.createElement("style");
                  styleEl.innerHTML = options.style;
                  document.body.append(styleEl);
              }

            this.activeComponent = this.paySettings[0].component_name;
            this.currentComponent = this.paySettings[0].component_name;
            this.currentWidgetKey = this.paySettings[0].widget_key;

            resolve(name, options);
            pageLoading.close();
          }, (err) => {
            pageLoading.close();
            reject(err);
          });
        })
      }      
  },

  data() {
    return {
        http_url:HttpUrl,
        paySetPageUrl:PaySetPageUrl,
        getPaySettingUrl:getPaySetUrl,
        savePaySettingUrl:saveDataUrl,
        // 防止快速点击保存
        saveStatus: true,
        activeComponent: '',
        currentComponent: '',
        currentWidgetKey:'',
        paySettings: [],
        componentLoaded: false,

    }
},
//定义全局的方法  
beforeCreate() {
},
filters: {

},
computed:{

},

});