define({
  name:"integrationPay",
  template:`
  <div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">聚合支付</div>
				<div class="vue-main-title-button">
				</div>
			</div>
    		<div class="vue-main-form">
    		
    		    <el-form-item label="" v-if="!service_provider_connect">
					<div style="font-size: 16px;color: red">数据通系统插件已禁用或未安装,支付无法使用</div>
				</el-form-item>
    		
				<el-form-item label="开启聚合支付">
					<template>
						<el-switch v-model="form_data.integration_pay" active-value="1" inactive-value="0"></el-switch>
					</template>
					<div style="font-size: 12px;">需开启并配置数据通系统插件，并在数据通系统完成进件开户！
					  <el-link style="font-size: 14px;font-weight: 600;" target="_blank" type="primary" :underline='false' 
					    :href="http_url + 'plugin.service-provider-connect.admin.setting.index'">
                        去配置！！
                      </el-link>
					</div>
					<div style="font-size: 12px;color: red">如需实现O2O门店多商户分账的，需要单独安装聚合分账插件！</div>
				</el-form-item>
				<div>
                    <el-form-item label="微信支付">
                        <template>
                            <el-switch v-model="form_data.wechat_pay" active-value="1" inactive-value="0"></el-switch>
                        </template>
                        <div style="font-size: 12px;" v-if="!supportPayType.includes('WECHAT')">请先在数据通系统开启微信支付</div>
                    </el-form-item>
                    
                      <el-form-item label="小程序端隐藏微信支付">
                        <template>
                            <el-switch v-model="form_data.hide_wechat_pay" active-value="1" inactive-value="0"></el-switch>
                        </template>
                        <div style="font-size: 12px;" >开启则在小程序端不显示：微信支付</div>
                    </el-form-item>
                    
                    <el-form-item label="支付宝支付">
                        <template>
                            <el-switch v-model="form_data.alipay_pay" active-value="1" inactive-value="0"></el-switch>
                        </template>
                         <div style="font-size: 12px;" v-if="!supportPayType.includes('ALIPAY')">请先在数据通系统开启支付宝支付</div>
                    </el-form-item>
                    
                    <div v-if="form_data.account_info && form_data.account_info.channel_type == 1">
                         <el-form-item label="拉卡拉收银台">
                            <template>
                                <el-switch v-model="form_data.lkl_cashier" active-value="1" inactive-value="0"></el-switch>
                            </template>
                             <div style="font-size: 12px;" v-if="!supportPayType.includes('CASH')">请先在数据通系统开启拉卡拉收银台</div>
                        </el-form-item>
                    </div>
                    
                     <div v-if="form_data.account_info && form_data.account_info.channel_type == 2">
                         <el-form-item label="快捷支付">
                            <template>
                                <el-switch v-model="form_data.QUICKPAY" active-value="1" inactive-value="0"></el-switch>
                            </template>
                             <div style="font-size: 12px;" v-if="!supportPayType.includes('QUICKPAY')">请先在数据通系统开启快捷支付</div>
                        </el-form-item>
                        
                         <el-form-item label="小程序终端支付">
                            <template>
                                <el-switch v-model="form_data.MINIAPP_PAY" active-value="1" inactive-value="0"></el-switch>
                            </template>
                             <div style="font-size: 12px;" v-if="!supportPayType.includes('MINIAPP_PAY')">请先在数据通系统开启小程序终端支付</div>
                        </el-form-item>
                    </div>
                    
                </div>
			</div>
  		</div>
  		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">
				    商户信息
				    <span style="font-size: 12px;font-weight:300;color: #3C4858">数据通系统更新了商户信息商城这边需要同步更新</span>
				</div>
				<div class="vue-main-title-button">
				    <el-button size="small" type="primary" @click="updateAccount()">同步更新</el-button>
				</div>
			</div>
    		<div class="vue-main-form">
                <div v-if="form_data.account_info && Object.keys(form_data.account_info).length > 0">
                    <el-form-item label="当前商户号">
                        <div style="">{{form_data.account_info.merchant_no}}</div>
                    </el-form-item>
                    <el-form-item label="当前支付通道">
                        <div style="">{{form_data.account_info.channel}}</div>
                    </el-form-item>
                    <div v-if="form_data.account_info.wallet_id && form_data.account_info.channel_type == 1">
                        <el-form-item label="钱包ID">
                            <div style="">{{form_data.account_info.wallet_id}}</div>
                        </el-form-item>
                        <el-form-item label="钱包">
                            <div>
                                <span>{{form_data.account_info.balance}}</span>&nbsp;&nbsp;&nbsp;
                                <span><el-button size="small" type="primary" @click="showWithdraw()">提 现</el-button></span>&nbsp;&nbsp;&nbsp;
                                <span>
                                    <el-link :underline='false' type="success"
                                             :href="http_url + 'setting.integration-pay.withdrawLog'">
                                        提现记录
                                    </el-link>
                                </span>
                            </div>
                        </el-form-item>
                    </div>
                   
                </div>
		
			</div>
  		</div>
 	</el-form>
 	<el-dialog :visible.sync="withdraw_show" width="650px" center title="提现"
                       v-loading="withdraw_loading"
                       element-loading-text="提现申请中..."
                       element-loading-spinner="el-icon-loading"
                       element-loading-background="rgba(0, 0, 0, 0.8)">
                <div style="height:200px;overflow:auto" >
                    <el-form label-width="20%">
                        <el-form-item label="提现金额">
                            <div v-html="withdraw_amount"></div>
                            <div style="font-size: 12px;">手续费 {{withdraw_fee}}</div>
                        </el-form-item>
                        <el-form-item label="账户类型">
                             <el-select v-model="pay_type" style="width:150px">
                                    <el-option v-for="(v,k) in account_type" :key="k" :label="v" :value="k"></el-option>
                                </el-select>
                        </el-form-item>

                    </el-form>
                </div>
                <span slot="footer" class="dialog-footer">
                    <el-button @click="withdraw_show = false">关 闭</el-button>
                     <el-button type="primary" @click="confirmWithdraw()">确定提现</el-button>
                </span>
            </el-dialog>
</div>
  `,
  style:`
     .aba {
        color: #29BA9C;
     }
  `,
  props: {
      http_url:{
          type:String,
          default() {
              return "";
          },
      },
      componentData:{
      default(){
        return {}
      }
    },
  },
  data(){
    return{
        plugin_integration_share:0,
        service_provider_connect:0,

        withdraw_show:false,
        withdraw_loading:false,
        withdraw_amount:0,
        withdraw_fee:0,
        pay_type:'01',
        account_type: {
            '01': '收款账户',
            '04': '分账接收方账户',
            '06': '结算代付账'
        },

        supportPayType:[],

        form_data:{
            account_info:{},
        },

    }
  },
  created() {
      //console.log(this.componentData);
      if(this.componentData){
          this.service_provider_connect = this.componentData.service_provider_connect;
          this.plugin_integration_share = this.componentData.plugin_integration_share;
          this.form_data = this.componentData.pay_set;

          if (this.form_data.account_info.pay_type) {
              this.supportPayType = this.form_data.account_info.pay_type.split(',');
          }
      }
  },
  methods: {

      updateAccount() {
          this.$http.post(this.http_url + 'setting.integration-pay.updPayAccount', {}).then(res => {
              if (res.data.result === 1) {
                  this.$set(this.form_data, 'account_info', res.data.data);
              } else {
                  this.$message.error(res.data.msg)
              }
          }), function (res) {
              console.log(res);
          };
      },

      showWithdraw() {
          this.withdraw_show = true;

          this.$http.post(this.http_url + 'setting.integration-pay.pre-withdraw', {
              merchant_no:this.form_data.account_info.merchant_no,
          }).then(res => {
              if (res.data.result === 1) {
                  this.withdraw_amount = res.data.data.withdraw_amt;
                  this.withdraw_fee = res.data.data.fee_amt;
              } else {
                  this.$message.error(res.data.msg)
              }

          }), function (res) {
              console.log(res);
          };

      },
      confirmWithdraw() {
          let json = {
              mer_id:this.form_data.account_info.merchant_no,
              pay_type:this.pay_type,
              amount:this.withdraw_amount,
          };

          if (json.amount <= 0) {
              this.$message.error('提现金额不能小于或等于0');
              return;
          }

          if (!json.mer_id) {
              this.$message.error('商户号不存在');
              return;
          }

          this.withdraw_loading = true;
          this.$http.post(this.http_url + 'setting.integration-pay.withdraw', json).then(res => {
              if (res.data.result === 1) {
                  if (this.form_data.account_info) {
                      this.form_data.account_info.balance = 0;
                  }
                  this.withdraw_amount = 0;

                  this.$message.success('提现申请成功');
                  this.withdraw_show = false;
              } else {
                  this.$message.error(res.data.msg)
              }
              this.withdraw_loading = false;

          }), function (res) {
              this.withdraw_loading = false;
              console.log(res);
          };
      },

      //验证方法
      filterData() {
          return true
      },
      //点击提交
      returnData() {
          if (this.filterData()) {
              return this.form_data
          } else {
              return false
          }
      },
  },
});