define({
  name: "wechat",
  template: `
<div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">微信支付</div>
				<div class="vue-main-title-button">
				</div>
			</div>
    		<div class="vue-main-form">
				<el-form-item label="是否开启">
					<template>
						<el-switch v-model="form_data.weixin" active-value="1" inactive-value="0"></el-switch>
					</template>
					<div style="font-size: 12px;">提示：标准微信支付、及其他微信支付接口（云收银）总开关。微信支付授权目录填写路径：域名/addons/yun_shop/</div>
					<div style="font-size: 14px;color: red;">开通第三方支付，可享受更实惠的费率、实现代付等功能，详询客服！</div>
				</el-form-item>
				<el-form-item label="标准微信支付">
					<template>
						<el-switch v-model="form_data.weixin_pay" active-value="1" inactive-value="0"></el-switch>
					</template>
				</el-form-item>
				<el-form-item label="微信H5支付">
					<template>
						<el-switch v-model="form_data.wechat_h5" active-value="1" inactive-value="0"></el-switch>
					</template>
					<div style="font-size: 12px;">提示：只支持手机浏览器，pc不支持</div>
				</el-form-item>
				<el-form-item label="微信付款码支付">
					<template>
						<el-switch v-model="form_data.wechat_micro" active-value="1" inactive-value="0"></el-switch>
					</template>
				</el-form-item>
				<el-form-item label="微信扫码支付">
					<template>
						<el-switch v-model="form_data.wechat_native" active-value="1" inactive-value="0"></el-switch>
					</template>
				</el-form-item>
				<el-form-item label="身份标识(appId)"  >
					<el-input v-model="form_data.weixin_appid" style="width:70%;"></el-input>
				</el-form-item>
				<el-form-item label="身份密钥(appSecret)">
					<el-input v-model="form_data.weixin_secret" type="text"style="width:70%;"  v-if="weixin_secret_show"></el-input>
					<span style="color:#29BA9C;font-size:12px;" v-if="form_data.weixin_secret && !weixin_secret_show ">已上传</span>
					<el-button type="primary" @click="ResetValue('weixin_secret')" v-if="form_data.weixin_secret && !weixin_secret_show">重置</el-button>
				</el-form-item>
				<el-form-item label="微信支付商户号(mchId)">
					<el-input v-model="form_data.weixin_mchid" style="width:70%;"></el-input>
					<div style="font-size: 12px;">微信公众号以邮件形式告知</div>
				</el-form-item>
				<el-form-item label="微信支付密钥(apiSecret)">
					<el-input v-model="form_data.weixin_apisecret" style="width:70%;" type="text"  v-if="weixin_apisecret_show" ></el-input>
					<span style="color:#29BA9C;font-size:12px;" v-if="form_data.weixin_apisecret && !weixin_apisecret_show ">已上传</span>
					<el-button type="primary" @click="ResetValue('weixin_apisecret')" v-if="form_data.weixin_apisecret && !weixin_apisecret_show">重置</el-button>
					<div>获取路径：微信支付商户平台>账户设置>API安全--设置支付密钥（32位数）</div>
				</el-form-item>
				<el-form-item label="微信支付证书">
					<template>
						<el-radio-group v-model="form_data.weixin_version">
							<el-radio label="0">文件上传</el-radio>
							<el-radio label="1">文本上传</el-radio>
						</el-radio-group>
					</template>
					<div style="font-size:12px;" >微信支付证书获取途径：登录微信商户平台--账户中心--API安全--下载证书</div>
				</el-form-item>        
				<el-form-item label="CERT证书文件"  v-if="form_data.weixin_version==0">
					<el-upload
							class="upload-demo"
							:action="http_url + 'setting.shop.newUpload'"
							name="weixin_cert"
							:show-file-list="false"
							:on-success="uploadSuccess"
							:on-error="uploadfail"
					>
						<el-button type="primary">文件上传</el-button>
					</el-upload>
					<span style="color:#5adda2;font-size:12px;"  v-if="form_data.weixin_cert">已上传</span>
					<div style="font-size:12px;">提示：下载证书 cert.zip 中的 apiclient_cert.pem 文件</div>
				</el-form-item>
				<el-form-item label="KEY密钥文件"  v-if="form_data.weixin_version==0">
					<el-upload
							class="upload-demo"
							:action="http_url + 'setting.shop.newUpload'"
							name="weixin_key"
							:show-file-list="false"
							:on-success="uploadkey"
							:on-error="uploadfail"
					>
						<el-button type="primary">文件上传</el-button>
					</el-upload>
					<div style="font-size:12px;">提示：下载证书 cert.zip 中的 apiclient_key.pem 文件</div>
					<span style="color:#5adda2;font-size:12px;"  v-if="form_data.weixin_key">已上传</span>
				</el-form-item>
				<el-form-item label="CERT证书文件"  v-if="form_data.weixin_version==1">
					<el-input v-model="form_data.new_weixin_cert"  type="textarea"  style="width:70%;" v-if="Certshow"></el-input>
				</el-form-item>
				<el-form-item label="KEY密钥文件"  v-if="form_data.weixin_version==1">
					<el-input v-model="form_data.new_weixin_key"  type="textarea" style="width:70%;" v-if="Certshow"></el-input>
				</el-form-item>
				<el-form-item label=" " v-if="form_data.weixin_version==1&&!Certshow" >
					<el-button type="primary" @click="certReset">重新设置</el-button>
				</el-form-item>                        
			</div>
  		</div>
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">微信支付-V3新版</div>
				<div class="vue-main-title-button"></div>
			</div>
			<div class="vue-main-form">
                <el-form-item label="是否开启">
                    <template>
                        <el-switch v-model="form_data.weixin_apiv3" active-value="1" inactive-value="0"></el-switch>
                    </template>
                    <div style="font-size: 12px;">提示：当前设置只针对新版微信提现打款（商家转账到零钱功能），用着旧版（企业付款到零钱）功能的请勿开启</div>
                </el-form-item>
                <el-form-item label="微信apiV3密钥">
                    <el-input v-model="form_data.weixin_apiv3_secret" style="width:70%;" type="text"  v-if="weixin_apiv3_secret_show" ></el-input>
                    <span style="color:#29BA9C;font-size:12px;" v-if="form_data.weixin_apiv3_secret && !weixin_apiv3_secret_show ">已上传</span>
                    <el-button type="primary" @click="ResetValue('weixin_apiv3_secret')" v-if="form_data.weixin_apiv3_secret && !weixin_apiv3_secret_show">重置</el-button>
                    <div>获取路径：微信支付商户平台>账户设置>API安全--设置ApiV3密钥（32位数）</div>
                </el-form-item>
			</div>
		</div>	
 	</el-form>
</div>
 
  `,
  style: `

  `,
  props: {
    componentData: {
      default() {
        return {};
      },
    },
    http_url:{
      type:String,
      default() {
        return "";
      },
    },
  },
  data() {
    return {
      show: true,
      Certshow: true,
      weixin_secret_show: true,
      weixin_apisecret_show: true,
      alipay_transfer_private_show: true,
      weixin_apiv3_secret_show: true,

      form_data: {},
    };
  },
  created() {
    if (this.componentData) {
      this.form_data = this.componentData;
    }
    console.log(this.componentData);

    this.getShow();
    this.getCert();
  },
  methods: {
    ResetValue(str) {
      switch (str) {
        case "weixin_secret":
          this.form_data.weixin_secret = "";
          this.weixin_secret_show = true;
          break;
        case "weixin_apisecret":
          this.form_data.weixin_apisecret = "";
          this.weixin_apisecret_show = true;
          break;
        case "weixin_apiv3_secret":
          this.form_data.weixin_apiv3_secret = "";
          this.weixin_apiv3_secret_show = true;
          break;
      }
    },
    uploadkey(res) {
      if (res.result) {
        this.$message({ message: res.msg, type: "success" });
        this.form_data.weixin_key = res.data.data.data.weixin_key;
      } else {
        this.$message({ message: res.msg, type: "error" });
      }
    },
    uploadfail(res) {
      this.$message({ 上传失败: "error" });
    },

    uploadSuccess(res) {
      if (res.result) {
        this.$message({ message: res.msg, type: "success" });
        this.form_data[res.data.data.data.key] = res.data.data.data.value;
      } else {
        this.$message({ message: res.msg, type: "error" });
      }
    },
    getCert() {
      if (this.form_data.new_weixin_cert || this.form_data.new_weixin_key) {
        this.Certshow = false;
      }
    },
    getShow() {
      if (this.form_data.rsa_private_key || this.form_data.rsa_public_key) {
        this.show = false;
      }
      if (this.form_data.weixin_secret) {
        this.weixin_secret_show = false;
      }
      if (this.form_data.weixin_apisecret) {
        this.weixin_apisecret_show = false;
      }
      if (this.form_data.weixin_apiv3_secret) {
        this.weixin_apiv3_secret_show = false;
      }
    },
    certReset() {
      this.Certshow = true;
      this.form_data.new_weixin_cert = "";
      this.form_data.new_weixin_key = "";
    },
    Reset() {
      this.show = true;
      this.form_data.rsa_private_key = "";
      this.form_data.rsa_public_key = "";
    },
    //验证方法
    filterData() {
      return true;
    },
    // 在main.js中调用此方法 点击提交返回数据
    returnData() {
      //当数据认证通过后
      if (this.filterData()) {
        return this.form_data;
      } else {
        return false;
      }
    },
  },
});
