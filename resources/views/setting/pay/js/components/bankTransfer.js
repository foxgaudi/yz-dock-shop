define({
  name:"bankTransfer",
  template:`
  <div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">银行转账</div>
				<div class="vue-main-title-button">
				</div>
			</div>
    		<div class="vue-main-form">
				<el-form-item label="是否开启">
					<template>
						<el-switch v-model="form_data.remittance" active-value="1" inactive-value="0"></el-switch>
					</template>
					  <div style="font-size: 12px;">提示：前端转账支付页选择汇款支付，上传支付凭证，后台财务审核</div>
				</el-form-item>
				
				 <el-form-item label="前端图片" prop="remittance_frontend_img" ref="remittance_frontend_img">
                    <div class="upload-box" @click="openUpload()" v-if="!form_data.remittance_frontend_img">
                        <i class="el-icon-plus" style="font-size:32px"></i>
                    </div>
                    <div @click="openUpload()" class="upload-boxed" v-if="form_data.remittance_frontend_img">
                        <img :src="form_data.remittance_frontend_img" alt="" style="width:150px;height:150px;border-radius: 5px;cursor: pointer;">
                        <i class="el-icon-close" @click.stop="clearImg()" title="点击清除图片"></i>
                        <div class="upload-boxed-text">点击重新上传</div>
                    </div>
                </el-form-item>
				
                <el-form-item label="开户行"  >
                    <el-input v-model="form_data.remittance_bank" style="width:70%;"></el-input>
                </el-form-item>
                <el-form-item label="开户支行"  >
                    <el-input v-model="form_data.remittance_sub_bank" style="width:70%;"></el-input>
                </el-form-item>
                <el-form-item label="开户名"  >
                    <el-input v-model="form_data.remittance_bank_account_name" style="width:70%;"></el-input>
                </el-form-item>
                <el-form-item label="开户账号"  >
                    <el-input v-model="form_data.remittance_bank_account" style="width:70%;"></el-input>
                </el-form-item>
			</div>
  		</div>
 	</el-form>
 	
 	 <upload-multimedia-img
      :upload-show="showSelectMaterialPopup"
      selNum="one"
      type="1"
      @replace="showSelectMaterialPopup = !showSelectMaterialPopup"
      @sure="selectedMaterial"
    ></upload-multimedia-img>
</div>
  `,
  style:`

  `,
  props: {
      http_url:{
          type:String,
          default() {
              return "";
          },
      },
      componentData:{
      default(){
        return {}
      }
    },
  },
  data(){
    return{
        form_data:{},

        showSelectMaterialPopup: false
    }
  },
  created() {
      if(this.componentData){
          this.form_data = this.componentData
      }
  },
  methods: {
      //验证方法
      filterData() {
          return true
      },
      //点击提交
      returnData() {
          if (this.filterData()) {
              return this.form_data
          } else {
              return false
          }
      },

      openUpload() {
          this.showSelectMaterialPopup = true;
      },
      selectedMaterial(name, image, imageUrl) {
          // console.log(name, image, imageUrl,22)
          this.form_data.remittance_frontend_img = imageUrl[0].url;
      },
      clearImg() {
          this.form_data.remittance_frontend_img = '';
          this.$forceUpdate();
      },

  },
});