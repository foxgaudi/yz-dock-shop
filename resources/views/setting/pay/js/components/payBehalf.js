define({
  name:"payBehalf",
  template:`
  <div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">找人代付</div>
				<div class="vue-main-title-button">
				</div>
			</div>
    		<div class="vue-main-form">
				<el-form-item label="是否开启">
					<template>
						<el-switch v-model="form_data.another" active-value="1" inactive-value="0"></el-switch>
					</template>
					  <div style="font-size: 12px;">开启后,(买家)下单后，可将订单分享给小伙伴(朋友圈、微信群、微信好友)请他帮忙付款。</div>
				</el-form-item>
                <el-form-item label="发起人求助">
                    <el-input v-model="form_data.another_share_title" style="width:70%;"></el-input>
                    <div style="font-size: 12px;">提示：默认分享标题：土豪大大，跪求代付</div>
                </el-form-item>
                <el-form-item label="代付页面">
                    <el-radio v-model="form_data.another_share_type" :label="1">样式一</el-radio>
                    <el-radio v-model="form_data.another_share_type" :label="2">样式二</el-radio>
                </el-form-item>
			</div>
  		</div>
 	</el-form>
</div>
  `,
  style:`

  `,
  props: {
      http_url:{
          type:String,
          default() {
              return "";
          },
      },
      componentData:{
      default(){
        return {}
      }
    },
  },
  data(){
    return{
        form_data:{},
    }
  },
  created() {
      if(this.componentData){
          this.form_data = this.componentData
      }
  },
  methods: {
      //验证方法
      filterData() {
          return true
      },
      //点击提交
      returnData() {
          if (this.filterData()) {
              return this.form_data
          } else {
              return false
          }
      },
  },
});