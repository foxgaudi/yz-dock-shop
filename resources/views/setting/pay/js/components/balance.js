define({
  name:"balance",
  template:`
  <div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">余额支付</div>
				<div class="vue-main-title-button"></div>
			</div>
			<div class="vue-main-form">
                 <el-form-item label="是否开启">
                    <template>
                        <el-switch v-model="form_data.credit" active-value="1" inactive-value="0">
                        </el-switch>
                    </template>
                </el-form-item>
			</div>
		</div>	
 	</el-form>
</div>
  `,
  style:`

  `,
  props: {
      http_url:{
          type:String,
          default() {
              return "";
          },
      },
      componentData:{
      default(){
        return {}
      }
    },
  },
  data(){
    return{
        form_data:{},
    }
  },
  created() {
      if(this.componentData){
          this.form_data = this.componentData
      }
  },
  methods: {
      //验证方法
      filterData() {
          return true
      },
      //点击提交
      returnData() {
          if (this.filterData()) {
              return this.form_data
          } else {
              return false
          }
      },
  },
});