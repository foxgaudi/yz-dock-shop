define({
  name:"alipay",
  template:`
  <div>
	<el-form :model="form_data" label-position="right" label-width="15%">
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">支付宝支付</div>
				<div class="vue-main-title-button">
				</div>
			</div>
    		<div class="vue-main-form">
				<el-form-item label="是否开启">
					<template>
						<el-switch v-model="form_data.alipay" active-value="1" inactive-value="0"></el-switch>
					</template>
					<div style="font-size: 12px;"></div>
				</el-form-item>
                <el-form-item label="应用ID">
                    <el-input v-model="form_data.alipay_app_id" style="width:60%;"></el-input>
                </el-form-item>
                <el-form-item label="开发者私钥">
                    <el-input v-model="form_data.rsa_private_key" type="textarea" style="width:70%;" v-if="show"></el-input>
                </el-form-item>
                <el-form-item label="支付宝公钥">
                    <el-input v-model="form_data.rsa_public_key" type="textarea" style="width:70%;" v-if="show"></el-input>
                </el-form-item>
                <el-form-item label="">
                    <el-button type="primary" @click="Reset">重新设置公私钥</el-button>
                </el-form-item>
			</div>
  		</div>
		<div class="vue-head">
			<div class="vue-main-title" style="margin-bottom:20px">
				<div class="vue-main-title-left"></div>
				<div class="vue-main-title-content">支付宝提现设置</div>
				<div class="vue-main-title-button"></div>
			</div>
			<div class="vue-main-form">
                 <el-form-item label="是否开启">
                    <template>
                        <el-switch v-model="form_data.alipay_withdrawals" active-value="1" inactive-value="0">
                        </el-switch>
                    </template>
                </el-form-item>
                 <div v-if="form_data.alipay_withdrawals=='1'">

                    <el-form-item label="应用ID">
                        <el-input v-model="form_data.alipay_transfer_app_id" style="width:60%;"></el-input>
                    </el-form-item>

                    <el-form-item label="应用私钥">
                        <el-input v-model="form_data.alipay_transfer_private" type="textarea" style="width:70%;" v-if="alipay_transfer_private_show"></el-input>
                        <span style="color:#29BA9C;font-size:12px;" v-if="form_data.alipay_transfer_private && !alipay_transfer_private_show ">已填写</span>
                        <el-button type="primary" @click="ResetValue('alipay_transfer_private')" v-if="!alipay_transfer_private_show">重置</el-button>
                    </el-form-item>
                    <el-form-item label="应用公钥证书">
                        <el-upload
                                class="upload-demo"
                                name="alipay_app_public_cert"
                                :action="http_url + 'setting.shop.newUpload'"
                                :show-file-list="false"
                                :on-success="uploadSuccess"
                                :on-error="uploadfail"
                        >
                            <el-button type="primary">文件上传</el-button>
                        </el-upload>
                        <span style="color:#5adda2;font-size:12px;"  v-if="form_data.alipay_app_public_cert">已上传</span>
                        <!--<div style="font-size:12px;">提示：下载应用公钥证书</div>-->
                    </el-form-item>
                    <el-form-item label="支付宝公钥证书">
                        <el-upload
                                class="upload-demo"
                                :action="http_url + 'setting.shop.newUpload'"
                                name="alipay_public_cert"
                                :show-file-list="false"
                                :on-success="uploadSuccess"
                                :on-error="uploadfail"
                        >
                            <el-button type="primary">文件上传</el-button>
                        </el-upload>
                        <span style="color:#5adda2;font-size:12px;"  v-if="form_data.alipay_public_cert">已上传</span>
                        <!--<div style="font-size:12px;">提示：下载支付宝公钥证书</div>-->
                    </el-form-item>
                    <el-form-item label="支付宝根证书">
                        <el-upload
                                class="upload-demo"
                                name="alipay_root_cert"
                                :action="http_url + 'setting.shop.newUpload'"
                                :show-file-list="false"
                                :on-success="uploadSuccess"
                                :on-error="uploadfail"
                        >
                            <el-button type="primary">文件上传</el-button>
                        </el-upload>
                        <span style="color:#5adda2;font-size:12px;"  v-if="form_data.alipay_root_cert">已上传</span>
                        <!--<div style="font-size:12px;">提示：下载支付宝根证书</div>-->
                    </el-form-item>
                </div>
			</div>
		</div>	
 	</el-form>
</div>
  `,
  style:`

  `,
  props: {
      http_url:{
          type:String,
          default() {
              return "";
          },
      },
      componentData:{
      default(){
        return {}
      }
    },
  },
  data(){
    return{
        show:true,
        alipay_transfer_private_show:true,

        form_data:{},
    }
  },
  created() {
      if(this.componentData){
          this.form_data = this.componentData
      }
      this.getShow();
  },
  methods: {
      getShow(){
          if(this.form_data.rsa_private_key||this.form_data.rsa_public_key){
              this.show=false
          }
          if(this.form_data.alipay_transfer_private){
              this.alipay_transfer_private_show=false
          }
      },
      Reset(){
          this.show= true
          this.form_data.rsa_private_key=''
          this.form_data.rsa_public_key=''
      },
      ResetValue(str){
          switch (str) {
              case 'alipay_transfer_private' :
                  this.form_data.alipay_transfer_private = ''
                  this.alipay_transfer_private_show = true
                  break;
          }
      },
      uploadkey(res) {
          if (res.result) {
              this.$message({ message: res.msg, type: "success" });
              this.form_data.weixin_key = res.data.data.data.weixin_key;
          } else {
              this.$message({ message: res.msg, type: "error" });
          }
      },
      uploadfail(res) {
          this.$message({ 上传失败: "error" });
      },

      uploadSuccess(res) {
          if (res.result) {
              this.$message({ message: res.msg, type: "success" });
              this.form_data[res.data.data.data.key] = res.data.data.data.value;
          } else {
              this.$message({ message: res.msg, type: "error" });
          }
      },
      //验证方法
      filterData() {
          return true
      },
      //点击提交
      returnData() {
          if (this.filterData()) {
              return this.form_data
          } else {
              return false
          }
      },
  },
});