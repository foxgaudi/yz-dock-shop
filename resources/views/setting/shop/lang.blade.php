@extends('layouts.base')
@section('content')
@section('title', trans('语言设置'))

<style>
    .content {
        background: #eff3f6;
        padding: 10px !important;
    }

    .con {
        padding-bottom: 40px;
        position: relative;
        border-radius: 8px;
        min-height: 100vh;
        background-color: #fff;
    }

    .con .setting .block {
        padding: 10px;
        background-color: #fff;
        border-radius: 8px;
    }

    .con .setting .block .title {
        font-size: 18px;
        margin-bottom: 15px;
        display: flex;
        align-items: center;
    }

    .el-form-item__label {
        margin-right: 30px;
    }

    b {
        font-size: 14px;
    }

    .confirm-btn {
        width: calc(100% - 266px);
        position: fixed;
        bottom: 0;
        right: 0;
        margin-right: 10px;
        line-height: 63px;
        background-color: #ffffff;
        box-shadow: 0px 8px 23px 1px rgba(51, 51, 51, 0.3);
        background-color: #fff;
        text-align: center;
    }

    .el-tabs {
        padding-right: 10px;
    }
</style>
<div id='re_content'>
    <template>
        <el-tabs v-model="activeName"
                 style="padding-left:10px;background-color:#fff;margin-bottom:10px;border-radius:8px;">

            <el-tab-pane v-for="(item,index) in form" :label="item.name" :name="index"></el-tab-pane>

        </el-tabs>
    </template>
    <div class="con">
        <div class="setting">
            <el-form ref="form" :model="form" label-width="15%">
                <div class="block">
                    <template v-for="(item,plugin_name) in form" v-if="activeName==plugin_name">
                        <el-form-item v-for="(item1,index1) in item.data" :key="item1.key" :label="item1.name">
                            <el-input v-model="item1.value" style="width:70%;"></el-input>
                            <div v-if="item1.remark" style="color:#ccc;">[[item1.remark]]</div>
                        </el-form-item>
                    </template>
                </div>
                <div class="confirm-btn">
                    <el-button type="primary" @click="submit">提交</el-button>
                </div>
            </el-form>
        </div>
    </div>
    </div>
    <script>
        var vm = new Vue({
            el: "#re_content",
            delimiters: ['[[', ']]'],
            data() {
                return {
                    activeName: 'member_center',
                    form:{},
                }
            },
            mounted() {
                this.getData()
            },
            methods: {
                getData() {
                    this.$http.post('{!! yzWebFullUrl('setting.lang.index') !!}').then(function (response) {
                        if (response.data.result) {
                            if (response.data.data) {
                                 this.form =  response.data.data;
                            }
                        } else {
                            this.$message({message: response.data.msg, type: 'error'});
                        }
                    }, function (response) {
                        this.$message({message: response.data.msg, type: 'error'});
                    })
                },
                submit() {
                    let loading = this.$loading({
                        target: document.querySelector(".content"),
                        background: 'rgba(0, 0, 0, 0)'
                    });
                    this.$http.post('{!! yzWebFullUrl('setting.lang.index') !!}', {'setdata': this.form}).then(function (response) {
                        if (response.data.result) {
                            this.$message({message: response.data.msg, type: 'success'});
                        } else {
                            this.$message({message: response.data.msg, type: 'error'});
                        }
                        loading.close();
                        location.reload();
                    }, function (response) {
                        this.$message({message: response.data.msg, type: 'error'});
                    })
                },
            },
        });
    </script>
@endsection
