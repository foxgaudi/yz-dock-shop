@extends('layouts.base')

@section('content')
    <style>
        .panel {
            margin-bottom: 10px !important;
            padding-left: 20px;
            border-radius: 10px;
        }

        .panel .active a {
            background-color: #29ba9c !important;
            border-radius: 18px !important;
            color: #fff;
        }

        .panel a {
            border: none !important;
            background-color: #fff !important;
        }

        .content {
            background: #eff3f6;
            padding: 10px !important;
        }

        .con {
            padding-bottom: 20px;
            position: relative;
            border-radius: 8px;
            min-height: 100vh;
            background-color: #fff;
        }

        .con .setting .block {
            padding: 10px;
            background-color: #fff;
            border-radius: 8px;
        }

        .con .setting .block .title {
            font-size: 18px;
            margin-bottom: 15px;
            display: flex;
            align-items: center;
        }

        .confirm-btn {
            width: calc(100% - 266px);
            position: fixed;
            bottom: 0;
            right: 0;
            margin-right: 10px;
            line-height: 63px;
            background-color: #ffffff;
            box-shadow: 0px 8px 23px 1px rgba(51, 51, 51, 0.3);
            background-color: #fff;
            text-align: center;
        }

        b {
            font-size: 14px;
        }
    </style>
    <div id='re_content'>
        @include('layouts.newTabs')
        <div class="con">
            <div class="setting">
                <el-form ref="form" label-width="15%">
                    <div class="block">
                        <div class="title">
                            <span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span>
                            <b>应用日志</b>
                        </div>

                    </div>
                </el-form>
                <div>
                    <el-form ref="form1" label-width="15%">
                        <el-form-item label="应用日志保留时长">
                            <el-input v-model="form.day" style="width:30%;">
                                <template slot="append">天</template>
                            </el-input>
                            <div style="font-size: 12px;color: #FF0000">默认7天，最低设置7天、最高无上限；根据《中华人民共和国网络安全法》等相关规定，应用日志留存至少要求6个月，服务器空间足够的建议设置180天！<br>
                                留存周期越长，对服务器空间要求越高，请大家务必注意预留足够的服务器空间！<br>
                                应用日志不同于服务器访问日志，如果需要设置服务器访问日志留存时长，需由专业的运维人员在服务器进行配置！
                            </div>

                        </el-form-item>
                        <el-form-item label="当前应用日志大小：">
                            [[data.site]]
                        </el-form-item>
                        <el-form-item label="需要清理的时间">
                            <el-date-picker
                                    v-model="times"
                                    type="daterange"
                                    value-format="timestamp"
                                    range-separator="至"
                                    start-placeholder="开始日期"
                                    end-placeholder="结束日期"
                                    style="margin-left:5px;"
                                    align="right"
                            >
                            </el-date-picker>
                            <el-button type="danger" @click="del()" plain> 删除</el-button>
                            <div style="font-size: 12px;color: #FF0000">
                                提交保存设置不会执行应用日志删除操作，需要选择日期区间后点击删除，才能删除选择日期区间的应用日志；<br>
                                请注意根据监管要求留存日志，如果不留存可能会面临监管处罚！
                            </div>
                        </el-form-item>
                        <div style="text-align: center">
                            <el-button type="primary" @click="submit()">提交</el-button>
                        </div>
                    </el-form>
                </div>
            </div>
        </div>
    </div>
    <script>
        new Vue({
            el: "#re_content",
            delimiters: ['[[', ']]'],
            data() {
                let data = JSON.parse(`{!! json_encode($data) !!}`);
                return {
                    times: [],
                    form: {
                        'day': data.day
                    },
                    setTime: false,
                    name: '',
                    plugin: '',
                    data: data,
                }
            },
            methods: {
                submit() {
                    let loading = this.$loading({
                        target: document.querySelector(".content"),
                        background: 'rgba(0, 0, 0, 0)'
                    });
                    let log = {
                        day: this.form.day
                    };
                    this.$http.post("{!! yzWebFullUrl('setting.shop.website-log-save') !!}", {log: log}).then(function (response) {
                        if (response.data.result) {
                            this.$message({
                                message: response.data.msg,
                                type: 'success'
                            });
                        } else {
                            this.$message({
                                message: response.data.msg,
                                type: 'error'
                            });
                        }
                        loading.close();
                    }, function (response) {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    })
                },
                del() {
                    this.$confirm('是否确定删除？', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        let loading = this.$loading({
                            target: document.querySelector(".content"),
                            background: 'rgba(0, 0, 0, 0)'
                        });
                        let search = {
                            start: '',
                            end: ''
                        };
                        if (this.times && this.times.length > 0) {
                            search.start = this.times[0] / 1000;
                            search.end = this.times[1] / 1000;
                        }
                        this.$http.post("{!! yzWebFullUrl('setting.shop.website-log-delete') !!}", search).then(function (response) {
                            if (response.data.result) {
                                this.$message({
                                    message: response.data.msg,
                                    type: 'success'
                                });
                            } else {
                                this.$message({
                                    message: response.data.msg,
                                    type: 'error'
                                });
                            }
                            loading.close();
                        }, function (response) {
                            this.$message({
                                message: response.data.msg,
                                type: 'error'
                            });
                        })
                    })
                },

                setTimeBox(name, plugin) {
                    this.name = name;
                    this.plugin = plugin;
                    this.setTime = true;
                },
                cancel() {
                    this.name = '';
                    this.plugin = '';
                    this.setTime = false;
                }
            },
        });
    </script>
@endsection