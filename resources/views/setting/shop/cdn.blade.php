@extends('layouts.base')
@section('title', '刷新cdn')
@section('content')
    <style>
        .panel{
            margin-bottom:10px!important;
            padding-left: 20px;
            border-radius: 10px;
        }
        .panel .active a {
            background-color: #29ba9c!important;
            border-radius: 18px!important;
            color:#fff;
        }
        .panel a{
            border:none!important;
            background-color:#fff!important;
        }
        .content{
            background: #eff3f6;
            padding: 10px!important;
        }
        .con{
            padding-bottom:20px;
            position:relative;
            border-radius: 8px;
            min-height:100vh;
            background-color:#fff;
        }
        .con .setting .block{
            padding:10px;
            background-color:#fff;
            border-radius: 8px;
        }
        .con .setting .block .title{
            font-size:18px;
            margin-bottom:15px;
            display:flex;
            align-items:center;
        }
        .confirm-btn{
            width: 100%;
            position:absolute;
            bottom:0;
            left:0;
            line-height:63px;
            background-color: #ffffff;
            box-shadow: 0px 8px 23px 1px
            rgba(51, 51, 51, 0.3);
            background-color:#fff;
            text-align:center;
        }
        b{
            font-size:14px;
        }
    </style>
    <div id='re_content' >
        <template>

        </template>
        @include('layouts.newTabs')
        <div class="con">
            <div class="setting">
                <div class="block">
                    <div class="title">
                        <span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span>
                        <b>刷新cdn</b>
                    </div>
                    <div style="margin-left: 50px" v-if="is_set!=1">
                        <el-form ref="form" :model="form" label-width="15%">
                            <el-form-item label="用户名">
                                <el-input v-model="form.username" placeholder="username" style="width:70%;"></el-input>
                            </el-form-item>
                            <el-form-item label="密码">
                                <el-input v-model="form.password" placeholder="password" style="width:70%;"></el-input>
                            </el-form-item>
                            <div class="help-block" style="color: red">如已注册pc端编译账号, 直接使用.不需要重新注册</div>
                        </el-form>
                        <div style="margin-bottom: 20px;margin-left: 510px">
                            <el-button type="primary" @click="submit">保存</el-button>
                        </div>
                    </div>
                    <div style="margin-left: 50px;margin-bottom: 20px" v-else>
                        <el-form ref="form" :model="form" label-width="15%">
                            <el-form-item label="alias_name">
                                <el-input v-model="second_form.alias_name" placeholder="alias_name" style="width:70%;"></el-input>
                                <div class="help-block" style="color: red">平台上记录的客户名称</div>
                            </el-form-item>
                            {{--<el-form-item label="domain_name">--}}
                                {{--<el-input v-model="second_form.domain_name" placeholder="domain_name" style="width:70%;"></el-input>--}}
                                {{--<div class="help-block" style="color: red">当前网站域名</div>--}}
                            {{--</el-form-item>--}}
                        </el-form>
                        <div style="margin-bottom: 20px;margin-left: 510px">
                            <el-button type="primary" @click="submitSecond">保存</el-button>
                        </div>
                        <el-button type="primary" @click="refresh">手动刷新</el-button>
                        {{--<el-button type="primary" @click="dialogVisible = true">修改用户资料</el-button>--}}
                    </div>
                    <div class="title">
                        <span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span>
                        <b>刷新cdn列表</b>
                    </div>
                    <div style="margin-left: 50px">
                        <el-table :data="tableData" style="padding:0 10px" style="width: 100%">
                            <el-table-column prop="create_time" align="center" label="时间"></el-table-column>
                            <el-table-column prop="status" align="center" label="状态">
                                <template slot-scope="scope">
                                    <div v-if="scope.row.status=='C'">成功</div>
                                    <div v-else="scope.row.status=='R'">运行中</div>
                                    <div v-else>失败</div>
                                </template>
                            </el-table-column>
                        </el-table>
                    </div>
                </div>
                <el-dialog title="username/password" :visible.sync="dialogVisible" width="40%">
                    <el-form ref="dialog_form" label-width="15%">
                        <el-form-item label="username">
                            <el-input v-model="dialog_form.username" style="width: 300px"></el-input>
                        </el-form-item>
                        <el-form-item label="password">
                            <el-input v-model="dialog_form.password" style="width: 300px"></el-input>
                        </el-form-item>
                    </el-form>
                    <span slot="footer" class="dialog-footer">
                        <el-button @click="dialogVisible = false">取 消</el-button>
                        <el-button type="primary" @click="submitDialog">提 交</el-button>
                    </span>
                </el-dialog>
            </div>
        </div>
    </div>
    <script>
        let set = {!! $set ?: [] !!};
        let cnd_data = {!! $cnd_data ?: [] !!};
        let is_set = {!! $is_set ?: 0 !!};
        var vm = new Vue({
            el: "#re_content",
            delimiters: ['[[', ']]'],
            data() {
                return {
                    form: {
                        username: '',
                        password: '',
                        ...set,
                    },
                    dialog_form: {
                        username: '',
                        password: '',
                    },
                    second_form: {
                        alias_name: '',
                        // domain_name: '',
                        ...cnd_data,
                    },
                    tableData: [],
                    is_set: is_set,
                    dialogVisible: false,
                }
            },
            mounted () {
                if (this.is_set==1) {
                    this.getList();
                }
            },
            methods: {
                refresh() {
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0.2)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.postCdn') !!}',{'data':this.form}).then(function (response) {
                            if (response.data.result) {
                                window.location.reload()
                            } else{
                                this.$message({type: 'error',message: response.data.msg});
                            }
                            loading.close();
                        },function (response) {
                            this.$message({type: 'error',message: response.data.msg});
                            loading.close();
                        }
                    );
                },
                getList() {
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0.2)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.getCdn') !!}',{'data':this.form}).then(function (response) {
                            if (response.data.result) {
                                this.tableData = response.data.data.list;
                            } else{
                                this.$message({type: 'error',message: response.data.msg});
                            }
                            loading.close();
                        },function (response) {
                            this.$message({type: 'error',message: response.data.msg});
                            loading.close();
                        }
                    );
                },
                submit() {
                    let json = {
                        username:this.form.username,
                        password:this.form.password,
                    };
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0.2)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.cdn') !!}', {set:json}).then(function (response) {
                            if (response.data.result) {
                                if(response.data.data){
                                    window.location.reload();
                                    this.$message({type: 'success',message: '保存成功!'});
                                }else{
                                    this.$message({type: 'error',message: '保存失败!'});
                                }
                            }
                            else{
                                this.$message({type: 'error',message: response.data.msg});
                            }
                            loading.close();
                        },function (response) {
                            this.$message({type: 'error',message: response.data.msg});
                            loading.close();
                        }
                    );
                },
                submitDialog() {
                    let json = {
                        username:this.dialog_form.username,
                        password:this.dialog_form.password,
                    };
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0.2)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.editCdn') !!}', {set:json}).then(function (response) {
                            if (response.data.result) {
                                if(response.data.data){
                                    this.$message({type: 'success',message: '修改成功!'});
                                }else{
                                    this.$message({type: 'error',message: '修改失败!'});
                                }
                            }
                            else{
                                this.$message({type: 'error',message: response.data.msg});
                            }
                            loading.close();
                        },function (response) {
                            this.$message({type: 'error',message: response.data.msg});
                            loading.close();
                        }
                    );
                },
                submitSecond() {
                    let json = {
                        alias_name:this.second_form.alias_name,
                        // domain_name:this.second_form.domain_name,
                    };
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0.2)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.saveCndData') !!}', {set:json}).then(function (response) {
                            if (response.data.result) {
                                if(response.data.data){
                                    window.location.reload();
                                    this.$message({type: 'success',message: '保存成功!'});
                                }else{
                                    this.$message({type: 'error',message: '保存失败!'});
                                }
                            }
                            else{
                                this.$message({type: 'error',message: response.data.msg});
                            }
                            loading.close();
                        },function (response) {
                            this.$message({type: 'error',message: response.data.msg});
                            loading.close();
                        }
                    );
                },
            },
        });
    </script>
@endsection('content')
