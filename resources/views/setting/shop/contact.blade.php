@extends('layouts.base')
@section('content')
    <style>
        .panel{
            margin-bottom:10px!important;
            padding-left: 20px;
            border-radius: 10px;
        }
        .panel .active a {
            background-color: #29ba9c!important;
            border-radius: 18px!important;
            color:#fff;
        }
        .panel a{
            border:none!important;
            background-color:#fff!important;
        }
        .content{
            background: #eff3f6;
            padding: 10px!important;
        }
        .con{
            padding-bottom:20px;
            position:relative;
            min-height:100vh;
            background-color:#fff;
            border-radius: 8px;
        }
        .con .setting .block{
            padding:10px;
            background-color:#fff;
            border-radius: 8px;
        }
        .con .setting .block .title{
            display:flex;
            align-items:center;
            margin-bottom:15px;
        }
        .confirm-btn{
            width: calc(100% - 266px);
            position:fixed;
            bottom:0;
            right:0;
            margin-right:10px;
            line-height:63px;
            background-color: #ffffff;
            box-shadow: 0px 8px 23px 1px
            rgba(51, 51, 51, 0.3);
            background-color:#fff;
            text-align:center;
        }

        b{
            font-size:14px;
        }
    </style>
    <div id='re_content' >
        @include('layouts.newTabs')
        <div class="con">
            <div class="setting">
                <div class="block">
                    <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>基础设置</b></div>
                    <el-form ref="form" :model="form" label-width="15%">
                        <el-form-item label="客服电话">
                            <el-input v-model="form.phone" placeholder="请输入客服电话" style="width:70%;"></el-input>
                        </el-form-item>
                        <el-form-item label="所在地址">
                            <el-input v-model="form.address" placeholder="请输入所在地址" style="width:70%;"></el-input>
                        </el-form-item>
                        <el-form-item label="商城简介">
                            <el-input v-model="form.description"  type="textarea" placeholder="请输入商城简介" style="width:70%;"></el-input>
                        </el-form-item>
                        <el-form-item label="店铺名称">
                            <el-input v-model="form.store_name" placeholder="请输入店铺名称" style="width:70%;"></el-input>
                        </el-form-item>
                        <el-form-item label="店铺位置">
                            <el-select  @change="changeProvince" v-model="form.province_id" :clearable="true"  :filterable="true">
                                <el-option :label="v.areaname" :value="v.id" v-for="(v,k) in province_list"></el-option>
                            </el-select>
                            <el-select @change="changeCity" v-model="form.city_id" :clearable="true"  :filterable="true">
                                <el-option :label="v.areaname" :value="v.id" v-for="(v,k) in city_list" v-if="v.parentid==form.province_id"></el-option>
                            </el-select>
                            <el-select @change="changeDistrict" v-model="form.district_id" :clearable="true"  :filterable="true">
                                <el-option :label="v.areaname" :value="v.id" v-for="(v,k) in district_list" v-if="v.parentid==form.city_id"></el-option>
                            </el-select>
                            <el-select v-model="form.street_id" :clearable="true"  :filterable="true">
                                <el-option :label="v.areaname" :value="v.id" v-for="(v,k) in street_list" v-if="v.parentid==form.district_id"></el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label="店铺地址">
                            <el-input v-model="form.store_address" placeholder="请输入店铺地址" style="width:70%;"></el-input>
                        </el-form-item>
                        <el-form-item label="店铺定位">
                            <el-input v-model="form.store_longitude" placeholder="请输入店铺经度" style="width:30%;">
                                <template slot="prepend">经度</template>
                            </el-input>
                            <el-input v-model="form.store_latitude" placeholder="请输入店铺纬度" style="width:30%;">
                                <template slot="prepend">纬度</template>
                            </el-input>
                            <el-button @click="mapDialogVisible = true">选择坐标</el-button>
                        </el-form-item>
                    </el-form>
                </div>
                <map_cpn :map-dialog-visible.sync="mapDialogVisible"
                         :lat="form.store_latitude"
                         :lng="form.store_longitude"
                         v-if="mapDialogVisible"
                         @map_data="getMapData"></map_cpn>
            </div>


            <div class="confirm-btn">
                <el-button type="primary" @click="submit">提交</el-button>
            </div>
        </div>
    </div>
    @include('public.admin.map_cpn')
    <script>
        var vm = new Vue({
            el: "#re_content",
            delimiters: ['[[', ']]'],
            data() {
                return {
                    activeName: 'first',
                    form:{
                        phone:'',
                        address:'',
                        description:'',
                        store_name:'',
                        store_address:'',
                        store_longitude:113.275995,
                        store_latitude:23.117055,
                        province_id: '',
                        city_id: '',
                        district_id: '',
                        street_id: '',
                    },
                    map: "",
                    marker: "",
                    centerParam: [113.275995, 23.117055],
                    zoomParam: "",
                    markersParam: [113.275995, 23.117055],
                    pointNew: "",
                    choose_center: [],
                    choose_marker: [],
                    map_show: false,
                    map_keyword: '',
                    province_list: [],
                    city_list: [],
                    district_list: [],
                    street_list: [],
                    mapDialogVisible: false,
                }
            },
            mounted () {
                this.getData();
                this.initProvince();
            },
            methods: {
                getMapData(data) {
                    console.log(data)
                    this.form.store_longitude = data.baidu_location.lng;
                    this.form.store_latitude = data.baidu_location.lat;
                },
                getData(){
                    this.$http.post('{!! yzWebFullUrl('setting.shop.contact') !!}').then( (response)=>{
                        if (response.data.result == 1) {
                            if(response.data.data.set){
                                for(let i in response.data.data.set){
                                    this.form[i]=response.data.data.set[i]
                                }
                                if (this.form.store_longitude && this.form.store_latitude) {
                                    this.centerParam = [this.form.store_longitude, this.form.store_latitude];
                                    this.markersParam = [this.form.store_longitude, this.form.store_latitude];
                                }
                                
                                let set = response.data.data.set;
                                // 省市区
                                if(set.province_id) {
                                    this.changeProvince(set.province_id);
                                    this.form.province_id = set.province_id;
                                }
                                if(set.city_id) {
                                    this.changeCity(set.city_id);
                                    this.form.city_id = set.city_id;
                                }
                                if(set.district_id) {
                                    this.changeDistrict(set.district_id);
                                    this.form.district_id = set.district_id;
                                    this.form.street_id = set.street_id;
                                }
                            }
                        }else {
                            this.$message({message: response.data.msg,type: 'error'});
                        }

                    }, (response)=> {
                        this.$message({message: response.data.msg,type: 'error'});
                    })
                },
                initProvince() {
                    this.areaLoading = true;
                    this.$http.get("{!! yzWebUrl('area.list.init', ['area_ids'=>'']) !!}").then(response => {
                        this.province_list = response.data.data;
                        this.areaLoading = false;
                    }, response => {
                        this.areaLoading = false;
                    });
                },
                changeProvince(val) {
                    this.city_list = [];
                    this.district_list = [];
                    this.street_list = [];
                    this.form.city_id = "";
                    this.form.district_id = "";
                    this.form.street_id = "";
                    this.areaLoading = true;
                    let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                    this.$http.get(url).then(response => {
                        if (response.data.data.length) {
                            this.city_list = response.data.data;
                        } else {
                            this.city_list = null;
                        }
                        this.areaLoading = false;
                    }, response => {
                        this.areaLoading = false;
                    });
                },
                // 市改变
                changeCity(val) {
                    this.district_list = [];
                    this.street_list = [];
                    this.form.district_id = "";
                    this.form.street_id = "";
                    this.areaLoading = true;
                    let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                    this.$http.get(url).then(response => {
                        if (response.data.data.length) {
                            this.district_list = response.data.data;
                        } else {
                            this.district_list = null;
                        }
                        this.areaLoading = false;
                    }, response => {
                        this.areaLoading = false;
                    });
                },
                // 区改变
                changeDistrict(val) {
                    this.street_list = [];
                    this.form.street_id = "";
                    this.areaLoading = true;
                    let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                    this.$http.get(url).then(response => {
                        if (response.data.data.length) {
                            this.street_list = response.data.data;
                        } else {
                            this.street_list = null;
                        }
                        this.areaLoading = false;
                    }, response => {
                        this.areaLoading = false;
                    });
                },
                submit() {
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0)'});
                    this.$http.post('{!! yzWebFullUrl('setting.shop.contact') !!}',{'contact':this.form}).then(function (response){
                        if (response.data.result) {
                            this.$message({message: response.data.msg,type: 'success'});
                        }else {
                            this.$message({message: response.data.msg,type: 'error'});
                        }
                        loading.close();
                        location.reload();
                    },function (response) {
                        this.$message({message: response.data.msg,type: 'error'});
                    })
                },
            },
        });
    </script>
@endsection
