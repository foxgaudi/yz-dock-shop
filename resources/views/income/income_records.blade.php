@extends('layouts.base')
@section('title', '收入明细')
@section('content')
    <link href="{{static_url('yunshop/css/member.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{static_url('yunshop/css/member.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
    <style>
        .content {
            background: #eff3f6;
            padding: 10px !important;
        }

        .vue-main-form {
            margin-top: 0;
        }

        .edit-i {
            display: inline;
        }

        .el-icon-edit {
            padding: 0;
        }
    </style>
    @include('public.admin.exportExcel')

    <div id="app" v-cloak class="main">
        <div class="block">
            <div class="vue-head">
                <div class="vue-main-title" style="margin-bottom:20px">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">明细管理</div>
                    <div class="vue-main-title-button">
                    </div>
                </div>
                <div class="vue-search">
                    <el-form :inline="true" :model="search_form" class="demo-form-inline">
                        <el-form-item label="">
                            <el-input
                                    placeholder="会员ID"
                                    v-model="search_form.member_id"
                                    clearable>
                            </el-input>
                        </el-form-item>
                        <el-form-item label="">
                            <el-input
                                    placeholder="姓名/昵称/手机号"
                                    v-model="search_form.realname"
                                    clearable>
                            </el-input>
                        </el-form-item>
                        <el-form-item label="">
                            <el-select clearable v-model="search_form.class" placeholder="业务类型">
                                <el-option
                                        v-for="(item,index) in income_type_comment"
                                        :key="item.index"
                                        :label="item.title"
                                        :value="item.class">
                                </el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label="">
                            <el-select clearable v-model="search_form.status" placeholder="提现状态">
                                <el-option
                                        v-for="(item,index) in income_status"
                                        :key="item.index"
                                        :label="item.label"
                                        :value="item.value">
                                </el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label="">
                            <el-select clearable v-model="search_form.pay_status" placeholder="打款状态">
                                <el-option
                                        v-for="(item,index) in pay_status"
                                        :key="item.index"
                                        :label="item.label"
                                        :value="item.value">
                                </el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label="">
                            <el-date-picker
                                    clearable
                                    value-format="timestamp"
                                    v-model="search_time"
                                    type="datetimerange"
                                    range-separator="至"
                                    start-placeholder="开始日期"
                                    end-placeholder="结束日期">
                            </el-date-picker>
                        </el-form-item>
                        <el-form-item label="">
                            <el-input
                                    placeholder="订单号"
                                    v-model="search_form.order_sn"
                                    clearable>
                            </el-input>
                        </el-form-item>
                        <el-form-item label="">
                            <el-button type="primary" @click="search(1)">搜索</el-button>
                        </el-form-item>
                        <el-form-item label="">
                            <el-button type="primary" @click="exportList()">导出 EXCEL</el-button>
                        </el-form-item>
                    </el-form>
                </div>
                <div style="color: red">
                    特别说明：并非所有的收入都和订单有关，也并非所有和订单有关的收入都会被记录订单号！
                    功能未更新前的收入也不保证会记录关联订单号！
                    如果未记录的，则不显示订单号，不承诺免费增加或者更新数据！
                    收入明细记录需在佣金、奖励等结算后才会产生，大多数情况是订单完成后；未结算之前请到各个插件查看记录！
                </div>
            </div>
            <export-excel :order-export-dialog.sync="orderExportDialog" :request_data="request_data"
                          :export_type="export_type" :export_url="export_url"></export-excel>

        </div>
        <div class="block">
            <div class="vue-main">
                <div class="vue-main-form">
                    <div class="vue-main-title" style="margin-bottom:20px">
                        <div class="vue-main-title-left"></div>
                        <div class="vue-main-title-content">
                            明细列表
                            <span style="margin-left:20px;font-weight: 0;color: #9b9da4">
                               总数：[[page_list.total]] &nbsp;
                               金额总合计：[[amount]]
                            </span>
                        </div>
                    </div>
                    <el-table :data="page_list.data" style="width: 100%">
                        <el-table-column label="时间" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                [[scope.row.created_at]]
                            </template>
                        </el-table-column>
                        {{--                        <el-table-column label="会员ID" align="center" prop="" width="auto">--}}
                        {{--                            <template slot-scope="scope">--}}
                        {{--                                [[scope.row.member_id]]--}}
                        {{--                            </template>--}}
                        {{--                        </el-table-column>--}}
                        <el-table-column label="粉丝" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                <div>
                                    <el-image style='width:30px;height:30px;padding:1px;border:1px solid #ccc'
                                              :src="scope.row.member.avatar"
                                              alt="">
                                    </el-image>
                                </div>
                                <div>
                                    <el-button type="text" @click="memberNav(scope.row.member_id)">
                                        （ID:[[scope.row.member_id]]） [[scope.row.member.nickname]]
                                    </el-button>
                                </div>
                            </template>
                        </el-table-column>

                        <el-table-column label="姓名/手机号" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                [[scope.row.member.realname]]<br>
                                [[scope.row.member.mobile]]
                            </template>
                        </el-table-column>
                        <el-table-column label="收入金额" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                <div>
                                    [[scope.row.amount]]
                                    <el-popover v-if="scope.row.status === 0" class="item" placement="top"
                                                effect="light">
                                        <div style="text-align:center;">
                                            <div style="text-align:left;margin-bottom:10px;font-weight:900">
                                                修改收入金额
                                            </div>
                                            <el-input v-model="change_amount" style="width:400px"
                                                      size="small"></el-input>
                                            <el-button size="small" @click="amountChange(scope)">确定</el-button>
                                        </div>
                                        <a slot="reference">
                                            <i class="el-icon-edit edit-i" title="点击编辑"
                                               @click="edit"></i>
                                        </a>
                                    </el-popover>
                                </div>
                                <div>
                                    <el-link type="success" :underline="false" @click="changeLog(scope)">修改记录
                                    </el-link>
                                </div>
                            </template>
                        </el-table-column>
                        <el-table-column label="业务类型" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                [[scope.row.type_name]]
                            </template>
                        </el-table-column>
                        <el-table-column :label="'关联订单号 \n \r \n 订单实付金额（元）'" align="center" prop=""
                                         width="105px">
                            <template slot-scope="scope">
                                <el-link v-if="scope.row.order_sn" type='success'
                                         @click='openOrder(scope.row.order_sn)'>
                                    [[scope.row.order_sn]]
                                </el-link>
                                <span v-if="!scope.row.order_sn">
                                    --
                                </span>
                                <span v-if="scope.row.order_sn">
                                    <br>
                                    [[scope.row.has_many_order ? scope.row.has_many_order.price : ""]]
                                </span>
                            </template>
                        </el-table-column>
                        <el-table-column label="提现状态" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                [[scope.row.status_name]]
                            </template>
                        </el-table-column>
                        <el-table-column label="打款状态" align="center" prop="" width="auto">
                            <template slot-scope="scope">
                                [[scope.row.pay_status_name]]
                            </template>
                        </el-table-column>
                    </el-table>
                </div>
            </div>
        </div>

        <!-- 分页 -->
        <div class="vue-page">
            <el-row>
                <el-col align="right">
                    <el-pagination layout="prev, pager, next,jumper" @current-change="search" :total="total"
                                   :page-size="per_page" :current-page="current_page" background
                    ></el-pagination>
                </el-col>
            </el-row>
        </div>
        <el-dialog
                title="修改记录"
                :visible.sync="dialogVisible"
                width="60%">
            <el-table
                    :data="changeLogList"
                    style="width: 100%">
                <el-table-column
                        prop="created_at"
                        label="时间"
                        min-width="180">
                </el-table-column>
                <el-table-column
                        prop="user.username"
                        label="操作员账号"
                        min-width="180">
                </el-table-column>
                <el-table-column
                        prop="before"
                        label="修改前金额"
                        min-width="180">
                </el-table-column>
                <el-table-column
                        prop="after"
                        label="修改后金额"
                        min-width="180">
                </el-table-column>
            </el-table>
        </el-dialog>
    </div>
    <script>
        let member_id = '{{request()->member_id ?: ''}}';
        var vm = new Vue({
            el: '#app',
            // 防止后端冲突,修改ma语法符号
            delimiters: ['[[', ']]'],
            data() {
                return {
                    request_data: {},
                    orderExportDialog: false,
                    export_url: 'income.income-records.export',
                    export_type: 'income',
                    search_form: {
                        status: '',
                        member_id: member_id,
                        realname: '',
                        class: '',
                        pay_status: '',
                        time: {
                            start: 0,
                            end: 0
                        },
                        order_sn: "{!! $_REQUEST['order_sn'] !!}",
                    },
                    page_list: {},
                    total: 0,
                    per_page: 0,
                    current_page: 0,
                    pageSize: 0,
                    amount: 0,
                    income_status: [
                        {
                            value: 0,
                            label: '未提现'
                        },
                        {
                            value: 1,
                            label: '已提现'
                        },
                    ],
                    pay_status: [
                        {
                            value: -1,
                            label: '无效'
                        },
                        {
                            value: 0,
                            label: '未审核'
                        },
                        {
                            value: 1,
                            label: '未打款'
                        },
                        {
                            value: 2,
                            label: '已打款'
                        },
                        {
                            value: 3,
                            label: '已驳回'
                        },
                    ],
                    income_type_comment: [],
                    search_time: [],
                    change_amount: '',
                    dialogVisible: false,
                    changeLogList: [],
                }
            },
            created() {
                this.getData(1)
            },
            //定义全局的方法
            beforeCreate() {
            },
            filters: {},
            methods: {
                getData(page) {
                    let search = this.search_form
                    if (this.search_time) {
                        search.time.start = this.search_time[0] ? this.search_time[0] : ''
                        search.time.end = this.search_time[1] ? this.search_time[1] : ''
                    } else {
                        search.time.start = ''
                        search.time.end = ''
                    }

                    let loading = this.$loading({
                        target: document.querySelector(".content"),
                        background: 'rgba(0, 0, 0, 0)'
                    });
                    this.$http.post('{!! yzWebFullUrl('income.income-records.index') !!}', {
                        search: search,
                        page: page
                    }).then(function (response) {
                        if (response.data.result) {
                            this.page_list = response.data.data.pageList
                            this.search_form = response.data.data.search
                            this.total = response.data.data.pageList.total
                            this.per_page = response.data.data.pageList.per_page
                            this.current_page = response.data.data.pageList.current_page
                            this.income_type_comment = response.data.data.income_type_comment
                            this.amount = response.data.data.amount
                            loading.close();
                        } else {
                            this.$message({
                                message: response.data.msg,
                                type: 'error'
                            });
                        }

                        loading.close();
                    }, function (response) {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                        loading.close();
                    });
                },
                search(page) {
                    this.getData(page)
                },
                memberNav(uid) {
                    let url = '{!! yzWebFullUrl('member.member.detail') !!}';
                    window.open(url + "&id=" + uid)
                },
                exportList() {
                    let search = this.search_form
                    if (this.search_time) {
                        search.time.start = this.search_time[0] ? this.search_time[0] : ''
                        search.time.end = this.search_time[1] ? this.search_time[1] : ''
                    } else {
                        search.time.start = ''
                        search.time.end = ''
                    }
                    this.request_data = {search: search}
                    this.orderExportDialog = true;

                },
                openOrder(order_sn) {
                    let url = '{!! yzWebFullUrl('order.order-list.index') !!}';
                    window.open(url + "&order_sn=" + order_sn)
                },
                amountChange(scope) {
                    let id = scope.row.id
                    console.log(this.change_amount)
                    this.$http.post('{!! yzWebFullUrl('income.income-records.change-amount') !!}', {
                        id: id,
                        amount: this.change_amount
                    }).then(function (response) {
                            if (response.data.result) {
                                this.$message.success("修金额成功");
                                this.getData(this.current_page)
                            } else {
                                this.$message({message: response.data.msg, type: 'error'});
                            }
                        }, function (response) {
                            this.$message({message: response.data.msg, type: 'error'});
                        }
                    );
                },
                edit() {
                    this.change_amount = '';
                },
                changeLog(scope) {
                    this.dialogVisible = true;
                    let id = scope.row.id
                    console.log(this.change_amount)
                    this.$http.post('{!! yzWebFullUrl('income.income-records.change-amount-log') !!}', {id: id}).then(function (response) {
                            if (response.data.result) {
                                this.changeLogList = response.data.data;
                            } else {
                                this.$message({message: response.data.msg, type: 'error'});
                            }
                        }, function (response) {
                            this.$message({message: response.data.msg, type: 'error'});
                        }
                    );
                },
            },
        })
    </script>
@endsection
