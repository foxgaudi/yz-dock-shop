@extends('layouts.base')
@section('title', '站点设置')
@section('content')
    <style>
        .panel{
            margin-bottom:10px!important;
            padding-left: 20px;
            border-radius: 10px;
        }
        .panel .active a {
            background-color: #29ba9c!important;
            border-radius: 18px!important;
            color:#fff;
        }
        .panel a{
            border:none!important;
            background-color:#fff!important;
        }
        .content{
            background: #eff3f6;
            padding: 10px!important;
        }
        .con{
            padding-bottom:20px;
            position:relative;
            min-height:100vh;
            background-color:#fff;
            border-radius: 8px;
        }
        .con .setting .block{
            padding:10px;
            background-color:#fff;
            border-radius: 8px;
        }
        .con .setting .block .title{
            display:flex;
            align-items:center;
            margin-bottom:15px;
        }
        .confirm-btn{
            width: calc(100% - 266px);
            position:fixed;
            bottom:0;
            right:0;
            margin-right:10px;
            line-height:63px;
            background-color: #ffffff;
            box-shadow: 0px 8px 23px 1px
            rgba(51, 51, 51, 0.3);
            background-color:#fff;
            text-align:center;
        }

        b{
            font-size:14px;
        }
    </style>
    <div id='re_content' >
        @include('layouts.newTabs')
        <div class="con">
            <div class="setting">
                <div class="block">
                    <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>MQTT设置</b></div>
                    <el-form ref="form" :model="form" label-width="15%">
                        <el-form-item label="MQTT">
                            <el-switch v-model="form.is_switch" active-value="1" inactive-value="0"></el-switch>
                            <div style="font-size:12px;">关闭则不接收订阅消息</div>
                        </el-form-item>
                        <el-form-item label="服务端地址">
                            <el-input v-model="form.address"  style="width:70%;" placeholder="127.0.0.1:4583"></el-input>
                            <div style="font-size: 12px">包含端口，例：127.0.0.1:4583</div>
                        </el-form-item>

                        <el-form-item label="用户名">
                            <el-input v-model="form.username"  style="width:70%;"></el-input>
                            <div style="font-size: 12px">如需账号密码，则填写</div>
                        </el-form-item>

                        <el-form-item label="密码" prop="password">
                            <el-input  :type="pass_input_type" v-model="form.password"  style="width:70%;">
                                <i slot="suffix" :class="icon" @click="showPass"></i>
                            </el-input>
                            <div style="font-size: 12px">如需账号密码，则填写</div>
                        </el-form-item>
                    </el-form>
                </div>
            </div>
            <div class="confirm-btn">
                <el-button type="primary" @click="onSubmit">提交</el-button>
            </div>
        </div>
    </div>
    <script>
        var app = new Vue({
            el: '#re_content',
            delimiters: ['[[', ']]'],
            data() {

                return {
                    form: {
                        is_switch:'0',
                    },
                    loading: false,
                    formLoading: false,

                    pass_input_type:"password",
                    //用于更换Input中的图标
                    icon:"el-input__icon fa fa-eye",
                }
            },
            created() {
                let result = this.viewReturn();
                this.__initial(result);
                this.search(1);
            },
            mounted() {
            },
            methods: {

                //视图返回数据
                viewReturn() {
                    return {!! $setting?:'{}' !!};
                },
                //初始化页面数据，请求链接
                __initial(data) {

                    if (data) {
                        this.form = data;
                    }

                    console.log(data);
                },
                //密码的隐藏和显示
                showPass(){
                    //点击图标是密码隐藏或显示
                    if( this.pass_input_type=="text"){
                        this.pass_input_type="password";
                        //更换图标
                        this.icon="el-input__icon fa fa-eye";
                    }else {
                        this.pass_input_type="text";
                        this.icon="el-input__icon fa fa-eye-slash";
                    }
                },

                onSubmit() {
                    let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0)'});
                    if (this.formLoading) {
                        return;
                    }
                    this.formLoading = true;

                    this.$refs.form.validate((valid) => {
                        console.log(valid)
                    });
                    this.$http.post("{!! yzWebUrl('siteSetting.store.mqtt') !!}", {'setting': this.form}).then(response => {
                        //console.log(response.data);
                        if (response.data.result) {
                            this.$message({
                                message: response.data.msg,
                                type: 'success'
                            });
                        } else {
                            this.$message({
                                message: response.data.msg,
                                type: 'error'
                            });
                        }
                        this.formLoading = false;
                        loading.close();
                        location.reload();
                    }, response => {
                        console.log(response);
                    });
                },


            }
        });
    </script>
@endsection

