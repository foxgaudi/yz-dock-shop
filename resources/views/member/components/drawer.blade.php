<style > 
    .el-tabs__item:focus.is-active.is-focus:not(:active) {
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .v-modal {
        /* z-index: 10 !important; */
    }

    .context {
        border-bottom: 1px solid #EBEEF5;
    }

    .img-box {
        display: flex;
        justify-content: center;
        margin-bottom: 20px;
    }

    .msg-line {
        display: flex;
        flex-direction: column;
        margin: 10px;
        align-items: center;
        cursor: pointer;
    }

    .msg-line span {
        margin: 10px 0;
    }

    .upload-line {
        color: #29BA9C;
        margin-top: 10px;
    }

    .el-image {
        width: 120px;
        height: 120px;
    }

    .space-input {
        justify-content: center;
        display: flex;
        align-items: center;
        margin-bottom: 20px;
    }

    .space-input span {
        width: 100px;
        text-align: center;
    }

    .space-input .el-input {
        width: 40%;
        margin: 0 10px;
    }

    .bottom {
        padding: 20px 10px;
        text-align: right;
    }

    .block .title {
        display: flex;
        align-items: center;
    }

    .el-drawer{
        overflow: scroll
    }

    .el-dialog__body {
        padding: 10px 20px !important;
    }

    .confirm-btn {
        width: 79% !important;
        position: fixed !important;
        bottom: 0 !important;
        right: 17px !important;
        left: inherit !important;
        z-index: 10;
    }
</style>
<template id="drawer">
    <div>
        <el-drawer
            :visible.sync="drawer_dialog"
            :with-header="false"
            size="80%"
            :before-close="closeDrawer"
            :lock-scroll="false">
            <div class="drawer-box" style="padding: 10px;">
                <div style="padding: 10px;display: flex;justify-content: space-between;font-size: 18px;position: fixed;top: 0;background: white;width: 78.5%;z-index: 10;">
                    <span>会员详情</span>
                    <i class="el-icon-close" @click="closeDrawer" style="cursor: pointer;"></i>
                </div>
                <div class="content-box" style="background: rgb(94%, 94%, 95%);padding: 10px;margin-top: 40px;">
                    <top_detail_cpn  :button_list="button_list" :info="info" :member_data_item="member_data_item" :form="form"></top_detail_cpn>
                    <div class="bottom-box" style="background: white;padding: 15px 25px;margin-top: 10px;">
                        <el-tabs v-model="activeName" @tab-click="handleClick" v-if="infoShow">
                            <el-tab-pane :label="item.label" :name="item.name" v-for="(item,index) in tabPane" :key="index">
                                <div style="padding-bottom: 100px;">
                                    <component :is_email="is_email" :parent_name="parent_name" :is="item.key"  :levels="levels" :groups="groups" :myform="myform" :form="form" :info="info" :set="set" :uid="member_data_item.uid" v-if="activeName && item.name == activeName"></component>
                                </div>
                            </el-tab-pane>
                        </el-tabs>
                    </div>
                </div>
            </div>
        </el-drawer>
    </div>
</template>
@include('public.admin.uploadMultimediaImg')
@include("member.components.member_base")
@include("member.components.label_cpn")
@include("member.components.address_cpn")
@include("member.components.blank_cpn")
@include("member.components.agent_parent")
@include("member.components.agent_old")
@include("member.components.agent_team")
@include("member.components.custom_cpn")
@include("member.components.account_number")
@include("member.components.member_group")
@include("member.components.member_information")
@include("member.components.top_detail_cpn")
@include("member.components.income_details")
<script>
    Vue.component('drawer', {
        delimiters: ['[[', ']]'],
        props: {
            drawer_dialog: {
                type: Boolean,
                default: false
            },
            member_data_item: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                activeName: "1",
                tabPane: [{
                    name: "1",
                    label: "基本信息",
                    key: "member_base"
                },{
                    name: "2",
                    label: "第三方账号",
                    key: "account_number"
                },{
                    name: "3",
                    label: "等级/分组",
                    key: "member_group"
                },{
                    name: "4",
                    label: "会员标签",
                    key: "label_cpn"
                },{
                    name: "5",
                    label: "自定义会员资料信息",
                    key: "member_information"
                },{
                    name: "6",
                    label: "自定义字段",
                    key: "custom_cpn"
                },{
                    name: "7",
                    label: "收货地址",
                    key: "address_cpn"
                },{
                    name: "8",
                    label: "银行卡",
                    key: "blank_cpn"
                },{
                    name: "9",
                    label: "推荐人",
                    key: "agent_parent"
                },{
                    name: "10",
                    label: "直推客户",
                    key: "agent_old"
                },{
                    name: "11",
                    label: "团队客户",
                    key: "agent_team"
                },{
                    name: "12",
                    label: "收入详情",
                    key: "income_details"
                }],
                form:{
                    id: "",
                    level_id: "",
                    group_id: "",
                    validity: "",
                    realname: "",
                    nickname: "",
                    avatar: "",
                    wechat: "",
                    alipayname: "",
                    alipay: "", 
                    is_black: "",
                    agent: "",
                    invite_code: "",
                    content : "",
                    custom_value: ""
                },
                info: {},
                set: {},
                levels: [],
                groups: [],
                myform: [],
                button_list: [],
                parent_name: "",
                is_email: "",
                infoShow: false
            }
        },
        mounted(){
            document.body.style.overflow = 'hidden';
            this.getInfo();
        },
        methods: {
            // 会员详情
            getInfo(){
                this.$http.get("{!! yzWebUrl('member.member.detail') !!}"+'&id='+ this.member_data_item.uid +'&type=1').then(response => {
                    if (response.data.result) {
                        this.info=response.data.data.member;
                        this.levels = response.data.data.levels;
                        this.groups = response.data.data.groups;
                        this.button_list = response.data.data.button_list;
                        this.levels.unshift({
                            id:0,
                            level:0,
                            level_name: response.data.data.set.level_name
                        })
                        this.groups.unshift({
                            id:0,
                            group_name:'无分组'
                        })
                        this.parent_name = response.data.data.parent_name;
                        this.myform = response.data.data.myform;
                        if(!this.myform || !this.myform.length) {
                            this.tabPane.splice(4,1)
                        }
                        this.set = response.data.data.set
                        
                        this.form.id = this.info.uid
                        this.form.level_id = this.info.yz_member?this.info.yz_member.level_id:0
                        this.form.group_id = this.info.yz_member?this.info.yz_member.group_id:0
                        this.form.validity = this.info.yz_member?this.info.yz_member.validity:''
                        this.form.realname = this.info.realname
                        this.form.nickname = this.info.nickname
                        this.form.avatar = this.info.avatar
                        this.form.wechat = this.info.yz_member?this.info.yz_member.wechat:''
                        this.form.alipayname = this.info.yz_member?this.info.yz_member.alipayname:''
                        this.form.alipay = this.info.yz_member?this.info.yz_member.alipay:''
                        this.form.is_black = this.info.yz_member?String(this.info.yz_member.is_black):'0'
                        this.form.agent = String(this.info.agent)
                        this.form.invite_code = this.info.yz_member?this.info.yz_member.invite_code:''
                        this.form.content = this.info.yz_member?this.info.yz_member.content:''
                        this.form.custom_value = this.info.yz_member?this.info.yz_member.custom_value:''
                        this.is_email = response.data.data.is_email
                        this.infoShow = true;
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            handleClick(tab, event) {
                console.log(tab,33, event);
            },
            closeDrawer() {
                if(this.$parent && this.$parent.searchBtn()) {
                    this.$parent.searchBtn();
                }
                document.body.style.overflow = '';
                this.$emit("update:drawer_dialog", false);
            },
            
        },
        template: '#drawer'
    });
</script>

<style scoped></style>