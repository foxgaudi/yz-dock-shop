
<style>
    .color-text {
        color: #29BA9C;
        cursor: pointer;
    }
</style>
<template id="account_number">
    <div class="account_number">
        <el-form label-width="15%" style="margin-top: 20px;">
            <div class="block">
                <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>微信信息</b></div>
                <el-form-item label="微信号">
                    <el-input v-model="form.wechat" style="margin-left:30px;width:60%;"></el-input>
                </el-form-item>
                <el-form-item label="公众号关注状态">
                    <div style="margin-left:30px;" v-if="info.has_one_fans&&info.has_one_fans.followed==1">
                        已关注
                    </div>
                    <div style="margin-left:30px;" v-else>
                        未关注
                    </div>
                </el-form-item>
                <el-form-item label="公众号openid">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_fans?info.has_one_fans.openid:'']] 
                        <div v-if="info.has_one_fans" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;"  @click="copy('fans_openid')">复制</span>
                            <el-input v-model="info.has_one_fans.openid" ref="fans_openid" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
                <el-form-item label="小程序openid">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_mini_app?info.has_one_mini_app.openid:'']]
                        <div v-if="info.has_one_mini_app" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;" @click="copy('min_id')">复制</span>
                            <el-input v-model="info.has_one_mini_app.openid" ref="min_id" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
                <el-form-item label="Union ID" v-if="info.has_one_unique && info.has_one_unique.scope == 1">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_unique?info.has_one_unique.unionid:'']]
                        <div  v-if="info.has_one_mini_app" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;" @click="copy('union_id')">复制</span>
                            <el-input v-model="info.has_one_mini_app.openid" ref="union_id" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
            </div>
            <div class="block">
                <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>支付宝信息</b></div>
                <el-form-item label="支付宝姓名">
                    <el-input v-model="form.alipayname" style="margin-left:30px;width:60%;"></el-input>
                </el-form-item>
                <el-form-item label="支付宝账号">
                    <el-input v-model="form.alipay" style="margin-left:30px;width:60%;"></el-input>
                </el-form-item>
                <el-form-item label="生活号 openid">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_alipay?info.has_one_alipay.user_id:'']]
                        <div v-if="info.has_one_alipay" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;" @click="copy('life_openid')">复制</span>
                            <el-input v-model="info.has_one_alipay.user_id" ref="life_openid" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
            </div>
            <div class="block">
                <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>抖音信息</b></div>
                <el-form-item label="移动/网站应用 openid">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_tiktok?info.has_one_tiktok.unionid:'']]
                        <div v-if="info.has_one_tiktok" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;" @click="copy('life_openid')">复制</span>
                            <el-input v-model="info.has_one_tiktok.openid" ref="life_openid" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
                <el-form-item label="抖音Union ID" v-if="info.has_one_unique && info.has_one_unique.scope == 2">
                    <div style="margin-left:30px;display: flex;">
                        [[info.has_one_unique?info.has_one_unique.unionid:'']]
                        <div v-if="info.has_one_unique" style="display: flex;">
                            <span class="color-text" style="white-space: nowrap; margin-left: 20px;" @click="copy('life_openid')">复制</span>
                            <el-input v-model="info.has_one_unique.unionid" ref="life_openid" style="opacity: 0;"></el-input>
                        </div>
                    </div>
                </el-form-item>
            </div>
        </el-form>
        <div class="confirm-btn">
            <el-button type="primary" @click="submit">提交</el-button>
        </div>
    </div>
</template>
<script>
    Vue.component('account_number', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
            set: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            myform: {
                type: Array,
                default: () => {
                    return []
                }
            }
        },
        data() {
            return {
            }
        },
        created() {

        },
        methods: {
            submit() {
                this.$http.post("{!! yzWebUrl('member.member.update') !!}",{id:this.form.id,member:this.form,myform:this.myform}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "提交成功",type: 'success'});
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            copy(type){
                let url = this.$refs[type];
                // console.log(url,'url');
                url.select(); // 选择对象
                document.execCommand("Copy",false);
                this.$message({message:"复制成功！",type:"success"});
            },
        },
        template: '#account_number'
    });
</script>

<style scoped></style>