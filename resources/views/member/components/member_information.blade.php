
<template id="member_information">
    <div>
        <el-form label-width="15%" style="margin-top: 20px;">
            <div class="block" v-show="myform && myform.length">
                <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>自定义会员资料信息</b></div>
                <el-form-item :label="item.name" v-for="(item,index) in myform" :key="index">
                        <el-input v-model="item.value" style="margin-left:30px;width:60%;"></el-input>
                </el-form-item>
            </div>
        </el-form>
        <div class="confirm-btn">
            <el-button type="primary" @click="submit">提交</el-button>
        </div>
    </div>
</template>
<script>
    Vue.component('member_information', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
            set: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            myform: {
                type: Array,
                default: () => {
                    return []
                }
            }
        },
        data() {
            return {
            }
        },
        created() {
            console.log(this.myform,3333333333);
        },
        methods: {
            submit() {
                this.$http.post("{!! yzWebUrl('member.member.update') !!}",{id:this.form.id,member:this.form,myform:this.myform}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "提交成功",type: 'success'});
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            }
        },
        template: '#member_information'
    });
</script>

<style scoped></style>