
<style>
    .confirm-btn{
        width: 100%;
        position:absolute;
        bottom:0;
        left:0;
        line-height:63px;
        background-color: #ffffff;
        box-shadow: 0px 8px 23px 1px
        rgba(51, 51, 51, 0.3);
        background-color:#fff;
        text-align:center;
    }
</style>
<template id="custom_cpn">
    <div style="margin: 40px 0 0 0">
        <el-form label-width="80px">
            <el-form-item :label="set.custom_title" >
                <el-input v-model="form.custom_value" style="margin-left:30px;width:60%;"></el-input>
            </el-form-item>
        </el-form>
        <div class="confirm-btn">
            <el-button type="primary" @click="submit">提交</el-button>
        </div>
    </div>
</template>
<script>
    Vue.component('custom_cpn', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
            set: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            myform: {
                type: Array,
                default: () => {
                    return []
                }
            }
        },
        data() {
            return {
            }
        },
        created() {
        },
        methods: {
            submit() {
                if(this.validity_time) {
                    let times = new Date(this.validity_time).getTime();
                    let today = new Date().getTime();
                    let day = '';
                    if(times>today) {
                        day = times-today;
                        console.log(day)
                        if(day>=0) {
                            day = Math.ceil(day/1000/60/60/24)
                        }
                    }
                    this.form.validity = day;
                }
                this.$http.post("{!! yzWebUrl('member.member.update') !!}",{id:this.form.id,member:this.form,myform:this.myform}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "提交成功",type: 'success'});
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            }
        },
        template: '#custom_cpn'
    });
</script>

<style scoped></style>