<style  lang="scss" scoped> 
</style>
<template id="label_cpn">
    <div>
        <div>
            <div  style="margin: 20px 0;" v-for="item in memberTagList" >
                <div style="display: flex;align-items: center;">
                    <span style="display: block;width: 3px;height: 25px;background: #25B191;margin-right: 10px;"></span>
                    <span style="font-weight: bold;">[[item.title]]</span>
                </div>
                <div style="margin: 20px;">
                    <div v-for="citem in item.tags" style="margin: 0 20px 20px 0;color: #3EBA9E;background: white;padding: 4px 10px;display: inline-block;border: 1px solid #3EBA9E;border-radius: 5px;cursor: pointer;">
                        <span>[[citem.title]]</span>
                        <!-- <span v-if="(scope.row.has_many_tag && item.tag)&&(index < 12 || scope.row.isShow)" v-for="(item,index) in scope.row.has_many_tag">
                                手动标签
                                <el-tag v-if="item.tag.type == 1" :style="{color: item.tag.color,borderColor: item.tag.color}" effect="plain" style="margin-right:10px; margin-bottom:10px; cursor: pointer;user-select: none;" :closable="true" @close="handleCloseTag(scope.row.uid,item.tag_id)" @click="handEventTag(item.tag_id)" :key="index">[[item.tag.title]]</el-tag>
                                自动标签
                                <el-tag v-if="item.tag.type == 2" :style="{color: item.tag.color,borderColor: item.tag.color}" effect="plain" style="margin-right:10px; margin-bottom:10px; cursor: pointer;user-select: none;" @close="handleCloseTag(scope.row.uid,item.tag_id)" @click="handEventTag(item.tag_id)" :key="index">[[item.tag.title]]</el-tag>
                            </span> -->
                        <i class="el-icon-close"  v-if="citem.type == 1" @click="handleCloseTag(citem.id)" ></i>
                    </div>
                </div>
            </div>


        </div>
        <el-button class="addTag" @click.stop="openDialog()" style="margin-top: 20px;">
            <i class="el-icon-plus"></i> 添加标签
        </el-button>

        <!-- 会话框 -->
        <el-dialog title="选择标签" :visible.sync="dialogVisible" width="55%" :before-close="handleClose" v-if="tagInfo" :append-to-body="true">
            <div class="tab-pane">
                <div class="left">
                    <div class="vue-title">
                        <div class="vue-title-left"></div>
                        <div class="vue-title-content">标签组</div>
                    </div>
                    <el-menu

                        default-active="1"
                        :default-openeds="openeds"
                        class="el-menu-vertical-demo">
                        <el-submenu index="1" :class="select_group_id === '' ? 'acitve-submenu' : ''" >
                            <template slot="title">
                                <div @click="handleSelect('')">
                                    <i class="el-icon-folder-opened" :class="select_group_id === '' ? 'acitve-submenu-item' : ''"></i>
                                    <span :class="select_group_id === '' ? 'acitve-submenu-item' : ''">全部分组</span>
                                </div>
                            </template>
                            <el-menu-item-group>
                                <el-menu-item v-for="(item,index) in menu_item_list" :key="index">
                                    <div class="el-menu-name" :style="[{color:( Number(select_group_id) == item.id ? '#29ba9c':'')}]" @click="handleSelect(item.id)">
                                        <i class="el-icon-folder-opened" :style="[{color:( Number(select_group_id) == item.id ? '#29ba9c':'')}]" ></i>
                                        <span style="white-space: normal !important;line-height: normal;">[[item.title]]</span>
                                    </div>
                                </el-menu-item>
                            </el-menu-item-group>
                        </el-submenu>
                    </el-menu>
                </div>
                <div class="right">
                    <div class="right-top">
                        <div class="vue-title">
                            <div class="vue-title-left"></div>
                            <div class="vue-title-content">标签列表</div>
                        </div>
                        <div class="search-pane">
                            <el-input placeholder="标签名称" style="width: 40%;" v-model="keyword"></el-input>
                            <el-button type="primary" @click="search1(1)">搜索</el-button>
                        </div>
                    </div>
                    <div class="single-table">
                        <el-table ref="singleTable" :data="tagData">
                            <el-table-column label="ID" prop="id"></el-table-column>
                            <el-table-column label="标签名称" prop="title"></el-table-column>
                            <el-table-column label="操作">
                                <template slot-scope="scope">
                                    <el-button size="mini" @click="makeMemberTags(scope.row)">选择</el-button>
                                </template>
                            </el-table-column>
                        </el-table>
                    </div>
                </div>
            </div>
            <div >
                <el-row>
                    <el-col align="right">
                        <el-pagination layout="prev, pager, next,jumper" @current-change="search1" :total="tag_total" :page-size="tag_per_page" :current-page="tag_current_page" background></el-pagination>
                    </el-col>
                </el-row>
                <div style="text-align: center;margin: 20px;">
                    <el-button @click="dialogVisible = false">取 消</el-button>
                </div>
            </div>
        </el-dialog>
    </div>
</template>
<script>
    Vue.component('label_cpn', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
        },
        data() {
            return {
                memberTagList: [],
                dialogVisible: false,
                keyword: "",
                select_group_id: "",
                //当前打开的 sub-menu 的 index 的数组
                openeds:["1"],
                menu_item_list:[],
                tag_total: 1,
                tag_current_page: 1,
                tag_per_page: 1,
                tagInfo: false,
                tagData: []
            }
        },
        mounted(){
            this.getTagsList();
        },
        methods: {
               // 获取会员的标签数据
                getTagsList() {
                    this.tagInfo = false;
                    this.$http.post("{!!yzWebFullUrl('member.member.get-member-tags')!!}", {
                        member_id: this.uid
                    }).then(({data,result,msg}) => {
                        if(data.result) {
                            this.memberTagList = data.data;
                            console.log(data,4567);
                        }else {
                            console.log(data.msg);
                        }
                        this.tagInfo = true;
                    })
                },
                //点击请求当前点击的标签的列表
                openDialog() {
                    this.dialogVisible = true
                    //请求标签组列表
                    this.getTagsGroupList();
                },
                // 获取标签分组数据
                getTagsGroupList() {
                    this.tagInfo = false;
                    this.$http.get("{!!yzWebFullUrl('plugin.member-tags.Backend.controllers.tag.getGroupList')!!}", {}).then(({data,result,msg}) => {
                        if(data.result) {
                            this.menu_item_list = data.data;
                        }else {
                            console.log(data.msg);
                        }
                        this.tagInfo = true;
                    })
                },
                handleClose() {
                    this.dialogVisible = false
                },
                // 选中的分组
                handleSelect(id){
                    this.select_group_id = id;
                    this.keyword = "";
                    this.handleTagsList(1);
                },
                search1(page) {
                    this.handleTagsList(page);
                },
                //.请求标签的列表数据
                handleTagsList(page) {
                    let group_id = {};
                    if(this.select_group_id) {
                        group_id = { group_id : this.select_group_id };
                    }
                    this.$http.post("{!!yzWebFullUrl('plugin.member-tags.Backend.controllers.tag.get-tags-list')!!}", {
                        search:{
                            ...group_id,
                            title:this.keyword,
                        },
                        page
                    }).then(({data,result,msg}) => {
                        if(data.result) {
                            this.tagData = data.data.data;
                            this.tag_current_page = data.data.current_page;
                            this.tag_total = data.data.total;
                            this.tag_per_page = data.data.per_page;
                        }else {
                            console.log(data.msg);
                        }
                    })
                },
                makeMemberTags(row) {
                    this.$http.post("{!!yzWebFullUrl('plugin.member-tags.Backend.controllers.tag.makeMemberTags')!!}", {
                        tag_id: row.id,
                        member_id: this.uid
                    }).then(res => {
                        if (res.data.result == 1) {
                            this.$message.success("标签添加" + res.data.msg);
                            //关闭回话框
                            this.dialogVisible = false;
                            this.getTagsList();
                        } else {
                            this.$message.error("标签添加" + res.data.msg + "已存在!" + "请勿重新添加");
                        }
                    })
                },
                //3.删除会员标签校验
                handleCloseTag(tag_id) {
                    this.$confirm('是否需要删除?', '提示').then(() => {
                        this.$http.post("{!!yzWebFullUrl('plugin.member-tags.Backend.controllers.tag.delMemberTags')!!}", {
                            member_id: this.uid,
                            tag_id,
                            member_tag_id:'-1'
                        }).then(res => {
                            if (res.data.result == 1) {
                                this.$message.success("删除" + res.data.msg);
                                this.getTagsList();
                            } else {
                                this.$message.error("删除" + res.data.msg);
                            }
                        })
                    }).catch(() => {});
                },
        },
        template: '#label_cpn'
    });
</script>

<style scoped></style>