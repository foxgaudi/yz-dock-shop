<template id="member_group">
    <div class="member_group">
        <el-form label-width="15%" style="margin-top: 20px;">
            <div class="block">
                <div class="title" style="margin-bottom: 15px"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>会员等级/分组信息</b></div>
                <el-form-item label="会员等级">
                    <template>
                        <el-select v-model="form.level_id" placeholder="请选择" style="margin-left:30px;">
                            <el-option
                                    v-for="(item,i) in levels"
                                    :label="item.level_name"
                                    :value="item.id" 
                                    :key="i">
                                    
                            </el-option>
                        </el-select>
                    </template>
                </el-form-item>
                <el-form-item label="会员等级期限">
                    <el-date-picker
                            v-model="validity_time"
                            type="date"
                            @change="changeDay"
                            placeholder="选择日期"
                            value-format="yyyy-MM-dd 23:59:59"
                            style="margin-left:30px;"
                    >
                    </el-date-picker>
                    <el-popover placement="bottom" width="200" trigger="click" v-model="changeDayNumShow">
                        <div style="text-align:center;">
                            <el-input v-model="validity_day" size="small" style="width:180px;" @input="changeDayNum"></el-input>
                        </div>
                        <i slot="reference" class="iconfont icon-ht_operation_edit" title="输入天数" style="cursor: pointer;"></i>
                    </el-popover>
                    <el-input v-model="validity_day" style="margin-left:30px;width:30%;" disabled>
                        <template slot="append">天</template>
                    </el-input>

                </el-form-item>
                <el-form-item label="会员分组">
                    <template>
                        <el-select v-model="form.group_id" placeholder="请选择" style="margin-left:30px;">
                            <el-option
                                    v-for="(item,i) in groups"
                                    :label="item.group_name"
                                    :value="item.id"
                                    :key="i">
                            </el-option>
                        </el-select>
                    </template>
                </el-form-item>
            </div>
        </el-form>
        <div class="confirm-btn">
            <el-button type="primary" @click="submit">提交</el-button>
        </div>
    </div>
</template>
<script>
    Vue.component('member_group', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
            set: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            levels: {
                type: Array,
                default: ()=> {
                    return []
                }
            },
            groups: {
                type: Array,
                default: ()=> {
                    return []
                }
            },
            myform: {
                type: Array,
                default: () => {
                    return []
                }
            }
        },
        data() {
            return {
                validity_time:'',//有限期
                validity_day:'',
                changeDayNumShow:false,
            }
        },
        created() {
            console.log(this.levels,'dddd');
            this.validity_day = this.form.validity;
            if(this.form.validity==0) {
                this.validity_time = this.timeStyle(new Date().getTime()-24*60*60*1000)
            }
            else if(this.form.validity==1) {
                this.validity_time = this.timeStyle(new Date().getTime())
            }
            else {
                this.validity_time = this.timeStyle(new Date().getTime()+((Number(this.form.validity)-1)*24*60*60*1000))
            }
        },
        methods: {
            timeStyle(time) {
                console.log(time)
                let time1 = new Date(time);
                let y = time1.getFullYear();
                let m = time1.getMonth()+1;
                let d = time1.getDate();
                let h = time1.getHours();
                let mm = time1.getMinutes();
                let s = time1.getSeconds();
                // return y+'-'+this.add0(m)+'-'+this.add0(d)+' '+this.add0(h)+':'+this.add0(mm)+':'+this.add0(s);
                return y+'-'+this.add0(m)+'-'+this.add0(d)+' '+'23:59:59';
            },
            add0(m) {
                return m<10?'0'+m:m
            },
            changeDay(val) {
                console.log(val)
                let times = new Date(this.validity_time).getTime();
                let today = new Date().getTime();
                let day = '';
                if(times>today) {
                    day = times-today;
                    console.log(day)
                    if(day>=0) {
                        day = Math.ceil(day/1000/60/60/24)
                    }
                }
            },
            changeDayNum() {
                if(!(/(^[0-9]\d*$)/.test(this.validity_day))) {
                    this.$message.error('请输入正确数字')
                    return
                }
                this.validity_time = this.timeStyle(new Date().getTime()+((Number(this.validity_day)-1)*24*60*60*1000))
            },
            submit() {
                if(this.validity_time) {
                    let times = new Date(this.validity_time).getTime();
                    let today = new Date().getTime();
                    let day = '';
                    if(times>today) {
                        day = times-today;
                        console.log(day)
                        if(day>=0) {
                            day = Math.ceil(day/1000/60/60/24)
                        }
                    }
                    this.form.validity = day;
                }
                this.$http.post("{!! yzWebUrl('member.member.update') !!}",{id:this.form.id,member:this.form,myform:this.myform}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "提交成功",type: 'success'});
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            }
        },
        template: '#member_group'
    });
</script>

<style scoped></style>