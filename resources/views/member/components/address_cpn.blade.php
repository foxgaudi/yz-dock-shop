
<!-- <style>
    .vue-title {
        display: flex;
        margin: 5px 0;
        line-height: 32px;
        font-size: 16px;
        color: #333;
        font-weight: 600;
    }

    .vue-title-left {
        width: 4px;
        height: 18px;
        margin-top: 6px;
        background: #29ba9c;
        display: inline-block;
        margin-right: 10px;
    }

    .vue-title-content {
        font-size: 14px;
        flex: 1;
    }

    .member-info {
        display: flex;
        margin: 38px;
        justify-content: space-between;
    }

    .search-top {
        display: flex;
        height: 200px;
    }

    .top-item1 {
        flex: 1;
    }

    .top-item2 {
        width: 540px;
    }

    .audit,
    .is_follow {
        width: 60px;
        height: 22px;
        text-align: center;
        line-height: 22px;
        font-size: 14px;
        color: #fff;
        margin: 0 auto;
        border-radius: 4px;
        background-color: #13c7a7;
    }

    .un_follow {
        width: 70px;
        height: 22px;
        text-align: center;
        line-height: 22px;
        font-size: 14px;
        color: #fff;
        margin: 0 auto;
        border-radius: 4px;
        background-color: #ffb025;
    }

    .name-over {
        line-height: 37px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .el-table_1_column_2 .cell {
        /* display: flex; */
    }

    /* 分页 */
    .pagination-right {
        text-align: center;
        margin: 50px auto;
    }

    /* 列表 */
    p {
        font-size: 12px;
        margin-top: 5px;
    }

    .cell {
        /* border:1px solid red; */
        text-align: center;
    }
</style> -->
<template id="address_cpn">
    <div >
            <div class="total-floo" style="margin: 0">
                <el-table :data="tableData" style="width: 100%;">
                    <el-table-column label="ID">
                        <template slot-scope="scope">
                            <p>[[ scope.row.id ]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="收货人">
                        <template slot-scope="scope">
                            <p>[[ scope.row.username ]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="手机号">
                        <template slot-scope="scope">
                            <p>[[ scope.row.mobile ]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="省份">
                        <template slot-scope="scope">
                            <p>[[ scope.row.province ]]</p>
                        </template>
                    </el-table-column>

                    <el-table-column label="城市">
                        <template slot-scope="scope">
                            <p>[[ scope.row.city ]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="区域">
                        <template slot-scope="scope">
                            <p>[[ scope.row.district]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="街道">
                        <template slot-scope="scope">
                            <p v-if="is_street==1">[[ scope.row.street]]</p>
                        </template>
                    </el-table-column>
                    <el-table-column label="详细地址">
                        <template slot-scope="scope">
                            <p>[[ scope.row.address ]]</p>
                        </template>
                    </el-table-column>
                </el-table>
            </div>
            <!-- <div class="fixed total-floo">
                <div class="fixed_box">
                    <el-form>
                        <el-form-item>
                            <el-button @click="returnEvent">返回</el-button>
                        </el-form-item>
                    </el-form>
                </div>
            </div> -->
    </div>
</template>
<script>
    Vue.component('address_cpn', {
        delimiters: ['[[', ']]'],
        props: {
            uid: {
                type: null,
                default: ""
            },
        },
        data() {
            return {
                tableData: [],
                currentPage: 1,
                pagesize: 6,
                total: 1,
                is_street: ""
            }
        },
        created() {
            this.postVipInfoList(this.uid);
        },
        methods: {
            //回退
            hisGo(i) {
                //  console.log(i);
                history.go(i)
            },
            //返回
            returnEvent() {
                history.go(-1);
            },
            //会员信息列表
            postVipInfoList(id) {
                this.$http.post("{!!yzWebFullUrl('member.member-address.show')!!}", {
                    member_id: id
                }).then(res => {
                    let {
                        member,
                        address,
                        is_street
                    } = res.body.data;
                    this.tableData = address
                    //判断街道
                    // console.log(is_street,89622);
                    this.is_street = is_street;
                })
            },
        },
        template: '#address_cpn'
    });
</script>

<style scoped></style>