
<style>

</style>
<template id="top_detail_cpn">
    <div class="top-box" style="background: white;padding: 15px 25px;display: flex;">
        <div class="top-left" style="flex: 1;">
            <div style="display: flex;align-items: center;">
                <img style="width: 50px;height: 50px;border-radius: 50%;margin-right: 10px;" :src="member.avatar_image" alt="">
                <div >
                    <div>[[member.uid]] [[member.realname]]</div>
                    <div>[[member.username]] [[member.mobile]]</div>
                </div>
            </div>
            <div style="height:40px;line-height:40px;margin: 5px 0;">
                <i class="iconfont icon_size" :class="openid(0,info.has_one_fans)" :title="title(info.has_one_fans)"></i>
                <i class="iconfont icon_size" :class="openid(1,info.has_one_mini_app)" title="小程序"></i>
                <i class="iconfont icon_size" :class="openid(2,info.has_one_wechat)" title="APP"></i>
                <i class="iconfont icon_size" :class="openid(3,info.has_one_unique)" title="微信开放平台"></i>
                <i class="iconfont icon_size" :class="openid(4,info.has_one_alipay)" title="支付宝"></i>
                <i class="iconfont icon_size" :class="openid(5,info.has_one_douyin)" title="抖音"></i>
                <i class="iconfont icon_size" :class="openid(6,info.has_one_customers)" title="企业微信"></i>
                <i class="iconfont icon_size" :class="openid(7,info.has_one_aggregation_cps_member)" title="聚合cps App"></i>
                <i class="iconfont icon_size" :class="openid(8,info.has_one_tiktok)" title="抖音"></i>
            </div>
            <div style="color:rgb(6%, 6%, 6%)">注册时间：[[member.createtime]]</div>
        </div>
        <div class="top-center" style="flex: 1.5; display: flex;align-items: center;justify-content: space-between;">
            <div style="width: 2px;background: rgb(73%, 73%, 73%);height: 40px;"></div>
            <div style="display: flex;justify-content: space-between;height: 92%;flex: 0.7;">
                <div style=" display: flex;flex-direction: column;justify-content: space-around;">
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.credit1]]</div>
                        <div>积分</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">￥[[info.has_one_order?info.has_one_order.sum:0]]</div>
                        <div style="white-space: nowrap;">订单总额</div>
                    </div>
                </div>
                <div style=" display: flex;flex-direction: column;justify-content: space-around;margin: 0 15px;">
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;margin: 0 20px;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.credit2]]</div>
                        <div>余额</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;margin: 0 20px;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.has_one_order?info.has_one_order.total:0]]</div>
                        <div style="white-space: nowrap;">订单总量（单）</div>
                    </div>
                </div>
                <div style=" display: flex;flex-direction: column;justify-content: space-around;">
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.coupon_total]]</div>
                        <div>优惠券</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.income_total]]</div>
                        <div style="white-space: nowrap;">收入总额</div>
                    </div>
                </div>
                <!-- <div style="display: flex;display: flex;justify-content: space-between;padding: 0 10px;">
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.credit1]]</div>
                        <div>积分</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;margin: 0 20px;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.credit2]]</div>
                        <div>余额</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.coupon_total]]</div>
                        <div>优惠券</div>
                    </div>
                </div>
                <div style="display: flex;justify-content: space-between;">
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">￥[[info.has_one_order?info.has_one_order.sum:0]]</div>
                        <div style="white-space: nowrap;">订单总额</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;margin: 0 20px;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.has_one_order?info.has_one_order.total:0]]</div>
                        <div style="white-space: nowrap;">订单总量（单）</div>
                    </div>
                    <div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                        <div style="color: rgb(15%, 69%, 57%);  font-size: 20px;">[[info.income_total]]</div>
                        <div style="white-space: nowrap;">收入总额</div>
                    </div>
                </div> -->
            </div>
            <div style="width: 2px;background: rgb(73%, 73%, 73%);height: 40px;"></div>
        </div>
        <div style="flex: 1.8;display: flex;flex-direction: column;justify-content: space-around;padding: 0 50px;">
            <div style="display: flex;flex-wrap: wrap;">
                <span v-for="(item,index) in button_list" :key="index" style="cursor: pointer;margin: 5px 20px 5px 0;color: #29BA9C;font-weight: 500;display: block;" @click="clickEve(item)">[[item.name]]</span>
            </div>
        </div>

        <el-dialog :title="dialog_name" center :show-close="false" :visible.sync="fileDialogVisible" width="28%" :append-to-body="true">
                    <div class="context">
                        <div class="img-box">
                        <div class="msg-line">
                            <el-image :src="line.qr_code"></el-image>
                            <a class="upload-line" :href="line.qr_code" download="qro.png">下载二维码图片</a>
                        </div>
                        <div class="msg-line">
                            <el-image :src="line.micro_qr_code"></el-image>
                            <div style="display: flex;align-items: center;" v-if="line.micro_qr_code">
                                <a class="upload-line" :href="line.micro_qr_code" download="qro.png" >下载小程序码图片</a>
                                <a class="upload-line el-icon-refresh"  @click="getMiniAppPageUri" style="margin-left: 10px;"></a>
                            </div>
                            <a class="upload-line"  @click="getMiniAppPageUri" v-if="!line.micro_qr_code">生成小程序二维码</a>
                        </div>
                        </div>
                        <div class="space-input">
                        <span>h5页面链接</span>
                        <el-input v-model="line.link"></el-input>
                        <el-button @click="copyLine(line.link)">复制</el-button>
                        </div>
                        <div class="space-input">
                        <span>小程序页面链接</span>
                        <el-input v-model="line.micro_link"></el-input>
                        <el-button @click="copyLine(line.micro_link)">复制</el-button>
                        </div>
                    </div>
                    <div class="bottom">
                        <el-button @click="fileDialogVisible = false">取消</el-button>
                    </div>
                </el-dialog>

                <el-dialog title="充值积分" center :show-close="false" :visible.sync="pointRechargeDialogVisible" width="40%" :append-to-body="true">
                    <el-form ref="form" >
                        <div class="vue-main">
                            <div class="vue-main-form">
                                <el-form-item label="粉丝"  label-width="80px">
                                    <div style="display: flex;flex-direction: column;justify-content: center">
                                        <div style="width: 100px; height: 100px">
                                            <el-image
                                                    style="width: 100px; height: 100px"
                                                    :src="memberInfo.avatar">
                                            </el-image>
                                        </div>
                                        <div style="width: 100px;text-align: center">
                                            <span class="demonstration">[[memberInfo.nickname]]</span>
                                        </div>
                                    </div>
                                </el-form-item>
                                <el-form-item label="会员信息">
                                    <div style="display: flex;flex-direction: column;justify-content: center">
                                        <div>
                                            <span class="demonstration">姓名：[[memberInfo.realname ? memberInfo.realname : memberInfo.nickname]]</span>
                                        </div>
                                        <div>
                                            <span class="demonstration">手机号: [[memberInfo.mobile]]</span>
                                        </div>
                                    </div>
                                </el-form-item>
                                <el-form-item label="当前积分">
                                    <span class="demonstration">[[memberInfo.credit1]]</span>
                                </el-form-item>
                                <el-form-item label="充值积分">
                                    <el-input style="width: 450px" v-model="form_recharge.point" placeholder="请输入内容"></el-input>
                                </el-form-item>
                                <el-form-item label="备注信息">
                                    <el-input style="width: 600px" type="textarea" rows="8" v-model="form_recharge.remark" placeholder=""></el-input>
                                </el-form-item>
                                <el-form-item label=" " label-width="70px">
                                    <el-button type="primary" @click="onSubmit">提交</el-button>
                                </el-form-item>
                            </div>
                        </div>
                    </el-form>
                </el-dialog>

                <el-dialog title="充值余额" center :show-close="false" :visible.sync="balanceRechargeDialogVisible" width="40%" :append-to-body="true">
                    <div class="con">
                        <div class="setting">
                            <el-form ref="form" label-width="15%" @submit.native.prevent>
                                <div class="block">
                                    <el-form-item label="粉丝">
                                        <div class="image" style="border:solid 1px #ccc;width:100px;height:100px;margin-left:30px;display: inline-block;">
                                            <img :src="balance_memberInfo.avatar" style="width: 100%;height:100%;">
                                        </div>
                                        <div style="display: inline-block;vertical-align: text-top;margin-left:10px;">[[balance_memberInfo.nickname]]</div>
                                    </el-form-item>
                                    <el-form-item label="会员信息">
                                        <div style="margin-left:30px;">
                                            姓名:[[balance_memberInfo.realname?balance_memberInfo.realname:balance_memberInfo.nicknamename]] / 手机号:[[balance_memberInfo.mobile]]
                                        </div>
                                    </el-form-item>
                                    <el-form-item :label="balance_rechargeMenu.old_value">
                                        <div style="margin-left:30px;">
                                            [[balance_memberInfo.credit2]]
                                        </div>
                                    </el-form-item>
                                    <el-form-item :label="balance_rechargeMenu.charge_value" required>
                                        <el-input v-model="form_balance.num" style="width:70%;"></el-input><span style="margin-left: 10px;">元</span>
                                    </el-form-item>

                                    <el-form-item label="备注信息"  >
                                        <el-input type="textarea" rows="5" v-model="form_balance.remark" placeholder="" style="width:70%;" maxlength="50" show-word-limit></el-input>
                                    </el-form-item>

                                    <el-form-item label="充值说明"  v-if="charge_check_swich" required>
                                        <el-input type="textarea" rows="5" v-model="form_balance.explain" placeholder="" style="width:70%;"></el-input>
                                    </el-form-item>

                                    <el-form-item label="附件"  v-if="charge_check_swich">
                                        [[form_balance.enclosure_src]]
                                        <el-upload
                                                class="upload-demo"
                                                action="{!! yzWebFullUrl('finance.balance-recharge-check.upload-file') !!}"
                                                name="file"
                                                :show-file-list="false"
                                                :on-success="uploadSuccess"
                                                :on-error="uploadFail"
                                                {{--:http-request="upload"--}}
                                        >
                                            <el-button type="primary">文件上传</el-button>
                                        </el-upload>
                                    </el-form-item>
                                </div>
                            </el-form>
                        </div>
                        <div style="text-align: center;">
                            <el-button type="primary" @click="submit">提交</el-button>
                        </div>
                    </div>
                </el-dialog>

                <el-dialog title="支付密码" center :show-close="false" :visible.sync="payDialogVisible" width="40%" :append-to-body="true">
                    <el-form label-width="15%">
                        <el-form-item label="会员">
                            <el-row>
                                <el-col :span="2">
                                    <el-avatar :src="member.avatar_image" size="large"></el-avatar>
                                </el-col>
                                <el-col :span="20">
                                    [[ member.nickname ]]</el-col>
                            </el-row>

                        </el-form-item>
                        <el-form-item label="密码">
                            <el-input type="password" v-model="newPassword" placeholder="请输入密码" show-password></el-input>
                        </el-form-item>
                        <el-form-item label="确认密码">
                            <el-input type="password" v-model="newPasswordConfirm" placeholder="请输入密码" show-password></el-input>
                        </el-form-item>
                        <el-form-item style="text-align: center;">
                            <el-button type="primary" @click="confirmChangePayPassword" :loading="saving">确认</el-button>
                        </el-form-item>
                    </el-form>
                </el-dialog>

                <el-dialog title="登录密码" center :show-close="false" :visible.sync="loginDialogVisible" width="40%" :append-to-body="true">
                    <el-form label-width="15%">
                        <el-form-item label="会员">
                            <el-row>
                                <el-col :span="2">
                                    <el-avatar :src="member.avatar_image" size="large"></el-avatar>
                                </el-col>
                                <el-col :span="20">
                                    [[ member.nickname ]]</el-col>
                            </el-row>

                        </el-form-item>
                        <el-form-item label="密码">
                            <el-input type="password" v-model="login_newPassword" placeholder="请输入密码" show-password></el-input>
                        </el-form-item>
                        <el-form-item label="确认密码">
                            <el-input type="password" v-model="login_newPasswordConfirm" placeholder="请输入密码" show-password></el-input>
                        </el-form-item>
                        <el-form-item style="text-align: center;">
                            <el-button type="primary" @click="loginConfirmChangePayPassword" >保存</el-button>
                        </el-form-item>
                    </el-form>
                </el-dialog>
    </div>
</template>
<script>
    Vue.component('top_detail_cpn', {
        delimiters: ['[[', ']]'],
        props: {
            member_data_item: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            button_list: {
                type: Array,
                default: () => {
                    return []
                }
            },
        },
        data() {
            return {
                fileList1: [{
                    name: "支付密码"
                },{
                    name: "充值积分"
                },{
                    name: "充值余额"
                },{
                    name: "优惠券明细"
                },{
                    name: "会员订单"
                },{
                    name: "余额明细"
                },{
                    name: "积分明细"
                },{
                    name: "收入明细"
                },{
                    name: "体现记录"
                },{
                    name: "登录密码"
                },{
                    name: "海报分享记录"
                },{
                    name: "会员微贴"
                },{
                    name: "会员视频"
                },{
                    name: "会员直播"
                }],
                fileList2: [{
                    name: "会员名片"
                },{
                    name: "微社区主页"
                },{
                    name: "短视频主页"
                },{
                    name: "主播主页"
                },{
                    name: "微店首页"
                },{
                    name: "门店首页"
                }],
                line: {
                    qr_code: "",
                    micro_qr_code: "",
                    link: "",
                    micro_link: ""
                },
                plugin_name: "",
                dialog_name: "",
                fileDialogVisible: false,
                pointRechargeDialogVisible: false,
                rechargeMenu: {},
                memberInfo: {},
                form_recharge: {
                    point:'',
                    remark:''
                },

                // 余额
                balanceRechargeDialogVisible: false,
                balance_memberInfo:{},
                balance_rechargeMenu:{},
                charge_check_swich: "",
                form_balance:{
                    num:'',
                    remark:"",
                    explain:'',
                    enclosure:"",
                    enclosure_src:"",
                    member_id: ""
                },

                // 支付密码
                payDialogVisible: false,
                member: {},
                newPassword: null,
                newPasswordConfirm: null,
                saving: false,

                // 登录密码
                login_newPassword: "",
                login_newPasswordConfirm: "",
                loginDialogVisible: false
            }
        },
        created() {
            this.member = this.member_data_item;
            this.form_balance.member_id = this.member_data_item.uid;
        },
        methods: {
            // 获取积分
            getPointRecharge() {
                this.$http.post("{!! yzWebUrl('point.recharge.index') !!}",{type: 1 , id: this.member.uid }).then(response => {
                    if (response.data.result) {
                        console.log(response.data.result);
                        this.rechargeMenu = response.data.data.rechargeMenu;
                        this.memberInfo = response.data.data.memberInfo ?  response.data.data.memberInfo : {};
                        this.pointRechargeDialogVisible = true
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
             // 获取余额充值
            getBalanceRecharge() {
                this.$http.post("{!! yzWebUrl('balance.recharge.index') !!}",{type: 1 , member_id: this.member.uid }).then(response => {
                    if (response.data.result) {
                        console.log(response.data.result);
                        this.balance_rechargeMenu = response.data.data.rechargeMenu;
                        this.balance_memberInfo = response.data.data.memberInfo ? response.data.data.memberInfo : {};
                        this.charge_check_swich =  response.data.data.charge_check_swich;
                        this.balanceRechargeDialogVisible = true;
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            // 复制链接
            copyLine(val) {
                const cInput = document.createElement("input");
                cInput.value = val;
                document.body.appendChild(cInput);
                cInput.select(); // 选取文本框内容
                document.execCommand("copy");
                this.$message({
                type: "success",
                message: "复制成功",
                });
                document.body.removeChild(cInput);
            },
            // 积分充值
            pointRecharge() {
                this.getPointRecharge();
            },
            onSubmit() {
                let loading = this.$loading({
                    target: document.querySelector(".content"),
                    background: 'rgba(0, 0, 0, 0)'
                });
                let form = this.form_recharge
                form.id = this.form.id
                this.$http.post('{!! yzWebFullUrl('point.recharge.index') !!}', form).then( (response)=> {
                    if (response.data.result) {
                        this.$message({
                            message: response.data.msg,
                            type: 'success'
                        });
                        location.reload()
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }

                    loading.close();
                }, function (response) {
                    this.$message({
                        message: response.data.msg,
                        type: 'error'
                    });
                    loading.close();
                });
            },

            // 余额充值
            balanceRecharge() {
                this.getBalanceRecharge();
            },
            submit() {
                console.log(this.form);
                let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0)'});
                this.$http.post('{!! yzWebFullUrl('balance.recharge.index') !!}',this.form_balance).then( (response) => {
                    if (response.data.result) {
                        this.$message({message: response.data.msg,type: 'success'});
                        location.reload();
                    }else {
                        this.$message({message: response.data.msg,type: 'error'});
                    }
                    loading.close();
                },function (response) {
                    this.$message({message: response.data.msg,type: 'error'});
                })
            },
            payClick() {
                this.newPassword = "";
                this.newPasswordConfirm = "";
                this.payDialogVisible = true;
            },
            // 支付密码
            confirmChangePayPassword() {
                if (!this.newPassword || !this.newPasswordConfirm) {
                    this.$message.error("请输入密码");
                    return;
                }
                if (this.newPassword != this.newPasswordConfirm) {
                    this.$message.error("确认密码不一致");
                    return;
                }

                this.saving = true;
                this.$http.post("{!! yzWebFullUrl('password.update.index') !!}", {
                    member_id: this.member.uid,
                    password: this.newPassword,
                    confirmed: this.newPasswordConfirm
                }).then(({
                    data: {
                        result,
                        msg
                    }
                }) => {
                    if (result == 0) {
                        this.$message.error(msg);
                        this.saving = false;
                        return;
                    }
                    this.$message.success("修改成功");
                    this.saving = false;
                }).catch(({
                    data: {
                        result,
                        msg
                    }
                }) => {
                    this.$message.success(msg);
                    this.saving = false;
                })
            },
            // 登录密码
            loginConfirmChangePayPassword() {
                if (!this.login_newPassword || !this.login_newPasswordConfirm) {
                    this.$message.error("请输入密码");
                    return;
                }
                if (this.login_newPassword != this.login_newPasswordConfirm) {
                    this.$message.error("确认密码不一致");
                    return;
                }

                this.$http.post("{!! yzWebFullUrl('member.member.change-password') !!}", {
                    member_id: this.member.uid,
                    password: this.login_newPassword,
                    re_password: this.login_newPasswordConfirm
                }).then(({
                    data: {
                        result,
                        msg
                    }
                }) => {
                    if (result == 0) {
                        this.$message.error(msg);
                        return;
                    }
                    this.$message.success(msg);
                }).catch(({
                    data: {
                        result,
                        msg
                    }
                }) => {
                    this.$message.success(msg);
                })
            },
            clickEve(item) {
                if(item.type == 1) {
                    this.payClick();
                }else if(item.type == 2) {
                    this.pointRecharge() 
                }else if(item.type == 3) {
                    this.balanceRecharge()
                }else if(item.type == 4) {
                    this.login_newPassword = ""
                    this.login_newPasswordConfirm = "";
                    this.loginDialogVisible = true;
                }else if(item.type == 5) {
                    this.checkGo(item.go_url)
                }else if(item.type == 6) {
                    this.dialog_name = item.name;
                    this.line.link = item.h5_url;
                    this.line.micro_link = item.min_url;
                    this.line.qr_code = item.h5_qr_url;
                    this.line.micro_qr_code = item.min_qr_url;
                    this.plugin_name = item.plugin_name;
                    this.fileDialogVisible  = true;
                }
            },
            checkGo(url) {
                let link = url + '&uid=' + this.member.uid;
                window.open(link, '_blank');
            },
            getMiniAppPageUri(plugin_name) {
                this.$http.post('{!! yzWebFullUrl('member.member.get-mini-app-page-uri') !!}',{
                    plugin_name: this.plugin_name,
                    member_id: this.member.uid
                }).then( (response) => {
                    if (response.data.result) {
                        this.line.micro_qr_code = response.data.data.url;
                        console.log(response.data,13333);
                    }else {
                        this.$message({message: response.data.msg,type: 'error'});
                    }
                },function (response) {
                    this.$message({message: response.data.msg,type: 'error'});
                })
            },
        },
        computed: {
            //图标
            openid() {
                return (id, val) => {
                    //关注
                    if (id == 0) {
                        if (val && val !== null && val.followed == 1) {
                            return "icon-all_wechat_public wechat_public_already"
                        } else if (val && val !== null && val.followed == 0) {
                            return "icon-all_wechat_public wechat_public_not"
                        } else {
                            return ""
                        }
                    }
                    //小程序
                    if (id == 1) {
                        return val && val !== null ? "icon-all_smallprogram smallprogram" : ""
                    }
                    //APP
                    if (id == 2) {
                        return val && val !== null ? "icon-all_app all_app" : ""
                    }
                    /* 微信开放平台 */
                    if (id == 3) {
                        return val && val !== null && val.scope == 1 ? "icon-all_wechat all_wechat" : "";
                    }
                    /* 支付宝 */
                    if (id == 4) {
                        return val && val !== null ? "icon-all_alipay all_alipay" : ""
                    }
                    /* 抖音 */
                    if (id == 5) {
                        return val && val !== null ? "icon-all_trill all_tril" : ""
                    }
                    /* 企业微信 */
                    if (id == 6) {
                        return val && val !== null ? "icon-qiyeweixin01 qiyeweixin01" : ""
                    }
                    // 聚合cps App
                    if(id == 7) {
                        return val && val !== null ? "icon-all_appshouji all_appshouji" : ""
                    }
                    // 抖音
                    if(id == 8) {
                        return val && val !== null ? "icon-all_trill all_tril" : ""
                    }
                }
            },
            title() {
                return (title) => {
                    if (title && title.followed == 1) {
                        return "已关注"
                    } else if (title && title.followed == 0) {
                        return "未关注"
                    } else {
                        return ""
                    }
                }
            },
        },
        template: '#top_detail_cpn'
    });
</script>

<style scoped></style>