<link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
<style  lang="scss" scoped> 
    .el-dialog__header {
        padding: 25px !important;
    }
</style>
<template id="member_base">
    <div id='re_content' style="margin-top: 20px;">
        <div class="con" >
            <el-form ref="form" :model="form" label-width="15%">
                <div class="block">
                    <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>基本信息</b></div>
                        <el-form-item label="粉丝">
                                <div class="upload-box" @click="openUpload('avatar','1','one')" v-if="!info.avatar" style="padding-left: 32px">
                                    <i class="el-icon-plus" style="font-size:32px"></i>
                                </div>
                                <div @click="openUpload('avatar','1','one')" class="upload-boxed" v-if="info.avatar" style="height:150px;padding-left: 32px">
                                    <img :src="info.avatar" alt="" style="width:150px;height:150px;border-radius: 5px;cursor: pointer;">
                                    <div class="upload-boxed-text">点击重新上传</div>
                                </div>
                        </el-form-item>
                        <upload-multimedia-img :append-to-body-type="true" :upload-show="uploadShow" :name="chooseImgName" @replace="changeProp" :type="type" @sure="sureImg"></upload-multimedia-img>
                        <el-form-item label="会员昵称">
                                <el-input v-model="form.nickname" style="margin-left:30px;width:60%;"></el-input>
                            <div style="margin-left:30px;">若开启微信授权登录，修改后会被微信的头像昵称替换</div>
                        </el-form-item>
                    <el-form-item label="会员ID">
                        <div style="margin-left:30px;">
                            [[info.uid]]
                        </div>
                    </el-form-item>
                    <el-form-item label="注册时间">
                        <div style="margin-left:30px;">
                            [[info.createtime]]
                        </div>
                    </el-form-item>

                    <el-form-item label="真实姓名">
                        <el-input v-model="form.realname" style="margin-left:30px;width:60%;"></el-input>
                    </el-form-item>
                    <el-form-item label="绑定手机">
                        <div style="margin-left:30px;display:inline-block">
                            [[info.mobile]]
                        </div>
                        <el-button @click="mobileShow">修改</el-button>
                        <el-button @click="getRecord">修改记录</el-button>
                    </el-form-item>
                    <el-form-item label="性别">
                        <div style="margin-left:30px;" v-if="info.gender=='1'">
                            男
                        </div>
                        <div style="margin-left:30px;" v-if="info.gender=='2'">
                            女
                        </div>
                        <div style="margin-left:30px;" v-if="info.gender=='0'">
                            未定义
                        </div>
                    </el-form-item>
                    <el-form-item label="生日">
                        <div style="margin-left:30px;">
                            [[info.birthyear]]-[[info.birthmonth]]-[[info.birthday]]
                        </div>
                    </el-form-item>
                    <el-form-item label="所在地信息">
                        <div style="margin-left:30px;display:inline-block">
                            [[info.yz_member?info.yz_member.province_name:'']][[info.yz_member?info.yz_member.city_name:'']][[info.yz_member?info.yz_member.area_name:'']][[info.yz_member?info.yz_member.address:'']]
                        </div>
                        <el-button @click="showAddressUpdate">修改</el-button>
                    </el-form-item>
                    <el-form-item label="身份证">
                        <div style="margin-left:30px;">
                            [[info.idcard]]
                        </div>
                    </el-form-item>
                    <el-form-item label="身份证地址">
                        <div style="margin-left:30px;">
                            [[info.idcard_addr]]
                        </div>
                    </el-form-item>
                </div>

                @foreach(\app\common\modules\widget\Widget::current()->getItem('member') as $key=>$value)
                    {!! widget($value['class'], ['id'=> $member['uid']])!!}
                @endforeach
                <div class="block">
                    <div class="confirm-btn">
                        <el-button type="primary" @click="submit">提交</el-button>
                    </div>
                </div>
                <div class="block">
                    <el-form-item label="黑名单">
                        <template>
                            <el-switch
                                    style="margin-left:30px;"
                                    v-model="form.is_black"
                                    active-value="1"
                                    inactive-value="0"
                            >
                            </el-switch>
                        </template>
                    </el-form-item>
                    <el-form-item label="备注" >
                        <el-input v-model="form.content" type="textarea" style="margin-left:30px;width:30%;"></el-input>
                    </el-form-item>
                </div>
                <div class="block" v-if="is_email==1">
                    <div class="title"><span style="width: 4px;height: 18px;background-color: #29ba9c;margin-right:15px;display:inline-block;"></span><b>邮箱信息</b></div>
                    <el-form-item label="邮箱">
                        <el-input v-model="info.email" style="margin-left:30px;width:40%;" disabled></el-input>
                    </el-form-item>
                </div>
            </el-form>
        </div>
        <el-dialog title="修改手机" :visible.sync="mobile_show" @close="closeMoblie" :append-to-body="true">
            <el-input v-model="mobile"></el-input>
            <span slot="footer" class="dialog-footer">
                <el-button @click="mobile_show = false">取 消</el-button>
                <el-button type="primary" @click="mobileChoose()">确 定</el-button>
            </span>
        </el-dialog>
        <el-dialog title="修改记录" :visible.sync="mobile_record" @close="closeRecord" :append-to-body="true">
            <el-table :data="record" style="width: 100%" >
                <el-table-column prop="member_id" label="会员id" align="center"></el-table-column>
                <el-table-column prop="mobile_before" label="修改前手机号" align="center"></el-table-column>
                <el-table-column prop="mobile_after" label="修改后手机号" align="center"></el-table-column>
                <el-table-column prop="created_at" label="修改时间" align="center"></el-table-column>
            </el-table>
        </el-dialog>
        <!-- 修改收货地址 -->
        <el-dialog :visible.sync="address_show" width="900px"  title="修改所在地" :append-to-body="true">
            <div style="overflow:auto">
                <div style="overflow:auto" id="update-address">
                    <el-form ref="updateAddress" :model="updateAddress" label-width="15%">
                        <el-form-item label="地址" v-loading="areaLoading">
                            <el-select v-model="updateAddress.province_id" clearable placeholder="省" @change="changeProvince(updateAddress.province_id)" style="width:150px">
                                <el-option v-for="(item,index) in province_list" :key="index" :label="item.areaname" :value="item.id"></el-option>
                            </el-select>
                            <el-select v-model="updateAddress.city_id" clearable placeholder="市" @change="changeCity(updateAddress.city_id)" style="width:150px">
                                <el-option v-for="(item,index) in city_list" :key="index" :label="item.areaname" :value="item.id"></el-option>
                            </el-select>
                            <el-select v-model="updateAddress.district_id" clearable placeholder="区/县" @change="changeDistrict(updateAddress.district_id)" style="width:150px">
                                <el-option v-for="(item,index) in district_list" :key="index" :label="item.areaname" :value="item.id"></el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label="详细地址">
                            <el-input v-model="updateAddress.address" placeholder="详细地址" style="width:70%"></el-input>
                        </el-form-item>
                    </el-form>

                </div>
            </div>
            <span slot="footer" class="dialog-footer">
                    <el-button @click="address_show = false">取 消</el-button>
                    <el-button type="primary" @click="addressUpdate">确 认</el-button>
                </span>
        </el-dialog>
    </div>
    </div>
</template>
<script>
    Vue.component('member_base', {
        delimiters: ['[[', ']]'],
        props: {
            set: {
                type: Object,
                default: () => {}
            },
            info: {
                type: Object,
                default: () => {}
            },
            form: {
                type: Object,
                default: () => {}
            },
            myform: {
                type: Array,
                default: () => {
                    return []
                }
            },
            parent_name: {
                type: String,
                default: ""
            },
            is_email: {
                type: null,
                default: ""
            }
        },
        data() {
            return {
                uploadShow:false,
                chooseImgName:'',
                record:[],
                mobile_record:false,
                mobile_show:false,
                updateAddress:{
                    street_id:'',
                    city_id:"",
                },
                address_show:false,
                areaLoading:false,
                province_list:[],
                city_list:[],
                district_list:[],
                street_list:[],
                mobile:'',
                type:'',
                selNum:'',
            }
        },
        created () {

        },
        methods: {
            openUpload(str,type,sel) {
                this.chooseImgName = str;
                this.uploadShow = true;
                this.type = type;
                this.selNum = sel;
            },
            changeProp(val) {
                if(val == true) {
                    this.uploadShow = false;
                }
                else {
                    this.uploadShow = true;
                }
            },
            sureImg(name,uploadShow,fileList) {
                if(fileList.length <= 0) {
                    return
                }
                this.form[name] = fileList[0].url;
                this.info[name] = fileList[0].url;
                this.info[name+'_image'] = fileList[0].url;
            },
            getRecord(){
                this.mobile_record=true;
                this.$http.post("{!! yzWebUrl('member.member.changeMobileLog') !!}",{uid:this.info.uid}).then(response => {
                    if (response.data.result) {
                        this.record=response.data.data.list
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            getInfo(){
                this.$http.get("{!! yzWebUrl('member.member.detail') !!}"+'&id='+this.info.uid+'&type='+1).then(response => {
                    if (response.data.result) {
                        this.info=response.data.data.member
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            mobileChoose(){
                this.$http.post("{!! yzWebUrl('member.member.changeMobile') !!}",{mobile:this.mobile,uid:this.info.uid}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "修改成功",type: 'success'});
                        this.getInfo()
                        this.mobile_show=false;
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },

            mobileShow(){
                this.mobile_show=true;
            },
            closeMoblie(){
                this.mobile_show=false;
            },
            closeRecord(){
                this.mobile_record=false;
            },
            // closeParent(){
            //     this.parent_record=false;
            // },
            submit(){
                this.$http.post("{!! yzWebUrl('member.member.update') !!}",{id:this.form.id,member:this.form,myform:this.myform}).then(response => {
                    if (response.data.result) {
                        this.$message({message: "提交成功",type: 'success'});
                    }else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                }, response => {
                    this.$message({type: 'error',message: response.data.msg});
                    console.log(response);
                });
            },
            //显示修改订单收货地址
            showAddressUpdate() {
                this.address_show = true;
                this.initProvince();
            },
            //修改订单收货地址
            addressUpdate() {

                let json = {
                    uid:this.form.id,
                    address:this.updateAddress.address,
                    province_id: this.updateAddress.province_id ? this.updateAddress.province_id : 0,
                    city_id:this.updateAddress.city_id ? this.updateAddress.city_id : 0,
                    district_id	:this.updateAddress.district_id ? this.updateAddress.district_id : 0,
                    street_id:this.updateAddress.street_id ? this.updateAddress.street_id : 0,
                };

                console.log(json);
                let loading = this.$loading({target:document.querySelector("#update-address"),background: 'rgba(0, 0, 0, 0)'});
                this.$http.post('{!! yzWebFullUrl('member.member.address-update') !!}',{data:json}).then(function (response) {
                    if (response.data.result) {
                        this.$message({type: 'success',message: '地址修改成功!'});
                    } else{
                        this.$message({type: 'error',message: response.data.msg});
                    }
                    loading.close();
                    this.address_show = false;
                },function (response) {
                    this.$message({type: 'error',message: response.data.msg});
                    loading.close();
                    this.address_show = false;
                })
            },
            //获取地址信息
            initProvince(val) {
                console.log(val);
                this.areaLoading = true;
                this.$http.get("{!! yzWebUrl('area.list.init', ['area_ids'=>'']) !!}"+val).then(response => {
                    this.province_list = response.data.data;
                    this.areaLoading = false;
                }, response => {
                    this.areaLoading = false;
                });
            },
            changeProvince(val) {
                this.city_list = [];
                this.district_list = [];
                this.street_list = [];
                // this.search_form.province_id = "";
                this.updateAddress.city_id = "";
                this.updateAddress.district_id = "";
                this.updateAddress.street_id = "";
                this.areaLoading = true;
                let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                this.$http.get(url).then(response => {
                    if (response.data.data.length) {
                        this.city_list = response.data.data;
                    } else {
                        this.city_list = null;
                    }
                    this.areaLoading = false;
                }, response => {
                    this.areaLoading = false;
                });
            },
            // 市改变
            changeCity(val) {
                this.district_list = [];
                this.street_list = [];
                this.updateAddress.district_id = "";
                this.updateAddress.street_id = "";
                this.areaLoading = true;
                let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                this.$http.get(url).then(response => {
                    if (response.data.data.length) {
                        this.district_list = response.data.data;
                    } else {
                        this.district_list = null;
                    }
                    this.areaLoading = false;
                }, response => {
                    this.areaLoading = false;
                });
            },
            // 区改变
            changeDistrict(val) {
                console.log(val)
                this.street_list = [];
                this.updateAddress.street_id = "";
                this.areaLoading = true;
                let url = "<?php echo yzWebUrl('area.list', ['parent_id'=> '']); ?>" + val;
                this.$http.get(url).then(response => {
                    if (response.data.data.length) {
                        this.street_list = response.data.data;
                    } else {
                        this.street_list = null;
                    }
                    this.areaLoading = false;
                }, response => {
                    this.areaLoading = false;
                });
            },
        },
        template: '#member_base'
    });
</script>

<style scoped></style>