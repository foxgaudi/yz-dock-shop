@extends('layouts.base')
@section('content')
@section('title', trans('提现设置'))
<link href="{{static_url('yunshop/balance/balance.css')}}" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
<link href="{{static_url('yunshop/css/member.css')}}" media="all" rel="stylesheet" type="text/css"/>
<style>
  .add-people{
      width: 91px;
      height: 91px;
      border: dashed 1px #dde2ee;
      display:flex;
      flex-direction:column;
      justify-content:center;
      align-items:center;
    }
    .tabs {
        display: flex;
        flex-wrap: wrap;
        margin-top: 15px;
    }

    .tab-item {
        padding: 5px 20px;
        font-size: 14px;
        font-weight: 500;
        color: #333;
        cursor: pointer;
    }

    .tab-item:hover {
        color: #29BA9C;
    }

    .tab-item.active {
        color: #29BA9C;
    }

    .tab-item::after {
        content: '';
        display: block;
        width: 100%;
        height: 2px;
        background-color: #ffffff;
        margin-top: 5px;
    }

    .tab-item.active::after {
        content: '';
        display: block;
        width: 100%;
        height: 2px;
        background-color: #29BA9C;
        margin-top: 5px;
    }
</style>
<div class="all">
    <div id="app" v-cloak>
      <div class="vue-head">
       <!-- <el-tabs v-model="activeComponent" @tab-click="handleClick">
       <el-tab-pane :label="tabItem.title" :name="tabItem.component_name" v-for="(tabItem,index1) in tabData" :key="index1">
          <div></div>
       </el-tab-pane>
      </el-tabs>
      <div></div> -->
          <div class="tabs">
                <!-- 韦总说要换行显示 -->
                <div v-for="(tabItem,index1) in tabData" :key="index1" class="tab-item"
                     @click.stop="handleClick(tabItem)"
                     :class="{'active': activeComponent == tabItem.component_name}">
                    [[tabItem.title]]
                </div>
            </div>
      </div>
      <ul v-if="componentLoaded" class="withdraw">
       <li v-for="(item,index) in tabData">
          <component :is="item.component_name" :ref="item.component_name" :component-data="item.data" v-show="activeComponent == item.component_name"
                    :key="index" v-if="$options.components[item.component_name]"></component>
       </li>
      </ul>
      <div class="vue-page" style="height: 80px;">
          <div class="vue-center">
                    <el-button type="primary" @click="submit_data" >提交</el-button>
                </div>
            </div>
      </div>
    </div>
</div>

<script>
    const tabDatas = {!! $data?:'{}' !!};
    const saveDataUrl = "{!! yzWebFullUrl('withdraw.withdraw-set.submit-save') !!}"; //* 保存数据地址
    const withdrawSet = "{!! yzWebFullUrl("withdraw.withdraw-set.index") !!}";
</script>
<script src="{{resource_get('static/yunshop/tinymce4.7.5/tinymce.min.js')}}"></script>
@include('public.admin.tinymceee')
<script src="{{static_url('../resources/views/withdraw/assets/js/main.js?time='.time())}}"></script>
@endsection
