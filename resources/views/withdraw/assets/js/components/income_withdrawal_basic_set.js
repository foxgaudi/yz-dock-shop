define({
    name: "withdrawal_basic_set",
    template: `
  <div class="vue-head">
      <div class="vue-main-title" style="margin-bottom:20px">
          <div class="vue-main-title-left"></div>
          <div class="vue-main-title-content">提现设置</div>
          <div class="vue-main-title-button">
          </div>
      </div>
      <div class="vue-search" >
          <el-form  :model="basic_set_form" label-position="right" label-width="210px">
          
          
          
             <el-form-item label='提现到余额' class="form-item" >
              <el-switch
                v-model="basic_set_form.balance"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
              <div class="tip">
              可开启独立手续费设置，独立手续费：提现到余额的收入(不含收银台)，计算比例按照独立手续费中的比例计算【优先级高于插件独立设置】
              </div>
             </el-form-item>
             
             
             
             
             
             <div v-if="basic_set_form.balance == 1">
                 <el-form-item label='独立手续费' class="form-item" >
                  <el-switch
                    v-model="basic_set_form.balance_special"
                    active-value="1"
                    inactive-value="0"
                  >
                  </el-switch>
               </el-form-item>
               
               <template v-if="basic_set_form.balance_special == 1">
               
                   <el-form-item  class="form-item" label="提现手续费类型"> 
                  <el-radio-group v-model="basic_set_form.special_poundage_type">
                  <el-radio label="1">固定金额</el-radio>
                  <el-radio label="0">手续费比例</el-radio>
                  </el-radio-group>
                 </el-form-item>
                   
                   
                    <el-form-item class="form-item" label="">
                  <el-input placeholder="" v-model="basic_set_form.special_poundage" style="width: 800px;">
                  <template slot="prepend">提现手续费</template>
                        <template slot="append"  v-if="basic_set_form.special_poundage_type == 1">元</template>
                        <template slot="append" v-else>%</template>
                  </el-input>
                 </el-form-item> 
                 
                 <el-form-item class="form-item" label="">
                  <el-input placeholder="" v-model="basic_set_form.special_service_tax" style="width: 800px;" >
                  <template slot="prepend">提现劳务税</template>
                        <template slot="append">%</template>
                  </el-input>
                 </el-form-item> 
               
               </template>
             
             
             </div>
            
             
             
             
             
             <el-form-item label='提现到微信' class="form-item" >
              <el-switch
                v-model="basic_set_form.wechat"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             <template v-if="basic_set_form.wechat == 1">
             

               
               <el-form-item class="form-item">
                <el-input v-model="basic_set_form.wechat_min" style="width: 400px;">
                      <template slot="prepend">单笔最低金额</template>
                </el-input>
                <el-input v-model="basic_set_form.wechat_max" style="width: 400px;">
                      <template slot="prepend">单笔最高金额</template>
                </el-input>
                 <div class="tip">可设置区间0.3-20000，设置为0为空则不限制，请参考微信商户平台--产品中心--企业付款到零钱--产品设置--额度设置中设置</div>
               </el-form-item>
               
               <el-form-item class="form-item" label="" >
                <el-input placeholder="" v-model="basic_set_form.wechat_frequency" style="width: 800px;" >
                      <template slot="prepend">每日向同一用户付款不允许超过</template>
                      <template slot="append">次</template>
                </el-input>
                <div class="tip">可设置1-10次,不设置或为空默认为10</div>
               </el-form-item> 
               
               
               <el-form-item label='商家转账到零钱' class="form-item" >
              <el-switch
                v-model="basic_set_form.wechat_api_v3"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
              <div class="tip">如在2022-5-18之后开通的微信商家转账零钱功能（原企业付款到零钱），请开启此功能，并在【系统】-【支付设置】处设置上传相关的证书及秘钥</div>
             </el-form-item>
             
             </template>
             
             
             
             
             
             <el-form-item label='提现到支付宝' class="form-item" >
              <el-switch
                v-model="basic_set_form.alipay"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             <template v-if="basic_set_form.alipay == 1">
             
             
              <el-form-item class="form-item"  label="单笔付款金额">
                <el-input v-model="basic_set_form.alipay_min" style="width: 400px;">
                      <template slot="prepend">单笔最低金额</template>
                </el-input>
                <el-input v-model="basic_set_form.alipay_max" style="width: 400px;">
                      <template slot="prepend">单笔最高金额</template>
                </el-input>
                 <div class="tip"> 设置为0为空不限制</div>
               </el-form-item>
               
               <el-form-item class="form-item" label="每日向同一用户付款不允许超过" >
                <el-input placeholder="" v-model="basic_set_form.alipay_frequency" style="width: 800px;" >
                      <template slot="append">次</template>
                </el-input>
                <div class="tip">设置为0为空不限制</div>
               </el-form-item> 
             
             
            </template>
             
             
             
             
             
             
             
             <el-form-item label='提现到环迅支付' v-if="plugin_open.huanxun" >
              <el-switch
                v-model="basic_set_form.huanxun"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
              <div class="tip">提现到环迅支付，支持收入提现免审核</div>
             </el-form-item>
             
             
              <template  v-if="plugin_open.cloud_pay_money">
                  <el-form-item label='提现到支付宝-云汇算' >
                      <el-switch
                        v-model="basic_set_form.huiis_ali"
                        active-value="1"
                        inactive-value="0"
                      >
                      </el-switch>
                 </el-form-item>
                 
                  <el-form-item label='提现到微信-云汇算' >
                      <el-switch
                        v-model="basic_set_form.huiis_wx"
                        active-value="1"
                        inactive-value="0"
                      >
                      </el-switch>
                 </el-form-item>
                 
                  <el-form-item label='提现到银行卡-云汇算' >
                      <el-switch
                        v-model="basic_set_form.huiis_bank"
                        active-value="1"
                        inactive-value="0"
                      >
                      </el-switch>
                 </el-form-item>
              </template>
             
             
             
             <el-form-item label='提现到EUP' v-if="plugin_open.eup_pay">
              <el-switch
                v-model="basic_set_form.eup_pay"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             
             
             
             
             
             <template v-if="plugin_open.converge_pay">
             
               
               <el-form-item label='提现到汇聚支付' class="form-item" >
                <el-switch
                  v-model="basic_set_form.converge_pay"
                  active-value="1"
                  inactive-value="0"
                >
                </el-switch>
               </el-form-item>
               
               
               <template v-if="basic_set_form.converge_pay == 1">
               
               
                <el-form-item class="form-item"  label="单笔付款金额">
                  <el-input v-model="basic_set_form.converge_pay_min" style="width: 400px;">
                        <template slot="prepend">单笔最低金额</template>
                  </el-input>
                  <el-input v-model="basic_set_form.converge_pay_max" style="width: 400px;">
                        <template slot="prepend">单笔最高金额</template>
                  </el-input>
                   <div class="tip"> 设置为0为空不限制</div>
                 </el-form-item>
                 
                 <el-form-item class="form-item" label="" >
                  <el-input placeholder="" v-model="basic_set_form.converge_pay_frequency" style="width: 800px;" >
                        <template slot="prepend">每日向同一用户付款不允许超过</template>
                        <template slot="append">次</template>
                  </el-input>
                  <div class="tip">设置为0为空不限制</div>
                 </el-form-item> 
               
               
              </template>
             
             
            </template>
             
             
             
             
              <el-form-item label='提现到易宝' v-if="plugin_open.yop_pay">
              <el-switch
                v-model="basic_set_form.yop_pay"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             
                 <el-form-item label='提现到易宝代付' v-if="plugin_open.yee_pay">
              <el-switch
                v-model="basic_set_form.yee_pay"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             
             
             
             
             <template v-if="plugin_open.high_light">
             
             
                <el-form-item label='提现到微信-高灯'>
              <el-switch
                v-model="basic_set_form.high_light_wechat"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             <el-form-item label='提现到支付宝-高灯'>
              <el-switch
                v-model="basic_set_form.high_light_alipay"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
             
             <el-form-item label='提现到银行卡-高灯'>
              <el-switch
                v-model="basic_set_form.high_light_bank"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
             
            </template>
             
           
             <el-form-item label='提现到银行卡-智E+' v-if="plugin_open.eplus_pay">
              <el-switch
                v-model="basic_set_form.eplus_withdraw_bank"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>

             
             
             <el-form-item label='提现到微信-好灵工' v-if="plugin_open.worker_withdraw_wechat">
                <el-switch
                  v-model="basic_set_form.worker_withdraw_wechat"
                  active-value="1"
                  inactive-value="0"
                >
                </el-switch>
             </el-form-item>
             
             <template v-if="plugin_open.worker_withdraw_alipay">
               
                 <el-form-item label='提现到支付宝-好灵工' >
                  <el-switch
                    v-model="basic_set_form.worker_withdraw_alipay"
                    active-value="1"
                    inactive-value="0"
                  >
                  </el-switch>
                 </el-form-item>
                 
                 
                 <el-form-item label='提现到银行卡-好灵工' >
                  <el-switch
                    v-model="basic_set_form.worker_withdraw_bank"
                    active-value="1"
                    inactive-value="0"
                  >
                  </el-switch>
                 </el-form-item>
               
            </template>
             
             
             
             
             
             
             
             <el-form-item :label='lang.manual_withdrawal' class="form-item" >
             <el-switch
                v-model="basic_set_form.manual"
                active-value="1"
                inactive-value="0"
             >
              </el-switch>
              <div class="tip">手动提现包含 银行卡、微信号、支付宝等三种类型，会员需要完善对应资料才可以提现</div>
             </el-form-item>  
             
             <el-form-item class="form-item" v-if="basic_set_form.manual == 1">
                <el-radio-group v-model="basic_set_form.manual_type">
                <el-radio label="1">银行卡</el-radio>
                <el-radio label="2">微信</el-radio>
                <el-radio label="3">支付宝</el-radio>
                </el-radio-group>
             </el-form-item>  
             
               
               <el-form-item label='提现到银典支付' v-if="plugin_open.silver_point_pay">
              <el-switch
                v-model="basic_set_form.silver_point"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
               
                <template v-if="plugin_open.gong_mall_withdraw == 1">
               <el-form-item  label='提现到支付宝-工猫' class="form-item">
                   <el-switch
                     v-model="basic_set_form.gong_mall_withdraw_ali"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
               </el-form-item>
               <el-form-item  label='提现到银行卡-工猫' class="form-item">
                   <el-switch
                     v-model="basic_set_form.gong_mall_withdraw_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
            </template>
            
            <template v-if="plugin_open.integration_pay_share_huifu_withdraw_bank == 1">
               <el-form-item  label='提现到银行卡-汇付' class="form-item">
                   <el-switch
                     v-model="basic_set_form.integration_pay_share_huifu_withdraw_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
               </el-form-item>
            </template>
            
            
            <template v-if="plugin_open.renlijia_withdraw == 1">
               <el-form-item  :label='lang.renlijia_withdraw_diy_name' class="form-item">
                   <el-switch
                     v-model="basic_set_form.renlijia_withdraw"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
               </el-form-item>
            </template>

            <template v-if="plugin_open.hema_withdraw_bank == 1">
               <el-form-item  label='提现到银行卡-杉德宝' class="form-item">
                   <el-switch
                     v-model="basic_set_form.hema_withdraw_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
               </el-form-item>
            </template>
               
                 <el-form-item label='提现到兼职猫-银行卡' v-if="plugin_open.jianzhimao_withdraw">
              <el-switch
                v-model="basic_set_form.jianzhimao_bank"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
               
               
               
               <el-form-item :label='lang.tax_withdraw_diy_name' v-if="plugin_open.tax_withdraw">
              <el-switch
                v-model="basic_set_form.tax_withdraw_bank"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
             </el-form-item>
               
               
               
               
               <template v-if="plugin_open.consol_withdraw">
               
                   
                   <el-form-item label='提现到耕耘灵活用工-银行卡'>
                  <el-switch
                    v-model="basic_set_form.consol_withdraw_bank"
                    active-value="1"
                    inactive-value="0"
                  >
                  </el-switch>
                 </el-form-item>
                 
                 
<!--                 <el-form-item label='提现到耕耘灵活用工-支付宝'>-->
<!--                  <el-switch-->
<!--                    v-model="basic_set_form.consol_withdraw_alipay"-->
<!--                    active-value="1"-->
<!--                    inactive-value="0"-->
<!--                  >-->
<!--                  </el-switch>-->
<!--                 </el-form-item>-->
<!--                 -->
<!--                 -->
<!--                 <el-form-item label='提现到耕耘灵活用工-微信'>-->
<!--                  <el-switch-->
<!--                    v-model="basic_set_form.consol_withdraw_wechat"-->
<!--                    active-value="1"-->
<!--                    inactive-value="0"-->
<!--                  >-->
<!--                  </el-switch>-->
<!--                 </el-form-item>-->
<!--                 -->
<!--                 -->
                </template>
               
               
               
               
               
               
               
               <el-form-item class="form-item" label="可提现日期"> 
              <el-checkbox :indeterminate="isIndeterminate" v-model="checkAll" @change="handleCheckAllChange">全选</el-checkbox>
               <div style="margin: 15px 0;"></div>
              <el-checkbox-group v-model="basic_set_form.show_withdraw_date" @change="handleCheckedCitiesChange">
              <el-checkbox v-for="(item,index) in date_option" :label="index" :key="index">{{item}}</el-checkbox>
              </el-checkbox-group>
               <div class="tip">全部不选择就默认是全部日期</div>
             </el-form-item>
             
             
             
             
             <el-form-item label='劳务税显示' class="form-item">
               <el-switch
                 v-model="basic_set_form.service_switch"
                 active-value="1"
                 inactive-value="0"
                >
              </el-switch>
               <div class="tip">关闭只隐藏前端显示，若有设置劳务税，前端提现还是会计算劳务税</div>
             </el-form-item>
             
             
             <el-form-item label='劳务税计算方式' class="form-item">
               <el-form-item class="form-item">
                <el-radio-group v-model="basic_set_form.service_tax_calculation">
                <el-radio label="0">提现金额-手续费</el-radio>
                <el-radio label="1">提现金额</el-radio>
                </el-radio-group>
             </el-form-item>  
             </el-form-item>
             
             
             
             
             
             
             
             
             <el-form-item label='劳务税比例' class="form-item">
              <el-input placeholder="" v-model="basic_set_form.servicetax_rate" style="width: 800px;" >
                    <template slot="append">%</template>
              </el-input>
            
             </el-form-item>
             
             
             
             
             <el-form-item class="form-item">
              <div v-for="(item,index) in basic_set_form.servicetax" :key='index'>
                  <el-input placeholder="" v-model="basic_set_form.servicetax[index].servicetax_money" style="width: 400px;" >
                        <template slot="prepend">范围</template>
                        <template slot="append">元</template>
                  </el-input>
                  <el-input placeholder="" v-model="basic_set_form.servicetax[index].servicetax_rate" style="width: 400px;" >
                        <template slot="append">%</template>
                  </el-input>
                  <i class="iconfont icon-all_delete_1" style="margin-left: 10px;cursor: pointer;" @click="deleteProportion(index)"></i>
                  </div>
                  <div class="tip">必须按金额从小到大填写</div>
                 </el-form-item>
                 <el-button style="margin-left: 200px;border: 1px solid #29BA9C;color: #29BA9C;margin-bottom: 20px;" @click="addProportion">添加比例</el-button>
                 <el-form-item label='提现规则说明'>
               <tinymceee v-model="basic_set_form.withdraw_rich_text" v-if="!isShowContent"></tinymceee>
               <tinymceee v-model="content" v-if="isShowContent"></tinymceee>
             </el-form-item>
             
             
             </el-form-item>
             
          </el-form>
      </div>
  </div>
</div>
  `,
    style: `

  `,
    props: {
        form: {
            default() {
                return {}
            }
        },
        componentData: {
            default() {
                return {}
            }
        },
    },
    data() {
        return {
            plugin_open: {},
            lang: {},
            isIndeterminate: true,
            checkAll: true,
            checked_date: [],
            date_option: ['1号', '2号', '3号', '4号', '5号', '6号', '7号', '8号', '9号', '10号', '11号', '12号', '13号', '14号', '15号', '16号', '17号', '18号', '19号', '20号', '21号', '22号', '23号', '24号', '25号', '26号', '27号', '28号', '29号', '30号', '31号'],
            //收入提现基础设置
            basic_set_form: {
                balance: 0,
                balance_special: 0,
                special_poundage_type: 0,
                special_poundage: '',
                special_service_tax: '',
                wechat: 0,
                wechat_min: '',
                wechat_max: '',
                wechat_frequency: '',
                manual: 0,
                manual_type: 1,
                withdraw_date: null,
                service_switch: 0,
                servicetax_rate: '',
                list: [{money: null, proportion: null}]
            },
            isShowContent:false,
            content:""
        }
    },
    created() {
        if (this.componentData) {
            this.plugin_open = this.componentData.common.plugin_open;
            this.lang = this.componentData.common.lang;
            delete this.componentData.common;
            this.basic_set_form = this.componentData
            if (this.basic_set_form.withdraw_date === undefined || this.basic_set_form.withdraw_date === '0') {
                this.basic_set_form.show_withdraw_date = this.date_option.map((val, key) => key);
            }
        }
        if(this.basic_set_form){
          if(this.form === null){
            this.isShowContent = true
            return
          }
          this.isShowContent = false
        }else{
          this.isShowContent = true
        }
        if(!this.basic_set_form.servicetax){
          this.basic_set_form.servicetax = []
        }
    },
    methods: {
        //验证方法
        filterData() {
            return true
        },
        //点击提交
        returnData() {
            if (this.filterData()) {
                return this.basic_set_form
            } else {
                return false
            }
        },
        handleCheckAllChange(val) {
            this.basic_set_form.show_withdraw_date = val ? this.date_option.map((val, key) => key) : [];
            this.isIndeterminate = false;
        },
        handleCheckedCitiesChange(val) {
            let checkedCount = val.length;
            this.checkAll = checkedCount === this.date_option.length;
            this.isIndeterminate = checkedCount > 0 && checkedCount < this.date_option.length;
        },
        addProportion() {
            this.basic_set_form.servicetax.push({money: null, proportion: null})
        },
        deleteProportion(index) {
            this.basic_set_form.servicetax.splice(index, 1)
        },
    },
})
