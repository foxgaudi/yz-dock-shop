define({
        name: "balance",
        template: `
  <div class="vue-head">
      <div class="vue-main-title" style="margin-bottom:20px">
          <div class="vue-main-title-left"></div>
          <div class="vue-main-title-content">提现设置</div>
          <div class="vue-main-title-button">
          </div>
      </div>
      <div class="vue-search" >
          <el-form  :model="search_form" label-position="right" label-width="210px">
             <el-form-item label='开启余额提现' class="form-item" >
              <el-switch
                v-model="search_form.status"
                active-value="1"
                inactive-value="0"
              >
              </el-switch>
              <div class="tip">
                  是否允许用户将余额提出
              </div>
             </el-form-item>
         <div v-if="search_form.status == 1">
             <el-form-item label='提现到微信' class="form-item">
             <el-switch
                v-model="search_form.wechat"
                active-value="1"
                inactive-value="0"
             >
              </el-switch>
            </el-form-item>
             <el-form-item class="form-item" v-if="search_form.wechat == 1">
             <el-input placeholder="为0为空则不限制" v-model="search_form.wechat_min" style="width: 400px;">
                    <template slot="prepend">单笔最低金额</template>
             </el-input>
             <el-input placeholder="为0为空则不限制" v-model="search_form.wechat_max" style="width: 400px;">
                    <template slot="prepend">单笔最高金额</template>
             </el-input>
             </el-form-item>
             <el-form-item class="form-item" v-if="search_form.wechat == 1">
             <el-input placeholder="为0为空则不限制" v-model="search_form.wechat_frequency" style="width: 800px;">
                    <template slot="prepend">每日提现次数</template>
                    <template slot="append">次</template>
             </el-input>
             </el-form-item>
             <el-form-item label='提现到支付宝' class="form-item">
             <el-switch
                v-model="search_form.alipay"
                active-value="1"
                inactive-value="0"
             >
              </el-switch>
             </el-form-item>
             <el-form-item class="form-item" v-if="search_form.alipay == 1">
             <el-input placeholder="为0为空则不限制" v-model="search_form.alipay_min" style="width: 400px;">
                    <template slot="prepend">单笔最低金额</template>
             </el-input>
             <el-input placeholder="为0为空则不限制" v-model="search_form.alipay_max" style="width: 400px;">
                    <template slot="prepend">单笔最高金额</template>
             </el-input>
             </el-form-item>
             <el-form-item class="form-item" v-if="search_form.alipay == 1">
             <el-input placeholder="为0为空则不限制" v-model="search_form.alipay_frequency" style="width: 800px;">
                    <template slot="prepend">每日提现次数</template>
                    <template slot="append">次</template>
             </el-input>
             </el-form-item>  
             
             <el-form-item v-if="plugin_open.huanxun" label='提现到环迅支付' class="form-item">
                 <el-switch
                    v-model="search_form.huanxun"
                    active-value="1"
                    inactive-value="0"
                 ></el-switch>
             </el-form-item>
             
             <el-form-item v-if="plugin_open.eup_pay" label='提现到EUP' class="form-item">
                 <el-switch
                    v-model="search_form.eup_pay"
                    active-value="1"
                    inactive-value="0"
                 ></el-switch>
             </el-form-item>
             
             <template v-if="plugin_open.converge_pay">
             
                 <el-form-item  label='提现到汇聚支付' class="form-item">
                   <el-switch
                     v-model="search_form.converge_pay"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                 </el-form-item>
                     <template v-if="search_form.converge_pay == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.converge_pay_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.converge_pay_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.converge_pay_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                    </template>
                </template>
             
             
             
             
             <template v-if="plugin_open.high_light">
             
                 <el-form-item  label='提现到微信-高灯' class="form-item">
                       <el-switch
                         v-model="search_form.high_light_wechat"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 <el-form-item  label='提现到支付宝-高灯' class="form-item">
                       <el-switch
                         v-model="search_form.high_light_alipay"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 <el-form-item  label='提现到银行卡-高灯' class="form-item">
                       <el-switch
                         v-model="search_form.high_light_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
             
            </template>
            
            
            
            
            
             <template v-if="plugin_open.eplus_pay">
                 
                 <el-form-item  label='提现到银行卡-智E+' class="form-item">
                       <el-switch
                         v-model="search_form.eplus_withdraw_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
                 <template v-if="search_form.eplus_withdraw_bank == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.eplus_withdraw_bank_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.eplus_withdraw_bank_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.eplus_withdraw_bank_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                    </template>
             
            </template>
            
            
            
            
            
            
            
            
             <template v-if="plugin_open.worker_withdraw_wechat">
                 
                 <el-form-item  label='提现到微信-好灵工' class="form-item">
                       <el-switch
                         v-model="search_form.worker_withdraw_wechat"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
                 <template v-if="search_form.worker_withdraw_wechat == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_wechat_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_wechat_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_wechat_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                    </template>
             
            </template>
            
            
            <template v-if="plugin_open.worker_withdraw_alipay">
                 
                 <el-form-item  label='提现到支付宝-好灵工' class="form-item">
                       <el-switch
                         v-model="search_form.worker_withdraw_alipay"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
                 <template v-if="search_form.worker_withdraw_alipay == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_alipay_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_alipay_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_alipay_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                    </template>
                    
                    
                 <el-form-item  label='提现到银行卡-好灵工' class="form-item">
                       <el-switch
                         v-model="search_form.worker_withdraw_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
                     <template v-if="search_form.worker_withdraw_bank == 1">
                             <el-form-item class="form-item" >
                                 <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_bank_min" style="width: 400px;">
                                        <template slot="prepend">单笔最低金额</template>
                                 </el-input>
                                 <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_bank_max" style="width: 400px;">
                                        <template slot="prepend">单笔最高金额</template>
                                 </el-input>
                             </el-form-item>
                             <el-form-item class="form-item">
                                 <el-input placeholder="为0为空则不限制" v-model="search_form.worker_withdraw_bank_frequency" style="width: 800px;">
                                        <template slot="prepend">每日提现次数</template>
                                        <template slot="append">次</template>
                                 </el-input>
                             </el-form-item>  
                      </template>
             
                </template>
            
            
            
             
             
                 
             <el-form-item :label='lang.manual_withdrawal' class="form-item" >
             <el-switch
                v-model="search_form.balance_manual"
                active-value="1"
                inactive-value="0"
             >
              </el-switch>
              <div class="tip">手动提现包含 银行卡、微信号、支付宝等三种类型，会员需要完善对应资料才可以提现</div>
             </el-form-item>    
             
             
             <template v-if="search_form.balance_manual == 1">
                 <el-form-item class="form-item">
                  <el-radio-group v-model="search_form.balance_manual_type">
                  <el-radio label="1">银行卡</el-radio>
                  <el-radio label="2">微信</el-radio>
                  <el-radio label="3">支付宝</el-radio>
                  </el-radio-group>
                 </el-form-item>    
                     <el-form-item class="form-item" >
                         <el-input placeholder="为0为空则不限制" v-model="search_form.manual_min" style="width: 400px;">
                                <template slot="prepend">单笔最低金额</template>
                         </el-input>
                         <el-input placeholder="为0为空则不限制" v-model="search_form.manual_max" style="width: 400px;">
                                <template slot="prepend">单笔最高金额</template>
                         </el-input>
                     </el-form-item>
                     <el-form-item class="form-item">
                         <el-input placeholder="为0为空则不限制" v-model="search_form.manual_frequency" style="width: 800px;">
                                <template slot="prepend">每日提现次数</template>
                                <template slot="append">次</template>
                         </el-input>
                     </el-form-item>  
              </template>
             
             
             
             
             
             
             
             
             
             <template v-if="plugin_open.silver_point_pay">
                 
                 <el-form-item  label='提现到银典支付' class="form-item">
                       <el-switch
                         v-model="search_form.silver_point"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
             <template v-if="search_form.silver_point == 1">
                     <el-form-item class="form-item" >
                         <el-input placeholder="为0为空则不限制" v-model="search_form.silver_point_min" style="width: 400px;">
                                <template slot="prepend">单笔最低金额</template>
                         </el-input>
                         <el-input placeholder="为0为空则不限制" v-model="search_form.silver_point_max" style="width: 400px;">
                                <template slot="prepend">单笔最高金额</template>
                         </el-input>
                     </el-form-item>
                     <el-form-item class="form-item">
                         <el-input placeholder="为0为空则不限制" v-model="search_form.silver_point_frequency" style="width: 800px;">
                                <template slot="prepend">每日提现次数</template>
                                <template slot="append">次</template>
                         </el-input>
                     </el-form-item>  
                </template>
             
            </template>
            
            <template v-if="plugin_open.gong_mall_withdraw == 1">
               <el-form-item  label='提现到支付宝-工猫' class="form-item">
                   <el-switch
                     v-model="search_form.gong_mall_withdraw_ali"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
               </el-form-item>
               <el-form-item  label='提现到银行卡-工猫' class="form-item">
                   <el-switch
                     v-model="search_form.gong_mall_withdraw_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
            </template>
            
            <template v-if="plugin_open.cloud_pay_money == 1">
            
               <el-form-item  label='提现到支付宝-云汇算' class="form-item">
                   <el-switch
                     v-model="search_form.huiis_ali"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
                
                <el-form-item  label='提现到微信-云汇算' class="form-item">
                   <el-switch
                     v-model="search_form.huiis_wx"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
                
                <el-form-item  label='提现到银行卡-云汇算' class="form-item">
                   <el-switch
                     v-model="search_form.huiis_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
                
            </template>
            
            
            
            <template v-if="plugin_open.integration_pay_share_huifu_withdraw_bank == 1">
               <el-form-item  label='提现到银行卡-汇付' class="form-item">
                   <el-switch
                     v-model="search_form.integration_pay_share_huifu_withdraw_bank"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
                </el-form-item>
            </template>
            
            
            
            
            
             <template v-if="plugin_open.jianzhimao_withdraw">
                 
                 <el-form-item  label='提现到兼职猫-银行卡' class="form-item">
                       <el-switch
                         v-model="search_form.jianzhimao_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
             <template v-if="search_form.jianzhimao_bank == 1">
                     <el-form-item class="form-item" >
                         <el-input placeholder="为0为空则不限制" v-model="search_form.jianzhimao_bank_min" style="width: 400px;">
                                <template slot="prepend">单笔最低金额</template>
                         </el-input>
                         <el-input placeholder="为0为空则不限制" v-model="search_form.jianzhimao_bank_max" style="width: 400px;">
                                <template slot="prepend">单笔最高金额</template>
                         </el-input>
                     </el-form-item>
                     <el-form-item class="form-item">
                         <el-input placeholder="为0为空则不限制" v-model="search_form.jianzhimao_bank_frequency" style="width: 800px;">
                                <template slot="prepend">每日提现次数</template>
                                <template slot="append">次</template>
                         </el-input>
                     </el-form-item>  
                </template>
             
            </template>
            
            
            
            
             
             <template v-if="plugin_open.tax_withdraw">
                 
                 <el-form-item  :label='lang.tax_withdraw_diy_name' class="form-item">
                       <el-switch
                         v-model="search_form.tax_withdraw_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                 
                 
             <template v-if="search_form.tax_withdraw_bank == 1">
                     <el-form-item class="form-item" >
                         <el-input placeholder="为0为空则不限制" v-model="search_form.tax_withdraw_bank_min" style="width: 400px;">
                                <template slot="prepend">单笔最低金额</template>
                         </el-input>
                         <el-input placeholder="为0为空则不限制" v-model="search_form.tax_withdraw_bank_max" style="width: 400px;">
                                <template slot="prepend">单笔最高金额</template>
                         </el-input>
                     </el-form-item>
                     <el-form-item class="form-item">
                         <el-input placeholder="为0为空则不限制" v-model="search_form.tax_withdraw_bank_frequency" style="width: 800px;">
                                <template slot="prepend">每日提现次数</template>
                                <template slot="append">次</template>
                         </el-input>
                     </el-form-item>  
                </template>
             
            </template>
            
            
            
            
            
            
            
            <template v-if="plugin_open.consol_withdraw">
                 
                 <el-form-item  label='提现到耕耘灵活用工-银行卡' class="form-item">
                       <el-switch
                         v-model="search_form.consol_withdraw_bank"
                         active-value="1"
                         inactive-value="0"
                       >
                       </el-switch>
                 </el-form-item>
                     
                     
                 <template v-if="search_form.consol_withdraw_bank == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_bank_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_bank_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_bank_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                 </template>
             
             
             
<!--                  <el-form-item  label='提现到耕耘灵活用工-支付宝' class="form-item">-->
<!--                       <el-switch-->
<!--                         v-model="search_form.consol_withdraw_alipay"-->
<!--                         active-value="1"-->
<!--                         inactive-value="0"-->
<!--                       >-->
<!--                       </el-switch>-->
<!--                 </el-form-item>-->
                     
                     
                 <template v-if="search_form.consol_withdraw_alipay == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_alipay_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_alipay_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_alipay_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                 </template>
                 
                 
                 
                 
<!--                  <el-form-item  label='提现到耕耘灵活用工-微信' class="form-item">-->
<!--                       <el-switch-->
<!--                         v-model="search_form.consol_withdraw_wechat"-->
<!--                         active-value="1"-->
<!--                         inactive-value="0"-->
<!--                       >-->
<!--                       </el-switch>-->
<!--                 </el-form-item>-->
                     
                     
                 <template v-if="search_form.consol_withdraw_wechat == 1">
                         <el-form-item class="form-item" >
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_wechat_min" style="width: 400px;">
                                    <template slot="prepend">单笔最低金额</template>
                             </el-input>
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_wechat_max" style="width: 400px;">
                                    <template slot="prepend">单笔最高金额</template>
                             </el-input>
                         </el-form-item>
                         <el-form-item class="form-item">
                             <el-input placeholder="为0为空则不限制" v-model="search_form.consol_withdraw_wechat_frequency" style="width: 800px;">
                                    <template slot="prepend">每日提现次数</template>
                                    <template slot="append">次</template>
                             </el-input>
                         </el-form-item>  
                 </template>
             
             
            </template>
             
             
             
             
               
             <el-form-item  class="form-item" label="提现手续费"> 
              <el-radio-group v-model="search_form.poundage_type">
              <el-radio label="1">固定金额</el-radio>
              <el-radio label="0">手续费比例</el-radio>
              </el-radio-group>
             </el-form-item>
             <el-form-item class="form-item" >
              <el-input placeholder="" v-model="search_form.poundage" style="width: 800px;">
                    <template slot="prepend">{{search_form.poundage_type == 1 ? '固定金额' : '手续费比例'}}</template>
                    <template slot="append">{{search_form.poundage_type == 1 ? '元' : '%'}}</template>
              </el-input>
             </el-form-item> 
              
             
             <el-form-item class="form-item" >
              <el-input placeholder="" v-model="search_form.poundage_full_cut" style="width: 800px;">
                    <template slot="prepend">满额减免手续费</template>
                    <template slot="append">元</template>
              </el-input>
             </el-form-item> 
             
             
             
             
             
             
             <el-form-item class="form-item" label="提现金额限制">
              <el-input placeholder="余额提现最小金额值" v-model="search_form.withdrawmoney" style="width: 800px;">
                    <template slot="append">元</template>
              </el-input>
             </el-form-item>
             <el-form-item class="form-item" label="提现倍数限制">
              <el-input placeholder="" v-model="search_form.withdraw_multiple" style="width: 800px;">
              </el-input>
              <div class="tip">倍数限制：为空则不限制，两位小数计算，建议使用正整数保证预算精确。</div>
             </el-form-item>
             
             
             
             
             
             <el-form-item  label='提现扣除积分' class="form-item">
                   <el-switch
                     v-model="search_form.deduct_status"
                     active-value="1"
                     inactive-value="0"
                   >
                   </el-switch>
             </el-form-item>
             
             <el-form-item  label='扣除比例' class="form-item" v-if="search_form.deduct_status == 1">
               <el-input placeholder="请输入内容" v-model="search_form.deduct_balance" style="width: 400px;">
                   <template slot="prepend">扣除比例</template>
                   <template slot="append">余额</template>
               </el-input>
               <el-input placeholder="请输入内容" v-model="search_form.deduct_point" style="width: 400px;right: 6px;">
               <template slot="append">积分</template>
               </el-input>
             </el-form-item>
             
             
          </div>
          </el-form>
      </div>
  </div>
  `,
        style: `

  `,
        props: {
            form: {
                default() {
                    return {}
                }
            },
            currentComponentData: {
                default() {
                    return {}
                }
            },
            componentData: {
                default() {
                    return {}
                }
            },
        },
        data() {
            return {
                search_form: {},
                plugin_open: {},
                lang: {},
            }
        },
        created() {
            if (this.componentData) {
                this.plugin_open = this.componentData.common.plugin_open;
                this.lang = this.componentData.common.lang;
                delete this.componentData.common;
                this.search_form = this.componentData;
            }
        },
        methods: {
            //验证方法
            filterData() {
                return true
            },
            // 在main.js中调用此方法 点击提交返回数据
            returnData() {
                //当数据认证通过后
                if (this.filterData()) {
                    return this.search_form
                } else {
                    return false
                }
            }
        },
    }
)
