define({
    name: "income_withdrawal_notice",
    template: `
  <div class="vue-head" >
  <div class="vue-head">
    <div class="vue-main-title" style="margin-bottom:20px">
           <div class="vue-main-title-left"></div>
           <div class="vue-main-title-content">提现设置</div>
           <div class="vue-main-title-button">
           </div>
    </div>
     <div class="vue-search">
        <el-form  :model="notice_form" label-position="right" label-width="210px">
        
        
        
             <el-form-item v-if="enabled.income_withdraw_title" class="form-item" label="提现申请通知(财务)">
              <el-select filterable  filterable v-model="notice_form.income_withdraw" placeholder="默认消息模板" style="width: 400px;" clearable > 
                  <el-option
                   v-for="item in news_options"
                   :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
               <el-switch
               style="margin-left: 20px;"
               @change="messageDefault($event,'income_withdraw')"
                 v-model="def.income_withdraw"
                 :active-value="1"
                 :inactive-value="0"
              >
               </el-switch>
             </el-form-item>
             
             
             
             
             
             
             
             
             
             <el-form-item v-if="enabled.income_withdraw_check_title" class="form-item" label="提现审核通知(财务)">
              <el-select filterable  v-model="notice_form.income_withdraw_check" placeholder="默认消息模板" style="width: 400px;" clearable >
                  <el-option
                   v-for="item in news_options"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
               <el-switch
               style="margin-left: 20px;"
                @change="messageDefault($event,'income_withdraw_check')"
                 v-model="def.income_withdraw_check"
                 :active-value="1"
                 :inactive-value="0"
              >
               </el-switch>
             </el-form-item>
             
             
             
             
             
             
             
             
             
              
             <el-form-item v-if="enabled.income_withdraw_pay_title" class="form-item" label="提现打款通知(财务)">
              <el-select filterable  v-model="notice_form.income_withdraw_pay" placeholder="默认消息模板" style="width: 400px;" clearable >
                  <el-option
                   v-for="item in news_options"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
               <el-switch
               style="margin-left: 20px;"
                @change="messageDefault($event,'income_withdraw_pay')"
                 v-model="def.income_withdraw_pay"
                 :active-value="1"
                 :inactive-value="0"
              >
               </el-switch>
             </el-form-item>
             
             
             
             
             
             
             <template v-if="enabled.income_withdraw_arrival_title">
             
             
             
             <el-form-item  class="form-item" label="提现到账通知(财务)">
              <el-select filterable  v-model="notice_form.income_withdraw_arrival" placeholder="默认消息模板" style="width: 400px;" clearable >
                  <el-option
                   v-for="item in news_options"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
               <el-switch
               style="margin-left: 20px;"
                @change="messageDefault($event,'income_withdraw_arrival')"
                 v-model="def.income_withdraw_arrival"
                 :active-value="1"
                 :inactive-value="0"
              >
               </el-switch>
             </el-form-item>
             
             
             
             
             <el-form-item  class="form-item" label="提现失败管理员通知(财务)">
              <el-select filterable  v-model="notice_form.income_withdraw_fail" placeholder="默认消息模板" style="width: 400px;" clearable >
                  <el-option
                   v-for="item in news_options"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
             </el-form-item>
             
             
            </template>
             
              
             
             
             <el-form-item class="form-item" label="会员提现管理员通知(财务)">
              <el-select filterable  v-model="notice_form.member_withdraw" placeholder="默认消息模板" style="width: 400px;" clearable >
                  <el-option
                   v-for="item in news_options"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value">
                 </el-option>
               </el-select>
                <el-switch
               style="margin-left: 20px;"
                @change="messageDefault($event,'member_withdraw')"
                 v-model="def.member_withdraw"
                 :active-value="1"
                 :inactive-value="0"
              >
               </el-switch>
             </el-form-item>
             
             
             
             
             
             
             
             
             
             
             
             <el-form-item class="form-item" label="通知人">
                       <div style="display:flex;flex-wrap: wrap;">
                           <div class="good" v-for="(value,key,index) in notice_form.withdraw_user" style="width:91px;display:flex;margin-right:20px;flex-direction: column">
                               <div class="img" style="position:relative;">
                                   <a style="color:#333;"><div style="width: 20px;height: 20px;background-color: #dde2ee;display:flex;align-items:center;justify-content:center; #999999;position:absolute;right:-10px;top:-10px;border-radius:50%;" @click="delPeople(key)">X</div></a>
                                   <img :src="value.avatar" style="width:91px;height:91px;">
                               </div>
                               <div style="display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp: 2;overflow: hidden;font-size:12px;line-height:20px">{{value.nickname}} </div>
                               <div style="line-height:20px">id:{{value.uid}}</div>
                           </div>
                           <div class="add-people" @click="openPeople">
                               <a style="font-size:32px;color: #999999;"><i class="el-icon-plus" ></i></a>
                               <div style="color: #999999;">选择通知人</div>
                           </div>
                       </div>
                       <div class="tip">会员收入提现商家通知，可以指定多人，如果不填写则不通知</div>
                   </el-form-item>
        </el-form>
     </div>
    </div>
    <el-dialog :visible.sync="peopleShow" width="60%" center title="选择通知人">
       <div style="text-align:center;">
           <el-input  style="width:80%" v-model="keyword"></el-input>
           <el-button @click="search" style="margin-left:10px;" type="primary">搜索</el-button>
       </div>
       <el-table :data="people_list" style="width: 100%;height:500px;overflow:auto">
           <el-table-column label="通知人信息" align="center">
               <template slot-scope="scope" >
                   <div v-if="scope.row" style="display:flex;align-items: center;justify-content: center;">
                       <img v-if="scope.row.avatar" :src="scope.row.avatar"  style="width:50px;height:50px" />
                       <div style="margin-left:10px">{{scope.row.nickname}}</div>
                   </div>
               </template>
           </el-table-column>
           <el-table-column label="手机" prop="mobile" align="center" ></el-table-column>
           <el-table-column label="会员ID" prop="uid" align="center" ></el-table-column>
           <el-table-column prop="refund_time" label="操作" align="center" >
               <template slot-scope="scope">
                   <el-button @click="surePeople(scope.row)">
                       选择
                   </el-button>

               </template>
           </el-table-column>
       </el-table>
   </el-dialog>
</div>
  `,
    style: `

  `,
    props: {
        form: {
            default() {
                return {}
            }
        },
        componentData: {
            default() {
                return {}
            }
        },
    },
    data() {
        return {
            notice_form: {
                apply_notice: '',
                apply_notice_value: 0,
                examine_notice: '',
                examine_notice_value: 0,
                payment_notice: '',
                payment_notice_value: 0,
                account_notice: '',
                account_notice_value: 0,
                fail_notice: '',
                fail_notice_value: 0,
                member_notice: '',
                member_notice_value: 0,
                withdraw_user: {},
            },
            news_options: [],
            peopleShow: false,
            keyword: '',
            people_list: [],
            enabled: [],
            def: [],
            url: [],
        }
    },
    created() {
        if (this.componentData) {
            this.enabled = this.componentData.common.enabled;
            this.news_options = this.componentData.common.news_options;
            this.def = this.componentData.common.def;
            this.url = this.componentData.common.url;
            delete this.componentData.common;
            this.notice_form = this.componentData;
            if(!this.notice_form.withdraw_user){
                this.notice_form.withdraw_user = {}
            }
        }
//  console.log(this.currentComponentData);
    },
    methods: {
        //验证方法
        filterData() {
            return true
        },
        //点击提交
        returnData() {
            if (this.filterData()) {
                if((JSON.stringify(this.notice_form.withdraw_user) === "{}")||(JSON.stringify(this.notice_form.withdraw_user) === "[]")){
                    this.notice_form.withdraw_user = null
                }
                return this.notice_form
            } else {
                return false
            }
        },
        openPeople() {
            this.peopleShow = true;
            this.search()
        },
        search() {
            let that = this;
            this.$http.post(that.url.search_member, {keyword: this.keyword}).then(response => {
                if (response.data.result) {
                    this.people_list = response.data.data.members
                } else {
                    this.$message({message: response.data.msg, type: 'error'});
                }

            }, response => {
                this.$message({message: response.data.msg, type: 'error'});
            });
        },
        surePeople(item) {
            var status = 0;
            if (JSON.stringify(this.notice_form.withdraw_user) !== "{}") {
                for(let key in this.notice_form.withdraw_user){
                     if (this.notice_form.withdraw_user[key].uid == item.uid) {
                        status = 1
                        this.$message({message: '该通知人已被选中', type: 'error'});
                        return true
                    }
                }
            }
            if (status == 1) {
                return false
            }
            this.notice_form.withdraw_user[item.uid] = item
        },

        messageDefault(value, name) {
            let url = value === 1 ? this.url.open : this.url.cancel;
            let json = {
                notice_name: name,
                setting_name: "withdraw.notice",
            };
            this.$http.post(url, json).then(response => {
                console.log('response',response)
                if (response.data.result === 1 || response.data.result === '1') {
                    this.$set(this.notice_form, name, response.id);
                } else {
                    this.$message({message: '开启失败，请检查微信模版', type: 'error'});
                    this.$set(this.def, name, '');
                }
            }, response => {
                this.$message({message: '开启失败！', type: 'error'});
                this.$set(this.def, name, '');
            });

        },
        delPeople(key){
            this.$delete(this.notice_form.withdraw_user,key)
        }
    },
})
