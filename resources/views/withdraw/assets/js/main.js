let vm = new Vue({
  el: "#app",
  delimiters: ['[[', ']]'],
  created() {
    this.tabData = tabDatas;
    this.saveDataUrl = saveDataUrl;
    this.withdrawSet = withdrawSet;
    this.activeComponent = this.tabData[0].component_name;
    let loadedComponentCount = 0;
    this.tabData.forEach(item => {
      require.config({
        paths:{
        [item.component_name] : item.page_path+item.component_name
        }
      })
       this.loadComponent(item.component_name).then((res) => {
        loadedComponentCount++;
        //* 当所有组件文件加载完成才会显示页面，因为vue的ref需要组件加载完后才能获取
        if (loadedComponentCount == this.tabData.length) {
          this.componentLoaded = true;
        }
      });
    })
  },
  mounted(){
    // console.log(this.$refs[this.currentComponent]);
  },
  methods: {
     async submit_data(){
      //json就是要提交的数据
       let json =  {};
       const components = this.tabData;
         for (const item of components) {
          const component = this.$refs[item.component_name][0];
          const data = await component.returnData();
          if(!data){
            this.activeComponent = item.component_name;
            this.$message.warning('表单验证不通过');
            break;
          }else{
             json[item.widget_key] = data
          }            
         }
         console.log(json);

         this.saveData(json);

      },
      //点击提交保存数据
      saveData(submitData){
          this.saveStatus = false;
          this.$http.post(this.saveDataUrl, submitData).then(response => {
              console.log(response,'response');
              if (response.data.result) {
                  this.$message({type: 'success',message: '成功!'});
                  window.location.href = this.withdrawSet;
              } else{
                  console.log(response.data.msg);
                  this.$message({type: 'error',message: response.data.msg});
              }
              this.saveStatus = true
          }),function(res){
              console.log(res);
          };
      },
      //切换tab选项  
      handleClick(item){
        this.activeComponent = item.component_name
      },
      //加载选项卡组件
      loadComponent(name){
        return new Promise((resolve, reject) => {
          const pageLoading = this.$loading({
            target: ".withdraw",
            text: "页面加载中",
          });
          require([name],(options) => {
            // 注册组件
            this.$options.components[name] = options;
            this.activeComponent = this.tabData[0].component_name;
            this.currentComponentData = this.tabData[0].data;
            this.widget_key = this.tabData[0].widget_key;
            resolve(name, options);
            pageLoading.close();
          }, (err) => {
            pageLoading.close();
            reject(err);
          });
        })
      }      
  },

  data() {
    return {
        // 防止快速点击保存
        saveStatus:true,

         widget_key:'',
         currentComponentData:{},
        //  currentComponent:'',
         activeComponent:'',
         tabData:[],
         componentLoaded:false,
        //区域分红提现  
        regional_dividends_form:{
          withdrawal_limit:'',
          withdrawal_handling_fee:0,
          fixed:'',
          fee_ratio:''
        },
        //佣金提现
        commission_withdrawal_form:{
          withdrawal_limit:'',
          withdrawal_handling_fee:0,
          fixed:'',
          fee_ratio:'',
          max_withdrawal_limit:'',
          max_time_withdrawal_limit:''
        },
        //门店提现  
        store_withdrawal_form:{
          withdrawal_limit:'',
          withdrawal_handling_fee:0,
          fixed:'',
          fee_ratio:'',
          set:0,
          checkList:[]
        },
      }
},
//定义全局的方法  
beforeCreate() {
},
filters: {},
computed:{

},
});