
<link rel="stylesheet" href="{{static_url('yunshop/css/a-colour.css')}}">
<div class="vue-head">
    @if (\app\common\services\PermissionService::can('finance_balance_records'))
    <a class="a-btn @if(request()->input('route') == 'finance.balance-records.index') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('finance.balance-records.index') !!}">余额明细</a>
    @endif
    @if (\app\common\services\PermissionService::can('finance_balance_member'))
    <a class="a-btn @if(request()->input('route') == 'balance.member.index') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('balance.member.index') !!}">会员余额</a>
    @endif
    @if (\app\common\services\PermissionService::can('finance_balance_rechargeRecord'))
    <a class="a-btn @if(request()->input('route') == 'finance.balance-recharge-records') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('finance.balance-recharge-records') !!}">充值记录</a>
    @endif
    @if (\app\common\services\PermissionService::can('finance_balance_tansferRecord'))
        <a class="a-btn @if(request()->input('route') == 'finance.balance.transfer-record') a-colour2 @else a-colour1 @endif"
           href="{!! yzWebFullUrl('finance.balance.transfer-record') !!}">转让记录</a>
    @endif

    @if (\app\common\services\PermissionService::can('balanceRechargeCheck'))
    <a class="a-btn @if(request()->input('route') == 'finance.balance-recharge-check.index') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('finance.balance-recharge-check.index') !!}">审核列表</a>
    @endif
    @if (\app\common\services\PermissionService::can('balanceRechargePaySet'))
    <a class="a-btn @if(request()->input('route') == 'finance.balance-recharge-set.index') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('finance.balance-recharge-set.index') !!}">指定支付</a>
    @endif
    @if (\app\common\services\PermissionService::can('finance_balance_set'))
        <a class="a-btn @if(request()->input('route') == 'finance.balance-set.see') a-colour2 @else a-colour1 @endif"
       href="{!! yzWebFullUrl('finance.balance-set.see') !!}">余额设置</a>
    @endif
</div>