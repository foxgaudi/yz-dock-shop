<template id="exportExcelId">
    <div class="export-excel">
        <!-- 导出模板 -->
        <el-dialog
            :title="template_type == 1 ? '' : id ? '编辑模板' : '新增模板'"
            :visible.sync="orderExportDialog"
            width="880px"
            class="order-dialog"
            :before-close="closeDialog"
            :append-to-body="appendToBodyType">
            <div class="top-content" v-if="template_type == 1">
                <span class="template-title">选择导出模板</span>
                <el-button type="primary" @click="getColumns('')">新增导出模板</el-button>
            </div>
            <div class="edit-template" v-if="template_type == 2">
                <div class="template-name" style="display: flex;align-items: center;">
                    <span>模板名称</span>
                    <el-input v-model="templates.name" style="width: 300px; margin: 20px;"></el-input>
                </div>
                <div class="all-choose" style="padding: 10px 20px;background: #F5F7FA;">
                    <span style="margin-right: 20px;">选择导出信息</span>
                    <el-checkbox :indeterminate="false" v-model="templates.allChecked"  @change="allChange">全选</el-checkbox>
                </div>
                <div class="template-box" v-for="(item,index) in allCheckList" style="margin: 20px 0;">
                    <el-checkbox v-model="item.allChecked" v-if="allCheckList.length > 1" @change="elAllChange($event,index)">[[item.title]]</el-checkbox><span style="color:#707070;margin-left: 10px;">(已选[[filterCheckNum(index)]])</span>
                    <div class="check-row">
                        <el-checkbox  v-for="(citem,cindex) in item.box" v-model="citem.checked" style="width: 20%;margin-bottom: 10px;"  @change="elChange($event,index)">[[citem.title]]</el-checkbox>
                    </div>
                </div>
            </div>
            <div v-if="template_type == 1">
                <el-table :data="templates.templateList" :header-cell-style="{ 'text-align': 'center' }" :cell-style="{ 'text-align': 'center' }" style="height: 500px;overflow: auto;">
                    <el-table-column  label=" ">
                        <template slot-scope="scope">
                            <el-checkbox @change="changeCheckbox(scope.row)" v-model="scope.row.checkbox"></el-checkbox>
                        </template>
                    </el-table-column>
                    <el-table-column prop="id" label="ID"></el-table-column>
                    <el-table-column prop="name" label="模板名称"></el-table-column>
                    <el-table-column  label="操作" >
                        <template slot-scope="scope">
                            <i class="el-icon-edit style_" style="color: #666;" @click="getColumns(scope.row.id)"></i>
                            <i class="el-icon-delete-solid style_" @click="delColumns(scope.row.id)"></i>
                        </template>
                    </el-table-column>
                </el-table>
            </div>

            <span slot="footer" class="dialog-footer">
                <el-button type="primary" @click="clickExport('')" v-if="template_type == 1">导出</el-button>
                <div style="text-align: center;" v-if="template_type == 2">
                    <el-button type="primary" @click="createTemplate">保存模板</el-button>
                    <el-button  @click="saveTemplateExport('saveExport')">保存模板并导出</el-button>
                </div>
            </span>
        </el-dialog>


        <el-dialog
            title="提示"
            :visible.sync="tipDialogVisible"
            width="600px"
            :append-to-body="appendToBodyType">
            <div class="tip-title">导出程序后台处理中，请于导出下载列表中查看进度！</div>
            <span slot="footer" class="dialog-footer">
                <el-button type="primary" @click="goExportPage">前往导出下载</el-button>
            </span>
        </el-dialog>
    </div>
</template>

<!-- 用法：
1.引用：@ include('public.admin.exportExcel')
2. <export-excel :order-export-dialog.sync="orderExportDialog" request_data="{}" export_type="shopOrder" export_url="export.index.exec"></export-excel> -->
<script>
Vue.component('exportExcel', {
    props: {
        orderExportDialog:{
            default: false
        },
        // 模版类型-自定义
        export_type: {
            type: String,
            default: "shopOrder"
        },
        // 导出接口-自定义
        export_url: {
            type: String,
            default: "export.index.exec"
        },
        request_data: {
            type: Object,
            default: {}
        },
        appendToBodyType: {
            type: Boolean,
            default: false
        }
    },
    delimiters: ['[[', ']]'],
    data(){
        return {
            templates: {
                name: "",
                allChecked: false,
                templateList: [],
                selectedValue: [], // checkbox 选中的值
            },
            allCheckList: [],
            template_type: 1,
            tipDialogVisible: false,
            id: ""
        }
    },
    watch: {
        orderExportDialog(newVal,oldVal) {
            if(newVal) {
                this.templates.name = "";
                this.templates.templateList = []
                this.templateSearchs(1);
            }
        }
    },
    methods: {
        // 获取模版字段
        async getColumns(id) {
            this.template_type = 2;
            this.id = id;
            let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.template.get-columns') !!}", { export_type: this.export_type });
            if(result) {
                let newArr = [];
                for(let item in data) {
                    let columns = []
                    for(let citem in data[item]) {
                        columns.push({
                            title: data[item][citem],
                            value: citem
                        })
                    }
                    newArr.push({
                        title: item,
                        box: columns
                    })
                }
                this.allCheckList = newArr;
                if(id) {
                    this.editColum(id);
                }
            }
        },
        // 保存模板
        async createTemplate(type) {
            if(this.templates.name == "") {
                this.$message.error("名称不能为空");
                return
            }
            let columns = [];
            for(let item of this.allCheckList) {
                for(let citem of item.box) {
                    if(citem.checked) {
                        columns.push(citem.value)
                    }
                }
            }
            let url = this.id ? "{!! yzWebUrl('export.template.update') !!}" : "{!! yzWebUrl('export.template.create') !!}"
            let { data : { data , result, msg} } = await this.$http.post(url, {
                export_type: this.export_type,
                name: this.templates.name,
                columns,
                id: this.id
            });
            if(result) {
                this.$message.success(msg);
                this.template_type = 1;
                this.templates.name = "";
                if(type == "saveExport") {
                    this.clickExport(data.id);
                }else {
                    this.templateSearchs(1);
                }
            }else {
                this.$message.success(msg);
            }
        },
        // 编辑
        async editColum(id) {
            let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.template.get-detail') !!}", {
                export_type: this.export_type,
                id
            });
            if(result) {
                this.templates.name = data.name;
                for(let item of this.allCheckList) {
                    for(let citem of item.box) {
                        if(data.columns.filter(zItem => zItem == citem.value).length) {
                            this.$set(citem,"checked",true)
                        }
                    }
                    if(item.box.filter(xItem => xItem.checked).length == item.box.length) {
                        this.$set(item,"allChecked",true)
                    }
                    if(item.allChecked) {
                        this.templates.allChecked = true;
                    }else {
                        this.templates.allChecked = false;
                    }
                }
                this.template_type = 2;
            }
        },
        // 删除
        delColumns(id) {
            this.$confirm('此操作将永久删除该数据, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(async () => {
                let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.template.delete') !!}", {
                    export_type: this.export_type,
                    id
                });
                if(result) {
                    this.$message.success(msg);
                    this.templateSearchs(1);
                }
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消删除'
                });
            });
        },
        // 选中数据
        filterCheckNum(index) {
            let newArr = this.allCheckList[index].box.filter(item => item.checked);

            let newAllCheckArr = this.allCheckList.filter(item => item.allChecked);
            if(newAllCheckArr.length == this.allCheckList.length) {
                this.templates.allChecked = true;
            }else {
                this.templates.allChecked = false;
            }

            return newArr.length
        },
        // 全选
        allChange(event) {
            for(let item of this.allCheckList) {
                this.$set(item,"allChecked",event)
                for(let citem of item.box) {
                    this.$set(citem,"checked",event)
                }
            }
        },
        // 模板子元素全选
        elAllChange(event,index) {
            for(let item of this.allCheckList[index].box) {
                this.$set(item,"checked",event)
            }
        },
        elChange(event,index) {
            if(!event) {
                this.$set(this.allCheckList[index],"allChecked",false)
            }else {
                let newArr = this.allCheckList[index].box.filter(item => item.checked);
                if(newArr.length == this.allCheckList[index].box.length) {
                    this.$set(this.allCheckList[index],"allChecked",true)
                }else {
                    this.$set(this.allCheckList[index],"allChecked",false)
                }
            }
        },
        async templateSearchs(page) {
            let {data : { result , data , msg}} = await this.$http.post("{!! yzWebUrl('export.template.get-list') !!}",{
                page,
                export_type: this.export_type
            })
            if(result) {
                this.templates.templateList = data
                // this.templates.current_page = data.pageList.current_page;
                // this.templates.total = data.pageList.total;
                // this.templates.per_page = data.pageList.per_page;

                // for(let citem of data) {
                //     for(let item of this.templates.selectedValue) {
                //         if(item == citem.id) {
                //             console.log(item,citem.id,888888888);
                //             this.$set(citem,"checkbox",true);
                //         break
                //         }else {
                //             console.log(item,citem.id,000000000);
                //             this.$set(citem,"checkbox",false);
                //         }
                //     }
                // }
            }else {
                this.$message.error(msg)
            }
        },
        // 是否选中
        changeCheckbox(item) {
            for(let citem of this.templates.templateList) {
                if(citem.id !== item.id) {
                    this.$set(citem,"checkbox",false);
                }
            }
        },
        async clickExport(id) {
            let { data : { data , result, msg} } = await this.$http.post(`{!!  request()->getSchemeAndHttpHost().yzUrl('') !!}` + this.export_url, {
                export_type: this.export_type,
                export_search: this.request_data,
                template_id: id ? id : this.templates.templateList.filter(item => item.checkbox).map(citem => {
                    return citem.id
                })[0]
            });
            if(result) {
                this.$message.success(msg);
                this.saveTemplateExport();
            }
        },
        closeDialog() {
            this.$emit("update:orderExportDialog", false);
            this.template_type = 1;
        },
        saveTemplateExport(type) {
            if(type == 'saveExport') {
                this.createTemplate(type);
            }else {
                this.template_type = 1;
                this.$emit("update:orderExportDialog", false);
                setTimeout(() => {
                    this.tipDialogVisible = true
                }, 500)
            }
        },
        goExportPage() {
            let url = "{!! yzWebUrl('export.index.index') !!}";
            window.location.href = url;
        }
    },
    template: '#exportExcelId'
});
</script>

<style scoped>
    .style_ {
        color: rgb(102, 102, 102);
        cursor: pointer
    }

    .template-title {
        margin-right: 20px;
    }

    .tip-title {
        margin: 30px 0 30px 5px;
    }

    .el-dialog__header {
        padding: 25px 25px 0 25px;
    }

    .el-dialog__body {
        padding: 0 20px;
    }

    .el-table::before {
        height: 0;
    }
</style>
