<style>
  .dialog-box {
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 2001;
    background-color: rgba(0, 0, 0, .1);
  }
</style>
<template id="printme">
  <el-dialog :visible="show" :before-close="clone">
    <div v-if="display_type==1" style="border-radius:10px;background-color: #fff;max-height: 90vh;overflow-y: auto;" v-loading="loading">
      <div ref="edit">
        <div style="color:#101010;padding: 10px 20px;">
          <div style="margin:10px 0;font-size: 18px;">发票明细</div>
          <div style="margin:10px;display:flex;flex-wrap:wrap;font-size: 14px;">
            <div style="font-weight:700;width:100%;">购货单位</div>
            <div style="padding: 0 10px;width: 100%;display:flex;flex-wrap:wrap;">
              <item l="名称" :c="purchase.name"></item>
              <item l="纳税人识别号" :c="purchase.tax_number"></item>
              <item l="地址/电话" :c="(purchase.register_address || '') + (purchase.register_address&&purchase.register_mobile?' / ':'') + (purchase.register_mobile || '')"></item>
              <item l="开户行/账号" :c="(purchase.bank || '') +' ' + (purchase.bank_admin || '')"></item>
              <item l="手机" :c="purchase.mobile"></item>
              <item l="邮箱" :c="purchase.email"></item>
            </div>

          </div>

          <div style="padding:10px;display:flex;flex-wrap:wrap;">
            <div style="font-weight:600;width:100%;">销货单位</div>
            <div style="padding: 0 10px;width: 100%;display:flex;flex-wrap:wrap;">
              <item l="名称" :c="sale.enterprise_name"></item>
              <item l="纳税人识别号" :c="sale.taxpayer_number"></item>
              <item l="地址/电话" :c="sale.sale_address" t="1" :c1="sale.sale_mobile"></item>
              <item l="开户行/账号" :c="(sale.sale_bank || '') + ' ' + (sale.sale_bank_admin || '')"></item>
              <div style="width:33.3%;margin: 10px 0;">收款人：[[sale.billing_payee]]</div>
              <div style="width:33.3%;margin: 10px 0;">复核人：[[sale.invoice_reviewer]]</div>
              <div style="width:33.4%;margin: 10px 0;">开票人：[[sale.invoice_drawer]]</div>
            </div>
          </div>

          <div style="padding-left:10px;margin:20px 10px;">
            <div style="display:flex;flex-wrap:wrap;">
              <div style="width:25%;">价税合计：[[total_tax_price]]</div>
              <div style="width:25%;">金额：[[total_amount]]</div>
              <div style="flex:1;">税额：[[total_tax_amount]]</div>
            </div>
          </div>

          <div style="text-align:center;margin-bottom:40px;">
            <div style="height:40px;line-height:40px;display:flex;background:#f0f0f0 !important;border-radius:4px;-webkit-print-color-adjust: exact;print-color-adjust: exact;color-adjust: exact;">
              <div style="flex:1.2;">商品</div>
              <div style="flex:1;">规格</div>
              <div style="flex:1;">数量</div>
              <div style="flex:1;">单价(含税)</div>
              <div style="flex:1;">金额(含税)</div>
              <div style="flex:1;">税率</div>
              <div style="flex:1;">税额</div>
            </div>
            <div style="height: 80px;display:flex;border-radius:4px;border-bottom: 1px solid #ECECEC;align-items: center;text-align: center;padding: 0 4px;" v-for="(item,i) in goods" :key="i">
              <div style="flex:1.2;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                <span>
                  <el-popover v-if="hand_movement"  class="item" placement="top" effect="light">
                  <div style="text-align:center;">
                    <div style="text-align:left;margin-bottom:10px;font-weight:900">修改商品名称</div>
                    <el-input v-model="change_title" style="width:400px" size="small"></el-input>
                    <el-button size="small" @click="titleChange(item,i,3)">确定</el-button>
                  </div>
                  <a slot="reference">
                    <i class="el-icon-edit edit-i" title="点击编辑" ></i>
                  </a>
                </el-popover>
                  [[item.title]]
                </span><br>
                <span v-if="item.is_refund&&item.order_goods&&item.order_goods.belongs_to_refund_goods_log&&item.order_goods.belongs_to_refund_goods_log.belongs_to_order_fund" style="color: #ff1717">
                  [[ item.order_goods.belongs_to_refund_goods_log.belongs_to_order_fund.refund_type_name ]]
                </span>
              </div>
              <div style="flex:1;">[[item.spec]]</div>
              <div style="flex:1;">
                <el-popover v-if="hand_movement" class="item" placement="top" effect="light">
                  <div style="text-align:center;">
                    <div style="text-align:left;margin-bottom:10px;font-weight:900">修改数量</div>
                    <el-input v-model="change_number" style="width:400px" size="small"></el-input>
                    <el-button size="small" @click="
                    titleChange(item,i,1)">确定</el-button>
                  </div>
                  <a slot="reference">
                    <i class="el-icon-edit edit-i" title="点击编辑" ></i>
                  </a>
                </el-popover>
                [[item.total]]
              </div>
              <div style="flex:1;">
                <el-popover v-if="hand_movement"  class="item" placement="top" effect="light">
                  <div style="text-align:center;">
                    <div style="text-align:left;margin-bottom:10px;font-weight:900">修改单价</div>
                    <el-input v-model="change_number" style="width:400px" size="small"></el-input>
                    <el-button size="small" @click="titleChange(item,i,2)">确定</el-button>
                  </div>
                  <a slot="reference">
                    <i class="el-icon-edit edit-i" title="点击编辑" ></i>
                  </a>
                </el-popover>
                [[item.unit_price]]
              </div>
              <div style="flex:1;">
                [[item.money]]
              </div>
              <div style="flex:1;">[[item.tax_rate]]</div>
              <div style="flex:1;">[[item.tax_amount]]</div>
            </div>
          </div>
        </div>
      </div>
      <div style="text-align:center;margin:10px 0;">
        <el-button @click.stop="clone">取消</el-button>
        <el-button type="primary" @click.stop="reInvoicing">确认开票</el-button>
      </div>
    </div>
    <div v-if="display_type==2" style="text-align: center">
      <div style="margin-top: -50px;font-size: 18px;"><b>短信验证</b></div>
      <div >[[code_msg]]</div>
      <div class="flex flex-a-c" style="position:relative ">
        <span>验证码</span>
        <el-input style="width: 200px" v-model="code" placeholder="验证码"></el-input>
        <el-link @click="sendVerify" style="color:#13c7a7;">重新发送</el-link>
      </div>
      <div style="color: red;text-align: left">
        1.当页面提示：验证码已发送到手机号：xxx时说明已经成功发送验证码了，若没有提示信息请点击一到两次重新发送，
        <br>若还是不行请联系客服处理，不要多次点击发送，如果点击次数过多会被税务局拉黑导致功能无法使用。 <br>
        2.⼀般⼀天(24小时)验证⼀次，后续就无需再次验证。<br>
        3.验证过后请不要登录电子税局否则会验证失效，多次验证同样会被税务局拉黑请谨慎操作!!!
      </div>
      <div style="text-align:center;margin:10px 0;">
        <el-button @click.stop="display_type=1">取消</el-button>
        <el-button type="primary" @click.stop="checkVerify">确认</el-button>
      </div>
    </div>
    <div v-if="display_type==3" style="text-align: center">
      <div style="margin-top: -50px;font-size: 18px;"><b>人脸验证</b></div>

      <div style="margin-top: 20px">
        1.使用电子税务APP右上角的扫一扫功能，进行扫码验证，完成验证后约5秒后点击查询结果就可开票。<br>
        2.若查询结果提示二维码过期，请重新获取二维码再试。
      </div>
      <div>
        <el-image
                style="width: 400px; height: 400px"
                :src="auth_qrcode">
        </el-image>
      </div>
      <div class="flex flex-a-c" style="position:relative ">
      </div>
      <el-button type="primary" @click.stop="qrcodeResult">查询结果</el-button>
      <el-button type="primary" @click.stop="authQrcode">重新获取二维码</el-button>
    </div>
    </div>
  </el-dialog>
</template>

<script>
  Vue.component("invoice", {
    template: "#printme",
    model: {
      prop: 'checked',
      event: 'click'
    },
    props: {
      value: {
        type: Boolean,
        default: false
      },
      show:{
        type: Boolean,
        default: false
      },
      url:{
        type: String
      },
      order_id:{
        default:""
      },
      type:{
        type: String,
        default:""
      },
      form:{
        type: Object
      }
    },
    delimiters: ["[[", ']]'],
    data() {
      return {
        dialogVisible: false,
        purchase: {},
        sale: {},
        goods: [],
        total_amount: "",
        total_tax_amount: "",
        total_tax_price: "",
        loading:false,
        invoice_type:"",
        change_number:"",
        api_type:0,
        display_type:1,
        code_msg:'',
        code:'',
        code_title:'发送验证码',
        auth_qrcode:'',
        change_title:'',
      }
    },
    watch:{
      show(nVal){
        if(nVal) this.getdata();
      }
    },
    methods: {
      clone(){
        this.$emit('update:show', false)
      },
      open(){
        this.$emit("click",true)
      },
      getdata() {
        let json = {id: this.order_id};
        if (this.form) json.data = this.form;
        this.loading = true;
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.preview') !!}", json).then(({data:{data, result,msg} }) => {
          this.loading = false;
          if (result == 1) {
            this.hand_movement = data.hand_movement;
            this.purchase = data.purchase;
            this.sale = data.sale;
            this.goods = data.goods || [];
            this.total_amount = data.total_amount;
            this.total_tax_amount = data.total_tax_amount;
            this.total_tax_price = data.total_tax_price;
            this.invoice_type = data.invoice_type;
            this.api_type = data.api_type;
          }else this.$message.error(msg)
        })
      },
      printme() {
        global_Html = document.body.innerHTML;
        document.body.innerHTML = this.$refs.edit.innerHTML;
        document.body.style.overflow = "hidden";
        window.print();
        window.location.href = this.url;
      },
      electronicsInvoice(){
        this.clone();
        this.$message.success("提交开票成功");
      },
      getInitJson(){
        let json ={order_id:this.order_id};
        return json;
      },
      invoicing() {
        if (this.api_type == 2) {
          this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.is-sms-code') !!}")
                  .then(({data: {result, msg, data}}) => {
                    this.loading = false;
                    if (result == 1) {
                      if(data.is_sms_code == 1)
                      {
                        this.reInvoicing()
                      }
                      if (data.is_sms_code == 2) {
                        console.log(data)
                        this.display_type = 2
                        this.sendVerify()
                        return
                      }
                      if(data.is_sms_code == 3)
                      {
                        this.display_type = 3
                        this.riskStatus()
                        return
                      }
                    } else {
                      this.$message.error(msg);
                    }
                  })
        } else {
          this.reInvoicing()
        }
      },
      reInvoicing() {
        this.loading = true;
        // let json = this.getInitJson();
        let json = this.form?{data:this.form}:{id: this.order_id};
        if (this.goods) {
          json.goods = this.goods
        }
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.confirm-invoicing') !!}", json)
                .then(({data: {result, msg, data}}) => {
                  this.loading = false;
                  if (result == 1) {
                    if (this.invoice_type == 0) {
                      this.electronicsInvoice();
                    } else this.printme();
                  } else {
                    //先开票，报错再跳转到短信验证页面
                    this.disply_type = 2;
                    this.$message.error(msg);
                  }
                })
      },
      sendVerify(){
        this.code_title = '重新发送验证码'
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.send-verify') !!}")
                .then(({data: {result, msg, data}}) => {
                  if (result == 1) {
                    this.code_msg = data.message
                  } else {
                    this.$message.error(msg);
                  }
                })
      },
      checkVerify() {
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.check-verify') !!}", {code:this.code})
                .then(({data: {result, msg, data}}) => {
                  if (result == 1) {
                    if (data.success == true) {
                      this.display_type = 3
                      this.riskStatus()
                    } else {
                      this.display_type = 1
                      this.reInvoicing();
                    }
                  } else {
                    this.display_type = 1
                    this.$message.error(msg);
                  }
                })
      },
      authQrcode() {
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.auth-qrcode') !!}", {code:this.code})
                .then(({data: {result, msg, data}}) => {
                  if (result == 1) {
                    if (data.success == true) {
                      this.auth_qrcode = data.qrcodeUrl
                    } else {
                      this.display_type = 2
                      this.$message.error(data.message);
                    }
                  } else {
                    this.display_type = 2
                    this.$message.error(msg);
                  }
                })
      },
      riskStatus() {
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.risk-status') !!}", {code:this.code})
                .then(({data: {result, msg, data}}) => {
                  if (result == 1) {
                    if (data.success == true) {
                      this.display_type = 3
                      this.authQrcode()
                    } else {
                      this.display_type = 1
                      this.reInvoicing()
                    }
                  } else {
                    this.$message.error(msg);
                  }
                })
      },
      qrcodeResult() {
        this.$http.post("{!! yzWebFullUrl('plugin.invoice.admin.invoicing_order.qrcode-result') !!}")
                .then(({data: {result, msg, data}}) => {
                  if (result == 1) {
                    if (data.success == true) {
                      switch (data.authResult) {
                        case 1:
                          this.$message.success(data.message)
                          this.display_type = 1
                          this.reInvoicing()
                          break
                        default :
                          this.$message.error(data.message)
                          this.display_type = 3
                          break
                      }
                    } else {
                      this.display_type= 3
                      this.$message.error(msg);
                    }
                  } else {
                    this.display_type= 3
                    this.$message.error(msg);
                  }
                })
      },
      titleChange(item,i,type) {
        switch (type) {
          case 1:
            this.goods[i].total = Number(this.change_number).toFixed(2);
            this.goods[i].money = (Number(this.goods[i].unit_price) * Number(this.goods[i].total)).toFixed(2);
            this.goods[i].tax_amount = (Number(this.goods[i].unit_price) / (1 + Number(this.goods[i].tax_rate) / 100) * Number(this.goods[i].tax_rate) / 100 * (this.goods[i].total)).toFixed(2);
            break;
          case 2:
            this.goods[i].unit_price = Number(this.change_number).toFixed(2);
            this.goods[i].money = (Number(this.goods[i].unit_price) * Number(this.goods[i].total)).toFixed(2);
            this.goods[i].tax_amount = (Number(this.goods[i].unit_price) * (this.goods[i].total) / (1 + Number(this.goods[i].tax_rate) / 100) * Number(this.goods[i].tax_rate) / 100).toFixed(2);
            break;
          case 3:
            this.goods[i].title = this.change_title
            this.goods[i].change_title = this.change_title
            return;
        }
        this.total_tax_price = 0;
        this.total_amount = 0;
        this.total_tax_amount = 0;
        for (let n = 0; n < this.goods.length; n++) {
          this.total_tax_price += Number(this.goods[n].money)
          this.total_amount += Number(this.goods[n].unit_price) / (1 + Number(this.goods[n].tax_rate) / 100) * this.goods[n].total
          this.total_tax_amount += Number(this.goods[n].tax_amount)
        }
        this.total_tax_price = (this.total_tax_price).toFixed(2)
        this.total_amount = (this.total_amount).toFixed(2)
        this.total_tax_amount = (this.total_tax_amount).toFixed(2)
      }
    },
    components: {
      item: {
        delimiters: ['[[', ']]'],
        template: `
        <div style="width:50%;display:flex;line-height:1;margin:10px 0;" v-if="t==1">
            <div>[[l]]：</div>
            <div>
              <div>[[c]]</div>
              <div style="margin-top:10px;" v-if="c1">[[c1]]</div>
            </div>
        </div>
        <div style="width:50%;margin:10px 0;" v-else>[[l]]：[[c]]</div>`,
        props: ["l", "c", "c1", "t"]
      }
    },
  })
</script>