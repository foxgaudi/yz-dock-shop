<style>
    .time-list {
        display: contents;
    }
    .button-color {
        height: 40px;
    }
    .active-colors {
        background: #FFF !important;
        border-color: #DCDFE6 !important;
        color: #666 !important;
    }
</style>
<template id="timeCard">
    <Header class="time-list">
        <el-date-picker
                v-model="times"
                value-format="timestamp"
                type="datetimerange"
                range-separator="至"
                start-placeholder="开始日期"
                end-placeholder="结束日期"
                @change="changeTime">
        </el-date-picker>
        <el-button style="height: 40px;" @click="switchSearchTime(1)" type="success" plain :class="timeType == 1 ? 'button-color' : 'active-colors'">今</el-button>
        <el-button style="height: 40px;" @click="switchSearchTime(2)" type="success" plain :class="timeType == 2 ? 'button-color' : 'active-colors'">昨</el-button>
        <el-button style="height: 40px;" @click="switchSearchTime(3)" type="success" plain :class="timeType == 3 ? 'button-color' : 'active-colors'">近7天</el-button>
        <el-button style="height: 40px;" @click="switchSearchTime(4)" type="success" plain :class="timeType == 4 ? 'button-color' : 'active-colors'">近30天</el-button>
        <el-button style="height: 40px;" @click="switchSearchTime(5)" type="success" plain :class="timeType == 5 ? 'button-color' : 'active-colors'">近1年</el-button>
    </Header>
</template>
<script>
    Vue.component('timeCard', {
        template: '#timeCard',
        delimiters: ['[[', ']]'],
        props: {
            days_of_last_year: {
                type: null,
                default: "365"//默认一年为365天，如有特殊需求（参考：plugins/audio-book/views/admin/book/list.blade.php）在对应js返回具体天数
            },
            initial_time_type: {
                type: null,
                default: 0//默认一年为365天，如有特殊需求（参考：plugins/audio-book/views/admin/book/list.blade.php）在对应js返回具体天数
            },
            initial_times: {
                type: Array,
                default: function() {
                    return [];
                }
            }
        },
        data() {
            return {
                times: [],
                timeType:0
            }
        },
        mounted() {
            // console.log(this.days_of_last_year,'days_of_last_year');

            if (this.initial_times.length > 0) {
                this.times = this.initial_times;
            }else {
                this.switchSearchTime(this.initial_time_type);
            }
        },
        methods: {
            getStartEndTime (num = 1) {
                // 一天的毫秒数
                const MillisecondsADay = 24*60*60*1000 * num;
                // 今日开始时间戳
                const todayStartTime = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
                // 今日结束时间戳
                const todayEndTime = new Date(new Date().setHours(23,59,59,999)).getTime();

                // 昨日开始时间戳
                const yesterdayStartTime = todayStartTime - MillisecondsADay;
                // 昨日结束时间戳
                const yesterdayEndTime = todayEndTime - MillisecondsADay;

                return [parseInt(yesterdayStartTime),parseInt(num > 1 ? todayEndTime : yesterdayEndTime)]
            },

            switchSearchTime(timeType) {
                switch (timeType) {
                    //今天
                    case 1:
                        timeArr = this.getStartEndTime(0);
                        break;
                    //昨天
                    case 2:
                        timeArr = this.getStartEndTime(1);
                        break;
                    //近7天
                    case 3:
                        timeArr = this.getStartEndTime(7);
                        break;
                    //近30天
                    case 4:
                        timeArr = this.getStartEndTime(30);
                        break;
                    //近1年
                    case 5:
                        timeArr = this.getStartEndTime(this.days_of_last_year);
                        break;
                    default:
                        timeArr = [];
                        break;
                }
                this.times = timeArr;

                if(this.timeType == timeType){
                    this.timeType = 0;
                    this.times = [];
                } else {
                    this.timeType = timeType;
                }

            },
            changeTime() {
                this.timeType = 0;
            }
        }
    })
    
</script>
