<!-- <script src="//map.qq.com/api/js?v=2.exp&key=HRSBZ-QOWAQ-23556-BO7ZX-VDO3S-UTB6G"></script>
<script src="https://map.qq.com/api/gljs?v=1.exp&libraries=service&key=HRSBZ-QOWAQ-23556-BO7ZX-VDO3S-UTB6G"></script> -->
<script type="text/javascript" src="//map.qq.com/api/js?v=2.exp&key={!!\Setting::get('plugin.min_app.qq_map_web_key')!!}"></script>
<script type="text/javascript" src="https://map.qq.com/api/gljs?v=1.exp&libraries=service&key={!!\Setting::get('plugin.min_app.qq_map_web_key')!!}"></script>
<!-- 
    使用步骤
    1.引用路径: @ include('public.admin.map_cpn')
    2.引用组件：<map_cpn :map-dialog-visible.sync="mapDialogVisible" :lat="39.90481372526591" :lng="116.38953575348262" v-if="mapDialogVisible" @map_data="getMapData" @search_map_data="getSearchMapData"></map_cpn>
        字段解释：
        2.1： mapDialogVisible: 控制整个组件的显示和关闭，Boolean布尔值，必填
        2.2： lat：经度，回显使需要用到，number类型，可以不传，不传默认空
        2.3： lng：纬度，回显使需要用到，number类型，可以不传，不传默认空
        2.4:  getMapData: 点击弹出中的确定，返回的选中的数据，对象类型，必填
        2.5： getSearchMapData：点击搜索的时候，返回搜索的数据，对象类型，可以不填
 -->
<style>
    .name-box {
        max-height: 400px;
        overflow: auto;
    }
    .name-li {
        margin-bottom: 10px;
        padding: 10px;
        cursor: pointer;
    }

    .name-li:hover { 
        background-color: #eaf8f5;
    }

    .el-dialog__body {
        padding-top: 0 !important;
    }

    .pane-bottom {
        margin: 20px;
        text-align: center;
    }
</style>
<template id="map_cpn">
    <div style="padding: 20px;">
    <el-dialog
        title="选择坐标"
        :visible.sync="mapDialogVisible"
        :before-close="closeDialog"
        center
        width="70%">
        <div style="display: flex;margin:0 0 20px 0">
            <el-popover
                placement="bottom"
                width="500"
                trigger="manual"
                v-model="visible">
                <div class="name-box">
                    <div class="name-li" v-for="(item,index) in options" @click="clickPos(item)">[[item.title]]([[item.address]])</div>
                </div>
                <el-input v-model="keyword" id="place" slot="reference" style="width: 500px;" @focus="visible = false" ></el-input>
            </el-popover>
            <el-button  @click="searchKeyword" style="margin-left: 20px;">搜索</el-button>
        </div>
        <div ref='thismap' id="map2" style="height: 600px; width: 100%;"></div>
        <div class="pane-bottom">
            <el-button type="primary" @click="sureMap">确定</el-button>
            <el-button @click="closeDialog">取消</el-button>
        </div>
    </el-dialog>
    </div>
</template>
<script>
    let searchService = null
    let geocoder  = null
    let qq_map_web_sign = "{!!\Setting::get('plugin.min_app.qq_map_web_sign')!!}"
    Vue.component('map_cpn', {
        delimiters: ['[[', ']]'],
        props: {
            mapDialogVisible: {
                type: Boolean,
                default: false
            },
            lat: {
                type: String | Number,
                default: ""
            },
            lng: {
                type: String | Number,
                default: ""
            },
        },
        data() {
            return {
                map2: null,
                keyword: "",
                markerLayer: "",
                options: [],
                visible: false,
                selectMapData: {},
                qq_map_web_sign: qq_map_web_sign
            }
        },
        mounted(){
            this.init();
        },
        methods: {
             //百度坐标转腾讯（传入经度、纬度）
            bd_decrypt(bd_lng, bd_lat) {
                var X_PI = (Math.PI * 3000.0) / 180.0;
                var x = bd_lng - 0.0065;
                var y = bd_lat - 0.006;
                var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * X_PI);
                var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * X_PI);
                var gg_lng = z * Math.cos(theta);
                var gg_lat = z * Math.sin(theta);
                return { lng: gg_lng.toFixed(6), lat: gg_lat.toFixed(6)  };
            },
             //腾讯坐标转百度（传入经度、纬度）
            bd_encrypt(gg_lng, gg_lat) {
                var X_PI = (Math.PI * 3000.0) / 180.0;
                var x = gg_lng,
                y = gg_lat;
                var z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * X_PI);
                var theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * X_PI);
                var bd_lng = z * Math.cos(theta) + 0.0065;
                var bd_lat = z * Math.sin(theta) + 0.006;
                return {
                    lat: bd_lat,
                    lng: bd_lng
                };
            },
            clickPos(item) {
                let location = {
                    lat: item.location.lat,
                    lng: item.location.lng
                };
                this.markerLayer.updateGeometries([
                    {
                    "id": "1",
                    "position": new TMap.LatLng(location.lat, location.lng),
                    }
                ])
                this.map2.setCenter(location);
                this.keyword = item.title;
                item.baidu_location = {
                    text: "腾讯坐标转百度",
                    ...this.bd_encrypt(item.location.lng,item.location.lat)
                }
                this.selectMapData = item;
                this.$emit("map_data", this.selectMapData); //确定选中的数据
                this.visible = false;
            },
            searchKeyword() {
                const suggest = new TMap.service.Suggestion({
                    pageSize: 20, // 返回结果每页条目数
                    regionFix: false, // 搜索无结果时是否固定在当前城市
                    servicesk: "", // key
                });
                suggest.getSuggestions({
                    keyword: this.keyword,
                    servicesk: this.qq_map_web_sign, // 签名（可在腾讯地图开放平台中拿） "a5xBnYK5GDq4gMjkutAAATNqZf0Gw5VY"
                }).then((result) => {
                    console.log(result, "搜索");
                    this.options = result.data;
                    this.visible = result.count > 0 ? true : false;
                    this.$emit("search_map_data", result); //搜索的数据
                }).catch(err => {
                    this.$emit("search_map_data", err);
                    this.$message.error(err.message);
                });
            },
            sureMap() {
                this.$emit("map_data", this.selectMapData); //确定选中的数据
                this.$emit("update:mapDialogVisible", false);
            },
            async init() {
                this.$nextTick(() => {
                    var center = new TMap.LatLng(this.bd_decrypt(this.lng,this.lat).lat, this.bd_decrypt(this.lng,this.lat).lng);
                    // 初始化地图
                    this.map2 = new TMap.Map("map2", {
                        rotation: 0, // 设置地图旋转角度
                        pitch: 0, // 设置俯仰角度（0~45）
                        zoom: 16, // 设置地图缩放级别
                        cursor: 'pointer',
                        center: center // 设置地图中心点坐标
                    });

                    // 创建并初始化MultiMarker
                    this.markerLayer = new TMap.MultiMarker({
                        map: this.map2,
                        // 样式定义
                        styles: {
                            "myStyle": new TMap.MarkerStyle({
                                "width": 35,
                                "height": 50,
                                // "src": 'C:/Users/16/Desktop/demo.jpeg',
                                // 焦点在图片中的像素位置
                                "anchor": {x: 0, y: 50}
                            })
                        },
                        // 点标记数据数组
                        geometries: [{
                            "id": "1", // 点标记唯一标识
                            "styleId": 'myStyle', // 指定样式id
                            "position": center,
                            "properties": {
                                "title": ""
                            }
                        }]
                    });
                    
                    // 鼠标点击获取定位
                    // this.map2.on("click",(evt) => {
                    //     let lat = evt.latLng.getLat().toFixed(6);
                    //     let lng = evt.latLng.getLng().toFixed(6);
                    //     this.$emit("click_map_data", evt); //鼠标点击的数据
                    //     this.selectMapData = evt;
                    //     //修改点标记的位置
                    //     this.markerLayer.updateGeometries([
                    //         {
                    //         "styleId":"myStyle",
                    //         "id": "1",
                    //         "position": new TMap.LatLng(lat, lng),
                    //         }
                    //     ])
                    // })
                })
            },
            closeDialog() {
                this.$emit("update:mapDialogVisible", false);
            }
        },
        template: '#map_cpn'
    });
</script>

<style scoped></style>