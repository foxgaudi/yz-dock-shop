@include('public.admin.box-item')
<style scoped>
  .time-list {
    display: contents;
  }
  .button-color {
    height: 40px;
  }
  .active-colors {
    background: #FFF !important;
    border-color: #DCDFE6 !important;
    color: #666 !important;
  }
  .search-box{display: flex;flex-wrap: wrap;}
  .search-item{width: 240px;margin: 10px;}
  .input-notice{margin-top: 20px}
  .page{width: calc(100% - 275px);position: fixed;bottom: 0;z-index: 199;box-sizing: border-box;box-shadow: 0px -1px 10px rgb(0 0 0 / 10%);background-color: #fff;height: 60px;display: flex;justify-content: center;align-items: center;border-radius: 10px 10px 0 0;}
</style>
<template id="table-template">
  <div>
    <box-item :text="searchText">
      <div class="search-box">
        <template v-for="(item,i) in searchList">
          <span v-if="item.notice" class="input-notice">[[item.notice]]</span><el-input v-model="search[item.key]" :placeholder="item.p" class="search-item" clearable  @keyup.enter.native="searchValue" v-if="!item.type || item.type == 'input'"></el-input>
          <el-select v-model="search[item.key]" :placeholder="item.p" class="search-item" clearable v-else-if="item.type == 'select'">
            <el-option v-for="(option,index) in item.options" :key="index" :label="option.label" :value="option.value" clearable></el-option>
          </el-select>
          <el-date-picker v-model="date" type="datetimerange" :picker-options="pickerOptions" range-separator="至" start-placeholder="开始日期" end-placeholder="结束日期"
            align="right" @change="changeDate" clearable v-else-if="item.type == 'date'" value-format="timestamp" style="margin:10px;">
          </el-date-picker>
          <div v-else-if="item.type == 'newdate'">
            <el-date-picker style="margin:10px;"
                            v-model="times"
                            value-format="timestamp"
                            type="datetimerange"
                            range-separator="至"
                            start-placeholder="开始日期"
                            end-placeholder="结束日期">
            </el-date-picker>
            <el-button style="margin:10px;" @click="switchSearchTime(1)" type="success" plain
                       :class="timeType == 1 ? 'button-color' : 'active-colors'">今
            </el-button>
            <el-button style="margin:10px;" @click="switchSearchTime(2)" type="success" plain
                       :class="timeType == 2 ? 'button-color' : 'active-colors'">昨
            </el-button>
            <el-button style="margin:10px;" @click="switchSearchTime(3)" type="success" plain
                       :class="timeType == 3 ? 'button-color' : 'active-colors'">近7天
            </el-button>
            <el-button style="margin:10px;" @click="switchSearchTime(4)" type="success" plain
                       :class="timeType == 4 ? 'button-color' : 'active-colors'">近30天
            </el-button>
            <el-button style="margin:10px;" @click="switchSearchTime(5)" type="success" plain
                       :class="timeType == 5 ? 'button-color' : 'active-colors'">近1年
            </el-button>
          </div>
          <div style="margin: 10px;" v-else-if="item.type=='slot'">
            <slot :name="item.name"></slot>
          </div>
        </template>
        <div style="margin:10px;">
          <el-button v-if="isInit" @click="initSearch">重置</el-button>
          <el-button v-if="exportUrl" @click="exportData">导出EXCEL</el-button>
          <slot name="search-btn"></slot>
          <el-button type="primary" icon="el-icon-search" v-if="isSearch" @click="searchValue" :disabled="loading">搜索</el-button>
        </div>
      </div>
      <slot name="btn" slot="btn"></slot>
    </box-item>
    <box-item :text="tableText" :button="tableButton">
      <slot name="teble-btn" slot="btn"></slot>
      <el-table :data="tableData" style="width: 100%;" v-loading="loading">
        <template v-for="(item,i) in tableList">
          <el-table-column :label="item.label" :align="item.align || 'center'" v-if="item.type == 'slot'">
            <template slot-scope="scope">
              <slot slot-scope="scope" :row="scope.row" :name="item.name"></slot>
            </template>
          </el-table-column>
          <el-table-column :prop="item.prop" :label="item.label" :align="item.align || 'center'" v-else></el-table-column>
        </template>
      </el-table>
    </box-item>
    <vue-page v-if="total > 0">
      <el-pagination @current-change="getdata" :current-page.sync="page" :page-size="per_page" layout="prev, pager, next, jumper" :total="total" background :disabled="loading"></el-pagination>
    </vue-page>
  </div>
</template>
<script>
  Vue.component("table-template", {
    delimiters: ['[[', ']]'],
    props: {
      "search-text": {
        type: String | Array,
        default:"筛选"
      },
      "table-text": {
        type: String | Array,
        default:"记录列表"
      },
      "table-button": {
         type: Array,
      },
      "search-list":{
        type:Array
      },
      "table-list":{
        type:Array
      },
      "is-search":{
        type:Boolean,
        default:true
      },
      "is-init":{
        type:Boolean,
        default:false
      },
      "search_url":{
        type:String
      },
      "params":{
        type:Function | Object
      },
      "data-key":{
        type:String,
        default:"list"
      },
      "tab-date-type":{},
      "export-url":{
        type:String,
        default:""
      },
      "data-format":{
        type:String,
        default:""
      }
    },
    data(){
      function dateData(day){
        function onClick(picker) {
            const end = new Date();
            const start = new Date();
            start.setTime(start.getTime() - 3600 * 1000 * 24 * day);
            picker.$emit('pick', [start, end]);
        }
        return onClick
      }
      return{
        search:{},
        searchData:{},
        date:[],
        pickerOptions: {
          shortcuts: [
            {text: '最近一周',onClick:dateData(7)},
            {text: '最近一个月',onClick:dateData(30)},
            {text: '最近三个月',onClick:dateData(90)}
          ]
        },
        tableData:[],
        loading:false,
        page:1,
        per_page:10,
        total:0,
        amount_total:0,
        times: [],
        timeType: 0,
        days_of_last_year:"365",
      }
    },
    created(){
      this.initdata();
      this.switchSearchTime(5);
      this.getdata();
    },
    methods:{
      initSearch(){
        this.$confirm('此操作会重置所有的筛选条件, 继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          this.$emit("init");
          this.search = {};
        });
      },
      initdata(){
        this.page = 1;
        this.total = 0;
        this.amount_total = 0;
        this.loading = false;
      },
      getJson(dataFormat){
        let params = {};
        if(typeof(this.params) === "function"){
          params = this.params()
        }else if(typeof(this.params) === "object" && !(Array.isArray(this.params))){
          params = this.params;
        }
        if(dataFormat){
          let json = {};
          json[dataFormat] = {...this.searchData,page:this.page,...params};
          return json;
        }
        return {...this.searchData,page:this.page,...params};
      },
      getdata(){
        this.loading = true;
        let data = this.getJson(this.dataFormat);
          if(this.times && this.times.length>0) {
              var newdate = {start:parseInt(this.times[0] / 1000),end:parseInt(this.times[1] / 1000)};
              data.new_time = JSON.stringify(newdate);
          }
          this.$http.post(this.search_url,data).then(({data:{result,msg,data}})=>{
          this.loading = false;
          if(result == 1){
            let list = data[this.dataKey];
            this.tableData = list.data;
            this.total = list.total;
            this.total = list.total;
            this.per_page = list.per_page;
            this.amount_total = data.amount_total;
            this.$emit("gettotal",this.total);
            this.$emit("getamounttotal",this.amount_total);
            this.$emit("getdata",{data,result,msg});
          }else this.$message.error(msg);
        })
      },
      searchValue(){
        this.page = 1;
        this.searchData = this.search;
        this.getdata();
        this.$emit("search",this.search);
      },
      changeDate(data){
        if(this.tabDateType){
          if(data){
            this.search.start = parseInt(data[0] / 1000);
            this.search.end = parseInt(data[1] / 1000);
          }else{
            delete this.search.start;
            delete this.search.end;
          }
          return false;
        }
        if(data) this.search.time = {
          start:parseInt(data[0] / 1000),
          end:parseInt(data[1] / 1000)
        }
        else delete this.search.time;
      },
        getStartEndTime(num = 1) {
            // 一天的毫秒数
            const MillisecondsADay = 24 * 60 * 60 * 1000 * num;
            // 今日开始时间戳
            const todayStartTime = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
            // 今日结束时间戳
            const todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)).getTime();

            // 昨日开始时间戳
            const yesterdayStartTime = todayStartTime - MillisecondsADay;
            // 昨日结束时间戳
            const yesterdayEndTime = todayEndTime - MillisecondsADay;

            return [parseInt(yesterdayStartTime), parseInt(num > 1 ? todayEndTime : yesterdayEndTime)]
        },
        switchSearchTime(timeType) {
            switch (timeType) {
                //今天
                case 1:
                    timeArr = this.getStartEndTime(0);
                    break;
                //昨天
                case 2:
                    timeArr = this.getStartEndTime(1);
                    break;
                //近7天
                case 3:
                    timeArr = this.getStartEndTime(7);
                    break;
                //近30天
                case 4:
                    timeArr = this.getStartEndTime(30);
                    break;
                //近1年
                case 5:
                    timeArr = this.getStartEndTime(this.days_of_last_year);
                    break;
            }
            this.times = timeArr;
            if (this.timeType == timeType) {
                this.timeType = 0;
                this.times = [];
            } else {
                this.timeType = timeType;
            }

        },
      exportData(){
        let data = this.getJson();
          if(this.times && this.times.length>0) {
              var newdate = {start:parseInt(this.times[0] / 1000),end:parseInt(this.times[1] / 1000)};
              data.new_time = JSON.stringify(newdate);
          }
        let url = this.exportUrl;
        for (const key in data) {
          url += `&${key}=${data[key]}`
        }
        window.open(url);
      }
    },
    template: `#table-template`,
  })
</script>

