@extends('layouts.base')
@section('title', '余额批量设置')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>

    <style>

        .el-tabs__item,.is-top{font-size:16px}
        .el-tabs__active-bar { height: 3px;}
        .el-scrollbar__wrap{
            overflow-x: hidden;
        }
        .firstCate{
            width: 100%;
            text-align: center;
        }
        .firstCate .el-checkbox__inner{
            display: none;
        }
        .el-checkbox-group {
            text-align: left;
        }


        .el-checkbox {
            padding-left: 20px;
            text-align: left;
        }

        .active-no-color {
            color: #666;
        }

        .checkbox-pane {
            display: flex;
        }

        .checkbox-color {
            color: #29BA9C;
        }

        .checkbox-name {
            line-height: 19px;
            margin-left: 5px;
            cursor: pointer;
        }

        .el-select-dropdown__list {
            margin-bottom: 20px;
        }

        .category-style {
            text-align:left;
            background-color:#eff3f6;
            border:3px solid #eff3f6;
            padding-left:5px;
        }
        .scrollbar-style {
            height:400px;
            border:3px solid #eff3f6;
            width:auto;
        }


    </style>
    <div class="all">
        <div id="app" v-cloak>
            <div class="vue-crumbs">
                <a @click="goParent">余额批量设置</a> > 余额抵扣设置
            </div>
            <div class="vue-main">
                <div class="vue-main-title">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">余额抵扣设置</div>
                </div>
                <div class="vue-main-form">

                    <el-form ref="form" :model="form" :rules="rules" label-width="15%">
                        <el-form-item label="选择分类" prop="classification">
                            <el-input :value="classification" style="width:60%;" disabled></el-input>
                            <el-button type="primary" @click="visDia()">选择分类</el-button>
                            <el-dialog title="选择分类" :visible.sync="dialogTableVisible" @close="closeDialog">
                                <el-select
                                        default-first-option
                                        value-key="id"
                                        v-model="search_categorys"
                                        filterable
                                        multiple
                                        remote
                                        reserve-keyword

                                        :remote-method="loadCategorys"
                                        :loading="loading"
                                        style="width:100%;margin-bottom:20px">
                                    <el-option v-for="item in categorys" :key="item.id" :label="'[ID:'+item.id+'][分类:'+item.name+']'" :value="item"></el-option>
                                </el-select>
                                <el-row :gutter="0">
                                    <el-col :span="8" >
                                        <div class="category-style">一级分类</div>
                                        <el-scrollbar class="scrollbar-style">
                                            <div v-for="(item, index) in firstCate" class="checkbox-pane" :key="index">
                                                <el-checkbox style="display:block;"  :label="item.id" :value="item.id" name="type" v-model="item.checked" @change="loadTwo($event,item, index)">[[empty]]</el-checkbox>
                                                <span @click="loadSecond(item, index)" :class="item.color ? 'checkbox-color checkbox-name' : 'checkbox-name'">[[item.name]]</span>
                                            </div>
{{--                                            <el-checkbox-group v-model="checkedFirstCate" class="firstCate" @change="loadSecond">--}}
{{--                                                <el-checkbox  style="display:block;" v-for="(item, index) in firstCate"--}}
{{--                                                              :label="item.id" :value="item.id" name="type">[[item.name]]--}}
{{--                                                </el-checkbox>--}}
{{--                                            </el-checkbox-group>--}}
                                        </el-scrollbar>
                                    </el-col>
{{--                                    <el-col :span="8" >--}}
{{--                                        <div class="category-style">二级分类</div>--}}
{{--                                        <el-scrollbar class="scrollbar-style">--}}
{{--                                            <!-- <el-checkbox-group v-model="checkedSecondCate" style="width:100%;text-align: center" @change="loadThird"> -->--}}
{{--                                            <div v-for="(item, index) in secondCate" class="checkbox-pane" :key="index">--}}
{{--                                                <el-checkbox style="display:block;"  :label="item.id" :value="item.id" name="type" v-model="item.checked" @change="loadThird($event,item, index)">[[empty]]</el-checkbox>--}}
{{--                                                <span @click="tapCheckBox(item, index,2)" :class="item.color ? 'checkbox-color checkbox-name' : 'checkbox-name'">[[item.name]]</span>--}}
{{--                                            </div>--}}
{{--                                            <!-- </el-checkbox-group> -->--}}
{{--                                        </el-scrollbar>--}}
{{--                                    </el-col>--}}
{{--                                    <el-col :span="8" >--}}
{{--                                        <div  class="category-style">三级分类</div>--}}
{{--                                        <el-scrollbar class="scrollbar-style">--}}
{{--                                            <!-- <el-checkbox-group v-model="checkedThirdCate" style="width:100%;text-align: center" style="width:100%" @change="loadThree"> -->--}}
{{--                                            <div v-for="(item, index) in thirdCate" class="checkbox-pane">--}}
{{--                                                <el-checkbox style="display:block;" :label="item.id" :value="item.id" name="type" v-model="item.checked" @change="loadThree($event,item, index)">[[empty]]</el-checkbox>--}}
{{--                                                <span class="checkbox-name">[[item.name]]</span>--}}
{{--                                            </div>--}}
{{--                                            <!-- </el-checkbox-group> -->--}}
{{--                                        </el-scrollbar>--}}
{{--                                    </el-col>--}}
                                </el-row>

                                <span slot="footer" class="dialog-footer">
                                    <!-- <el-button @click="dialogVisible = false">取 消</el-button> -->
                                    <el-button type="primary" @click="choose()">确 定</el-button>
                                </span>
                            </el-dialog>
                        </el-form-item>
                            <el-form-item label="余额抵扣" label-width="155px">
                            <el-switch
                                    v-model="form.balance_deduct"
                                    :active-value="1"
                                    :inactive-value="0">
                            </el-switch>
                            <div class="form-item_tips">开启余额抵扣，订单使用余额抵扣则不能使用余额支付。关闭则不支持余额抵扣</div>
                        </el-form-item>
                        <el-form-item v-show="form.balance_deduct" prop="" label-width="155px">
                            <el-radio  v-model="form.balance_deduct_type" :label="0">固定金额</el-radio>
                            <el-radio v-model="form.balance_deduct_type" :label="1">支付金额比例抵扣</el-radio>
                            <div style="display: flex;">
                                <el-form-item prop="max_balance_deduct">
                                    <el-input  v-model.trim="form.max_balance_deduct" maxlength="300">
                                        <template slot="prepend">最多抵扣</template>
                                        <template slot="append">[[form.balance_deduct_type==1?'%':'元']]</template>
                                    </el-input>
                                </el-form-item>
                                <el-form-item prop="min_balance_deduct">
                                    <el-input  v-model.trim="form.min_balance_deduct" maxlength="300">
                                        <template slot="prepend">最少抵扣</template>
                                        <template slot="append">[[form.balance_deduct_type==1?'%':'元']]</template>
                                    </el-input>
                                </el-form-item>
                            </div>
                            <div class="form-item_tips">抵扣金额不能大于商品现价</div>
                            <div class="form-item_tips">如果设置为空为0，则采用余额统一设置</div>
                        </el-form-item>
                    </el-form>
                </div>
            </div>
            <el-dialog
                    title="提示"
                    :visible.sync="dialogVisible"
                    width="30%"
                    :show-close="false"
                    :close-on-click-modal="false">
                <div style="margin-top: -20px">
                    <span v-if="current==0">确认修改<span style="color: red">[[count_goods]]</span>个商品？</span>
                    <br>
                    <span v-if="current==0">注：<span style="color: red">确认修改后请勿刷新页面</span></span>
                    <div style="margin-top: -30px">
                        <span style="color: red" v-if="current>0">导入中，请勿刷新页面!</span>
                        <el-progress v-if="current>0" :percentage="schedule"></el-progress>
                    </div>
                </div>
                <span v-if="current==0" slot="footer" class="dialog-footer">
                    <el-button @click="handleClose()">取 消</el-button>
                    <el-button type="primary" @click="saveGoods()">确 定</el-button>
                    <el-button type="primary" @click="submitFormQuick()" :loading="loading">快速设置</el-button>
                </span>

            </el-dialog>
            <!-- 分页 -->
            <div class="vue-page">
                <div class="vue-center">
                    <el-button type="primary" @click="submitForm('form')">保存设置</el-button>
                    <el-button @click="goBack">返回</el-button>
                </div>
            </div>
            <!--end-->
        </div>
    </div>


    <script>
        var vm = new Vue({
            el:"#app",
            delimiters: ['[[', ']]'],
            data() {
                let url = JSON.parse('{!! $url !!}');
                let categoryBalance = JSON.parse('{!! $categoryBalance?:'{}' !!}');
                let classify = JSON.parse('{!! $classify?:'{}' !!}');
                let form ={
                    category_ids:[],
                    balance_deduct:0,
                    balance_deduct_type:0,
                    max_balance_deduct:"",
                    min_balance_deduct:'',
                    ...categoryBalance,
                };
                let classic =[];
                let firstCate = JSON.parse('{!! $firstCate?:"{}" !!}');
                let secondCate = [];
                let thirdCate = [];
                let checkedFirstCate = [];
                let checkedSecondCate = [];
                let checkedThirdCate = [];

                return{
                    url:url,
                    form:form,
                    classic:classic,
                    categorys:[],
                    dialogVisible:false,
                    dialogTableVisible:false,
                    loading: false,
                    submit_loading: false,
                    firstCate,
                    secondCate,
                    thirdCate,
                    checkedFirstCate,
                    checkedSecondCate,
                    checkedThirdCate,
                    classify,
                    rules: {

                    },
                    keyword:"",//搜索分类
                    checkedFirstText:[], //获取二级点击文字时的id
                    categoryArr:[], //保存回显编辑的数据
                    empty:"",
                    search_categorys:[],
                    classification:"",
                    save_data:[],
                    count_goods:0,
                    item_id:[],
                    current:0,
                    schedule:0,
                    loading:false,
                }
            },
            mounted() {
                if(this.form.category_ids.length) {
                    this.categoryArr = this.form.category_ids;
                    for(let item of this.form.category_ids) {
                        this.classification += `[ID:${item.id}][分类: ${item.name}],`;
                    }
                }
            },
            watch: {
                'search_categorys'(newVal,oldVal){
                    if(oldVal.length > newVal.length) {
                        this.publicData = newVal.length ? [...oldVal].filter(x => [...newVal].every(y => y.id !== x.id)) : oldVal;
                        for(let item of this.firstCate) {
                            if(this.publicData[0].id == item.id) {
                                this.$set(item,"checked",false)
                            }
                        }
                    }else {
                        this.publicData = [];
                    }
                    // 二级
                    for(let cItem of this.secondCate) {
                        this.$set(cItem,'checked',false)
                        for(let item of this.search_categorys) {
                            if(item.id == cItem.id) {
                                this.$nextTick(() => {
                                    this.$set(cItem,'checked',true)
                                })
                                break;
                            }
                        }
                    }
                    // 三级
                    for(let cItem of this.thirdCate) {
                        this.$set(cItem,'checked',false)
                        for(let item of this.search_categorys) {
                            if(item.id == cItem.id) {
                                this.$nextTick(() => {
                                    this.$set(cItem,'checked',true)
                                })
                                break;
                            }
                        }
                    }
                }
            },
            methods: {
                goBack() {
                    window.location.href='{!! yzWebFullUrl('discount.batch-balance.index') !!}';
                },
                loadCategorys(query) {
                    if (query !== '') {
                        this.loading = true;
                        this.$http.get("{!! yzWebUrl('discount.batch-balance.select-category', ['keyword' => '']) !!}" + query).then(response => {
                            this.categorys = response.data.data;
                            this.data=response.data.data;
                            this.loading = false;
                        }, response => {
                            console.log(response);
                        });
                    } else {
                        this.categorys = [];
                    }
                },
                submitForm(formName) {
                    console.log(this.form,'this.formthis.form');

                    this.$refs[formName].validate((valid) => {
                        if (valid) {
                            let loading = this.$loading({target:document.querySelector(".content"),background: 'rgba(0, 0, 0, 0)'});
                            this.$http.post(this.url,{'form_data':this.form}).then(response => {
                                if (response.data.result) {
                                    this.save_data = response.data.data.save_data;
                                    this.count_goods = response.data.data.count_goods;
                                    this.item_id = response.data.data.item_id;
                                    this.dialogVisible = true
                                    this.$message({type: 'success', message: '操作成功!'});
                                } else {
                                    this.$message({message: response.data.msg,type: 'error'});
                                }
                            },response => {
                                loading.close();
                            });
                        }else {
                            console.log('error submit!!');
                            return false;
                        }
                    });
                },
                submitFormQuick() {
                    this.loading = true;
                    let json = {
                        save_data:this.save_data,
                        item_id:this.item_id,
                        current:this.current,
                    }
                    this.$http.post("{!! yzWebFullUrl('discount.batch-balance.goods-save-quick') !!}", json).then(response => {
                        if (response.data.result == 1) {
                            this.dialogVisible = false
                            this.$message.success('商品修改完成');
                            window.location.href = '{!! yzWebFullUrl('discount.batch-balance.index') !!}';

                        } else {
                            this.$message.error(response.data.msg);
                        }
                    })
                },
                goParent() {
                    window.location.href = `{!! yzWebFullUrl('discount.batch-balance.index') !!}`;
                },
                choose(){
                    this.dialogTableVisible = false;
                },
                visDia() {
                    if(this.categoryArr.length) {

                        console.log(1111111);
                        this.getCategoryData();
                        return

                        this.categorys = this.categoryArr;
                        this.search_categorys = this.categoryArr;

                    }
                    this.dialogTableVisible = true;
                },
                // 获取所有的分类数据

                getCategoryData() {
                    this.$http.get("{!! yzWebUrl('discount.batch-balance.get-all-cate') !!}").then(({data}) => {
                        if(data.result) {
                            console.log(data.data);
                            this.categorys = data.data;
                            this.search_categorys = this.categoryArr;
                            this.dialogTableVisible = true;
                        }else {
                            this.$message.error(data.msg);
                        }
                    });
                },

                // 关闭弹窗回调
                closeDialog() {
                    let newArr = [];
                    this.classification = "";
                    for(let item of this.search_categorys) {
                        this.classification += `[ID:${item.id}][分类: ${item.name}],`
                        newArr.push(item.id);
                    }
                    this.categoryArr = this.search_categorys;
                    this.form.category_ids = newArr;
                },
                //根据选择的第一级分类请求第二级分类
                loadSecond(row,index){
                    // 处理多选搜索
                    this.$set(this.firstCate[index],'color',this.firstCate[index].color ? false : true);
                    let secondIndex = this.checkedFirstText.findIndex(item => {
                        return item == row.id
                    })
                    if(secondIndex == -1) {
                        this.checkedFirstText.push(row.id);
                    }else {
                        this.checkedFirstText.splice(secondIndex,1);
                    }
                    this.$http.get("{!! yzWebUrl('discount.batch-balance.get-child') !!}"+`&level=2&cate=${this.checkedFirstText}`).then(({data}) => {
                        if(data.result) {
                            this.secondCate = data.data;
                            this.thirdCate = [];
                            this.checkedFirstText = [];
                            // 回显二级分类 ✔
                            for(let item of this.search_categorys) {
                                for(let cItem of this.secondCate) {
                                    this.$set(cItem,'checked',false)
                                    if(item.id == cItem.id) {
                                        this.$nextTick(() => {
                                            this.$set(cItem,'checked',true)
                                        })
                                        break;
                                    }
                                }
                            }
                        }else {
                            this.$message.error(data.msg);
                        }
                    });
                },
                tapCheckBox(row,index) {
                    // 处理多选搜索
                    this.$set(this.secondCate[index],'color',this.secondCate[index].color ? false : true);
                    let secondIndex = this.checkedFirstText.findIndex(item => {
                        return item == row.id
                    })
                    if(secondIndex == -1) {
                        this.checkedFirstText.push(row.id);
                    }else {
                        this.checkedFirstText.splice(secondIndex,1);
                    }
                    this.$http.get("{!! yzWebUrl('discount.batch-balance.get-child') !!}"+`&level=3&cate=${this.checkedFirstText}`).then(({data}) => {
                        if(data.result) {
                            this.thirdCate = data.data;
                            // 回显三级分类 ✔
                            for(let item of this.search_categorys) {
                                for(let cItem of this.thirdCate) {
                                    this.$set(cItem,'checked',false)
                                    if(item.id == cItem.id) {
                                        this.$nextTick(() => {
                                            this.$set(cItem,'checked',true)
                                        })
                                        break;
                                    }
                                }
                            }
                        }else {
                            this.$message.error(data.msg);
                        }
                    });
                },
                //根据选择的第一级分类请求第二级分类
                loadTwo($event,row,index){
                    if(!row.checked) {
                        // 删除多选框
                        let firstIndex = this.search_categorys.findIndex(item => {
                            return item.id == row.id
                        })
                        this.search_categorys.splice(firstIndex,1);
                        return
                    }
                    // 选中后添加复选框
                    this.categorys = [row];
                    let firstIndex = this.search_categorys.findIndex(item => {
                        return item.id == row.id
                    })
                    if(firstIndex == -1) {
                        this.search_categorys.push(row);
                    }
                },
                //根据选择的第二级分类请求第三级分类
                loadThird($event,row,index){
                    if(!row.checked) {
                        // 删除多选框
                        let secondIndex = this.search_categorys.findIndex(item => {
                            return item.id == row.id
                        })
                        this.search_categorys.splice(secondIndex,1);
                        return
                    }
                    // 选中后添加复选框
                    this.categorys = [row];
                    let secondIndex = this.search_categorys.findIndex(item => {
                        return item.id == row.id
                    })
                    if(secondIndex == -1) {
                        this.search_categorys.push(row);
                    }
                },
                loadThree($event,row,index) {
                    if(!row.checked) {
                        // 删除多选框
                        let currentIndex = this.search_categorys.findIndex(item => {
                            return item.id == row.id
                        })
                        this.search_categorys.splice(currentIndex,1);
                        return
                    }
                    // 选中后添加复选框
                    this.categorys = [row];
                    let currentIndex = this.search_categorys.findIndex(item => {
                        return item.id == row.id
                    })
                    if(currentIndex == -1) {
                        this.search_categorys.push(row);
                    }
                },
                saveGoods()
                {
                    let json = {
                        save_data:this.save_data,
                        item_id:this.item_id,
                        current:this.current,
                    }
                    this.$http.post("{!! yzWebFullUrl('discount.batch-balance.goods-save') !!}", json).then(response => {
                        if (response.data.result == 1) {
                            this.current = response.data.data.current;
                            if (this.current < this.count_goods) {
                                this.schedule = (this.current / this.count_goods * 100).toFixed(2)
                                this.saveGoods()
                            } else {
                                this.dialogVisible = false
                                this.$message.success('商品修改完成');
                                window.location.href = '{!! yzWebFullUrl('discount.batch-balance.index') !!}';
                            }
                        } else {
                            this.$message.error(response.data.msg);
                        }
                    })


                },
                handleClose() {
                    window.location.href = '{!! yzWebFullUrl('discount.batch-balance.index') !!}';
                }
            },
        });
    </script>
@endsection



