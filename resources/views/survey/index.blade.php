<!-- <script src="{{static_url('js/echarts.js')}}" type="text/javascript"></script> -->
<script src="{{static_url('assets/js/echarts.min@5.1.2.js')}}"></script>
<script src="{{static_url('assets/js/china.js')}}"></script>
@extends('layouts.base')
@section('content')
<link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}" />
<link rel="stylesheet" href="//at.alicdn.com/t/c/font_432132_z1vt3vqw8ek.css" />
<style>
    [v-cloak] {
        display: none
    }
    .all {
        padding-top: 0;
        background-image: url('/resources/views/survey/assets/img/bg.png'); /* 替换为你的图片路径 */
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }
    #app {
        padding: 0 ;
    }
    .top-pane {
        padding-top: 40px;
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;
        position: relative;
        z-index: 1;
    }
    .top-pane .tab-lan {
        width: 786px;
        display: flex;
        justify-content: space-evenly;
    }
    .top-pane .tab-div {
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }
    .top-pane .tabs-name {
        font-weight: 400;
        font-size: 18px;
        color: #333333;
        margin-bottom: 5px;
    }
    /* .top-pane .tabs-han {

    } */
    .top-pane .active-tabs-name {
        font-weight: bold;
        font-size: 18px;
        color: #333333;
        margin-bottom: 5px;
    }
    .top-pane .active-tabs-han {
        display: block;
        width: 30px;
        height: 4px;
        background: #2CC08D;
        border-radius: 30px 30px 30px 30px;
    }
    .top-pane .search-box  {
        margin-top: 20px;
        position: relative;
    }
    .top-pane .search-box .el-input__inner {
        width: 716px;
        height: 40px;
        background: #FFFFFF;
        border-radius: 20px 20px 20px 20px;
        border: 1px solid #FFFFFF;
        padding-right: 70px;
    }
    .top-pane .search-box .icon-sousuo1 {
        position: absolute;
        right: 20px;
        top: 10px;
        color: #BCBCBC;
        font-size: 20px;
    }
    .top-pane .search-box .el-input__clear {
        right: 40px;
    }
    .history-box {

    }
    .history-box .history-name {
        
    }

    .active-name-plu {
        font-weight: bold;
    }

    .rank-bg {
        background: #F9F9F9;
        border-radius: 8px 8px 8px 8px;
    }

    /* 设置 */
    /* .vue-setting {
        color: #4d8bfc;
        font-size: 14px;
        cursor: pointer;
    }
    
    .vue-setting .el-icon-setting {
        margin-right: 5px;
    }

    .setting-title {
        margin: 10px 20px 20px;
    }

    .active-tab {
        margin: 20px 0 0 40px;
    }

    .el-tabs__item {
        /* margin-right: 60px; */
        /* padding: 0 60px */
    }

    .active-tab .el-form-item__label {
        margin-right: 30px;
    }

    .active-tab .switch-style .el-form-item__label {
        line-height: 1;
    }

    .del-style {
        margin-left: 20px;
        color: #f56c6c;
        font-weight: 600;
        cursor: pointer;
    }

    .add-style {
        color: #29BA9C;
        font-weight: 600;
        cursor: pointer;
        margin-left: 30px;
    }

    .list-rol {
        display: flex;
        flex-wrap: wrap;
    }

    .list-row {
        margin: 0 20px 30px 0;
        position: relative;
    }

    .list-rol .list-line {
        position: absolute;
        left: 0;
        z-index: 1;
        background-color: #F2F6FC;
        border-color: #DCDFE6;
        width: 14px;
        height: 14px;
        display: flex;
        align-items: center;
        justify-content: center;
        top: 14px;
    }

    .upload-boxed .el-icon-plus-style {
        line-height: 136px;
        width: 150px;
        font-size: 54px;
        text-align: center;
        color: #ccc;
        border: 1px dashed #ccc;
        border-bottom: 0px;
        border-radius: 5px;
    } */
</style>
<div class="all">
    <div id="app" v-cloak>
        <div v-show="active_type == 1" style="padding: 0px 20px 20px 20px;">
            <div class="top-pane">
                <div class="tab-lan">
                    <span v-for="(item,index) in tabPane" :key="index"  class="tab-div" @click="clickActiveTopTab(item.key)">
                        <span :class="item.key == activeTopTab ? 'active-tabs-name' : 'tabs-name'">[[item.name]]</span>
                        <span :class="item.key == activeTopTab ? 'active-tabs-han' : 'tabs-han'"></span>
                    </span>
                </div>
                <search_box></search_box>
            </div>

            <!-- 总驾驶舱 -->
            <div v-if="info && activeTopTab == 1">
                <div style="display: flex;margin-top: 33px;justify-content: space-between;">
                    <turnover_left  tip_bottom="累计今日商城已支付单数，不剔除退款订单" tip_top="累计商城支付订单金额，不剔除退款金额" v-if="activeTopTab == 1" :main_cockpit_data="mainCockpitData"></turnover_left>
                    <map_ch :main_cockpit_data="mainCockpitData" v-if="activeTopTab == 1"></map_ch>
                    <turnover_right></turnover_right>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <oder_analysis></oder_analysis>
                    <member_analysis></member_analysis>
                    <commodity_analysis :goods_analysis="mainCockpitData.goods_analysis"></commodity_analysis>
                </div>
                <bottom_pane :main_cockpit_data="mainCockpitData" @click_set="clickSetEv"></bottom_pane>
            </div>
            

            <!-- 订单视角 -->
            <div v-if="info && activeTopTab == 2">
                <div style="display: flex;margin-top: 33px;justify-content: space-between;">
                    <turnover_left :main_cockpit_data="mainCockpitData" type="order" tip_bottom="累计今日商城已支付单数，不剔除退款订单" tip_top="累计商城已支付单数，不剔除退款订单" title_top="订单总数（单）" title="订单数据" title_bottom="今日订单（单）"></turnover_left>
                    <map_ch :main_cockpit_data="mainCockpitData" type="order_map" ></map_ch>
                    <order_view_analysis  :main_cockpit_data="mainCockpitData" type="order"></order_view_analysis>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <order_rank :main_cockpit_data="mainCockpitData" type="order"></order_rank>
                    <order_data_analysis type="order"></order_data_analysis>
                    <product_sort type="order_member" title="下单会员占比" :main_cockpit_data="mainCockpitData"  ></product_sort>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <product_sort type="order_sort" title="订单分类占比" :main_cockpit_data="mainCockpitData"></product_sort>
                    <line_order type="line_order" ></line_order>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <pay_analysis :main_cockpit_data="mainCockpitData"></pay_analysis>
                    <line_order title="配送方式" type="distribution_mode" ></line_order>
                </div>
            </div>

            <!-- 会员视角 -->
            <div  v-if="info && activeTopTab == 3">
                <div style="display: flex;margin-top: 33px;justify-content: space-between;">
                    <goods_data  title="交易概况" type="trading" :main_cockpit_data="mainCockpitData" tip_="区分新、老会员的规则：注册时间超过30天才算老会员"></goods_data>
                    <map_ch type="trading_map"  :main_cockpit_data="mainCockpitData"></map_ch>
                    <turnover_right type="trading_bar" title="会员数据分析" height="480px"></turnover_right>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <product_sort type="trading" title="会员等级情况"  :main_cockpit_data="mainCockpitData"></product_sort>
                    <line_order width_="48%" title="会员访问数据" type="trading_" ></line_order>
                    <sex_pie type="trading" :main_cockpit_data="mainCockpitData"></sex_pie>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <integration_path title="会员结构分部" width_="49.5%" type="trading_member" :main_cockpit_data="mainCockpitData"></integration_path>
                    <turnover_right type="age_consumption" :main_cockpit_data="mainCockpitData" title="年龄分布与消费金额" :list_data="[]" height_bar="calc(100% - 60px)" width_="49.5%" height="480px"></turnover_right>
                </div>
            </div>

            <!-- 商品视角 -->
            <div v-if="info && activeTopTab == 4">
                <div style="display: flex;margin-top: 33px;justify-content: space-between;">
                    <goods_data  type="goods" :main_cockpit_data="mainCockpitData"></goods_data>
                    <good_total :main_cockpit_data="mainCockpitData"></good_total>
                    <rank_box :main_cockpit_data="mainCockpitData"></rank_box>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <product_review :main_cockpit_data="mainCockpitData"></product_review>
                    <trading_trend :main_cockpit_data="mainCockpitData"></trading_trend>
                    <product_sort type="goods_sort" :main_cockpit_data="mainCockpitData"></product_sort>
                </div>
            </div>

            <!-- 财务视角 -->
            <div v-if="info && activeTopTab == 5">
                <div style="display: flex;margin-top: 33px;justify-content: space-between;">
                    <financial_list :main_cockpit_data="mainCockpitData"></financial_list>
                </div>
                <div style="display: flex;justify-content: space-between;margin-top: 20px;">
                    <member_income_list :main_cockpit_data="mainCockpitData"></member_income_list>
                    <order_data_analysis width_="74.5%" :y_show="false" type="financial"  title="会员数据统计" ></order_data_analysis>
                </div>
                <financial_buttom></financial_buttom>
            </div>
        </div>
        <set_cp v-show="active_type == 2" :active_type.sync="active_type" ref="set_cp"></set_cp>
    </div>
</div>
@include('public.admin.uploadMultimediaImg')
@include('survey.components.map')
@include('survey.components.turnover_right')
@include('survey.components.oder_analysis')
@include('survey.components.turnover_left')
@include('survey.components.commodity_analysis')
@include('survey.components.member_analysis')
@include('survey.components.bottom_pane')
@include('survey.components.good_total')
@include('survey.components.rank_box')
@include('survey.components.goods_data')
@include('survey.components.product_review')
@include('survey.components.trading_trend')
@include('survey.components.product_sort')
@include('survey.components.order_view_analysis')
@include('survey.components.order_rank')
@include('survey.components.order_data_analysis')
@include('survey.components.line_order')
@include('survey.components.pay_analysis')
@include('survey.components.financial_list')
@include('survey.components.member_income_list')
@include('survey.components.integration_path')
@include('survey.components.sex_pie')
@include('survey.components.financial_buttom')
@include('survey.components.set_cp')
@include('survey.components.search_box')
<script>
    var app = new Vue({
        el: "#app",
        delimiters: ['[[', ']]'],
        name: 'test',
        data() {
            return {
                tabPane: [{
                    name: "总驾驶舱",
                    key: 1
                },{
                    name: "订单视角",
                    key: 2
                },{
                    name: "会员视角",
                    key: 3
                },{
                    name: "商品视角",
                    key: 4
                },{
                    name: "财务视角",
                    key: 5
                }],
                activeTopTab: 1,
                info: false,
                mainCockpitData: {},
                // 设置
                active_type: 1,
            }
        },
        mounted() {
            this.mainCockpit(1);
        },
        methods: {
            // 点击切换
            clickActiveTopTab(key) {
                this.activeTopTab = "";
                this.$nextTick(() => {
                    this.mainCockpit(key);
                })
            }, 
            async mainCockpit(key){
                let url = "";
                if(key == 1) {
                    url = "{!! yzWebFullUrl('survey.survey.main-cockpit') !!}"
                }else if(key == 2) {
                    url = "{!! yzWebFullUrl('survey.survey.order-view') !!}"
                }else if(key == 3) {
                    url = "{!! yzWebFullUrl('survey.survey.member-view') !!}"
                }else if(key == 4) {
                    url = "{!! yzWebFullUrl('survey.survey.goods-view') !!}"
                }else if(key == 5) {
                    url = "{!! yzWebFullUrl('survey.survey.finance-view') !!}"
                }
                let {data : { result , data , msg}} = await this.$http.post(url,{})
                console.log(result , data , msg,'data');
                if(result) {
                    this.mainCockpitData = data;
                    this.activeTopTab = key;
                    this.info = true;
                    window.dispatchEvent(new Event('resize'))
                }else {
                    this.$message.error(msg)
                }
            },
            // 点击设置
            clickSetEv() {
                this.$refs.set_cp.clickSet();
            },
        },
    })
</script>
@endsection
