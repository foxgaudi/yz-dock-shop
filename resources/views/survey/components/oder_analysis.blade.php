<style> 
</style>
<template id="oder_analysis">
    <div class="oder-analysis" style="width: 25%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
        <div class="oder-analysis" style="padding:16px;    justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">订单分析</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div class="order-list" style="margin-bottom: 20px;display: flex;padding: 0px 16px;justify-content: space-between;">
            <div class="order-num" style="width: 48%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;
                display: flex;flex-direction: column;justify-content: space-between;line-height: 1;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">订单总数（单）</div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[total]]</div>
            </div>
            <div class="order-num" style="width: 48%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;
                display: flex;flex-direction: column;justify-content: space-between;line-height: 1;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;"> 已发货（单）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[send_total]]</span></div>
                <div style="font-weight: 400;font-size: 14px;color: #707070;"> 已完成（单）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[complete_total]]</span></div>
            </div>
        </div>
        <div id="line_chart" style="width: 100% ; height: calc(100% - 180px);padding: 0px 16px;"></div>
    </div>
</template>
<script>
    Vue.component('oder_analysis', {
        delimiters: ['[[', ']]'],
        props: {
        
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                complete_total: "",
                total: "",
                send_total: ""
            }
        },
        mounted(){
            this.orderAnalysis(this.search.time_type,this.search.start_time);
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async orderAnalysis(time_type,start_time){
                let {data : { result , data , msg}} = await this.$http.post("{!! yzWebFullUrl('survey.survey.order-analysis') !!}",{
                    time_type,
                    start_time,
                })
                if(result) {
                    this.init(data.chart_data.order_complete,data.chart_data.order_send);
                    this.total = data.total;
                    this.send_total = data.send_total;
                    this.complete_total = data.complete_total;
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.orderAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(order_complete,order_send) {
                var chartDom = document.getElementById('line_chart');
                var myChart = echarts.init(chartDom);
                var option  = {
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['已发货','已完成']
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: order_complete.x_axis
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [
                        {
                            name:'已发货',
                            type:'line',
                            stack: '总量',
                            data: order_send.series,
                            itemStyle: {
                                normal: {
                                    color: "#4D8BFC",
                                    lineStyle: {
                                        color: "#4D8BFC"
                                    }
                                }
                            }
                        },
                        {
                            name:'已完成',
                            type:'line',
                            stack: '总量',
                            data: order_complete.series,
                            itemStyle: {
                                normal: {
                                    color: "#2CC08D",
                                    lineStyle: {
                                        color: "#2CC08D"
                                    }
                                }
                            }
                        },
                    ]
                };
                    myChart.setOption(option);
                }
        },
        template: '#oder_analysis'
    });
</script>

<style scoped></style>