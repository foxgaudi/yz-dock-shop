<style> 
</style>
<template id="financial_list">
    <div style="display: flex;width: 100%;justify-content: space-between;">
        <div style="border-radius: 16px;display: flex;align-items: center;padding: 24px;width: 24.5%;height: 148px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <img src="/resources/views/survey/assets/img/accumulated_income.png" alt="" style="width: 80px;height: 80px;">
            <div style="margin-left: 24px;height: 80px;display: flex;flex-direction: column;justify-content: space-around;">
                <div style="font-weight: 400;font-size: 16px;color: #333333;">累计收入（元）</div>
                <div style="font-weight: bold;font-size: 28px;color: #333333;">[[main_cockpit_data.income_total]]</div>
            </div>
        </div>
        <div style="border-radius: 16px;display: flex;align-items: center;padding: 24px;width: 24.5%;height: 148px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <img src="/resources/views/survey/assets/img/accumulated_withdrawal_income.png" alt="" style="width: 80px;height: 80px;">
            <div style="margin-left: 24px;height: 80px;display: flex;flex-direction: column;justify-content: space-around;">
                <div style="font-weight: 400;font-size: 16px;color: #333333;">累计提现收入（元）</div>
                <div style="font-weight: bold;font-size: 28px;color: #333333;">[[main_cockpit_data.withdraw_income]]</div>
            </div>
        </div>
        <div style="border-radius: 16px;display: flex;align-items: center;padding: 24px;width: 24.5%;height: 148px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <img src="/resources/views/survey/assets/img/service_tax.png" alt="" style="width: 80px;height: 80px;">
            <div style="margin-left: 24px;height: 80px;display: flex;flex-direction: column;justify-content: space-around;">
                <div style="font-weight: 400;font-size: 16px;color: #333333;">累计劳务税（元）</div>
                <div style="font-weight: bold;font-size: 28px;color: #333333;">[[main_cockpit_data.servicetax_total]]</div>
            </div>
        </div>
        <div style="border-radius: 16px;display: flex;align-items: center;padding: 24px;width: 24.5%;height: 148px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <img src="/resources/views/survey/assets/img/withdrawal_commission.png" alt="" style="width: 80px;height: 80px;">
            <div style="margin-left: 24px;height: 80px;display: flex;flex-direction: column;justify-content: space-around;">
                <div style="font-weight: 400;font-size: 16px;color: #333333;">累计提现手续费（元）</div>
                <div style="font-weight: bold;font-size: 28px;color: #333333;">[[main_cockpit_data.poundage_total]]</div>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('financial_list', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: 'financial_list'
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
        },
        methods: {
        },
        template: '#financial_list'
    });
</script>

<style scoped></style>