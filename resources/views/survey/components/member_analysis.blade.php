<style> 
</style>
<template id="member_analysis">
    <div class="member-box" style="width: 48%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;padding:24px 12px;">
        <div class="member-top" style="display: flex;align-items: center;justify-content: space-between;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">会员分析</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div style="display:flex;margin: 16px 0;">
            <div style="width: 30%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;
                margin-right: 20px;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">会员总数（人）</div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[member_count]]</div>
            </div>
            <div style="width: 30%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">浏览量（次）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[browse_count]]</span></div>
                <div style="font-weight: 400;font-size: 14px;color: #707070;">访客量（次）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[visitor_count]]</span></div>
            </div>
        </div>
        <div style="width: 100%%;height: calc(100% - 90px);" id="bar_chart"></div>
    </div>
</template>
<script>
    Vue.component('member_analysis', {
        delimiters: ['[[', ']]'],
        props: {
        
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                member_count: "",
                browse_count: "",
                visitor_count: ""
            }
        },
        mounted(){
            this.membeAccessAnalysis();
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async membeAccessAnalysis(time_type,start_time){
                let {data : { result , data , msg}} = await this.$http.post("{!! yzWebFullUrl('survey.survey.membe-access-analysis') !!}",{
                    time_type,
                    start_time,
                })
                console.log(result , data , msg,3333);
                if(result) {
                    this.browse_count= data.browse_count;
                    this.member_count= data.browse_count;
                    this.visitor_count= data.browse_count;
                    
                    this.init(data.chart_data.browse,data.chart_data.visitor);
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.membeAccessAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(browse,visitor) {
                var chartDom = document.getElementById('bar_chart');
                var myChart = echarts.init(chartDom);
                var option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'cross',
                            crossStyle: {
                                color: '#999'
                            }
                        }
                    },
                    legend: {
                        itemHeight: 12,
                        itemWidth: 12,
                        data: [{
                            icon: 'roundRect',
                            name: '浏览量'
                        },{
                            icon: 'roundRect',
                            name: '访客量'
                        }],
                    },
                    xAxis: [
                        {
                        type: 'category',
                        data: browse.x_axis,
                        axisPointer: {
                            type: 'shadow'
                        }
                        }
                    ],
                    yAxis: [
                        {
                        type: 'value',
                        name: ''
                        }
                    ],
                    series: [
                        {
                            name: '访客量',
                            type: 'bar',
                            tooltip: {
                                valueFormatter: function (value) {
                                    return value + ' ml';
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: '#2CC08D'
                                }
                            },
                            data: visitor.series,
                            barGap: '0%',
                            barWidth: '20%'
                        },
                        {
                            name: '浏览量',
                            type: 'bar',
                            itemStyle: {
                                normal: {
                                color: '#4D8BFC'
                                }
                            },
                            data: browse.series,
                            barGap: '0%',
                            barWidth: '20%'
                        }
                    ]
                };

                myChart.setOption(option);
            }
        },
        template: '#member_analysis'
    });
</script>

<style scoped></style>