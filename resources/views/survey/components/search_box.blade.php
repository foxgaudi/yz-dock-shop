<style> 
    .history-box .history-record {
        font-weight: 400;
        font-size: 14px;
        color: #999999;
    }
    .history-record-box {
        margin-top: 10px;
        display: flex;
        flex-wrap: wrap;
    }
    .history-record-box .history-list {
        background: #F2F2F2;
        border-radius: 8px 8px 8px 8px;
        padding: 5px 10px;
        border-radius: 5px;
        margin: 0 20px 10px 0;
        cursor: pointer;
        font-weight: 400;
        font-size: 14px;
        color: #333333;
    }
</style>
<template id="search_box">
    <div class="search-box">
        <el-popover placement="bottom" width="716" trigger="manual"  v-model="visiblepop" >
            <div class="history-box" v-if="isShowHistory && historyList.length">
                <div class="history-name">
                    <span class="history-record">历史记录</span>
                    <i class="el-icon-delete"  style="cursor: pointer;" @click="delHistory"></i>
                </div>
                <div class="history-record-box">
                    <span class="history-list" v-for="(item,index) in historyList" :key="index" @click="clickHistory(item)">[[item]]</span>
                </div>
            </div>
            <div v-show="!isShowHistory">
                <div style="font-size:12px;color:#8C8C8C" v-if="installed_list.length > 0">已安装应用  共[[installed_list.length]]个 点击进入插件!</div>
                <div  class="installed_list" v-if="installed_list.length > 0">
                    <el-scrollbar >
                    <div v-for="(item,index) in installed_list" :key="index" style="position: relative;cursor: pointer;margin-top:12px" @click="toPlugin(item.jump_url)">
                            <img :src="item.imageUrl" alt="" style="width: 32px;height:32px;border-radius: 8px ;">
                            <span style="margin-left: 6px;font-size:12px;font-weight: bold;color:#353535">[[item.title]]</span>
                        </div>
                    </el-scrollbar>
                </div>
                <div style="font-size:12px;color:#8C8C8C;margin-top: 10px;" v-if="has_founder && not_installed_list.length > 0">更多应用</div>
                <div style="margin:12px 0px;position: relative;display:flex" v-for="(item,index) in not_installed_list" :key="index" v-if="has_founder">
                    <img :src="item.imageUrl" alt="" style="width: 32px;height:32px;border-radius: 8px ;">
                    <div style="margin-left: 6px;">
                        <span style="font-size:12px;font-weight: bold;color:#353535">[[item.title]]</span>
                        <div style="font-size: 11px;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;word-break: break-all;width: 150px;">[[item.description]]</div>
                    </div>
                    <el-button style="border-radius:4px;background: #29BA9C;color:#ffffff;right: 0px;position: absolute;top: 5px;" size="mini" @click="install(item)">安装</el-button>
                </div>
            </div>
            <el-input slot="reference" placeholder="请输入关键字搜索功能" style="width: 100%;height: 50px !important;" v-model="search_word" @input="search" @blur="searchBlur" @focus="focus" ref="search" >
                <!-- <i slot="prefix" class="el-input__icon el-icon-search"></i> -->
            </el-input>
        </el-popover>
        <i class="iconfont icon-sousuo1" @click="clickIconSearch" style="cursor: pointer;"></i>
    </div>
</template>
<script>
    Vue.component('search_box', {
        delimiters: ['[[', ']]'],
        props: {
        },
        data() {
            return {
                // 搜索
                search_word: '',
                timer:null,
                installed_list:[],
                not_installed_list:[],
                visiblepop:false,
                has_founder:false,
                historyList: [],
                isShowHistory: true
            }
        },
        mounted(){
            this.getCategory()
        },
        methods: {
            //获取插件列表
            getCategory() {
                this.$http.post('{!! yzWebFullUrl('plugins.get-plugin-list') !!}', {}).then(function(response) {
                    if (response.data.result) {
                        this.has_founder = response.data.data.has_founder
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    console.log(response);
                });
            },
            //跳转相对应插件页面
            toPlugin(urls,) {
                if(urls){
                    window.location.href = urls
                }else{
                    this.$message({
                            message: '该插件未开启',
                            type: 'error'
                        });
                }
            },
            searchApp() {
                this.$http.post('{!! yzWebFullUrl('plugins.search-plugin-list') !!}', {
                        keyword: this.search_word
                    }).then(function(response) {
                    if (response.data.result) {
                        this.installed_list  = response.data.data.installed_list
                        this.not_installed_list = response.data.data.not_installed_list
                        if(!this.has_founder){
                            this.not_installed_list = []
                        }
                        if(this.installed_list.length !== 0 || this.not_installed_list.length !==0){
                            this.visiblepop = true
                        }else{
                            this.visiblepop = false
                        }
                        this.isShowHistory = false;
                        // 存历史记录
                        let historyAllData = localStorage.getItem('shopOverviewHistory') ? JSON.parse(localStorage.getItem('shopOverviewHistory')) :  ""
                        let newArr = historyAllData && historyAllData.data_ ? historyAllData.data_ : [];
                        newArr.push(this.search_word)
                        localStorage.setItem('shopOverviewHistory', JSON.stringify({
                            name: "商城后台概况历史记录",
                            data_: [...new Set(newArr)].filter(item => item || item === 0)
                        }));
                        window.dispatchEvent(new Event('resize'))
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    console.log(response);
                });
            },
            // /搜索防抖
            _debounce(fn, delay) {
                var delay = delay || 200;
                return function () {
                    var th = this;
                    var args = arguments;
                    if (this.timer) {
                        clearTimeout(timer);
                    }
                    this.timer = setTimeout(function () {
                        this.timer = null;
                        fn.apply(th, args);
                    }, delay);
                };
            },
            //关键字搜索插件
            search(){
                this._debounce(this.searchApp,500)()
            },
            searchBlur(){
                this.visiblepop = false
            },
            //点击搜索聚焦 判断是否展示弹出框
            focus(){
                // 获取历史数据
                let historyAllData = localStorage.getItem('shopOverviewHistory') ? JSON.parse(localStorage.getItem('shopOverviewHistory')) :  ""
                this.historyList = historyAllData && historyAllData.data_ ? historyAllData.data_ : [];
                this.isShowHistory = true;
                this.visiblepop = this.historyList.length ? true : false
                // if(this.installed_list.length !== 0 || this.not_installed_list.length !==0){
                //     this.visiblepop = true
                // }                    
            },
            // 删除记录
            delHistory() {
                localStorage.removeItem('shopOverviewHistory');
            },
            // 搜索历史记录
            clickHistory(search_word) {
                this.search_word = search_word;
                // this.$refs.search.focus();
                this.search();
            },
            // 点击搜索
            clickIconSearch() {
                this.$refs.search.focus();
                this.search();
            },
            //安装应用
            install(item){
                this.$http.post('{!! yzWebFullUrl('plugins.install-plugin') !!}', {
                        version: item.version,
                        name: item.name
                    }).then(function(response) {
                    if (response.data.result) {
                        this.$message({
                            message: response.data.msg,
                            type: 'success'
                        });
                        location.reload();
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    console.log(response);
                });                    
            },
        },
        template: '#search_box'
    });
</script>

<style scoped></style>