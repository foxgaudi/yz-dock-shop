<style> 
</style>
<template id="pay_analysis">
    <div style="width: 25%;height: 480px;background: #FFFFFF; padding: 24px 12px;border-radius: 16px 16px 16px 16px;">
        <div class="oder-analysis" style="justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">支付分析</span>
        </div>
        <div id="pay_analysis" style="width: 100% ; height: calc(100% - 5px)"></div>
    </div>
</template>
<script>
    Vue.component('pay_analysis', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                let arrData = [];
                for(let item of this.main_cockpit_data.pay_type_analysis) {
                    arrData.push({
                        value: item.order_count,
                        name: item.name,
                        itemStyle: {
                            normal: {
                                color: item.color
                            }
                        },
                    })
                }
                var chartDom = document.getElementById('pay_analysis');
                var myChart = echarts.init(chartDom);
                var option = {
                    legend: {
                        top: '5%',
                        icon: 'circle',
                        left: 'left'
                    },
                    tooltip: {
                        trigger: 'item',
                    },
                    series: [
                        {
                            name: '支付分析',
                            type: 'pie',
                            radius: [50, 130],
                            center: ['50%', '60%'],
                            roseType: 'area',
                            data: arrData
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },
        template: '#pay_analysis'
    });
</script>

<style scoped></style>