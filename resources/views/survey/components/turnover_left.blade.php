<style> 
    .left-turnover-box {
        width: 25%;
        height: 440px;
        background: linear-gradient( 360deg, #FBFDFE 0%, #E6F3FE 36%, #C1DCFF 100%);
        border-radius: 16px 16px 16px 16px;
        border: 1px solid #FFFFFF;
        padding: 22px 0;
    }
    .left-turnover-box .turnover-title {
        max-width: 154px;
        height: 49px;
        background: linear-gradient( 339deg, rgba(255,255,255,0) 0%, #B2F1E4 20%, #4D8BFC 100%);
        border-radius: 0px 24px 24px 0px;
        font-weight: bold;
        font-size: 22px;
        color: #FFFFFF;
        text-align: left;
        line-height: 49px;
        padding: 0 22px;
    }
    .turnover-outer-box {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
    .left-turnover-box .turnover-box {
        width: 90%;
        height: 158px;
        background: #FFFFFF;
        border-radius: 20px 20px 20px 20px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin: 16px 22px 0 22px;
        padding: 0 21px;
    }
    .left-turnover-box .turnover-left {
        
    }
    .left-turnover-box .turnover-name {
        
    }
    .left-turnover-box .turnover-des {
        font-weight: 400;
        font-size: 18px;
        color: #333333;
    }
    .left-turnover-box .icon-fontclass-gantanhao {
        color: #D1D4D9
    }
    .left-turnover-box .turnover-value {
        font-weight: bold;
        font-size: 30px;
        color: #333333;
        margin-top: 16px;
    }
    
</style>
<template id="turnover_left">
    <div class="left-turnover-box">
        <div class="turnover-title" :style="{'max-width': type == 'order' ? '145px' : '154px'}">[[title]]</div>
        <div class="turnover-outer-box">
            <div class="turnover-box">
                <div class="turnover-left">
                    <div class="turnover-name">
                        <span class="turnover-des" style="position: relative;">
                            [[type == "turnover" ? main_cockpit_data.total_turnover.name : title_top]]
                            <el-tooltip class="item" effect="light" :content="tip_top" placement="top">
                                <i class="iconfont icon-fontclass-gantanhao" style="position: absolute;font-size: 17px; top: -2px;"></i> 
                            </el-tooltip>
                        </span>
                    </div>
                    <div class="turnover-value">[[type == "turnover" ? main_cockpit_data.total_turnover.value : type == "order" ? main_cockpit_data.order_count : 0]]</div>
                </div>
                <image :src="`/resources/views/survey/assets/img/${type == 'turnover' ? 'turnover_icon.png' : type == 'order' ? 'goods_totall.png' : '' }`" style="width: 71px;height: 71px"></image>
            </div>
            <div class="turnover-box">
                <el-link :underline="false" :href="type == 'order' ? '{!! yzWebFullUrl('order.order-list.index', ['o_time' => 'create_time']) !!}' : 'javascript:void(0)'">
                    <div class="turnover-left">
                        <div class="turnover-name">
                            <span class="turnover-des" style="position: relative;">
                                [[title_bottom]]
                                <el-tooltip class="item" effect="light" :content="tip_bottom" placement="top">
                                    <i class="iconfont icon-fontclass-gantanhao" style="position: absolute;font-size: 17px; top: -2px;"></i> 
                                </el-tooltip>
                            </span>
                        </div>
                        <div class="turnover-value">[[type == "turnover" ? main_cockpit_data.today_turnover :  type == "order" ? main_cockpit_data.today_order_count : 0]]</div>
                    </div>
                </el-link>
                <image :src="`/resources/views/survey/assets/img/${type == 'turnover' ? 'today_turnover.png' : type == 'order' ? 'today_order.png' : '' }`" style="width: 71px;height: 71px"></image>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('turnover_left', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "turnover"
            },
            title: {
                type: String,
                default: "营业额数据"
            },
            title_bottom: {
                type: String,
                default: "今日营业额（元）"
            },
            title_top: {
                type: String,
                default: ""
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            },
            tip_bottom: {
                type: String,
                default: ""
            },
            tip_top: {
                type: String,
                default: ""
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            console.log(this.tip_top,999999,this.type);
        },
        methods: {
        },
        template: '#turnover_left'
    });
</script>

<style scoped></style>