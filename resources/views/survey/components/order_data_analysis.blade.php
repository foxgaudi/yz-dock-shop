<style> 
</style>
<template id="order_data_analysis">
    <div class="member-box" :style="{width: width_ , height: '480px', background: '#FFFFFF' , 'border-radius': '16px 16px 16px 16px' ,padding:'24px 12px'}">
        <div class="member-top" style="display: flex;align-items: center;justify-content: space-between;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[title]]</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 80px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div style="display:flex;margin: 16px 0;">
            <div v-for="(item,index) in list_title" style="width: 30%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;
                margin-right: 20px;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;position: relative;">
                    [[item.name]]
                    <el-tooltip class="item" effect="light" :content="item.tips" placement="top">
                        <i v-if="type == 'financial'" class="iconfont icon-fontclass-gantanhao" style="position: absolute;font-size: 17px; margin-left: 5px;top: -2px;"></i> 
                    </el-tooltip>
                </div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[item.value]]</div>
            </div>
        </div>
        <div style="width: 100%%;height: calc(100% - 90px);" id="bar_line"></div>
    </div>
</template>
<script>
    Vue.component('order_data_analysis', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: 'line_order'
            },
            width_: {
                type: String,
                default: "48%"
            },
            y_show: {
                type: Boolean,
                default: false
            },
            title: {
                type: String,
                default: "订单数据分析"
            }
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                list_title: [],
                // list_type: []
            }
        },
        mounted(){
            this.turnoverAnalysis(this.search.time_type,this.search.start_time);
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async turnoverAnalysis(time_type,start_time){
                let url = "";
                if(this.type == "order") {
                    url = "{!! yzWebFullUrl('survey.survey.order-analysis') !!}"
                    
                }else if(this.type == "financial") {
                    url = "{!! yzWebFullUrl('survey.survey.member-income-statistic') !!}"
                }
                let {data : { result , data , msg}} = await this.$http.post(url,{
                    time_type,
                    start_time,
                })
                if(result) {
                    if(this.type == "order") {
                        this.list_title = [{
                            name: "订单总数（单）",
                            color: "#2CC08D",
                            value: data.total
                        },{
                            name: "下单人数（个）",
                            color: "#4D8BFC",
                            value: data.order_people
                        }]
                        this.init(data.chart_data);
                    }else if(this.type == "financial") {
                        this.list_title = data.statistic
                        this.init(data.chart);
                    }
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.turnoverAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(data) {
                var chartDom = document.getElementById('bar_line');
                var myChart = echarts.init(chartDom);
                let series= [];
                if(this.type == "order") {
                    series.push({
                        name: "下单人数",
                        type: 'bar',
                        yAxisIndex: 1,
                        data: data.order_people.series,
                        itemStyle: {
                            // 为每个柱子设置颜色
                            color: "#2CC08D"
                        }
                    })
                    series.push({
                        name: "订单件数",
                        type: 'line',
                        yAxisIndex: 1,
                        data: data.order_count.series,
                        itemStyle: {
                            // 为每个柱子设置颜色
                            color: "#4D8BFC"
                        }
                    })
                }else if(this.type == "financial") {
                    for(let item in data) {
                        series.push({
                            name: data[item].name,
                            type: 'line',
                            yAxisIndex: 1,
                            data: data[item].series,
                            itemStyle: {
                                // 为每个柱子设置颜色
                                color: data[item].color
                            }
                        })
                    }
                }
                var option  = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                        }
                    },
                    legend: {
                        data: series.map(item => item.name)
                    },
                    grid: {
                        left: "3%",
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: this.type == "order" ? data.order_count ?  data.order_count.x_axis : [] : this.type == "financial" ? data.amount_total ? data.amount_total.x_axis : [] : [],
                            axisPointer: {
                                type: 'shadow'
                            }
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            name: '',
                            min: 0,
                            interval: 50
                        },
                        {
                            type: 'value',
                            name: '',
                            show: this.y_show
                        }
                    ],
                    series
                };

                myChart.setOption(option);
            }
        },
        template: '#order_data_analysis'
    });
</script>

<style scoped></style>