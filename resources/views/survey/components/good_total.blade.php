<style> 
</style>
<template id="good_total">
    <div style="width: 48%;display: flex;justify-content: center; position: relative;" >
        <div style="width: 450px;height: 450px;background-image: url('/resources/views/survey/assets/img/goods_num.png'); /* 替换为你的图片路径 */
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;position: absolute;"></div>
        <div class="goods-in-num" style="display: flex;flex-direction: column;align-items: center; z-index: 1;position: absolute;top: 200px;">
            <div style="margin-left: 10px;">商品总数（件）</div>
            <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 5px;">[[main_cockpit_data.goods_count]]</div>
        </div>
        <div id="good_pie" style="width:500px;height:500px;position: absolute;top: -30px;margin: 5px 0 0 5px;"></div>
    </div>
</template>
<script>
    Vue.component('good_total', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                var chartDom = document.getElementById('good_pie');
                var myChart = echarts.init(chartDom);
                var option = {
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        top: 'bottom',
                        padding:[0,0,0,0],
                    },
                    color: ['#2CC08D', '#4D8BFC'],
                    series: [
                        {
                        name: '',
                        type: 'pie',
                        radius: ['46%', '36%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 0,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        label: {
                            show: false
                        },
                        data: [
                            { value: this.main_cockpit_data.goods_grounding, name: '上架商品数' },
                            { value: this.main_cockpit_data.goods_off_grounding, name: '下架商品数' }
                        ],
                        emphasis: {
                            label: {
                            show: true,
                            fontSize: 20,
                            fontWeight: 'bold'
                            }
                        }
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },
        template: '#good_total'
    });
</script>

<style scoped></style>