<style> 
    .title_ {
        font-weight: 400;
        font-size: 18px;
        color: #333333;
    }
    .active-title {
        font-weight: bold;
        font-size: 20px;
        color: #333333;
    }
</style>
<template id="line_order">
    <div class="member-box" :style="{width: width_ , height: '480px', background: '#FFFFFF' , 'border-radius': '16px 16px 16px 16px' , padding: '12px'}">
        <div class="member-top" style="display: flex;align-items: center;justify-content: space-between;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;" v-if="title">[[title]]</span>
            <div v-if="!title" style="display: flex;">
                <span :class="active_title == item.value ? 'active-title' : 'title_'" 
                    style="display: flex;flex-direction: column;margin-right: 40px;align-items: center;" 
                    v-for="(item,index) in title_list"
                    @click="clickTitle(item.value)">
                    <span style="margin-bottom: 5px; cursor: pointer;">[[item.name]]</span>
                    <span v-if="active_title == item.value " style="display: flex;width: 35px;height: 4px;background: #2CC08D;border-radius: 30px 30px 30px 30px;"></span>
                </span>
            </div>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div style="display:flex;margin: 16px 0;">
            <div 
                style="width: 30%;height: 80px;background: #F9F9F9;
                border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;
                margin-right: 20px;"
                v-for="(item,index) in list_type">
                <div style="font-weight: 400;font-size: 14px;color: #707070; position: relative;">
                    [[item.name]]
                    <el-tooltip class="item" effect="light" :content="item.tips" placement="top">
                        <i v-if="type == 'financial_line'" class="iconfont icon-fontclass-gantanhao" style="position: absolute;font-size: 17px; margin-left: 5px;top: -2px;"></i> 
                    </el-tooltip>
                </div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">
                    <span>[[item.order_count]]</span>
                    <span  v-if="type == 'trading_'" style="font-weight: 400;font-size: 12px;color: #707070;margin-left: 10px;margin-right: 5px;">昨日 [[item.yesterday > 0 ? `+${item.yesterday}` : item.yesterday]]</span>
                    <span  v-if="type == 'trading_'" style="font-weight: 400;font-size: 12px;color: #FF5B6C;">今日 [[item.today > 0 ? `+${item.today}` : item.today]] <i class="el-icon-caret-top"></i> </span>
                </div>
            </div>
        </div>
        <div :id="type" style="width: 100% ; height: calc(100% - 140px);padding: 0px 16px;"></div>
    </div>
</template>
<script>
    Vue.component('line_order', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: 'line_order'
            },
            title: {
                type: String,
                default: "订单分析"
            },
            title_list: {
                type: Array,
                default: () => {
                    return []
                }
            },
            width_: {
                type: String,
                default: '74%'
            },
            active_title: {
                type: Number || String,
                default: 2
            },
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                list_type: []
            }
        },
        mounted(){
            this.turnoverAnalysis(this.search.time_type,this.search.start_time);
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async turnoverAnalysis(time_type,start_time){
                let url = "";
                let chartData = "";
                if(this.type == "line_order") {
                    url = "{!! yzWebFullUrl('survey.survey.order-analysis') !!}";
                }else if(this.type == "distribution_mode") {
                    url = "{!! yzWebFullUrl('survey.survey.delivery-method') !!}";
                }else if(this.type == "trading_") {
                    url = "{!! yzWebFullUrl('survey.survey.member-access-data') !!}";
                }else if(this.type == "financial_line") {
                    if(this.active_title == 1) {
                        url = "{!! yzWebFullUrl('survey.survey.balance-statistic') !!}"
                    }else if(this.active_title == 2) {
                        url = "{!! yzWebFullUrl('survey.survey.point-statistic') !!}"
                    }else if(this.active_title == 3) {
                        url = "{!! yzWebFullUrl('survey.survey.coupon-statistic') !!}"
                    }
                }
                let {data : { result , data , msg}} = await this.$http.post(url,{
                    time_type,
                    start_time,
                })
                if(result) {
                    this.list_type = [];
                    if(this.type == "line_order") {
                        chartData = data.chart_data
                    }else if(this.type == "distribution_mode") {
                        chartData = data.chart 
                        this.list_type = data.total;
                    }else if(this.type == "trading_") {
                        chartData = data.chart 
                        this.list_type = [{
                            name: "访客量",
                            order_count: data.visitor_count,
                            yesterday: data.yesterdayNewVisitor,
                            today: data.todayNewVisitor
                        },{
                            name: "浏览量",
                            order_count: data.browse_count,
                            yesterday: data.yesterdayNewVBrowse,
                            today: data.todayNewBrowse
                        }];
                    }else if(this.type == "financial_line") {
                        chartData = data.chart 
                        for(let item of data.statistic) {
                            this.list_type.push({
                                ...item,
                                order_count: item.value
                            })
                        }
                        this.$emit("recharge_data", data)
                    }
                    this.init(chartData,data);
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.turnoverAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(data_,allData) {
                var chartDom = document.getElementById(this.type);
                var myChart = echarts.init(chartDom);
                let arrSeries = [];
                let xAxis_ = [];
                if(this.type == "distribution_mode") {
                    for(let item of data_) {
                        arrSeries.push({
                            name: item.name,
                            type:'line',
                            data: item.series,
                            itemStyle: {
                                normal: {
                                    color: item.color,
                                    lineStyle: {
                                        color: item.color
                                    }
                                }
                            }
                        })
                    }
                    xAxis_ =  data_.length ? data_[0].x_axis : []
                }else if(this.type == "line_order") {
                    for(let item in data_) {
                        if(item !== "order_count" && item !== "order_people") {
                            arrSeries.push({
                                name: data_[item].name,
                                type:'line',
                                data: data_[item].series,
                                itemStyle: {
                                    normal: {
                                        color: data_[item].color,
                                        lineStyle: {
                                            color: data_[item].color
                                        }
                                    }
                                }
                            })
                            this.list_type.push({
                                name: data_[item].name,
                                order_count: item == "order_wait_pay" ? allData.wait_pay_total : item == "order_wait_send" ?
                                allData.wait_send_total : item == "order_send" ? allData.send_total :  item == "refund_order" ?
                                allData.refund_order : item == 'order_complete' ? allData.complete_total : ""
                            })
                        }
                        
                    }
                    xAxis_ = data_.order_count ? data_.order_count.x_axis : []
                }else if(this.type == "trading_") {
                    for(let item in data_) {
                        arrSeries.push({
                            name: data_[item].name,
                            type:'line',
                            data: data_[item].series,
                            itemStyle: {
                                normal: {
                                    color: item.color,
                                    lineStyle: {
                                        color: item.color
                                    }
                                }
                            }
                        })
                    }
                    xAxis_ = data_.browse ? data_.browse.x_axis : []
                }else if(this.type == "financial_line") {
                    for(let item in data_) {
                        arrSeries.push({
                            name: data_[item].name,
                            type:'line',
                            data: data_[item].series,
                            itemStyle: {
                                normal: {
                                    color: data_[item].color,
                                    lineStyle: {
                                        color: data_[item].color
                                    }
                                }
                            }
                        })
                    }
                    if(this.active_title == 1) {
                        xAxis_ = data_.income_balance ? data_.income_balance.x_axis : []
                    }else if(this.active_title == 2) {
                        xAxis_ = data_.give_point ? data_.give_point.x_axis : []
                    }else if(this.active_title == 3) {
                        xAxis_ = data_.deduction_price ? data_.deduction_price.x_axis : []
                    }
                }
                let newLegend = this.list_type.map(item => item.name)
                var option  = {
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: newLegend
                    },
                    grid: {
                        left: '3%',
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: xAxis_
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: arrSeries
                };
                myChart.setOption(option);
            },
            clickTitle(value) {
                this.search.time_type = "day"
                this.search.start_time = ""
                this.$emit("update:active_title", value);
                this.$emit('event_title',value);
                this.$nextTick(() => {
                    this.turnoverAnalysis(this.search.time_type,this.search.start_time);
                })
            }
        },
        template: '#line_order'
    });
</script>

<style scoped></style>