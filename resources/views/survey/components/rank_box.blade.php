<style> 
</style>
<template id="rank_box">
    <div class="rank_box" style="width: 25%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
        <div class="turnover-analysis" style="display: flex;align-items: center;justify-content: space-between;margin: 16px;white-space: nowrap;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">商品销售排名</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-date-picker
                    v-model="search.start_time"
                    type="date"
                    placeholder="选择日期"
                    style="width: 80%;margin-left: 5px"
                    @change="goodsSaleRanking">
                </el-date-picker>
            </div>
        </div>
        <div class="ranking-box" style="padding: 0 16px;">
            <div class="rank-name rank-bg " style="font-weight: bold;font-size: 14px;color: #333333;display: flex;padding: 7px 16px;">
                <span style="flex: 1;">排名</span>
                <span style="flex: 2;">商品名称</span>
                <span style="flex: 1;">销量</span>
            </div>
            <div :class="(index%2) === 0 ? '' : 'rank-bg'"v-for="(item,index) in rankingList" style="font-weight: 400;font-size: 14px;color: #333333;display: flex;padding: 7px 16px;">
                <span style="flex: 1;">
                    <img :src="`/resources/views/survey/assets/img/${index == 0 ? 'rank_1.png' : index == 1 ? 'rank_2.png' : 'rank_3.png'}`" alt="" v-if="index <= 2">
                    <span v-if="index > 2" :style="{'margin-left': index > 2 && index < 9  ? '10px' :  index == 9 ? '5px' : ''}">[[item.key]]</span>
                </span>
                <span style="flex: 2;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" :title="item.title">[[item.title]]</span>
                <span style="flex: 1;">[[item.value]]</span>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('rank_box', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                search: {
                    start_time: ""
                },
                rankingList: []
            }
        },
        mounted(){
            this.goodsSaleRanking();
        },
        methods: {
            async goodsSaleRanking(){
                let {data : { result , data , msg}} = await this.$http.post("{!! yzWebFullUrl('survey.survey.goods-sale-ranking') !!}",{
                    start_time: this.search.start_time ? this.search.start_time / 1000 : ""
                })
                if(result) {
                    this.rankingList = data;
                }else {
                    this.$message.error(msg)
                }
            },
        },
        template: '#rank_box'
    });
</script>

<style scoped></style>