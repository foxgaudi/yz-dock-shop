<style> 
  
</style>
<template id="goods_data">
    <div class="goods-data" style="width: 25%;
        height: 480px;
        background: linear-gradient( 360deg, #FBFDFE 0%, #E6F3FE 36%, #C1DCFF 100%);
        border-radius: 16px 16px 16px 16px;
        border: 1px solid #FFFFFF;
        padding: 22px 0;">
        <div style="max-width: 154px;
            height: 49px;
            background: linear-gradient( 339deg, rgba(255,255,255,0) 0%, #B2F1E4 20%, #4D8BFC 100%);
            border-radius: 0px 24px 24px 0px;
            font-weight: bold;
            font-size: 22px;
            color: #FFFFFF;
            text-align: left;
            line-height: 49px;
            padding: 0 22px;
            position: relative;">
                [[title]] 
                <el-tooltip class="item" effect="light" :content="tip_" placement="top">
                    <i v-if="type == 'trading'" class="iconfont icon-fontclass-gantanhao" style="position: absolute;right: 15px;font-size: 22px;"></i> 
                </el-tooltip>
            </div>
        <div class="goods-box" style="display: flex;flex-direction: column;justify-content: center;align-items: center;">
            <div v-for="(item,index) in list_data" :key="index" style="margin-top:16px;justify-content: space-between;align-items: center;display:flex;width: 90%;height: 114px;padding:20px; 20px;background: #FFFFFF;border-radius: 20px 20px 20px 20px;">
                <div class="" style="">
                    <div style="font-weight: 400;font-size: 16px;color: #333333;">
                        [[item.name]]
                        <el-tooltip class="item" effect="light" :content="item.tip" placement="top">
                            <i class="iconfont icon-fontclass-gantanhao" style="width: 16px;height: 16px;color: #D1D4D9;" v-if="type !== 'goods'"> </i>
                        </el-tooltip>
                    </div>
                    <div style="font-weight: bold;margin-top:10px;font-size: 24px;color: #333333;">[[item.num]]</div>
                </div>
                <image :src="item.url" style="width: 71px;height: 71px"></image>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('goods_data', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "goods"
            },
            title: {
                type: String,
                default: "商品数据"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            },
            tip_: {
                type: String,
                default: ""
            }
        },
        data() {
            return {
                list_data: []
            }
        },
        mounted(){
            if(this.type == "trading") {
                this.list_data = [{
                    name: "总交易额（元）",
                    num: this.main_cockpit_data.total_trade,
                    url: "/resources/views/survey/assets/img/trading_pie.png",
                    tip: " 统计所有的交易明细总和"
                },{
                    name: "充值金额（元）",
                    num:  this.main_cockpit_data.total_recharge,
                    url: "/resources/views/survey/assets/img/trading_pie.png",
                    tip: "统计所有的充值明细总和"
                },{
                    name: "交易订单（单）",
                    num: this.main_cockpit_data.total_order,
                    url: "/resources/views/survey/assets/img/trading_pie.png",
                    tip: " 统计所有的交易订单总和"
                }]
            }else if(this.type == "goods") {
                this.list_data = [{
                        name: "商品总数（件）",
                        num: this.main_cockpit_data.goods_count,
                        url: "/resources/views/survey/assets/img/goods_totall.png"
                    },{
                        name: "上架商品数（件）",
                        num: this.main_cockpit_data.goods_grounding,
                        url: "/resources/views/survey/assets/img/listing.png"
                    },{
                        name: "下架商品数（件）",
                        num: this.main_cockpit_data.goods_off_grounding,
                        url: "/resources/views/survey/assets/img/delist.png"
                    }]
            }
        },
        methods: {
        },
        template: '#goods_data'
    });
</script>

<style scoped></style>