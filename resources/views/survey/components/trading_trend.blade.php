<style> 
</style>
<template id="trading_trend">
    <div class="member-box" style="width: 48%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;padding: 12px;">
        <div class="member-top" style="display: flex;align-items: center;justify-content: space-between;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">交易趋势</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div style="display:flex;margin: 16px 0;">
            <div style="width: 30%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;
                margin-right: 20px;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">商品下单数（件）</div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[order_goods_count]]</div>
            </div>
            <div style="width: 30%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;line-height: 1;
                justify-content: space-between;
                display: flex;
                flex-direction: column;
                margin-right: 20px;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">商品成交数（件）</div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[pay_goods_count]]</div>
            </div>
        </div>
        <div id="line_chart" style="width: 100% ; height: calc(100% - 140px);padding: 0px 16px;"></div>
    </div>
</template>
<script>
    Vue.component('trading_trend', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                pay_goods_count: "",
                order_goods_count: ""
            }
        },
        mounted(){
            this.turnoverAnalysis(this.search.time_type,this.search.start_time);
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async turnoverAnalysis(time_type,start_time){
                let {data : { result , data , msg }} = await this.$http.post("{!! yzWebFullUrl('survey.survey.trade-trend') !!}",{
                    time_type,
                    start_time,
                })
                if(result) {
                    console.log(result , data , msg,666);
                    this.order_goods_count = data.order_goods_count;
                    this.pay_goods_count = data.pay_goods_count;
                    this.init(data.chart);
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.turnoverAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(chart) {
                var chartDom = document.getElementById('line_chart');
                var myChart = echarts.init(chartDom);
                let legendData = [];
                let seriesData = [];
                let xAxisData =  chart.cart_goods ? chart.cart_goods.x_axis : [];
                for(let item in chart){
                    legendData.push(chart[item].name)
                    seriesData.push({
                        name: chart[item].name,
                        type:'line',
                        stack: '总量',
                        data: chart[item].series,
                        itemStyle: {
                            normal: {
                                color: chart[item].color,
                                lineStyle: {
                                    color: chart[item].color
                                }
                            }
                        }
                    })
                }
                var option  = {
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: legendData
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: xAxisData
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: seriesData
                    // [
                    //     {
                    //         name:'商品加购件数',
                    //         type:'line',
                    //         stack: '总量',
                    //         data:[120, 132, 101, 134, 90, 230, 210],
                    //         itemStyle: {
                    //             normal: {
                    //                 color: "#825EE8",
                    //                 lineStyle: {
                    //                     color: "#825EE8"
                    //                 }
                    //             }
                    //         }
                    //     },
                    //     {
                    //         name:'商品成交件数',
                    //         type:'line',
                    //         stack: '总量',
                    //         data:[220, 182, 191, 234, 290, 330, 310],
                    //         itemStyle: {
                    //             normal: {
                    //                 color: "#4D8BFC",
                    //                 lineStyle: {
                    //                     color: "#4D8BFC"
                    //                 }
                    //             }
                    //         }
                    //     }, {
                    //         name:'动销商品数',
                    //         type:'line',
                    //         stack: '总量',
                    //         data:[220, 182, 191, 234, 290, 330, 310],
                    //         itemStyle: {
                    //             normal: {
                    //                 color: "#2CC08D",
                    //                 lineStyle: {
                    //                     color: "#2CC08D"
                    //                 }
                    //             }
                    //         }
                    //     },
                    // ]
                };
                myChart.setOption(option);
            }
        },
        template: '#trading_trend'
    });
</script>

<style scoped></style>