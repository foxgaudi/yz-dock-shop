<style> 
</style>
<template id="product_review">
    <div class="product-review" style="width: 25%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;padding:24px 12px;">
        <div style="font-weight: bold;font-size: 20px;color: #333333;">商品评论</div>
        <div id="radar_review" style="width: 100% ; height: calc(100% - 20px);padding: 0px 16px;"></div>
    </div>
</template>
<script>
    Vue.component('product_review', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                    var chartDom = document.getElementById('radar_review');
                    var myChart = echarts.init(chartDom);
                    let indicator = [];
                    for(let item of this.main_cockpit_data.comment) {
                        indicator.push({
                            text: item.level === 0 ? "0" : item.level,
                        })
                    }
                    console.log(this.main_cockpit_data.comment.map(citem => citem.comment_count),indicator,4444);
                    var option = {
                        title: {
                            text: ''
                        },
                        legend: {
                            data: ['Allocated Budget'],
                            show: false
                        },
                        textStyle: {
                            //设置颜色
                            color: '#000000'
                        },
                        radar: {
                            // shape: 'circle',
                            splitArea: {
                                areaStyle: {
                                    color: '#fff'
                                }
                            },
                            indicator
                        },
                        series: [
                            {
                                name: '',
                                type: 'radar',
                                data: [
                                    {
                                        value: this.main_cockpit_data.comment.map(citem => citem.comment_count),
                                        name: ''
                                    }
                                ],
                                // 设置雷达图边线颜色
                                lineStyle: {
                                    color: '#2CC08D' // 红色
                                },
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2,
                                        color: '#2CC08D'
                                    }
                                }
                            }
                        ]
                    };

                    myChart.setOption(option);
                }
        },
        template: '#product_review'
    });
</script>

<style scoped></style>