<style> 
    .turnover-analysis-right {
        width: 25%;
        height: 440px;
        background: #FFFFFF;
        border-radius: 16px 16px 16px 16px;
    }
    .turnover-analysis {
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin: 16px;
        white-space: nowrap;
    }
    .total-business-volume {
        width: 194px;
        height: 88px;
        background: #F9F9F9;
        border-radius: 12px 12px 12px 12px;
        padding: 20px 12px 18px 20px;    
        display: flex;
        /* flex-direction: column; */
        /* justify-content: space-between; */
        margin-left: 16px;
    }
</style>
<template id="turnover_right">
    <div class="turnover-analysis-right" :style="{height : height , width: width_ }">
        <div class="turnover-analysis">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[title]]</span>
            <div style="display: flex;justify-content: flex-end;" v-if="type !== 'age_consumption'">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div style="display: flex;">
            <div class="total-business-volume" v-for="(item,index) in list_data" :key="index">
                <el-link :underline="false" :href="item.link">
                    <div style="font-weight: 400;font-size: 14px;color: #707070;margin-bottom: 10px;">[[item.name]]</div>
                    <div style="font-weight: bold;font-size: 22px;color: #333333;">[[item.num]]</div>
                </el-link>
            </div>
        </div>
        <div :id="type" :style="{width: '100%', height: height_bar }"></div>
    </div>
</template>
<script>
    Vue.component('turnover_right', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "turnover"
            },
            height: {
                type: String,
                default: "440px"
            },
            height_bar: {
                type: String,
                default: "calc(100% - 120px)"
            },
            width_: {
                type: String,
                default: "25%"
            },
            title: {
                type: String,
                default: "营业额数据分析"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                list_data: []
            }
        },
        mounted(){
            if(this.type == "age_consumption") {
                this.init(this.main_cockpit_data.age_turnover.x_axis,this.main_cockpit_data.age_turnover.series);
            }else if(this.type == "turnover" || this.type == "trading_bar") {
                this.turnoverAnalysis(this.search.time_type,this.search.start_time);
            }
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            }
        },
        methods: {
            async turnoverAnalysis(time_type,start_time){
                let url = "";
                if(this.type == "turnover") {
                    url = "{!! yzWebFullUrl('survey.survey.turnover-analysis') !!}"
                }else if(this.type == "trading_bar") {
                    url = "{!! yzWebFullUrl('survey.survey.member-data-analysis') !!}"
                }
                let {data : { result , data , msg}} = await this.$http.post(url,{
                    time_type,
                    start_time,
                })
                if(result) {
                    if(this.type == "turnover") {
                        this.list_data.push({
                            name: "营业总额（元）",
                            num: data.total,
                        })
                        this.init(data.chart_data.x_axis,data.chart_data.series);
                    }else if(this.type == "trading_bar") {
                        this.list_data.push({
                            name: "会员总数（人）",
                            num: data.total_count,
                            link: '{!! yzWebFullUrl('member.member') !!}',
                        },{
                            name: "新增会员数（人）",
                            num: data.today_count,
                        })
                        this.init(data.chart.x_axis,data.chart.series);
                    }
                    
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                // this.list_data.splice(0,1)
                this.list_data = [];
                this.turnoverAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
            init(x_axis,series) {
                var chartDom = document.getElementById(this.type);
                var myChart = echarts.init(chartDom);
                var option = {
                    xAxis: {
                        type: 'category',
                        data: x_axis
                    },
                    yAxis: {
                        type: 'value'
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    series: [
                        {
                            data: series,
                            type: 'bar',
                            barWidth: '22',
                            itemStyle: {
                                color: '#2CC08D'
                            }
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },
        template: '#turnover_right'
    });
</script>

<style scoped></style>