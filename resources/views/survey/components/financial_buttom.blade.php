<style> 
</style>
<template id="financial_buttom">
    <div style="display: flex;justify-content: space-between;margin-top: 20px;">
        <line_order type="financial_line" :list_type="points_list" title="" :active_title.sync="active_title" :title_list="title_list" 
        @event_title="eventTitle" @recharge_data="getRechargeData"></line_order>
        <integration_path v-if="(active_title == 2 || active_title == 3) && infoRecharge" :active_title="active_title" type="financial_path"  :title="point_title" :main_cockpit_data="recharge_data"></integration_path>
        <product_sort type="financial_balance" title="余额充值占比" v-if="active_title == 1 && infoRecharge" :main_cockpit_data="recharge_data"></product_sort>
    </div>
</template>
<script>
    Vue.component('financial_buttom', {
        delimiters: ['[[', ']]'],
        props: {

        },
        data() {
            return {
                points_list: [{
                    name: "可使用积分",
                    color: "#2CC08D",
                    num: "136,213"
                },{
                    name: "已消耗积分",
                    color: "#4D8BFC",
                    num: "136,213"
                },{
                    name: "已赠送积分",
                    color: "#825EE8",
                    num: "136,213"
                },{
                    name: "充值积分",
                    color: "#FF8E5B",
                    num: "136,213"
                }],
                title_list: [{
                    name: "余额数据统计",
                    value: 1
                },{
                    name: "积分数据统计",
                    value: 2
                },{
                    name: "优惠券数据统计",
                    value: 3
                }],
                active_title: 1,
                point_title: "",
                recharge_data: {},
                infoRecharge: false
            }
        },
        mounted(){

        },
        methods: {
            eventTitle(active_title) {
                this.infoRecharge = false;
                // this.active_title = active_title;
                this.point_title = active_title == 3 ? '优惠券获取途径' : active_title == 2 ? '积分获取途径' : "";
                console.log(active_title);
            },
            getRechargeData(data) {
                this.recharge_data = data;
                this.infoRecharge = true;
            }
        },
        template: '#financial_buttom'
    });
</script>

<style scoped></style>