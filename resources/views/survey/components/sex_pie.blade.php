<style> 
</style>
<template id="sex_pie">
    <div style="width: 25%;height: 480px;background: #FFFFFF; padding: 24px 12px;border-radius: 16px 16px 16px 16px;">
        <div class="oder-analysis" style="justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">性别占比图</span>
        </div>
        <div id="sex_pie" style="width: 100% ; height: calc(100% - 10px); margin-top: 20px;"></div>
    </div>
</template>
<script>
    Vue.component('sex_pie', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "sex_pie"
            },
            title: {
                type: String,
                default: "商品分类占比"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                var chartDom = document.getElementById('sex_pie');
                var myChart = echarts.init(chartDom);
                let arrData = [];
                if(this.type == "trading") {
                    for(let item of this.main_cockpit_data.gender) {
                        arrData.push({
                            value: item.value,
                            name: item.name,
                            itemStyle: {
                                normal: {
                                    color: item.color
                                }
                            },
                        })
                    }
                }
                var option = {
                    title: {
                        text: ''
                    },
                    legend: {
                        left: 'left',
                        icon: 'circle'
                    },
                    tooltip: {
                        trigger: 'item',
                    },
                    series: [
                        {
                        type: 'pie',
                        radius: '65%',
                        center: ['50%', '50%'],
                        selectedMode: 'single',
                        data: arrData,
                        emphasis: {
                            itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                        }
                    ]
                };

                option && myChart.setOption(option);
            }
        },
        template: '#sex_pie'
    });
</script>

<style scoped></style>