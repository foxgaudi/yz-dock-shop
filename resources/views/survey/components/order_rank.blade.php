<style> 
</style>
<template id="order_rank">
    <div class="order_rank" style="width: 25%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
        <div class="turnover-analysis" style="display: flex;align-items: center;justify-content: space-between;margin: 16px;white-space: nowrap;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">订单排名</span>
            <div style="display: flex;justify-content: flex-end;">
                <el-select v-model="search.time_type" placeholder="日" style="width: 70px;"  @change="dateChanged">
                    <el-option
                        v-for="item in optionsDay"
                        :key="item.value"
                        :label="item.name"
                        :value="item.value">
                    </el-option>
                </el-select>
                <el-date-picker v-model="search.start_time" v-show="datePickerType !=='daterange'" :type="datePickerType" placeholder="选择日期"
                    :format="datePickerFormat"
                    clearable
                    :picker-options="{'firstDayOfWeek': 1}"
                    @change="dateChanged()"
                    style="width: 60%;margin-left: 5px">
                </el-date-picker>
            </div>
        </div>
        <div class="ranking-box" style="padding: 0 16px;">
            <div class="rank-name rank-bg " style="font-weight: bold;font-size: 14px;color: #333333;display: flex;padding: 7px 16px;">
                <span style="flex: 2;">排名</span>
                <span style="flex: 2;">地区</span>
                <span style="flex: 2;">支付金额</span>
                <span style="flex: 2;">支付笔数</span>
            </div>
            <div :class="(index%2) === 0 ? '' : 'rank-bg'"v-for="(item,index) in areaList" style="font-weight: 400;font-size: 14px;color: #333333;display: flex;padding: 7px 16px;">
                <span  style="flex: 2;">
                    <img :src="`/resources/views/survey/assets/img/${index == 0 ? 'rank_1.png' : index == 1 ? 'rank_2.png' : 'rank_3.png'}`" alt="" v-if="index <= 2">
                    <span v-if="index > 2" :style="{'margin-left': index > 2 && index < 9  ? '10px' :  index == 9 ? '5px' : ''}">[[index + 1]]</span>
                </span>
                <span style="flex: 2 ">[[item.areaname]]</span>
                <span style="flex: 2 ">￥[[item.order_count]]</span>
                <span style="flex: 2;">[[item.order_count]]</span>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('order_rank', {
        delimiters: ['[[', ']]'],
        props: {
        
        },
        data() {
            return {
                datePickerFormat: "",
                search: {
                    time_type: "day",
                    start_time: ""
                },
                optionsDay: [{
                    name: "日",
                    value: "day"
                },{
                    name: "周",
                    value: "week"
                },{
                    name: "月",
                    value: "month"
                }],
                areaList: []
            }
        },
        mounted(){
            this.turnoverAnalysis(this.search.time_type,this.search.start_time);
        },
        computed: {
            datePickerType() {
                switch (this.search.time_type) {
                    case "day":
                    this.datePickerFormat = "yyyy 年 MM 月 dd 日";
                    return "date";
                    break;
                    case "week":
                    this.datePickerFormat = "yyyy 第 WW 周";
                    return "week";
                    break;
                    case "month":
                    this.datePickerFormat = "yyyy 年 MM月";
                    return "month";
                    break;
                }
            },
        },
        methods: {
            async turnoverAnalysis(time_type,start_time){
                let {data : { result , data , msg}} = await this.$http.post("{!! yzWebFullUrl('survey.survey.order-ranking') !!}",{
                    time_type,
                    start_time,
                })
                if(result) {
                    this.areaList = data;
                }else {
                    this.$message.error(msg)
                }
            },
            dateChanged() {
                this.turnoverAnalysis(this.search.time_type,new Date(this.search.start_time).getTime() / 1000);
            },
            handelDate(date){
                date[0] = date[0].getFullYear()+'-'+(date[0].getMonth()+1).toString().padStart(2,'0')+'-'+date[0].getDate().toString().padStart(2,'0')
                date[1] = date[1].getFullYear()+'-'+(date[1].getMonth()+1).toString().padStart(2,'0')+'-'+date[1].getDate().toString().padStart(2,'0')
                return date
            },
        },
        template: '#order_rank'
    });
</script>

<style scoped></style>