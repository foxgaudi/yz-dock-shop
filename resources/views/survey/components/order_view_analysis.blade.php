<style> 

</style>
<template id="order_view_analysis">
    <div class="order_view_analysis" style="width: 25%;height: 440px;padding: 22px 12px ; background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
        <div class="" style="font-weight: bold;font-size: 20px;color: #333333;">订单分析</div>
        <div style="margin-top: 20px;background: #F9F9F9;padding: 27px 21px; border-radius: 16px;display: flex;align-items: center;justify-content: space-between;line-height: 1;border-radius: 16px 16px 16px 16px;">
            <div style="display: flex;align-items: center;">
                <i class="iconfont icon-manefanxian" style="width: 24px;margin-right: 3px;font-size: 20px;color: #333333;"></i>
                <span style="font-weight: 400;font-size: 16px;color: #333333;">营业额（元）</span>
            </div>
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[main_cockpit_data.total_turnover]]</span>
        </div>
        <div style="margin-top: 20px;background: #F9F9F9;padding: 27px 21px; border-radius: 16px;display: flex;align-items: center;justify-content: space-between;line-height: 1;border-radius: 16px 16px 16px 16px;">
            <div style="display: flex;align-items: center;">
                <i class="iconfont icon-geren" style="width: 24px;margin-right: 3px;font-size: 20px;color: #333333;"></i>
                <span style="font-weight: 400;font-size: 16px;color: #333333;">下单人数（个）</span>
            </div>
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[main_cockpit_data.buyer_count]]</span>
        </div>
        <div style="margin-top: 20px;background: #F9F9F9;padding: 27px 21px; border-radius: 16px;display: flex;align-items: center;justify-content: space-between;line-height: 1;border-radius: 16px 16px 16px 16px;">
            <el-link :underline="false" href="{!! yzWebFullUrl('order.order-list.index', ['o_status' => 'waitPay']) !!}">
                <div style="display: flex;align-items: center;">
                    <i class="iconfont icon-fontclass-daifukuan" style="width: 24px;margin-right: 3px;font-size: 20px;color: #333333;"></i>
                    <span style="font-weight: 400;font-size: 16px;color: #333333;">待付款订单（单）</span>
                </div>
            </el-link>
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[main_cockpit_data.wait_pay]]</span>
        </div>
            <div style="margin-top: 20px;background: #F9F9F9;padding: 27px 21px; border-radius: 16px;display: flex;align-items: center;justify-content: space-between;line-height: 1;border-radius: 16px 16px 16px 16px;">
                <el-link :underline="false" href="{!! yzWebFullUrl('order.order-list.index', ['o_status' => 1]) !!}">
                    <div style="display: flex;align-items: center;">
                        <i class="iconfont icon-daifahuo1" style="width: 24px;margin-right: 3px;font-size: 20px;color: #333333;"></i>
                        <span style="font-weight: 400;font-size: 16px;color: #333333;">待发货订单（单）</span>
                    </div>
                </el-link>
                <span style="font-weight: bold;font-size: 20px;color: #333333;">[[main_cockpit_data.wait_send]]</span>

            </div>
    </div>
</template>
<script>
    Vue.component('order_view_analysis', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "turnover"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
        },
        methods: {
        },
        template: '#order_view_analysis'
    });
</script>

<style scoped></style>