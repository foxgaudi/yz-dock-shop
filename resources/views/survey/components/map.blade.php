<style> 
    .map-box {
        position: relative;
        top: 0;
        width: 48%;
        text-align: center;
        display: flex;
        justify-content: center;
    }
    .desc-pane {
        width: 200px;
        height: 100px;
        background: rgb(94%, 97%, 100%,0.35);
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: flex-start;
        border-radius: 10px ;
        position: absolute;
        bottom: 10px;
        left: 60px;
    }
    .amount {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
    }
    .strip-box {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .strip-lab {
        display: block;
        width: 100px;
        height: 6px;
        background: linear-gradient(90deg, #C5DAFF 0%, #4D8BFC 100%);
        border-radius: 30px 30px 30px 30px;
        margin: 0 10px;
    }
    .strip-round-box {
        display: flex;
        flex-direction: row-reverse;
        align-items: center;
        width: 100px;
        justify-content: space-between;
        margin: 0 10px;
    }
    .strip-round {
        display: block;
        background: #2CC08D;
        border-radius: 50%;
    }
</style>
<template id="map_ch">
    <div class="map-box">
        <div id="chinaMap"  style="width:700px;height:440px"></div>
        <div class="desc-pane">
            <div class="amount">
                <div style="margin-left: 30px;">[[type == "trading_map" ? "交易金额 （元）" : type == "turnover" ? "销售金额 (元)" : type == "order_map" ? "订单数量" : ""]]</div>
                <div class="strip-box">
                    <span>0</span>
                    <span class="strip-lab"></span>
                    <span>[[type == "turnover" || type == "trading_map" ? main_cockpit_data.turnover_to_area.max : type == "order_map" ?  main_cockpit_data.order_count_to_area.max : 0]]</span>
                </div>
            </div>
            <div class="amount" v-if="type == 'turnover'">
                <div style="margin-left: 30px;">门店数量 (家)</div>
                <div class="strip-box">
                    <span>0</span>
                    <div class="strip-round-box">
                        <span class="strip-round" v-for="item in 6" :style="{ width: (16 - item*2) + 'px' , height: (16 - item*2) + 'px'}"></span>
                    </div>
                    <span>[[main_cockpit_data.store_to_area.max]]</span>
                </div>
            </div>
        </div>
    </div>
</template>
<script>
    const chinaJsonUrl = "{{ resource_get('resources/views/survey/assets/maps/china.json') }}";
    Vue.component('map_ch', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "turnover"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                list_to_area: [],
                store_to_area: [],
                newGeoCoord: [],
            }
        },
        mounted(){
            if(this.type == "turnover") { //总驾驶舱
                if(this.main_cockpit_data.turnover_to_area && this.main_cockpit_data.turnover_to_area.data.length) {
                    for(let item of this.main_cockpit_data.turnover_to_area.data) {
                        if(item.areaname) {
                            this.list_to_area.push({
                                name: item.areaname.includes("省") ? item.areaname.replace("省", "") : item.areaname.includes("自治区") ? item.areaname.replace("自治区", "") : item.areaname,
                                value: item.turnover
                            })
                        } 
                    }
                }
                if(this.main_cockpit_data.store_to_area && this.main_cockpit_data.store_to_area.data.length) {
                    for(let item of this.main_cockpit_data.store_to_area.data) {
                        if(item.areaname) {
                            this.store_to_area.push({
                                name: item.areaname.includes("省") ? item.areaname.replace("省", "") : item.areaname.includes("自治区") ? item.areaname.replace("自治区", "") : item.areaname,
                                value: item.store_count
                            })
                        }
                    }
                }
                for(let item of this.list_to_area) {
                    for(citem of this.store_to_area) {
                        if(citem.name.includes(item.name)) {
                            item.store_count = citem.value
                        }
                    }
                }
                // console.log(this.list_to_area,'this.list_to_areathis.list_to_area',this.store_to_area,22222222);
            }else if(this.type == "order_map") {
                if(this.main_cockpit_data.order_count_to_area && this.main_cockpit_data.order_count_to_area.data.length) {
                    for(let item of this.main_cockpit_data.order_count_to_area.data) {
                        if(item.areaname) {
                            this.list_to_area.push({
                                name: item.areaname.includes("省") ? item.areaname.replace("省", "") : item.areaname.includes("自治区") ? item.areaname.replace("自治区", "") : item.areaname,
                                value: item.order_count
                            })
                        }
                    }
                }
            }else if(this.type == "trading_map") {
                if(this.main_cockpit_data.turnover_to_area && this.main_cockpit_data.turnover_to_area.data.length) {
                    for(let item of this.main_cockpit_data.turnover_to_area.data) {
                        if(item.areaname) {
                            this.list_to_area.push({
                                name: item.areaname.includes("省") ? item.areaname.replace("省", "") : item.areaname.includes("自治区") ? item.areaname.replace("自治区", "") : item.areaname,
                                value: item.turnover
                            })
                        }
                    }
                }
            }
            this.convertData(JSON.parse(JSON.stringify(this.store_to_area)).sort((a, b) => { return b.value - a.value; }), (scatterData) => {
                this.drawmap(scatterData);
            })
        },
        methods: {
            convertData(data,callback) {
                this.newGeoCoord = []
                let info = false
                console.log(chinaJsonUrl,333333333);
                this.$http.get(chinaJsonUrl).then(async res => {
                    for(let citem of  data) {
                        for(let item of res.data.features) {
                            if(citem.name == item.properties.name || item.properties.name.includes(citem.name)) {
                                this.newGeoCoord.push({
                                    name: citem.name,
                                    value: item.properties.center ? [...item.properties.center,citem.value] : ""
                                })
                            }
                        }
                    }
                    callback(this.newGeoCoord)
                });
            },
            drawmap(scatterData) {
                let allData = JSON.parse(JSON.stringify(this.list_to_area));
                for(let item of allData) {
                    if(item.value*1 >= 5000) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#5298FF',
                            areaColor: '#5298FF'
                        }
                    }else if(item.value*1 >= 2000 && item.value*1 < 5000) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#73ABFF'
                        }
                    }else if(item.value*1 >= 1000 && item.value*1 < 2000) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#8AB9FF'
                        }
                    }else if(item.value*1 >= 500 && item.value*1 < 1000) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#9EC4FF'
                        }
                    }else if(item.value*1 >= 100 && item.value*1 < 500) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#AFCFFF'
                        }
                    }else if(item.value*1 >= 1 && item.value*1 < 100) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#DCEAFF'
                        }
                    }else if(item.value*1 < 1) {
                        item.itemStyle = {
                            borderWidth: 2,
                            borderColor: '#A5BEE0',
                            areaColor: '#FCFDFF'
                        }
                    }
                }

                let scatter_ = {};
                if(this.type == "turnover") {
                    scatter_ = {
                        name: "Top 5",
                        type: "scatter",
                        coordinateSystem: "geo",
                        data: scatterData,
                        symbolSize: function(val) {
                            return val[2]*1 ? Math.floor((val[2]*1 / 1000)) >= 15 ? 15 : Math.floor((val[2]*1 / 1000)) < 8 ? 8 : Math.floor((val[2]*1 / 1000)) : val[2]*1;
                        },
                        rippleEffect: {
                            period: 1, //波纹秒数
                            brushType: 'fill', //stroke(涟漪)和fill(扩散)，两种效果
                            scale: 3 //波纹范围
                        },
                        hoverAnimation: true,
                            label: {
                            normal: {
                                show: false, //省份名称
                                position: "right",
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: "#2CC08D",
                                shadowBlur: 10,
                                shadowColor: "#2CC08D",
                                borderWidth:3 ,
                            },
                        },
                        zlevel: 5
                    }
                }
                this.option = {
                    backgroundColor: "",
                    tooltip: {
                        // trigger: 'item',
                        backgroundColor: "#fff", //设置背景图片 rgba格式
                        borderWidth: "2", //边框宽度设置1
                        borderColor: "#fff", //设置边框颜色
                        textStyle: {
                            color: "#666666" //设置文字颜色
                        },
                        extraCssText:'min-width:100px;',
                        formatter:  (params) => {
                            console.log(params,8888888);
                            // let textHtml = `<div style="color:#09c1d1;font-size:18px;margin:8px 0 0 0;">${params.name}</div>
                            // <div style="color:#dee6ea;font-size:20px;margin:8px 0 0 0;">${typeof params.value == 'object' ? params.value[2] : isNaN(params.value) ? 0 : params.value}</div>
                            // <div style="color:#dee6ea;font-size:16px;margin:0 0 8px 0;">${params.componentSubType == "map" ? "销售额" : "门店数量"}</div>`
                            // return textHtml; 
                            let textHtml = `<div style="text-align: left;font-size:18px;margin:4px 0 0 0;color:#333333;font-weight: bold;">${params.name}</div>
                            <div style="font-size:16px;margin:8px 0 0 0;">${params.componentSubType == "map" ? this.type == "turnover" ? "销售额" : this.type == "trading_map" ? "交易金额" : this.type == "order_map" ? "订单数量" : "" : "门店数量"}：${typeof params.value == 'object' ? params.value[2] : isNaN(params.value) ? 0 : params.value}${params.componentSubType == "map" ? this.type == "order_map" ? "单" :"元" : "家"}</div>
                            `
                            return textHtml; 
                        },
                    },
                    grid: {
                        left: '3%',
                        right: '10%',
                        bottom: '0%',
                        top: '0%',
                        containLabel: true,
                    },
                    // visualMap: {
                    //     show:false,
                    //     min: 0,
                    //     max: 5000,
                    //     text:['High','Low'],
                    //     realtime: false,
                    //     calculable: true,
                    //     inRange: {
                    //         color: ['#2e6ffe', 'rgba(0,0,0,0)']
                    //     }
                    // },
                    // visualMap: {
                    //     show: true,
                    //     min: Math.min.apply(null, allData.map(item => item.value)),
                    //     max: Math.max.apply(null, allData.map(item => item.value)),
                    //     realtime: false,
                    //     calculable: true,
                    //     inRange: {
                    //         // color: ['#2e6ffe', '#377ffe', '#408bfe', '#4b9cff', '#51a5ff', '#56aeff']
                    //         color: ['#73ABFF' , '#8AB9FF', '#9EC4FF', '#AFCFFF', '#DCEAFF']
                    //     },
                    //     textStyle: {
                    //         color: "#fff"
                    //     },
                    // },
                    geo: {
                        show: false,
                        map: "china",
                    },
                    //配置属性
                    series: [
                        {
                            name: "数据",
                            type: "map",
                            mapType: "china",
                            roam: false, //鼠标滑动缩放
                            // left: "20%",
                            label: {
                                normal: {
                                    show: false, //省份名称
                                    textStyle: {
                                    color: '#fff',
                                    // fontSize:8
                                    },
                                },
                                emphasis: {
                                    show: false,
                                },
                            },
                            itemStyle: {
                                emphasis:{
                                    borderWidth:1.5,
                                    borderColor:'#00d7f4',
                                    shadowColor:'#00d7f4',
                                    shadowBlur:2,
                                    areaColor:'#00d7f4',
                                    label: {
                                    textStyle: {
                                        color: '#fff'//鼠标经过字体颜色
                                    }
                                    }
                                },
                                normal: {
                                    borderColor: "#7795C9", 
                                    // areaColor: 'transparent', //默认背景颜色
                                    areaColor: "#fff"
                                },
                            },
                            scaleLimit: {
                                min: 1,
                                max: 10
                            },
                            // zoom: 1.26,
                            data: allData //数据
                        }, 
                        scatter_
                    ]
                };


                this.chart = echarts.init(document.getElementById("chinaMap"));
                this.chart.setOption(this.option);
            },
        },
        template: '#map_ch'
    });
</script>

<style scoped></style>