<style> 
</style>
<template id="product_sort">
    <div style="width: 25%;height: 480px;background: #FFFFFF; padding: 24px 12px;border-radius: 16px 16px 16px 16px;">
        <div class="oder-analysis" style="justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[title]]</span>
        </div>
        <div :id="type" style="width: 100% ; height: calc(100% - 10px)"></div>
    </div>
</template>
<script>
    Vue.component('product_sort', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: "product_sort"
            },
            title: {
                type: String,
                default: "商品分类占比"
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                let arrData = [];
                if(this.type == "order_member") {
                    for(let item of this.main_cockpit_data.buyer_level_proportion) {
                        arrData.push({
                            value: item.member_count,
                            name: item.level_name,
                            itemStyle: {
                                normal: {
                                color: item.color
                                }
                            },
                        })
                    }
                }else if(this.type == "order_sort") {
                    for(let item of this.main_cockpit_data.order_plugin_proportion) {
                        arrData.push({
                            value: item.order_count,
                            name: item.name,
                            itemStyle: {
                                normal: {
                                    color: item.color
                                }
                            },
                        })
                    }
                }else if(this.type == "trading") {
                    for(let item of this.main_cockpit_data.member_level) {
                        arrData.push({
                            value: item.member_count,
                            name: item.level_name,
                            itemStyle: {
                                normal: {
                                    color: item.color
                                }
                            },
                        })
                    }
                }else if(this.type == "goods_sort") {
                    for(let item of this.main_cockpit_data.goods_category) {
                        arrData.push({
                            value: item.goods_count,
                            name: item.category_name,
                            itemStyle: {
                                normal: {
                                    color: item.color
                                }
                            },
                        })
                    }
                }else if(this.type == "financial_balance") {
                    for(let item of this.main_cockpit_data.recharge_data) {
                        arrData.push({
                            value: item.recharge_count,
                            name: item.name,
                            itemStyle: {
                                normal: {
                                    color: item.color
                                }
                            },
                        })
                    }
                }
                var chartDom = document.getElementById(this.type);
                var myChart = echarts.init(chartDom);
                let legend = this.type == "financial_balance" ? "" : {
                    top: '5%',
                    icon: 'circle',
                    left: 'left'
                }
                var option = {
                    tooltip: {
                        trigger: 'item'
                    },
                    legend,
                    series: [
                        {
                            name: '',
                            type: 'pie',
                            radius: ['45%', '55%'],
                            center: ['50%',  '60%'],
                            padAngle: 5,
                            itemStyle: {
                                borderRadius: 3
                            },
                            label: {
                                show: this.type == "trading" ? false : true
                            },
                            data: arrData
                        }
                    ]
                };

                option && myChart.setOption(option);
            }
        },
        template: '#product_sort'
    });
</script>

<style scoped></style>