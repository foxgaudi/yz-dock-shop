<style> 
</style>
<template id="set_cp">
    <div class="setting-rol">
        <div class="vue-head">
            <div class="setting-title" style="margin: 10px 0 0 0">基础功能指引 > 设置</div>
            <div class="vue-main-title">
                <div class="vue-main-title-left"></div>
                <div class="vue-main-title-content">设置</div>
            </div>
            <el-tabs v-model="activeTab" class="active-tab" @tab-click="handleClick">
                <el-tab-pane label="快捷入口显示" name="fast">
                    <el-form label-width="200px" style="padding: 20px;">
                        <el-form-item label="设置" class="switch-style">
                            <el-switch
                                v-model="setting.is_open"
                                :active-value="1"
                                :inactive-value="0">
                            </el-switch>
                            <div class="tip">开启后按设置内容显示</div>
                        </el-form-item>
                        <el-form-item label=" " >
                            <el-button type="primary" icon="el-icon-plus" @click="openDialogCustomVisible" :disabled="activeEdit">新增自定义入口</el-button>
                            <span class="add-style" @click="clickManage(true)" v-show="!activeEdit">管理</span>
                            <span class="del-style" v-show="activeEdit" @click="delSelectData">删除</span>
                            <span class="add-style" v-show="activeEdit"@click="clickManage(false)">完成</span>
                        </el-form-item>
                        <el-form-item :label="item.title"  v-for="(item,index) in pluginData" :key="index">
                            <div class="list-rol">
                                <div class="list-row" v-for="(citem,cindex) in item.list">
                                    <el-checkbox v-model="citem.selected"  :true-label="1" :false-label="0" :disabled="item.item.indexOf('div_') == -1 && activeEdit && !citem.item"> </el-checkbox>
                                    <span class="list-line" v-show="item.item.indexOf('div_') == -1 && activeEdit && !citem.item">-</span>
                                    <span>[[citem.name]]</span>
                                    <el-input v-model="citem.sort" style="width: 120px; margin-left: 10px;" :disabled="item.item.indexOf('div_') == -1  && activeEdit && !citem.item" placeholder="排序"></el-input>
                                </div>
                            </div>
                        </el-form-item>
                    </el-form>
                </el-tab-pane> 
                <el-tab-pane label="tab列表显示" name="tab">
                    <el-form label-width="80px" style="padding: 20px;">
                        <el-form-item label=" " v-for="(item,index) in tabData" :key="index">
                            <div class="list-row" >
                                <el-checkbox v-model="item.selected" :true-label="1" :false-label="0"> </el-checkbox>
                                <span>[[item.fix_name]]</span>
                                <el-input v-model="item.name" style="width: 150px; margin-left: 10px;" placeholder="名称"></el-input>
                                <el-input v-model="item.sort" style="width: 150px; margin-left: 10px;" placeholder="排序"></el-input>
                                <i class="el-icon-delete" @click="delTabList(index)" v-if="item.item.indexOf('div_') !== -1 " style="cursor: pointer; "></i>
                            </div>
                        </el-form-item>
                        <el-form-item label=" " class="switch-style" style="margin-left: 20px;">
                            <el-button  type="primary" icon="el-icon-plus" @click="addTabList">新增tab</el-button>
                        </el-form-item>
                    </el-form>
                </el-tab-pane>
            </el-tabs>
        </div>
        <div class="vue-page" style="width: calc(100% - 110px);">
            <div class="vue-center">
                <el-button type="primary" :disabled="activeTab =='fast' && activeEdit" @click="saveTab('')">保存</el-button>
            </div>
        </div>

        
        <el-dialog
            title="新增自定义入口"
            :visible.sync="dialogCustomVisible"
            width="700px"
            center> 
            <el-form label-width="120px" style="padding:0 20px;">
                <el-form-item label="入口所属类型" class="switch-style" required>
                    <el-select v-model="customs.item" clearable placeholder="请选择" >
                        <el-option
                        v-for="item in tabData"
                        :key="item.item"
                        :label="item.name"
                        :value="item.item">
                        </el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="入口名称" class="switch-style" required>
                    <el-input v-model="customs.name" style="width: 300px;"></el-input>
                </el-form-item>
                <el-form-item label="入口图标" class="switch-style" required>
                    <div style="display: flex;align-items: flex-end;">
                        <div @click="uploadShow = true" style="flex-direction: column;width: 100px;height: 100px;border: 1px dashed #ccc;display: flex;justify-content: center;align-items: center;cursor: pointer;">
                            <div style="display: flex;justify-content: center;align-items: center;flex-direction: column;" v-if="!customs.icon">
                                <i class="el-icon-plus" style="font-size: 30px;"></i>
                                <span style="line-height: 2;">选择图片</span>
                            </div>
                            <div style="flex-direction: column;display: flex;" v-if="customs.icon">
                                <img :src="customs.icon" alt="" style="width: 100px;height: 80px;">
                                <span style="line-height: 20px;height: 20px;color: #fff;text-align: center;width: 100px;background: #000;opacity: 0.7;font-size: 12px;">点击重新上传</span>
                            </div>
                        </div>
                        <div style="line-height: 1;margin-left: 20px;">建议尺寸：35 * 35</div>
                    </div>
                </el-form-item>
                <el-form-item label="是否为插件" class="switch-style">
                    <el-switch
                        v-model="customs.is_plugin"
                        :active-value="1"
                        :inactive-value="0"
                        style=" margin-top: 10px;">
                    </el-switch>
                </el-form-item>
                <el-form-item label="入口跳转链接" class="switch-style" required>
                    <el-input v-model="customs.route" style="width: 300px;"></el-input>
                    <div class="tip">注:请确保设置页面链接对应的插件是开启的，否则跳转会报错</div>
                </el-form-item>
            </el-form>
            <span slot="footer" class="dialog-footer">
                <el-button @click="dialogCustomVisible = false">取 消</el-button>
                <el-button type="primary" @click="sureAddPluginData" style="margin-left: 30px;">确 定</el-button>
            </span>
        </el-dialog>

        <upload-multimedia-img 
            :upload-show="uploadShow" 
            type="1" 
            sel-num="one"
            @sure="sureImg"
            @replace="uploadShow = false"
            >
        </upload-multimedia-img>
    </div>
</template>
<script>
    Vue.component('set_cp', {
        delimiters: ['[[', ']]'],
        props: {
            active_type: {
                type: Number,
                default: 1
            }
        },
        data() {
            return {
                //设置
                uploadShow: false,
                activeTab: "fast",
                setting: {
                    is_open: 1
                }, 
                customs : {
                    item: "",
                    name: "",
                    route: "",
                    icon: "",
                    attachment: "",
                    is_plugin: 0
                },
                activeEdit: false,
                dialogCustomVisible: false,
                pluginData: [],
                tabData: []
            }
        },
        mounted(){

        },
        methods: {
            // 点击设置
            clickSet() {
                this.$emit("update:active_type", 2);
                this.$http.post("{!! yzWebFullUrl('survey.survey.showGuide') !!}").then(function(response) {
                    if (response.data.result) {
                        this.pluginData = response.data.data.list ? response.data.data.list : response.data.data;
                        this.setting.is_open = response.data.data.is_open ? response.data.data.is_open : 0;
                        for(let item of this.pluginData) {
                            for(let citem of item.list) {
                                citem.is_self = true;
                            }
                        }
                        // this.tabData = response.data.data;
                        // for(let item of response.data.data) {
                        //     item.fix_name = item.name;
                        // }
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    this.$message({
                        message: response.data.msg,
                        type: 'error'
                    });
                });
            },
            handleClick(tab, event) {
                if(this.activeTab == "tab") {
                    this.showTab();
                }else {
                    this.showGuide();
                }
            },
            // 确定
            sureAddPluginData() {
                if(this.customs.item == "") {  
                    this.$message.error("入口所属类型为空") 
                    return 
                };
                if(this.customs.name == "") {  
                    this.$message.error("入口名称为空填") 
                    return 
                };
                if(this.customs.icon == "") {  
                    this.$message.error("入口图标为空") 
                    return 
                };
                if(this.customs.route == "") {  
                    this.$message.error("入口跳转链接为空") 
                    return 
                };
                let index_ = this.pluginData.findIndex(item => item.item == this.customs.item);
                if(index_ !== -1 ) {
                    this.pluginData[index_].list.push({
                        icon: this.customs.attachment,
                        name: this.customs.name,
                        route: this.customs.route,
                        selected: 1,
                        sort: "",
                        is_plugin: this.customs.is_plugin,
                        item: `div_${this.guid()}`
                    })
                }else {
                    let filterData = this.tabData.filter(item => {
                        return item.item == this.customs.item
                    })
                    this.pluginData.push({
                        item: filterData[0].item,
                        title: filterData[0].name,
                        list: [{
                            icon: this.customs.attachment,
                            name: this.customs.name,
                            route: this.customs.route,
                            selected: 0,
                            sort: "",
                            is_plugin: this.customs.is_plugin,
                            item: `div_${this.guid()}`
                        }]
                    })
                }
                this.dialogCustomVisible = false;
            },
            saveTab(msg) {
                let form = {}
                if(this.activeTab == "tab") {
                    form = this.tabData.map(item => {
                        return {
                            selected : item.selected,
                            name : item.name,
                            sort : item.sort,
                            item : item.item
                        }
                    })
                }else {
                    form.list = this.pluginData.map(item => {
                        return {
                            ...item,
                            list: item.list.map(({ is_self , ...rest }) => rest)
                        }
                    });
                    form.is_open = this.setting.is_open;
                }
                let url = this.activeTab == "tab" ? "{!! yzWebFullUrl('survey.survey.addTab') !!}" : "{!! yzWebFullUrl('survey.survey.saveGuide') !!}";
                this.$http.post(url,{ form }).then(function(response) {
                    if (response.data.result) {
                        this.$message({
                            message: msg ? msg : response.data.msg,
                            type: 'success'
                        });
                        if(this.activeTab == "tab") {
                            this.showTab();
                        }
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    this.$message({
                        message: response.data.msg,
                        type: 'error'
                    });
                });
            },
            showTab() {
                this.$http.post("{!! yzWebFullUrl('survey.survey.showTab') !!}").then(function(response) {
                    if (response.data.result) {
                        this.tabData = response.data.data;
                        for(let item of response.data.data) {
                            item.fix_name = item.name;
                        }
                    } else {
                        this.$message({
                            message: response.data.msg,
                            type: 'error'
                        });
                    }
                }, function(response) {
                    this.$message({
                        message: response.data.msg,
                        type: 'error'
                    });
                });
            },
            clickManage(type) {
                this.activeEdit = type;
            },
            delSelectData() {
                let newArr = this.pluginData.map(item => {
                    return {
                        ...item,
                        list: item.list.filter(obj => (item.item.indexOf('div_') !== -1 && !obj.selected) || (item.item.indexOf('div_') == -1 && !obj.item && obj.selected))
                    }
                })
                this.pluginData = newArr;
                this.saveTab("删除成功");
            },
            sureImg(name, image, imageUrl) {
                this.uploadShow = false;
                console.log(imageUrl,'imageUrlimageUrl');
                this.customs.icon = imageUrl[0].url;
                this.customs.attachment = imageUrl[0].attachment;
            },
            addTabList() {
                this.tabData.push({
                    selected: 1,
                    name: "",
                    fix_name: "自定义tab",
                    sort: "",
                    item: `div_${this.guid()}`
                })
                
            },
            openDialogCustomVisible() {
                this.customs.item  = "";
                this.customs.name  = "";
                this.customs.icon  = "";
                this.customs.route = "";
                this.customs.attachment = "";
                this.dialogCustomVisible  = true;
                this.showTab();
            },
            // 生成唯一id
            guid(){
                let nowTime = Date.now() || new Date().getTime(),
                    str = 'xxxxxxxx-xxxx-7xxx-yxxx-xxxxxxxxxxxx';
                // 高精度计时器
                if (window.performance && typeof window.performance.now === "function") nowTime += performance.now();
                return str.replace(/[xy]/ig, c => {
                    let r = (nowTime + Math.random() * 16) % 16 | 0;
                    nowTime = Math.floor(nowTime / 16);
                    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
            },
        },
        template: '#set_cp'
    });
</script>

<style scoped></style>