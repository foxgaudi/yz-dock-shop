<style> 
</style>
<template id="bottom_pane">
    <div class="bottom-pane" style="display: flex;margin: 16px 0;justify-content: space-between;">
        <div class="plugin-left" style="width:74%;min-height: 300px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <div style="display: flex;margin-top:16px;padding: 20px 26px;justify-content: space-between;">
                <div style="display: flex;flex-wrap: wrap;">
                    <div style="display: flex;flex-direction: column;justify-content: center;align-items: center;margin: 0 60px 10px 0;
                        color: #333333;font-size: 18px; font-weight: 400;" v-for="(item,index) in main_cockpit_data.guide.list"
                        @click="clickTab(item)">
                        <div style="cursor: pointer;margin-bottom: 5px;" :class="activePlugin == item.item ? 'active-name-plu' : ''" >[[item.title]]</div>
                        <div style="width: 40px;height: 4px;background: #2CC08D;border-radius: 30px 30px 30px 30px;" v-show="activePlugin == item.item"></div>
                    </div>
                </div>
                <div  @click="$emit('click_set', true)" style="display:flex;font-weight: 400;font-size: 14px;color: #2CC08D;align-items: center;height: 21px;cursor: pointer;">
                    <span style="white-space: nowrap;">设置</span>
                    <i class="el-icon-arrow-right"></i>
                </div>
            </div>
            <div class="img-box" style="padding: 0 26px;display: flex;flex-wrap: wrap;">
                <div  @click="goPageLink(guideItem)" v-for="(guideItem,index) in pluginList"  :key="guideItem.name" v-if="guideItem.is_plugin===0||guideItem.is_plugin===1&&guideItem.is_enabled" style="cursor: pointer;margin: 0 40px 10px 0;display: flex;flex-direction: column;align-items: center; width: 84px;">
                    <img  :src="guideItem.icon" style="width: 56px;height: 56px;border-radius: 20px 20px 20px 20px;"  v-if="guideItem.icon.indexOf('http') !== -1">
                    <div class="iconfont" :class="[guideItem.icon]" v-if="guideItem.icon.indexOf('http') == -1" :style="{'font-size': '22px',
                        'width': '46px',
                        'height': '46px',
                        'color': 'white',
                        'background': guideItem.background_color ? guideItem.background_color : '#f8a544',
                        'border-radius': '16px',
                        'display': 'flex',
                        'align-items': 'center',
                        'justify-content': 'center'}">
                    </div>
                    <div style="font-weight: 400;font-size: 14px;color: #333333;margin-top: 10px;">[[guideItem.name]]</div>
                </div>
            </div>
        </div>
        <div class="qr-right" style="width: 25%;min-height: 300px;padding:20px 12px; background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
            <div class="title" style="font-weight: bold;font-size: 20px;color: #333333;">主要入口</div>
            <div class="qr-tip" style="margin: 16px 0 ;">门店、供应商、酒店、企业管理、区域代理等独立后台登录域名同总平台，使用对应的店员账号密码即可，注意不能使用同一浏览器登录。</div>
            <div class="qr-div" style="display: flex;justify-content: space-evenly;">
                <div style="display: flex;flex-direction: column;align-items: center;">
                    <div style="padding: 8px;width: 120px;height: 120px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;border: 1px solid #DCDFE6;" >
                        <img  style="width: 100%;height: 100%;" :src="main_cockpit_data.entrance.home_code" alt="">
                    </div>
                    <a :href="main_cockpit_data.entrance.home_url"  target="_blank">
                        <div style="font-weight: 400;cursor: pointer;font-size: 14px;color: #2CC08D;margin-top: 10px;">
                            <span>访问商城首页</span>
                            <i class="el-icon-arrow-right"></i>
                        </div>
                    </a>
                </div>
                <div style="display: flex;flex-direction: column;align-items: center;">
                    <div style="padding: 8px;width: 120px;height: 120px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;border: 1px solid #DCDFE6;" >
                        <img  style="width: 100%;height: 100%;" :src="main_cockpit_data.entrance.mini_app_code" :alt="main_cockpit_data.entrance.mini_app_code_error" v-if="main_cockpit_data.entrance.is_enabled_mini_app && main_cockpit_data.entrance.is_mini_app_config">
                    </div>
                    <a :href="main_cockpit_data.entrance.more_home_url" class="head-more" > 
                        <div style="font-weight: 400;cursor: pointer;font-size: 14px;color: #2CC08D;margin-top: 10px;">
                            <span>更多前端入口</span>
                            <i class="el-icon-arrow-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('bottom_pane', {
        delimiters: ['[[', ']]'],
        props: {
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
                activePlugin: this.main_cockpit_data.guide && this.main_cockpit_data.guide.list.length ? this.main_cockpit_data.guide.list[0].item : "",
                pluginList: this.main_cockpit_data.guide && this.main_cockpit_data.guide.list.length ? this.main_cockpit_data.guide.list[0].list : [],
            }
        },
        mounted(){
        },
        methods: {
            clickTab(item) {
                console.log(item,33333333333);
                this.activePlugin = item.item;
                this.pluginList = item.list;
            },
            // 跳转
            goPageLink(item) {
                if(item.item !== "business") {
                    window.location.href = item.url;
                }else {
                    this.$http.post("{!! yzWebFullUrl('survey.survey.jumpBusiness') !!}",{
                        url: item.url
                    }).then(function(response) {
                        if (response.data.result) {
                            window.open(response.data.data.url, '_blank');
                        } else {
                            this.$message({
                                message: response.data.msg,
                                type: 'error'
                            });
                        }
                    });
                }
            },
        },
        template: '#bottom_pane'
    });
</script>

<style scoped></style>