<style> 
</style>
<template id="member_income_list">
    <div style="width: 24.5%; height: 480px;padding: 24px 16px; background: #FFFFFF;border-radius: 16px 16px 16px 16px;border: 1px solid #FFFFFF;">
        <div class="" style="font-weight: bold;font-size: 20px;color: #333333;">会员收入统计</div>
        <div style="display: flex;flex-wrap: wrap;justify-content: space-between;">
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">未提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.un_withdraw_income]]</div>
            </div>
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">未审核提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.no_check_income]]</div>
            </div>
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">无效提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.invalid_income]]</div>
            </div>
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">已驳回提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.reject_income]]</div>
            </div>
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">未打款提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.un_pay_income]]</div>
            </div>
            <div style="display: flex;flex-direction: column;justify-content: center;padding-left: 16px;width: 48%;height: 115px;margin-top: 20px;background: #F9F9F9;border-radius: 20px 20px 20px 20px;">
                <div style="font-weight: 400;font-size: 16px;color: #999999;">已打款提现收入（元）</div>
                <div style="font-weight: bold;font-size: 24px;color: #333333;margin-top: 16px;">[[main_cockpit_data.pay_income]]</div>
            </div>
        </div>
    </div>
</template>
<script>
    Vue.component('member_income_list', {
        delimiters: ['[[', ']]'],
        props: {
            type: {
                type: String,
                default: 'member_income_list'
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
        },
        methods: {
        },
        template: '#member_income_list'
    });
</script>

<style scoped></style>