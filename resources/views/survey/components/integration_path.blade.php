<style> 
</style>
<template id="integration_path">
    <div :style="{width: width_ , height: '480px' , background: '#FFFFFF', padding: '24px 12px' , 'border-radius': '16px 16px 16px 16px'}">
        <div class="oder-analysis" style="justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">[[title]]</span>
        </div>
        <div :id="type" style="width: 100% ; height: calc(100% - 5px)"></div>
    </div>
</template>
<script>
    Vue.component('integration_path', {
        delimiters: ['[[', ']]'],
        props: {
            title: {
                type: String,
                default: "积分获取途径"
            },
            width_: {
                type: String,
                default: "25%"
            },
            type: {
                type: String,
                default: ""
            },
            main_cockpit_data: {
                type: Object,
                default: () => {}
            },
            active_title: {
                type: Number || String,
                default: 2
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                var chartDom = document.getElementById(this.type);
                var myChart = echarts.init(chartDom);
                let yAxisData = [];
                let seriesData = []
                if(this.type == "trading_member") {
                    yAxisData = this.main_cockpit_data.member_structure.map(item => item.name)
                    seriesData = this.main_cockpit_data.member_structure.map(item => item.value)
                }else if(this.type == "financial_path") {
                    if(this.active_title == 2) {
                        yAxisData = this.main_cockpit_data.point_data.map(item => item.name)
                        seriesData = this.main_cockpit_data.point_data.map(item => item.point_total)
                    }else if(this.active_title == 3) {
                        yAxisData = this.main_cockpit_data.coupon_data.map(item => item.name)
                        seriesData = this.main_cockpit_data.coupon_data.map(item => item.get_count)
                    }
                }
                var option = {
                    title: {
                        text: ''
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
                    grid: {
                        left: '20%',
                    },
                    legend: {
                        show: false
                    },
                    xAxis: {
                        type: 'value',
                        boundaryGap: [0, 0.01]
                    },
                    yAxis: {
                        type: 'category',
                        data: yAxisData
                    },
                    series: [
                        {
                        name: '',
                        type: 'bar',
                        data: seriesData,
                        barWidth: '30%',
                        itemStyle: {
                            // 设置柱子颜色
                            color: '#2CC08D'
                        }
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },
        template: '#integration_path'
    });
</script>

<style scoped></style>