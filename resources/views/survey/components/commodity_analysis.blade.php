<style> 
</style>
<template id="commodity_analysis">
    <div style="width: 25%;height: 480px;background: #FFFFFF;border-radius: 16px 16px 16px 16px;">
        <div class="oder-analysis" style="padding:16px;    justify-content: space-between;display: flex;align-items: center;">
            <span style="font-weight: bold;font-size: 20px;color: #333333;">商品分析</span>
        </div>
        <div class="order-list" style="margin-bottom: 20px;display: flex;padding: 0px 16px;justify-content: space-between;">
            <div class="order-num" style="width: 48%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;
                display: flex;flex-direction: column;justify-content: space-between;line-height: 1;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;">商品总数（件）</div>
                <div style="font-weight: bold;font-size: 20px;color: #333333;">[[goods_analysis.goods_total]]</div>
            </div>
            <div class="order-num" style="width: 48%;height: 80px;background: #F9F9F9;border-radius: 12px 12px 12px 12px;padding:18px 12px;
                display: flex;flex-direction: column;justify-content: space-between;line-height: 1;">
                <div style="font-weight: 400;font-size: 14px;color: #707070;"> 上架商品数（件）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[goods_analysis.goods_grounding]]</span></div>
                <div style="font-weight: 400;font-size: 14px;color: #707070;"> 下架商品数（件）<span style="font-weight: bold;font-size: 16px;color: #333333;">[[goods_analysis.goods_off_grounding]]</span></div>
            </div>
        </div>
        <div id="roundness" style="width: 100% ; height: calc(100% - 160px)"></div>
    </div>
</template>
<script>
    Vue.component('commodity_analysis', {
        delimiters: ['[[', ']]'],
        props: {
            goods_analysis: {
                type: Object,
                default: () => {}
            }
        },
        data() {
            return {
            }
        },
        mounted(){
            this.init();
        },
        methods: {
            init() {
                var chartDom = document.getElementById('roundness');
                var myChart = echarts.init(chartDom);
                var option = {
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        top: '5%',
                        left: 'center',
                        show: false
                    },
                    color: ['#4D8BFC', '#2CC08D'],
                    series: [
                        {
                        name: '',
                        type: 'pie',
                        radius: ['70%', '60%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        data: [
                            { value: this.goods_analysis.goods_off_grounding, name: `下架` },
                            { value: this.goods_analysis.goods_grounding, name: `上架` }
                        ],
                        emphasis: {
                            label: {
                            show: true,
                            fontSize: 20,
                            fontWeight: 'bold'
                            }
                        }
                        }
                    ]
                };

                myChart.setOption(option);
            }
        },
        template: '#commodity_analysis'
    });
</script>

<style scoped></style>