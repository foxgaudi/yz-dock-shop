define({
    name: "imageLink",
    template: `
<div>

    <el-form label-width="200px">
        <div id="vue_head">
            <div class="base_set">
                <el-divider></el-divider>
                <div class="vue-main-title">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content"></div>
                </div>
                <div class="vue-main-form">


              <el-form-item label="跳转按钮">
                            <template>
                                <el-switch v-model="plugin_goods.status" :active-value="1"
                                           :inactive-value="0"></el-switch>
                            </template>
                            <div class="tip">仅支持与自营商品使用相同前端详情页的商品，部分独立详情页商品不支持该功能</div>
                        </el-form-item>
                        
                        <el-form-item label="按钮名称">
                            <el-input placeholder="按钮名称" style="width:50%;"
                                      v-model="plugin_goods.button_name"></el-input>
                        </el-form-item>
                        

                        <el-form-item label="H5跳转链接">
                            <el-input placeholder="H5跳转链接" style="width:50%;"
                                      v-model="plugin_goods.image_web_link"></el-input>
                            <el-button type="primary" @click="showLink('image_web_link','web')">选择链接</el-button>
                        </el-form-item>

                        <el-form-item label="小程序跳转路由">
                            <el-input placeholder="小程序跳转路由" style="width: 60%"
                                      v-model="plugin_goods.image_mini_link">
                            </el-input>
                            <el-button type="primary" @click="showLink('image_mini_link','mini')">选择小程序链接</el-button>
                            </el-button>
                        </el-form-item>






<program :pro="pro" @replacepro="changeprogram" @addpro="parpro"></program>
            <pop :show="show" @replace="changeLink" @add="parHref"></pop>


<el-divider></el-divider>




</div>
</div>
</div>
</el-form>
</div>
`,
    style: `
    .el - scrollbar__wrap
{
    overflow: scroll;
    width: 100 %;
    height: 103 %;
}
.
el - upload--
picture - card
{
    display: none;
}
.
el - upload--
picture
{
    display: none;
}
.
help - block
{
    font - size
:
    12
    px;
}
.
help - block - detail
{
    white - space
:
    pre - wrap;
    display: flex;
    flex - direction
:
    column;
}
.
location
{
    display:flex;
    justify - content
:
    flex - end;
}
.
gutter
{
    text - align
:
    center;
}
.
flex - col
{
    display:flex;
    margin - top
:
    5
    px;
}
.
el - input - group__append,
.
el - input - group__prepend
{
    padding: 0
    10
    px;
}
`,
    props: {
        form: {
            type: Object,
            default() {
                return {}
            }
        }
    },
    data() {
        return {
            dialogVisible: false,
            plugin_goods: {},
            chooseMiniLink: '',
            pro: false,
            show: false,//是否开启公众号弹窗
            chooseLink: '',
        }
    },
    mounted() {
        if (this.form.plugin_goods) {
            this.plugin_goods = this.form.plugin_goods;
            // this.$set(this.store_goods, "time_arr", [this.store_goods.start_time * 1000, this.store_goods.end_time * 1000]);
            // this.$set(this.store_goods, "expire_time", this.store_goods.expire_time * 1000);
        }
    },

    methods: {
        parHref(child, confirm) {
            this.show = confirm;
            // this.form.link=child;
            this.plugin_goods[this.chooseLink] = child;
        },
        //弹窗显示与隐藏的控制
        changeLink(item) {
            this.show = item;
        },
        showLink(name,type) {
            if (type === 'mini') {
                this.chooseMiniLink = name;
                this.pro = true;
            } else {
                this.chooseLink = name;
                this.show = true;
            }
        },
        parpro(child, confirm) {
            this.pro = confirm;
            this.plugin_goods[this.chooseMiniLink] = child;
        },
        changeprogram(item) {
            this.pro = item;
        },
        pick(obj, arr) {
            arr.reduce((iter, val) => (val in obj && (iter[val] = obj[val]), iter), {});
        },
        validate() {
            var plugin_goods = Object.assign({}, this.plugin_goods);
            return {
                'plugin_goods': plugin_goods,
            }
        },
    },

})