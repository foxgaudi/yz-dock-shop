define({
  template: `
  <div>
    <div class="vue-main-title">
      <div class="vue-main-title-left"></div>
      <div class="vue-main-title-content">
        <span>{{lang.content && lang.content.content}}</span>
        <el-button :type="locale_index == index ? 'primary' : ''" size="mini" v-for="(item,index) in nationList" :key="index" v-if="nationList.length" style="margin-left: 10px;" @click="checkTiny(index)">{{item.name}}</el-button>
      </div>
    </div>
    <tinymceee v-model="form.content" v-if="!isShowContent && info"></tinymceee>
    <tinymceee v-model="content" v-if="isShowContent && info"></tinymceee>
  </div>
  `,
  style: `
  .tox-tinymce {
    min-height:600px;
  }
  `,
  props: {
    form: {
      default() {
        return {};
      },
    },
    lang: {
      default() {
        return {}
      }
    },
    formKey: {
      type: String,
    },
  },
  data() {
    return {
      isShowContent:false,
      content:"",
      nationList: nationList,
      // 获取当前语言的下标
      locale_index: !nationList.length ? 0 : nationList.findIndex(item => item.value == locale),
      info: true
    };
  },
  created() {
    if(this.form && !Array.isArray(this.form.lang_content) && this.form.lang_content) {
      console.log(this.nationList,this.locale_index,locale,66666);
      for(let key in this.form.lang_content) {
        for(let item of this.nationList) {
          if(item.value == key) {
            this.$set(item,'content',this.form.lang_content[key]['content'])
          }
        }
      }
      this.form.content = this.nationList[this.locale_index]['content']
    }
    if(this.form){
      if(this.form === null){
        this.isShowContent = true
        return
      }
      this.isShowContent = false
    }else{
      this.isShowContent = true
    }

  },
  methods: {
    checkTiny(index) {
      this.nationList[this.locale_index].content = this.isShowContent ? this.content : this.form.content;
      if(this.isShowContent){
        this.content = this.nationList[index].content
      }else{
        this.form.content = this.nationList[index].content
      }
      this.info  = false;
      setTimeout(() => {
        this.locale_index = index;
        this.info  = true;
      }, 100);
    },
    // extraDate(){
    //   return {
    //     'extraContent':"商品描述"
    //   }
    // },
    validate() {
      let form = {};
      if(this.nationList.length) {
        this.nationList[this.locale_index].content = this.isShowContent ? this.content : this.form.content;
        form.lang_content = {}
        for(let item of this.nationList) {
          form.lang_content[item.value] =  {
            "content" : item.content
          }
        }
      }
      if(this.isShowContent){
        return {
          content: this.nationList.length ? this.nationList[0].content : this.content ? this.content : "",
          ...form,
        }
      }else{
        return {
          content: this.nationList.length ? this.nationList[0].content : this.form.content ? this.form.content : "",
          ...form
        }
      }
    },
  },
});
