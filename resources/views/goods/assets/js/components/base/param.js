define({
  template: `
  <div>
    <div class="vue-main-title">
      <div class="vue-main-title-left"></div>
      <div class="vue-main-title-content">{{lang.param && lang.param.attribute}}</div>
    </div>
    <ul class="attributes">
      <ul class="attribute-row">
        <li class="attribute-col">
          {{lang.param && lang.param.attribute_title}}
        </li>
        <li class="attribute-col">
          {{lang.param && lang.param.attribute_value}}
        </li>
        <li class="attribute-col"></li>
      </ul>
      <draggable v-model="attributes">
        <ul class="attribute-row" v-for="(attrItem,itemIndex) in attributes" :key="attrItem.itemIndex">
          <li class="attribute-col">
            <el-input v-model="attrItem.title" :placeholder="lang.param && lang.param.please_enter_attribute_title" v-if="!nationList.length"></el-input>
            <div v-for="(item,index) in nationList" :key="index" style="display: flex;margin-bottom: 10px;" v-if="nationList.length">
              <span :style="{background: item.background,color: item.color,'white-space':'nowrap',height:'inherit',padding: '0 12px','border-radius': '2px','line-height': '40px'}" v-if="item.name">{{item.name}}</span>
              <el-input v-model="attrItem.lang[item.value].title" :placeholder="lang.param && lang.param.please_enter_attribute_title"></el-input>
            </div>
          </li>
          <li class="attribute-col">
            <el-input v-model="attrItem.value" :placeholder="lang.param && lang.param.please_enter_attribute_value" v-if="!nationList.length"></el-input>
            <div v-for="(item,index) in nationList" :key="index" style="display: flex;margin-bottom: 10px;" v-if="nationList.length">
              <span :style="{background: item.background,color: item.color,'white-space':'nowrap',height:'inherit',padding: '0 12px','border-radius': '2px','line-height': '40px'}" v-if="item.name">{{item.name}}</span>
              <el-input v-model="attrItem.lang[item.value].value" :placeholder="lang.param && lang.param.please_enter_attribute_value"></el-input>
            </div>
          </li>
          <li class="attribute-col">
            <el-button icon="el-icon-rank"></el-button>
            <el-button icon="el-icon-delete" @click="removeAttr(itemIndex)"></el-button>
          </li>
        </ul>
      </draggable>
    </ul>
    <el-button style="margin-top:20px;" @click="addAttr">{{lang.param && lang.param.attribute_add}}</el-button>
  </div>
  `,
  style: `
  .attribute-row {
    display:flex;
    align:center;
    justify-content:space-around;
    padding:18px 0;
    font-size:14px;
    color:#101010;
    border-bottom:1px solid #e8e8e8;
  }
  `,
  props: {
    form: {
      default() {
        return {};
      },
    },
    lang: {
      default() {
        return {}
      }
    }
  },
  data() {
    return {
      attributes: [],
      nationList: nationList
    };
  },
  mounted() {
    this.attributes = this.form.map(item => ({id:item.id,'title':item.title,'value':item.value,'lang':item.lang}))
    for(let citem of this.attributes) {
      if(!citem.lang) {
        this.$set(citem,'lang',{})
        for(let item of this.nationList) {
          this.$set(citem.lang,item.value,{
            title: "",
            value: ""
          })
        }
      }
    }
  },
  methods: {
    removeAttr(itemIndex) {
      this.attributes.splice(itemIndex, 1);
    },
    addAttr() {
      let arr =  {
        id: "",
        title: "",
        value: "",
        lang: {}
      }
      for(let item of this.nationList) {
        this.$set(arr.lang,item.value,{
          title: "",
          value: ""
        })
      }
      this.attributes.push(arr);
    },
    extraDate(){
      
    },
    validate() {
      let yes = true;
      if(this.nationList.length) {
        for(let item of this.attributes) {
          item.title = item.lang[this.nationList[0].value].title
          item.value = item.lang[this.nationList[0].value].value
        }
      }
      // 过滤空数据
      this.attributes.forEach(element => {
        if(!element.title || !element.value){
          this.$message.error(this.lang.param.error_add_param_message);
          yes = false;
          return
        }
      });
      if (yes) {
        return this.attributes;
      } else {
        return false;
      }
    },
  },
});
