@extends('layouts.base')
@section('title', '导出列表')
@section('content')
<style>
.vue-main {
    min-height: auto !important;
    margin-bottom: 20px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="{{static_url('yunshop/goods/vue-goods1.css')}}"/>
    <div class="all">
        <div id="app" v-cloak>
            <div class="vue-main">
                <div class="vue-main-title">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">导出列表</div>
                </div>
                <div class="vue-main-form">
                    <el-form :inline="true">
                        <el-form-item label=" ">
                            <el-select v-model="search.export_type" clearable placeholder="请选择类型">
                                <el-option
                                v-for="item in typeOptions"
                                :key="item.value"
                                :label="item.label"
                                :value="item.value">
                                </el-option>
                            </el-select>
                        </el-form-item>
                        <el-form-item label=" ">
                            <el-input placeholder="导出人" v-model="search.user_id"></el-input>
                        </el-form-item>
                        <el-form-item label=" ">
                            <el-select v-model="search.export_status" clearable placeholder="请选择导出状态">
                                <el-option
                                v-for="item in statusOptions"
                                :key="item.value"
                                :label="item.label"
                                :value="item.value">
                                </el-option>
                            </el-select>
                        </el-form-item>
                        <time-card ref="time_card"></time-card>
                        <el-form-item label=" ">
                            <el-button type="primary" @click="searchPage(1)">搜索</el-button>
                        </el-form-item>
                    </el-form>
                </div>
            </div>
            <div class="vue-main">
                <div class="vue-main-title">
                    <div class="vue-main-title-left"></div>
                    <div class="vue-main-title-content">
                        <span>导出记录</span>
                        <el-checkbox v-model="allChecked" style="margin: 0 20px;" @change="allChange">全选</el-checkbox>
                        <el-button @click="batchDel">批量删除</el-button>
                    </div>
                </div>
                <el-table :data="dataList" :header-cell-style="{ 'text-align': 'center' }" :cell-style="{ 'text-align': 'center' }">
                    <el-table-column  label="选择">
                        <template slot-scope="scope">
                            <el-checkbox  v-model="scope.row.checked" @change="changeCheckbox"></el-checkbox>
                        </template>
                    </el-table-column>
                    <el-table-column prop="export_type_name" label="类型"></el-table-column>
                    <el-table-column prop="total" label="导出数量"></el-table-column>
                    <el-table-column prop="user_name" label="导出人"></el-table-column>
                    <el-table-column prop="created_at" label="导出时间"></el-table-column>
                    <el-table-column prop="status_name" label="导出状态"></el-table-column>
                    <el-table-column  label="操作">
                        <template slot-scope="scope">
                            <span style="color: #29BA9C;cursor: pointer;margin-right: 10px;" @click="download(scope.row.id)" v-if="scope.row.status == 1">下载</span>
                            <span style="color: #f56c6c;cursor: pointer;" @click="delete_([scope.row.id])">删除</span>
                        </template>
                    </el-table-column>
                </el-table>
            </div>
            <!-- 分页 -->
            <div class="vue-page">
                <el-row>
                    <el-col align="right">
                        <el-pagination layout="prev, pager, next,jumper" @current-change="searchPage" :total="total" :page-size="per_page" :current-page="current_page" background></el-pagination>
                    </el-col>
                </el-row>
            </div>
        </div>
    </div>
    @include('public.admin.timeCard')
    <script>
        var app = new Vue({
            el:"#app",
            delimiters: ['[[', ']]'],
            name: 'test',
            data() {

                return{
                    search:{
                        export_status: "",
                        user_id: "",
                        export_type: ""
                    },
                    typeOptions: [],
                    statusOptions: [{
                        value: 1,
                        label: "导出成功"
                    },{
                        value: 2,
                        label: "导出失败"
                    },{
                        value: 0,
                        label: "导出中"
                    }],
                    current_page: 1,
                    total: 1,
                    per_page: 1,
                    dataList: [],
                    allChecked: false
                }
            },
            mounted() {
                this.searchPage(1);
                this.getType();
            },
            methods: {
                async searchPage(page) {
                    if(this.$refs.time_card.times && this.$refs.time_card.times.length>0) {
                        this.search.search_time[0] = parseInt(this.$refs.time_card.times[0] / 1000);
                        this.search.search_time[1] = parseInt(this.$refs.time_card.times[1] / 1000);
                    } else {
                        this.search.search_time = [];
                    }
                    let { data : { result , msg , data } } = await this.$http.post("{!! yzWebFullUrl('export.index.get-list') !!}",{
                        page,
                        search: {
                            ...this.search
                        },
                    })
                    if(result) {
                        this.dataList = data.data;
                        for(let item of this.dataList) {
                            this.$set(item,'checked',false)
                        }
                        this.current_page = data.current_page;
                        this.total = data.total;
                        this.per_page = data.per_page;
                    }else {
                        this.$message.error(msg);
                    }
                },
                // 获取模版类型
                async getType() {
                    let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.index.getType') !!}", {  });
                    if(result) {
                        let newArr = [];
                        for(let item in data) {
                            newArr.push({
                                value: item,
                                label: data[item]
                            })
                        }
                        this.typeOptions = newArr;
                    }
                },
                // 导出列表
                download(id) {
                    // let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.index.download') !!}", { id });
                    // if(result) {
                    //     console.log(data , result, msg,777);
                    //     this.$message.success(msg)
                    // }


                    if(this.$refs.time_card.times && this.$refs.time_card.times.length>0) {
                        this.search.search_time[0] = parseInt(this.$refs.time_card.times[0] / 1000);
                        this.search.search_time[1] = parseInt(this.$refs.time_card.times[1] / 1000);
                    }
                    let json = this.search;

                    let url = "{!! yzWebUrl('export.index.download') !!}";
                    url += "&id="+id;

                    for(let i in json){
                        if (json[i]) {
                            url+="&search["+i+"]="+ json[i]
                        }
                    }
                    window.location.href = url;
                } ,
                delete_(ids) {
                    this.$confirm('此操作将永久删除该数据, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(async () => {
                        let { data : { data , result, msg} } = await this.$http.post("{!! yzWebUrl('export.index.delete') !!}", {
                            ids
                        });
                        if(result) {
                            this.$message.success(msg)
                            this.searchPage(1);
                        }else {
                            this.$message.error(msg)
                        }
                    }).catch(() => {
                        this.$message({
                            type: 'info',
                            message: '已取消删除'
                        });
                    });
                },
                // 批量删除
                batchDel() {
                    let checkedArr = this.dataList.filter(item => item.checked).map(citem => {
                        return citem.id
                    })
                    this.delete_(checkedArr);
                },
                allChange(status) {
                    for(let item of this.dataList) {
                        this.$set(item, "checked" ,status)
                    }
                },
                changeCheckbox(status) {
                    if(status) {
                        let checkedArr = this.dataList.filter(item => item.checked)
                        if(checkedArr.length == this.dataList.length) {
                            this.allChecked = true;
                        }
                    }else {
                        this.allChecked = false;
                    }
                },
            },
        })

    </script>
@endsection


