<?php

return [
    'manual_withdrawal'          => 'Ручной вывод',
    'wechat_withdrawal'          => 'Вывод наличных в WeChat',
    'alipay_withdrawal'          => 'Вывод наличных через Alipay',
    'yeepay_withdrawal'          => 'Вывод наличных Ибао',
    'convergence_and_withdrawal' => 'Отход конвергенции',
    'balance_withdrawal'         => 'Вывод баланса',
];