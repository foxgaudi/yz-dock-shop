<?php
return [
    'option'                     => 'спецификация товара',
    'has_option'                 => 'Включить спецификацию товара или нет：',
    'has_option_tips_1'          => ' 1. После включения спецификации товара цены и запасы товаров определяются на основе спецификации.',
    'has_option_tips_2'          => '2.После создания товаров для групповых покупок не вносите изменения в спецификацию, не снимайте с продажи, не удаляйте и т.д. В противном случае это повлияет на покупки во время акции. После окончания акции товар можно нормально редактировать!',
    'has_option_tips_3'          => '3.Каждая спецификация представляет собой другой тип, например, цвет - это одна спецификация, размер - это другая. Если заданы несколько спецификаций, пользователи мобильных устройств должны выбрать один пункт для каждой спецификации, чтобы добавить в корзину или купить.',
    'spec_title'                 => 'Имя спецификации：',
    'spec_value'                 => 'Значение спецификации：',
    'spec_value_add'             => 'Добавить значение спецификации',
    'spec_title_add'             => 'добавить пункт спецификации',
    'spec_title_clear'           => 'очистить все пункты спецификации',
    'batch_set'                  => 'групповая настройка',
    'set_stock'                  => 'Установить запасы',
    'set_market_price'           => 'Установить рыночную цену',
    'set_product_price'          => 'Установить себестоимость',
    'set_cost_price'             => ' Установить код товара',
    'set_goods_sn'               => 'Установить штрих-код товара',
    'set_product_sn'             => 'Установить вес',
    'set_weight'                 => 'Установить объем',
    'set_volume'                 => 'Настраиваемая сортировка спецификации',
    'set_diy_spec_sort'          => 'Сортировать по пользовательским характеристикам',
    'set_diy_spec_sort_tips'     => 'Перетащите элементы спецификации или значения спецификации для сортировки.',
    'spec_detail'                => 'детальный список спецификаций',
    'market_price'               => 'рыночная цена',
    'weight'                     => 'Вес (грамм)',
    'volume'                     => 'Объем (кубометров)',
    'thumb'                      => 'Изображение (размер 300x300)',
    'goods_sn'                   => 'Код товара',
    'product_sn'                 => 'Штрих-код товара',
    'stock'                      => 'Запасы',
    'withhold_stock'             => 'Предварительно занятый инвентарь',
    'product_price'              => 'Текущая цена',
    'cost_price'                 => 'Себестоимость',
    'please_enter_stock'         => 'Пожалуйста, введите инвентарь',
    'please_enter_market_price'  => 'Пожалуйста, введите рыночную цену',
    'please_enter_product_price' => 'Пожалуйста, введите текущую цену',
    'please_enter_cost_price'    => 'Пожалуйста, укажите себестоимость',
    'please_enter_goods_sn'      => 'Пожалуйста, введите код продукта',
    'please_enter_product_sn'    => 'Пожалуйста, введите штрих-код продукта',
    'please_enter_weight'        => 'Пожалуйста, введите вес',
    'please_enter_volume'        => 'Пожалуйста, введите объем',
    'page_tips'                  => '(рекомендуется не превышать 100)',
    'error_add_same_spec_title'  => 'Невозможно добавить то же имя спецификации',
    'error_add_same_spec_value'  => 'Значения спецификации не могут повторяться',
    'error_spec_title_empty'     => 'Имя спецификации не может быть пустым',
    'error_spec_value_empty'     => 'Значение спецификации не может быть пустым',
    'error_stock_empty'          => 'Инвентарь не может быть пустым',
    'error_product_price'        => 'Текущая цена не может быть пустой',
];