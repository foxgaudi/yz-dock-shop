<?php
return [
    'param'                        => 'атрибуты',
    'attribute'                    => 'Товарные атрибуты',
    'attribute_title'              => 'название атрибута',
    'please_enter_attribute_title' => 'введите имя атрибута',
    'attribute_value'              => 'значение атрибута',
    'please_enter_attribute_value' => 'введите значение атрибута',
    'attribute_add'                => 'добавить атрибут',
    'error_add_param_message'      => 'Введите имена и значения атрибутов'
];