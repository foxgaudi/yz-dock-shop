<?php
return [
    'param'                        => '属性',
    'attribute'                    => '商品属性',
    'attribute_title'              => '属性名称',
    'please_enter_attribute_title' => '请输入属性名称',
    'attribute_value'              => '属性值',
    'please_enter_attribute_value' => '请输入属性值',
    'attribute_add'                => '添加属性',
    'error_add_param_message'      => '请输入属性名和属性值'
];