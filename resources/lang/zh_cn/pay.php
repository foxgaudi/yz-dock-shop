<?php

return [
    'manual_withdrawal'          => '手动提现',
    'wechat_withdrawal'          => '微信提现',
    'alipay_withdrawal'          => '支付宝提现',
    'yeepay_withdrawal'          => '易宝提现',
    'convergence_and_withdrawal' => '汇聚提现',
    'balance_withdrawal'         => '余额提现',
    'hema_withdraw_bank'         => '杉德宝提现',
];