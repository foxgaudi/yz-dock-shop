<?php

return [
    'member'   => '会员',
    'realname' => '用户姓名',
    'nickname' => '昵称',
    'mobile'   => '手机号',
];