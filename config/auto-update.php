<?php
/**
 * Created by PhpStorm.
 * Author: 
 * Date: 20/04/2017
 * Time: 21:58
 */

return [
    'checkUrl' => env('UPDATE_CHECK_URL', 'https://yun.yunzmall.com/update'),
    'registerUrl' => env('REGISTER_CHECK_URL', 'https://yun.yunzmall.com/register'),
    'proAuthUrl' => env('REGISTER_CHECK_URL', 'https://yun.yunzmall.com/register'),
    'workOrderUrl' => env('WORK_ORDER_URL', 'https://yun.yunzmall.com'),
    'diyMarket' => env('DIY_MARKET','https://yun.yunzmall.com')
];
