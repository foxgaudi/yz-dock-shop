<?php
/**
 * Created by PhpStorm.
 * Date: 2023/5/9
 * Time: 14:39
 */

namespace app\common\models\goods;

use app\common\models\BaseModel;

class ContactTel extends BaseModel
{
    public $table = 'yz_goods_contact_tel';
    protected $guarded = [''];
}