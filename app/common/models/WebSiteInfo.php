<?php
/**
 * Created by PhpStorm.
 * User: 
 * Date: 2019/7/1
 * Time: 14:50
 */

namespace app\common\models;


class WebSiteInfo extends BaseModel
{
    protected $table = 'yz_website_info';
    protected $guarded = [''];
}