<?php
/**
 * Created by PhpStorm.
 * Author:
 * Date: 2017/3/20
 * Time: 上午10:45
 */

namespace app\common\models;


class PayResponseDataLog extends BaseModel
{
    public $table = 'yz_pay_response_data';
    protected $fillable = [];
    protected $guarded = [];
}