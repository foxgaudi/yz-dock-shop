<?php
/**
 * Created by PhpStorm.
 *
 *
 *
 * Date: 2021/9/29
 * Time: 16:04
 */

namespace app\common\models\coupon;

use app\common\models\BaseModel;

class CouponIncreaseRecords extends BaseModel
{
    /**
     * @var string
     */
    protected $table = "yz_coupon_increase_records";

}