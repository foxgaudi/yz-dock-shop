<?php
/**
 * Created by PhpStorm.
 * Name: 商城系统
 * Author: blank
 * Profile: shop
 * Date: 2023/8/9
 * Time: 14:38
 */

namespace app\common\modules\refund\optional;


use app\common\models\Order;
use app\common\models\refund\RefundApply;

class RefundOnly extends OptionalTypesAbstract
{
    public function enable()
    {
        return $this->getOrder()->status >= Order::WAIT_SEND;
    }

    public function getName()
    {
        return '退款(仅退款不退货)';
    }

    public function getValue()
    {
        return  RefundApply::REFUND_TYPE_REFUND_MONEY;
    }

    public function getIcon()
    {
        return 'icon-fontclass-daizhifu';
    }

    public function getDesc()
    {
        return '未收到货或者不用退货只退款';
    }

    public function notReceived()
    {
        return [
            '拍错/多拍/不想要',
            '货物破损',
            '快递送货问题',
            '差价',
            '其他原因'
        ];
    }

    public function received()
    {
        return [
            '货物破损',
            '少件、漏发',
            '差价',
            '商品质量问题',
            '其他原因',
        ];
    }

}