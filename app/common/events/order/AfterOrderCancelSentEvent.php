<?php
/**
 * 取消发货事件后
 * Created by PhpStorm.
 * Author:
 * Date: 2017/3/3
 * Time: 上午11:44
 */

namespace app\common\events\order;

class AfterOrderCancelSentEvent extends CreatedOrderStatusChangedEvent
{

}