<?php
/**
 * Created by PhpStorm.
 * Author:
 * Date: 17/2/23
 * Time: 上午10:53
 */

/**
 * 微信平台配置信息表
 */
namespace app\backend\modules\system\modules;

use Illuminate\Database\Eloquent\Model;

class AccountOpenConfigModel extends Model
{
    public $table = 'yz_account_open_config';
}