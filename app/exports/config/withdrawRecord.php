<?php
return
    [
        'columns' => [
            '基础信息' => [
                'withdraw_sn' => '提现编号',
                'member_id' => '会员ID',
                'nickname' => '粉丝',
                'name' => '姓名、手机',
                'type_name' => '收入类型',
                'pay_way_name' => '提现方式',
                'amounts' => '申请金额',
                'actual_poundage' => '手续费',
                'actual_servicetax' => '劳务税',
                'actual_amounts' => '应打款金额',
                'created_at' => '申请时间',
                'audit_at' => '审核时间',
                'pay_at' => '打款时间',
                'arrival_at' => '到账时间',
                'custom_value' => '自定义信息',
                'manual_type' => '打款至',
                'wechat' => '打款微信号',
                'alipay_name' => '支付宝姓名',
                'alipay_account' => '支付宝账号',
                'bank_name' => '开户行',
                'bank_province' => '开户行省份',
                'bank_city' => '开户行城市',
                'bank_branch' => '开户行支行',
                'bank_card' => '银行卡信息',
                'member_name' => '开户人姓名',
                'idcard' => '身份证',
                'mobile' => '银行卡预留手机号',
            ]
        ]
    ];

