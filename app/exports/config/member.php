<?php
return
    [
        'columns' => [
            '基础信息' => [
                'member_id' => '会员ID',
                'parent' => '推荐人',
                'parent_id' => '推荐人ID',
                'parent_mobile' => '推荐人手机号',
                'child' => '粉丝',
                'name' => '姓名',
                'mobile' => '手机号',
                'level' => '等级',
                'group' => '分组',
                'register_time' => '注册时间',
                'point' => '积分',
                'balance' => '余额',
                'order' => '订单',
                'money' => '金额',
                'followed' => '关注',
                'withdraw_mobile' => '提现手机号',
                'email' => '邮箱'
            ]
        ]
    ];
