<?php
return
    [
        'columns' => [
            '基础信息' => [
                'time' => '时间',
                'member_id' => '会员ID',
                'nickname' => '昵称',
                'name' => '会员姓名',
                'mobile' => '会员手机号',
                'money' => '余额',
            ]
        ]
    ];
