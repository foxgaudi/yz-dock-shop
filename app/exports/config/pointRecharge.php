<?php
return
    [
        'columns' => [
            '基础信息' => [
                'time' => '充值时间',
                'member_id' => '会员ID',
                'nickname' => '昵称',
                'name' => '姓名',
                'mobile' => '手机号',
                'recharge_sn' => '充值单号',
                'money' => '充值积分',
                'type' => '充值方式',
                'status' => '充值状态',
                'remark' => '充值备注'
            ]
        ]
    ];


