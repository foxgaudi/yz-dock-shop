<?php
return
    [
        'columns' => [
            '基础信息' => [
                'member_id' => '会员ID',
                'nickname' => '昵称',
                'name' => '真实姓名',
                'mobile' => '电话',
            ]
        ]
    ];
