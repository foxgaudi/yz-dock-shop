<?php
return
    [
        'columns' => [
            '基础信息' => [
                'member_id' => '会员ID',
                'nickname' => '昵称',
                'name' => '真实姓名',
                'mobile' => '电话',
                'register_time' => '会员注册时间',
                'performance' => '业绩',
                'fans' => '粉丝数',
                'parent' => '推荐人',
                'parent_id' => '推荐人ID',
            ]
        ]
    ];
