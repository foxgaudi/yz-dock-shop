<?php
return
    [
        'columns' => [
            '基础信息' => [
                'time' => '时间',
                'member_id' => '会员ID',
                'name' => '会员姓名',
                'mobile' => '会员手机号',
                'level' => '会员等级',
                'group' => '会员分组',
                'order_sn' => '订单号',
                'type' => '业务类型',
                'income_type' => '收入／支出',
                'before_money' => '变动前余额',
                'money' => '变动余额',
                'after_money' => '变动后余额',
                'remake' => '备注'
            ]
        ]
    ];





