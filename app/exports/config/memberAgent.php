<?php
return
    [
        'columns' => [
            '基础信息' => [
                'member_id' => '会员ID',
                'parent' => '推荐人',
                'child' => '粉丝',
                'name' => '姓名',
                'mobile' => '手机号',
                'status' => '状态',
                'child_status' => '下线状态',
                'register_time' => '注册时间',
                'followed' => '关注',
            ]
        ]
    ];
