<?php
return
    [
        'columns' => [
            '基础信息' => [
                'id' => '商品ID',
                'display_order' => '商品排序',
                'title' => '商品名称',
                'label' => '标签',
                'free_shipping' => '是否包邮',
                'point_max_deduction' => '积分抵扣最高比例',
                'point_min_deduction' => '积分抵扣最低比例',
                'brand_id' => '品牌',
                'alias' => '商品简称',
                'category' => '分类',
                'status' => '商品状态',
                'sku' => '商品单位',
                'real_sales' => '销量',
                'option' => '商品规格',
                'stock' => '库存',
                'price' => '现价',
                'market_price' => '原价',
                'cost_price' => '成本价',
                'goods_sn' => '商品编码',
                'product_sn' => '商品条形码',
            ]
        ]
    ];





