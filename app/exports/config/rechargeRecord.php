<?php
return
    [
        'columns' => [
            '基础信息' => [
                'recharge_sn' => '充值单号',
                'nickname' => '粉丝',
                'member_id' => '会员ID',
                'mobile' => '会员手机号',
                'level' => '会员等级',
                'group' => '会员分组',
                'time' => '充值时间',
                'type' => '充值方式',
                'money' => '充值金额',
                'status' => '状态',
                'remark' => '备注信息'
            ]
        ]
    ];




