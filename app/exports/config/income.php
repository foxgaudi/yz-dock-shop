<?php
return
    [
        'columns' => [
            '基础信息' => [
                'member_id' => '会员ID',
                'nickname' => '粉丝',
                'name' => '姓名',
                'mobile' => '手机',
                'time' => '时间',
                'money' => '收入金额',
                'type' => '业务类型',
                'withdraw_status' => '提现状态',
                'pay_status' => '打款状态',
                'order_sn' => '订单号',
                'order_price' => '订单金额'
            ]
        ]
    ];

