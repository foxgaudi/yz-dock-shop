<?php
return
    [
        'columns' => [
            '基础信息' => [
                'time' => '时间',
                'member_id' => '会员ID',
                'nickname' => '昵称',
                'name' => '姓名',
                'mobile' => '手机号',
                'type' => '业务类型',
                'before_money' => '原有积分',
                'money' => '变动积分',
                'after_money' => '剩余积分',
                'remark' => '备注',
                'member_remark' => '会员备注'
            ]
        ]
    ];




