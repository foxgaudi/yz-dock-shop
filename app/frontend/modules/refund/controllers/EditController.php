<?php
/**
 * Created by PhpStorm.
 * Author:  
 * Date: 2017/4/14
 * Time: 上午11:59
 */

namespace app\frontend\modules\refund\controllers;

use app\common\components\ApiController;
use app\common\exceptions\AppException;
use app\common\models\Order;
use app\frontend\modules\refund\models\RefundApply;
use app\frontend\modules\refund\services\operation\RefundEditApply;
use app\frontend\modules\refund\services\RefundService;

class EditController extends ApiController
{
    public function index()
    {
        $this->validate([
            'refund_id' => 'required|integer',
        ]);


        $refundApply = RefundApply::detail()->find(request()->input('refund_id'));
        if(!isset($refundApply)){
            throw new AppException('未找到该退款申请');
        }

        $order = Order::find($refundApply->order_id);
        if (!isset($order)) {
            throw new AppException('订单不存在');
        }


        $data = RefundService::editRefundApply($order);

        $data['refundApply'] = $refundApply->toArray();

        return $this->successJson('成功',$data);

    }

    //订单其他费用退款
    protected function getOrderOtherPrice($order)
    {
        return $order->fee_amount + $order->service_fee_amount;
    }

    public function store()
    {

        $this->validate([
//            'reason' => 'required|string',
            'content' => 'sometimes|string',
            'refund_type' => 'required|integer',
            'refund_id' => 'required|integer'
        ],request(), []);


        $refundApply = RefundEditApply::find(request()->input('refund_id'));


        if ($refundApply->uid != \YunShop::app()->getMemberId()) {
            throw new AppException('无效申请,该订单属于其他用户');
        }

        if (!isset($refundApply)) {
            throw new AppException('退款申请不存在');
        }

        $refundApply->execute();


        return $this->successJson('成功', $refundApply->toArray());
    }
}