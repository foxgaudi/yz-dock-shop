<?php
/**
 * Created by PhpStorm.
 * Author:
 * Date: 2017/3/2
 * Time: 下午4:55
 */

namespace app\frontend\modules\order\services\status;


use app\common\models\Order;

class WaitSend extends Status
{
    private $order;
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getStatusName()
    {
        $name = __('order.wait_send');
        switch ($this->order->dispatch_type_id) {
            case 4:
                $name = __('order.to_be_confirmed');
                break;

        }

        return $name;
    }
}