<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImsYzOrderPluginBonusTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('yz_order_plugin_bonus')) {

            Schema::create('yz_order_plugin_bonus', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id')->default(0)->index('idx_order_id')->comment('订单id');
                $table->string('table_name', 100)->default('')->comment('分红记录表名');
                $table->string('ids', 1000)->default('')->comment('分红记录id数组');
                $table->string('code', 100)->default('')->index('idx_code')->comment('分红类型编号');
                $table->decimal('amount', 11)->default(0.00)->comment('分红数额');
                $table->integer('created_at')->nullable();
                $table->integer('updated_at')->nullable();
                $table->integer('deleted_at')->nullable();
            });
            \Illuminate\Support\Facades\DB::statement("ALTER TABLE " . app('db')->getTablePrefix() . "yz_order_plugin_bonus comment '订单--订单插件分红记录表'");//表注释
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('yz_order_plugin_bonus')) {

            Schema::drop('ims_yz_order_plugin_bonus');
        }
    }

}
